﻿using System.Collections.Generic;

namespace ILIM.Bratsk.Infrastructure.CallBackService
{
    public interface IPublisher<T>
    {
        void Publish(List<T> items);
        void Subscribe(ISubscriber<T> logSubscriber);
        void UnSubscribe(ISubscriber<T> logSubscriber);
    }
}