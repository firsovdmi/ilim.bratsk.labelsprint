﻿using System.ServiceModel;

namespace ILIM.Bratsk.Infrastructure.CallBackService
{
    [ServiceContract]
    public interface IPublisherRemote
    {
        [OperationContract(IsOneWay = true)]
        void Subscribe();

        [OperationContract(IsOneWay = true)]
        void UnSubscribe();
    }
}