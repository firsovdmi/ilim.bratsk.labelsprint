﻿using System.Collections.Generic;
using System.ServiceModel;

namespace ILIM.Bratsk.Infrastructure.CallBackService.ReceiveData
{
    [ServiceContract]
    public interface IReceivingTaskSubscriber
    {
        [OperationContract(IsOneWay = true)]
        void Publish(List<ReceivingTaskItem> ReceivingTaskitems);

        [OperationContract]
        void CheckSubscriber();
    }
}