﻿using System.ServiceModel;

namespace ILIM.Bratsk.Infrastructure.CallBackService.ReceiveData
{
    [ServiceContract(CallbackContract = typeof (IReceivingTaskSubscriber))]
    public interface IReceivingTaskPublisherRemote
    {
        [OperationContract(IsOneWay = true)]
        void Subscribe();

        [OperationContract(IsOneWay = true)]
        void UnSubscribe();
    }
}