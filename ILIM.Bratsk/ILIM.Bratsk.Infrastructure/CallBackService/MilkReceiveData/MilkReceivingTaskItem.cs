﻿using System;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.CallBackService.ReceiveData
{
    [DataContract]
    public class ReceivingTaskItem
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid ParentId { get; set; }

        [DataMember]
        public ReceivingChangeType ItemType { get; set; }

        [DataMember]
        public ChangeType ChangeType { get; set; }

        [DataMember]
        public Guid TaskId { get; set; }

        [DataMember]
        public bool IsCanLoad { get; set; }
    }

    public enum ReceivingChangeType
    {
        Task,
        SubTask,
        ReceiveAnalysis,
        SendAnalysis
    }

    public enum ChangeType
    {
        Update,
        Insert,
        Delete
    }
}