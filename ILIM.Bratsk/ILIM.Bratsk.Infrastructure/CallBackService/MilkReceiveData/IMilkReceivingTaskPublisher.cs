﻿using System.Collections.Generic;

namespace ILIM.Bratsk.Infrastructure.CallBackService.ReceiveData
{
    public interface IReceivingTaskPublisher
    {
        void Publish(List<ReceivingTaskItem> items);
        void Subscribe(IReceivingTaskSubscriber ReceivingTaskSubscriber);
        void UnSubscribe(IReceivingTaskSubscriber ReceivingTaskSubscriber);
    }
}