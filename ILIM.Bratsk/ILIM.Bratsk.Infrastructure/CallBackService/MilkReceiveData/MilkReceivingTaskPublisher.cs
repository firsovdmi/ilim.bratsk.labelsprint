﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ILIM.Bratsk.Infrastructure.CallBackService.ReceiveData
{
    public class ReceivingTaskPublisher : IReceivingTaskPublisher
    {
        protected object _locker = new object();
        protected Timer _timer;
        protected List<IReceivingTaskSubscriber> ReceivingTaskSubscribers = new List<IReceivingTaskSubscriber>();

        public ReceivingTaskPublisher(int pingPeriod)
        {
            _timer = new Timer(CheckConnection, null, 0, pingPeriod);
        }

        private void CheckConnection(object state)
        {
            if (!Monitor.TryEnter(_locker))
                return;
            try
            {
                for (var index = 0; index < ReceivingTaskSubscribers.Count; index++)
                {
                    var ReceivingTaskSubscriber = ReceivingTaskSubscribers[index];
                    try
                    {
                        ReceivingTaskSubscriber.CheckSubscriber();
                    }
                    catch (Exception)
                    {
                        ReceivingTaskSubscribers.RemoveAt(index);
                    }
                }
            }
            finally
            {
                Monitor.Exit(_locker);
            }
        }

        #region Implementation of IReceivingTaskPublisher

        public void Publish(List<ReceivingTaskItem> ReceivingTaskitems)
        {
            var task = new Task(() => PublishAction(ReceivingTaskitems));
            task.Start();
        }

        private void PublishAction(List<ReceivingTaskItem> ReceivingTaskitems)
        {
            lock (_locker)
                for (var index = 0; index < ReceivingTaskSubscribers.Count; index++)
                {
                    var ReceivingTaskSubscriber = ReceivingTaskSubscribers[index];
                    try
                    {
                        ReceivingTaskSubscriber.Publish(ReceivingTaskitems);
                    }
                    catch (Exception)
                    {
                        ReceivingTaskSubscribers.RemoveAt(index);
                    }
                }
        }

        public void Subscribe(IReceivingTaskSubscriber ReceivingTaskSubscriber)
        {
            var task = new Task(() => SubscribeAction(ReceivingTaskSubscriber));
            task.Start();
        }

        private void SubscribeAction(IReceivingTaskSubscriber ReceivingTaskSubscriber)
        {
            lock (_locker)
                if (!ReceivingTaskSubscribers.Contains(ReceivingTaskSubscriber))
                    ReceivingTaskSubscribers.Add(ReceivingTaskSubscriber);
        }

        public void UnSubscribe(IReceivingTaskSubscriber ReceivingTaskSubscriber)
        {
            var task = new Task(() => UnSubscribeAction(ReceivingTaskSubscriber));
            task.Start();
        }

        public void UnSubscribeAction(IReceivingTaskSubscriber ReceivingTaskSubscriber)
        {
            lock (_locker)
                if (ReceivingTaskSubscribers.Contains(ReceivingTaskSubscriber))
                    ReceivingTaskSubscribers.Remove(ReceivingTaskSubscriber);
        }

        #endregion
    }
}