using System;
using System.Collections.Generic;
using System.Threading;

namespace ILIM.Bratsk.Infrastructure.CallBackService
{
    public class Publisher<T> : IPublisher<T>
    {
        protected object _locker = new object();
        protected List<ISubscriber<T>> _logSubscribers = new List<ISubscriber<T>>();
        protected Timer _timer;

        public Publisher(int pingPeriod)
        {
            _timer = new Timer(CheckConnection, null, 0, pingPeriod);
        }

        private void CheckConnection(object state)
        {
            if (!Monitor.TryEnter(_locker))
                return;
            try
            {
                for (var index = 0; index < _logSubscribers.Count; index++)
                {
                    var logSubscriber = _logSubscribers[index];
                    try
                    {
                        logSubscriber.CheckSubscriber();
                    }
                    catch (Exception)
                    {
                        _logSubscribers.RemoveAt(index);
                    }
                }
            }
            finally
            {
                Monitor.Exit(_locker);
            }
        }

        #region Implementation of ILogPublisher

        public void Publish(List<T> logitems)
        {
            lock (_locker)
                for (var index = 0; index < _logSubscribers.Count; index++)
                {
                    var logSubscriber = _logSubscribers[index];
                    try
                    {
                        logSubscriber.Publish(logitems);
                    }
                    catch (Exception)
                    {
                        _logSubscribers.RemoveAt(index);
                    }
                }
        }

        public void Subscribe(ISubscriber<T> logSubscriber)
        {
            lock (_locker)
                if (!_logSubscribers.Contains(logSubscriber))
                    _logSubscribers.Add(logSubscriber);
        }

        public void UnSubscribe(ISubscriber<T> logSubscriber)
        {
            lock (_locker)
                if (_logSubscribers.Contains(logSubscriber))
                    _logSubscribers.Remove(logSubscriber);
        }

        #endregion
    }
}