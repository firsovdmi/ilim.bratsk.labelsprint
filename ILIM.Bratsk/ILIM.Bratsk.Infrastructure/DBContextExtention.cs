using System;
using System.Data.Entity;
using System.Linq;

namespace ILIM.Bratsk.Infrastructure
{
    public static class DBContextExtention
    {
        public static void DeleteBatch<T>(this DbContext context, IQueryable<T> query) where T : class
        {
            var sqlClause = GetClause(query);
            context.Database.ExecuteSqlCommand(String.Format("DELETE {0}", sqlClause));
        }

        private static string GetClause<T>(IQueryable<T> clause) where T : class
        {
            var snippet = "FROM [dbo].[";

            var sql = clause.ToString();
            var sqlFirstPart = sql.Substring(sql.IndexOf(snippet));

            sqlFirstPart = sqlFirstPart.Replace("AS [Extent1]", "");
            sqlFirstPart = sqlFirstPart.Replace("[Extent1].", "");

            return sqlFirstPart;
        }
    }
}