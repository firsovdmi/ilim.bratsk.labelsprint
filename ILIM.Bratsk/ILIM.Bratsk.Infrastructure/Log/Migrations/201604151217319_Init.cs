namespace ILIM.Bratsk.Infrastructure.Log.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Log",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LogLevel = c.Int(nullable: false),
                        Time = c.DateTime(nullable: false),
                        AccountID = c.Guid(),
                        Type = c.String(maxLength: 80),
                        Par1 = c.String(maxLength: 80),
                        Par2 = c.String(maxLength: 80),
                        Par3 = c.String(maxLength: 80),
                        Message = c.String(),
                        Source = c.String(maxLength: 80),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Log");
        }
    }
}
