using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log.MapConfig
{
    public class LogItemMapConfiguration : EntityTypeConfiguration<LogItem>
    {
        private const string TableName = "Log";

        public LogItemMapConfiguration()
        {
            HasKey(p => p.ID);
            Property(p => p.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Map(p => p.ToTable(TableName));
        }
    }
}