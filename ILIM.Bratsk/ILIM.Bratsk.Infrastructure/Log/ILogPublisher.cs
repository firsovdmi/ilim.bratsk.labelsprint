﻿using System.Collections.Generic;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public interface ILogPublisher
    {
        void Publish(List<LogItem> items);
        void Subscribe(ILogSubscriber logSubscriber);
        void UnSubscribe(ILogSubscriber logSubscriber);
    }
}