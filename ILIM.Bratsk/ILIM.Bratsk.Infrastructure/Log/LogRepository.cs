﻿using System.Linq;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public class LogRepository : ILogRepository
    {
        private readonly IUnitOfWorkLog _unitOfWorkLog;

        public LogRepository(IUnitOfWorkLog unitOfWorkLog)
        {
            _unitOfWorkLog = unitOfWorkLog;
        }

        #region Implementation of ILogRepository

        public IQueryable<LogItem> Get()
        {
            return _unitOfWorkLog.Get();
        }

        public void Delete(LogItem logItem)
        {
            _unitOfWorkLog.Delete(logItem);
        }

        public LogItem Update(LogItem logItem)
        {
            return _unitOfWorkLog.Update(logItem);
        }

        public void Delete(IQueryable<LogItem> items)
        {
            _unitOfWorkLog.DeleteBatch(items);
        }

        #endregion
    }
}