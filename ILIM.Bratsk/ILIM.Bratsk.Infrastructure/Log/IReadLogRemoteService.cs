﻿using System.Collections.Generic;
using System.ServiceModel;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log
{
    [ServiceContract]
    public interface IReadLogRemoteService
    {
        [OperationContract]
        List<LogItem> GetLog(LogRequestParameters logRequestParameters);
    }
}