using System.Collections.Generic;
using System.ServiceModel;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log
{
    [ServiceContract]
    public interface ILogSubscriber
    {
        [OperationContract(IsOneWay = true)]
        void Publish(List<LogItem> items);

        [OperationContract]
        void CheckSubscriber();
    }
}