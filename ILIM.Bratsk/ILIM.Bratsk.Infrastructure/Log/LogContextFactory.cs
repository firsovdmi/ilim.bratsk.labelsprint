using System.Data.Entity.Infrastructure;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public class LogContextFactory : IDbContextFactory<LogContext>
    {
        #region Implementation of IDbContextFactory<out FactoryContext>

        /// <summary>
        ///     Creates a new instance of a derived <see cref="T:System.Data.Entity.DbContext" /> type.
        /// </summary>
        /// <returns>
        ///     An instance of TContext
        /// </returns>
        public LogContext Create()
        {
            return new LogContext("ILIM.Bratsk");
        }

        #endregion
    }
}