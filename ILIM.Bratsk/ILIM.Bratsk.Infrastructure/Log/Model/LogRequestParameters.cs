﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ILIM.Bratsk.Infrastructure.Log.Model
{
    public class LogRequestParameters : INotifyPropertyChanged
    {
        private List<Guid?> _accountIds;
        private DateTime _from;
        private bool _isAutoRefresh;
        private bool _isFilterByDateTime;
        private bool _isGetAllAccounts;
        private bool _isGetAllLoginLevels;
        private bool _isUseTop;
        private List<LogingLevel> _loginLevels;
        private string _logSource;
        private string _par1;
        private string _par2;
        private string _par3;
        private DateTime _to;
        private int _topCount;
        private string _type;

        public LogRequestParameters()
        {
            From = DateTime.UtcNow.AddDays(-1);
            To = DateTime.UtcNow;
            IsGetAllLoginLevels = true;
            IsGetAllAccounts = true;
            IsUseTop = true;
        }

        public bool IsFilterByDateTime
        {
            get { return _isFilterByDateTime; }
            set
            {
                _isFilterByDateTime = value;
                OnPropertyChanged("IsFilterByDateTime");
            }
        }

        public DateTime From
        {
            get { return _from; }
            set
            {
                _from = value;
                OnPropertyChanged("From");
            }
        }

        public DateTime To
        {
            get { return _to; }
            set
            {
                _to = value;
                OnPropertyChanged("To");
            }
        }

        public bool IsGetAllLoginLevels
        {
            get { return _isGetAllLoginLevels; }
            set
            {
                _isGetAllLoginLevels = value;
                OnPropertyChanged("IsGetAllLoginLevels");
            }
        }

        public List<LogingLevel> LoginLevels
        {
            get { return _loginLevels; }
            set
            {
                _loginLevels = value;
                OnPropertyChanged("LoginLevels");
            }
        }

        public bool IsGetAllAccounts
        {
            get { return _isGetAllAccounts; }
            set
            {
                _isGetAllAccounts = value;
                OnPropertyChanged("IsGetAllAccounts");
            }
        }

        public List<Guid?> AccountIds
        {
            get { return _accountIds; }
            set
            {
                _accountIds = value;
                OnPropertyChanged("AccountIds");
            }
        }

        public string Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged("Type");
            }
        }

        public string Par1
        {
            get { return _par1; }
            set
            {
                _par1 = value;
                OnPropertyChanged("Par1");
            }
        }

        public string Par2
        {
            get { return _par2; }
            set
            {
                _par2 = value;
                OnPropertyChanged("Par2");
            }
        }

        public string Par3
        {
            get { return _par3; }
            set
            {
                _par3 = value;
                OnPropertyChanged("Par3");
            }
        }

        public bool IsUseTop
        {
            get { return _isUseTop; }
            set
            {
                _isUseTop = value;
                OnPropertyChanged("IsUseTop");
            }
        }

        public int TopCount
        {
            get { return _topCount; }
            set
            {
                _topCount = value;
                OnPropertyChanged("TopCount");
            }
        }

        public string LogSource
        {
            get { return _logSource; }
            set
            {
                _logSource = value;
                OnPropertyChanged("LogSource");
            }
        }

        public bool IsAutoRefresh
        {
            get { return _isAutoRefresh; }
            set
            {
                if (value.Equals(_isAutoRefresh)) return;
                _isAutoRefresh = value;
                OnPropertyChanged("IsAutoRefresh");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}