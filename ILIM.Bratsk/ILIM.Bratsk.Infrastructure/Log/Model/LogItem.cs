﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.Log.Model
{
    [DataContract]
    public class LogItem
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public LogingLevel LogLevel { get; set; }

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public Guid? AccountID { get; set; }

        [MaxLength(80)]
        [DataMember]
        public string Type { get; set; }

        [MaxLength(80)]
        [DataMember]
        public string Par1 { get; set; }

        [MaxLength(80)]
        [DataMember]
        public string Par2 { get; set; }

        [MaxLength(80)]
        [DataMember]
        public string Par3 { get; set; }

        [DataMember]
        public string Message { get; set; }

        [MaxLength(80)]
        [DataMember]
        public string Source { get; set; }
    }
}