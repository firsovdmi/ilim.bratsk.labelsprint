﻿using System.Linq;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public interface ILogRepository
    {
        IQueryable<LogItem> Get();
        void Delete(LogItem logItem);
        LogItem Update(LogItem logItem);
        void Delete(IQueryable<LogItem> items);
    }
}