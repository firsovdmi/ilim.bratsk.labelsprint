﻿using System.Collections.Generic;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public interface IReadLogService
    {
        List<LogItem> GetLog(LogRequestParameters logRequestParameters);
    }
}