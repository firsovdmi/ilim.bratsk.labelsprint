using System;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using NLog;
using NLog.Config;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;

namespace ILIM.Bratsk.Infrastructure.Log.NlogImplementation
{
    public class LogService : ILogService
    {
        private readonly IActiveAccountManager _aaaManager;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public LogService(IActiveAccountManager aaaManager = null)
        {
            _aaaManager = aaaManager;
        }

        #region Implementation of ILog

        public void Log(LogingLevel loglevel, string message, string type,
            string par1 = null, string par2 = null, string par3 = null, string par4 = null, string source="Server")
        {
            var theEvent = new LogEventInfo(LogLevel.FromOrdinal((int)loglevel), source, message);
            theEvent.Properties["Type"] = type;
            if (_aaaManager != null && _aaaManager.ActiveAccount != null)
            {
                theEvent.Properties["AccountID"] = _aaaManager.ActiveAccount.Account.ID;
                theEvent.LoggerName = string.IsNullOrEmpty(_aaaManager.ActiveAccount.MachineName) ? "Server" : _aaaManager.ActiveAccount.MachineName;
            }
            if (par1 != null)
                theEvent.Properties["par1"] = par1;
            if (par2 != null)
                theEvent.Properties["par2"] = par1;
            if (par3 != null)
                theEvent.Properties["par3"] = par1;
            if (par4 != null);
                theEvent.Properties["par4"] = par1;
            _logger.Log(theEvent);
        }

        #endregion
    }

    public class NlogInstaller : IWindsorInstaller
    {
        /// <summary>
        ///     Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public virtual void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<WebServiceTarget>());
            container.Register(Component.For<LogContextTarget>());
            var defaultConfigurationItemFactory = ConfigurationItemFactory.Default.CreateInstance;
            ConfigurationItemFactory.Default.CreateInstance = type =>
            {
                try
                {
                    return defaultConfigurationItemFactory(type);
                }
                catch (Exception)
                {
                    return container.Resolve(type);
                }
            };
        }
    }
}