﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log.NlogImplementation
{
    [Target("LogContextTarget")]
    public class LogContextTarget : Target
    {
        private LogContext _database;

        /// <summary>
        ///     Constructs a new Entity Framework log target.
        /// </summary>
        public LogContextTarget()
        {
            KeepConnection = true;
            LogLevel = "${LogLevelOrdinal}";
            Message = "${message}";
            Time = @"${date:universalTime=true:format=yyyy-MM-ddTHH\:mm\:ss.fff}";
            AccountID = "${event-context:item=AccountID}";
            Type = "${event-context:item=Type}";
            Par1 = "${event-context:item=Par1}";
            Par2 = "${event-context:item=Par2}";
            Par3 = "${event-context:item=Par3}";
            Source = "${logger}";
        }

        /// <summary>
        ///     Gets or sets the name of the connection string to be used for this log target.
        /// </summary>
        [RequiredParameter]
        public string ConnectionStringName { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether to keep the database connection open between the log events.
        /// </summary>
        [DefaultValue(true)]
        public bool KeepConnection { get; set; }

        public Layout LogLevel { get; set; }
        public Layout Time { get; set; }
        public Layout AccountID { get; set; }
        public Layout Type { get; set; }
        public Layout Par1 { get; set; }
        public Layout Par2 { get; set; }
        public Layout Par3 { get; set; }
        public Layout Message { get; set; }
        public Layout Source { get; set; }

        private LogContext Database
        {
            get
            {
                if (_database == null)
                    _database = new LogContext(ConnectionStringName);
                return _database;
            }
        }

        /// <summary>
        ///     Entry point from NLog.
        /// </summary>
        protected override void CloseTarget()
        {
            base.CloseTarget();
            CloseDatabase();
        }

        /// <summary>
        ///     Entry point from NLog.
        /// </summary>
        /// <param name="logEvent">Event to write to the log.</param>
        protected override void Write(LogEventInfo logEvent)
        {
            try
            {
                WriteToDatabase(logEvent);
            }
            catch (Exception e)
            {
                Trace.WriteLine(String.Format("Error writing event to database: {0}\n{1}", e.Message, e));
                CloseDatabase();
                throw;
            }
            finally
            {
                if (!KeepConnection)
                    CloseDatabase();
            }
        }

        #region Low-level database methods

        private void WriteToDatabase(LogEventInfo logEvent)
        {
            Guid accountID;
            LogingLevel level;
            var time = DateTime.Now;
            Database.Create(new LogItem
            {
                AccountID = Guid.TryParse(AccountID.Render(logEvent), out accountID) ? accountID : (Guid?) null,
                LogLevel = Enum.TryParse(LogLevel.Render(logEvent), out level) ? level : LogingLevel.Off,
                Message = Message.Render(logEvent),
                Time = DateTime.TryParse(Time.Render(logEvent), out time) ? time : time,
                Par1 = Par1.Render(logEvent),
                Par2 = Par2.Render(logEvent),
                Par3 = Par3.Render(logEvent),
                Type = Type.Render(logEvent),
                Source = Source.Render(logEvent)
            });
            Database.Save();
        }


        private void CloseDatabase()
        {
            if (_database == null)
                return;

            _database.Dispose();
            _database = null;
        }

        #endregion
    }
}