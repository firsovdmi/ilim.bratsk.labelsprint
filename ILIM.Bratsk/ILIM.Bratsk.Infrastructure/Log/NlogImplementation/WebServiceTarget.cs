using System;
using System.Collections.Generic;
using NLog;
using NLog.Layouts;
using NLog.Targets;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log.NlogImplementation
{
    [Target("WebServiceTarget")]
    public class WebServiceTarget : Target
    {
        private LogContext _database;
        private readonly ILogPublisher _logPublisher;

        /// <summary>
        ///     Constructs a new Entity Framework log target.
        /// </summary>
        public WebServiceTarget(ILogPublisher logPublisher)
        {
            _logPublisher = logPublisher;
            LogLevel = "${LogLevelOrdinal}";
            Message = "${message}";
            Time = @"${date:universalTime=true:format=yyyy-MM-ddTHH\:mm\:ss.fff}";
            AccountID = "${event-context:item=AccountID}";
            Type = "${event-context:item=Type}";
            Par1 = "${event-context:item=Par1}";
            Par2 = "${event-context:item=Par2}";
            Par3 = "${event-context:item=Par3}";
            Source = "${logger}";
        }

        public Layout LogLevel { get; set; }
        public Layout Time { get; set; }
        public Layout AccountID { get; set; }
        public Layout Type { get; set; }
        public Layout Par1 { get; set; }
        public Layout Par2 { get; set; }
        public Layout Par3 { get; set; }
        public Layout Message { get; set; }
        public Layout Source { get; set; }

        /// <summary>
        ///     Entry point from NLog.
        /// </summary>
        /// <param name="logEvent">Event to write to the log.</param>
        protected override void Write(LogEventInfo logEvent)
        {
            Guid accountID;
            LogingLevel level;
            var time = DateTime.Now;
            var item = new LogItem
            {
                AccountID = Guid.TryParse(AccountID.Render(logEvent), out accountID) ? accountID : (Guid?) null,
                LogLevel = Enum.TryParse(LogLevel.Render(logEvent), out level) ? level : LogingLevel.Off,
                Message = Message.Render(logEvent),
                Time = DateTime.TryParse(Time.Render(logEvent), out time) ? time : time,
                Par1 = Par1.Render(logEvent),
                Par2 = Par2.Render(logEvent),
                Par3 = Par3.Render(logEvent),
                Type = Type.Render(logEvent),
                Source = Source.Render(logEvent)
            };

            _logPublisher.Publish(new List<LogItem> {item});
        }
    }
}