﻿using System.Collections.Generic;
using System.Linq;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public class ReadLogService : IReadLogService
    {
        private readonly ILogRepository _logRepository;

        public ReadLogService(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }

        #region Implementation of IReadLogService

        public List<LogItem> GetLog(LogRequestParameters logRequestParameters)
        {
            var query = _logRepository.Get().OrderByDescending(p => p.Time).AsQueryable();
            if (logRequestParameters.IsFilterByDateTime)
                query = query.Where(p => p.Time >= logRequestParameters.From && p.Time <= logRequestParameters.To);
            if (logRequestParameters.IsUseTop)
                query = query.Take(logRequestParameters.TopCount);
            if (!string.IsNullOrEmpty(logRequestParameters.Type))
                query = query.Where(p => p.Type.Contains(logRequestParameters.Type));
            if (!string.IsNullOrEmpty(logRequestParameters.Par1))
                query = query.Where(p => p.Par1.Contains(logRequestParameters.Par1));
            if (!string.IsNullOrEmpty(logRequestParameters.Par2))
                query = query.Where(p => p.Par2.Contains(logRequestParameters.Par2));
            if (!string.IsNullOrEmpty(logRequestParameters.Par3))
                query = query.Where(p => p.Par3.Contains(logRequestParameters.Par3));
            if (!string.IsNullOrEmpty(logRequestParameters.Par1))
                query = query.Where(p => p.Par1.Contains(logRequestParameters.Par1));
            if (!string.IsNullOrEmpty(logRequestParameters.LogSource))
                query = query.Where(p => p.Source.Contains(logRequestParameters.LogSource));

            if (!logRequestParameters.IsGetAllLoginLevels)
                query = query.Where(p => logRequestParameters.LoginLevels.Contains(p.LogLevel));
            if (!logRequestParameters.IsGetAllAccounts)
                query = query.Where(p => logRequestParameters.AccountIds.Contains(p.AccountID));


            return query.ToList();
        }

        #endregion
    }
}