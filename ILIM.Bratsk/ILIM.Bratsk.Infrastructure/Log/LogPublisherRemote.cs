﻿using System.ServiceModel;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public class LogPublisherRemote : ILogPublisherRemote
    {
        private readonly ILogPublisher _logPublisher;

        public LogPublisherRemote(ILogPublisher logPublisher)
        {
            _logPublisher = logPublisher;
        }

        #region Implementation of ILogRemotePublisher

        public void Subscribe()
        {
            _logPublisher.Subscribe(OperationContext.Current.GetCallbackChannel<ILogSubscriber>());
        }

        public void UnSubscribe()
        {
            _logPublisher.UnSubscribe(OperationContext.Current.GetCallbackChannel<ILogSubscriber>());
        }

        #endregion
    }
}