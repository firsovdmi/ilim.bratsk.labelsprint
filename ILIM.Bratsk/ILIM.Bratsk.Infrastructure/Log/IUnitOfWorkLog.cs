using System.Linq;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public interface IUnitOfWorkLog : IUnitOfWork
    {
        IQueryable<LogItem> Get();
        void Delete(LogItem entity);
        LogItem Create(LogItem entity);
        LogItem Update(LogItem entity);
        void DeleteBatch(IQueryable<LogItem> items);
    }
}