using System.ServiceModel;

namespace ILIM.Bratsk.Infrastructure.Log
{
    [ServiceContract(CallbackContract = typeof (ILogSubscriber))]
    public interface ILogPublisherRemote
    {
        [OperationContract(IsOneWay = true)]
        void Subscribe();

        [OperationContract(IsOneWay = true)]
        void UnSubscribe();
    }
}