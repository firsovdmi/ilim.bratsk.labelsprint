﻿namespace ILIM.Bratsk.Infrastructure.Log
{
    public interface ILogService
    {
        void Log(LogingLevel loglevel, string message, string type,
                 string par1 = null, string par2 = null, string par3 = null, string par4 = null,
                 string source = "Server");
    }
}