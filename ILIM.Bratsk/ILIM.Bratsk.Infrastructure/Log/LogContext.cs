﻿using System.Data.Entity;
using System.Linq;
using ILIM.Bratsk.Infrastructure.DateTimeManager;
using ILIM.Bratsk.Infrastructure.Log.MapConfig;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public class LogContext : DbContext, IUnitOfWorkLog
    {
        private IDateTimeManager _dateTimeManager;

        public LogContext(string connectionString)
            : this(new DateTimeManager.DateTimeManager(), connectionString)
        {
        }

        public LogContext(IDateTimeManager dateTimeManager, string connectionString)
            : base(connectionString)
        {
            _dateTimeManager = dateTimeManager;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<LogItem> LogItems { get; set; }

        #region Implementation of IUnitOfWork

        public void Save()
        {
            this.SaveChanges();
        }

        #endregion

        #region Overrides of DbContext

        /// <summary>
        ///     This method is called when the model for a derived context has been initialized, but
        ///     before the model has been locked down and used to initialize the context.  The default
        ///     implementation of this method does nothing, but it can be overridden in a derived class
        ///     such that the model can be further configured before it is locked down.
        /// </summary>
        /// <remarks>
        ///     Typically, this method is called only once when the first instance of a derived context
        ///     is created.  The model for that context is then cached and is for all further instances of
        ///     the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///     property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///     More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///     classes directly.
        /// </remarks>
        /// <param name="modelBuilder">The builder that defines the model for the context being created. </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new LogItemMapConfiguration());
        }

        #endregion

        #region Implementation of IUnitOfWorkLog

        public IQueryable<LogItem> Get()
        {
            return this.Set<LogItem>();
        }

        public void Delete(LogItem entity)
        {
            Set<LogItem>().Attach(entity);
            if (Entry(entity).State != EntityState.Deleted)
                Set<LogItem>().Remove(entity);
        }

        public LogItem Create(LogItem entity)
        {
            if (Entry(entity).State == EntityState.Detached)
                Set<LogItem>().Add(entity);
            return entity;
        }

        public LogItem Update(LogItem entity)
        {
            Set<LogItem>().Attach(entity);
            var entry = Entry(entity);
            entry.State = EntityState.Modified;

            return entity;
        }

        public void DeleteBatch(IQueryable<LogItem> items)
        {
            DeleteBatch(items);
        }

        #endregion
    }
}