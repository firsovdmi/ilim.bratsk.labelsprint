﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.Log
{
    public enum LogingLevel
    {
        [Description("Трассировка")] [EnumMember] Trace = 0,
        [Description("Отладка")] [EnumMember] Debug = 1,
        [Description("Информация")] [EnumMember] Info = 2,
        [Description("Предупреждение")] [EnumMember] Warn = 3,
        [Description("Ошибка")] [EnumMember] Error = 4,
        [Description("Фатальная ошибка")] [EnumMember] Fatal = 5,
        [Description("Без уровня")] [EnumMember] Off = 6
    }
}