using System;

namespace ILIM.Bratsk.Infrastructure.DateTimeManager
{
    public interface IDateTimeManager
    {
        DateTime GetDateTimeNow();
    }
}