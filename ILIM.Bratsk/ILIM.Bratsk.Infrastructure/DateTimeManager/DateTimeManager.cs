using System;

namespace ILIM.Bratsk.Infrastructure.DateTimeManager
{
    public class DateTimeManager : IDateTimeManager
    {
        public DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }
    }
}