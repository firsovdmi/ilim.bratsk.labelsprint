﻿namespace ILIM.Bratsk.Infrastructure.Model
{
    public interface IMarkableForDelete
    {
        bool IsDeleted { get; set; }
    }
}