﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.Model
{
    [DataContract]
    public class ShortEntity : INotifyPropertyChanged
    {
        [DataMember] private Guid _id;
        [DataMember] private string _name;

        public Guid ID
        {
            get { return _id; }
            set
            {
                if (value.Equals(_id)) return;
                _id = value;
                OnPropertyChanged("Name");
            }
        }

        [MaxLength(255, ErrorMessage = @"Превышен максимальный размер(255 символов)")]
        public String Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}