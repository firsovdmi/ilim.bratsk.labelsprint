using System.ComponentModel.DataAnnotations.Schema;

namespace ILIM.Bratsk.Infrastructure.Model
{
    public class EntityMapConfiguration<T> : EntityBaseMapConfiguration<T> where T : Entity
    {
        private const string TableName = "Entity";

        public EntityMapConfiguration()
        {
            Property(p => p.DateTimeCreate).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            //Property(p => p.Name).HasMaxLength(255);
            Property(p => p.TimeStamp).IsRowVersion().IsConcurrencyToken();
            Ignore(p => p.RowVersion);
        }
    }
}