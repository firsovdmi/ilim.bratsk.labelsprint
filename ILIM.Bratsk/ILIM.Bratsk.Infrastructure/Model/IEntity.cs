﻿using System;

namespace ILIM.Bratsk.Infrastructure.Model
{
    public interface IEntity
    {
        Guid ID { get; set; }
    }
}