﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.Model
{
    [Serializable]
    [DataContract]
    public class Entity : EntityBase, IMarkableForDelete
    {
        [DataMember] private DateTime _dateTimeCreate;
        [DataMember] private DateTime _dateTimeUpdate;
        [DataMember] protected string _name;

        [MaxLength(255, ErrorMessage = @"Превышен максимальный размер(255 символов)")]
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        [DataMember]
        public Guid? UserCreated { get; set; }

        [DataMember]
        public Guid? UserUpdated { get; set; }

        [DataMember]
        public bool IsSystem { get; set; }

        /// <summary>
        ///     Дата и время создания
        /// </summary>
        public DateTime DateTimeCreate
        {
            get
            {
                if (_dateTimeCreate < SqlDateTime.MinValue.Value)
                {
                    _dateTimeCreate = SqlDateTime.MinValue.Value;
                }
                return _dateTimeCreate;
            }
            set { _dateTimeCreate = value; }
        }

        /// <summary>
        ///     Дата и время обновления
        /// </summary>
        public DateTime DateTimeUpdate
        {
            get
            {
                if (_dateTimeUpdate < SqlDateTime.MinValue.Value)
                {
                    _dateTimeUpdate = DateTime.Now;
                }
                return _dateTimeUpdate.ToLocalTime();
            }
            set { _dateTimeUpdate = value; }
        }

        [DataMember]
        [Browsable(false)]
        public byte[] TimeStamp { get; set; }

        public string RowVersion
        {
            get
            {
                if (TimeStamp != null)
                {
                    return Convert.ToBase64String(TimeStamp);
                }

                return string.Empty;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    TimeStamp = null;
                }
                else
                {
                    TimeStamp = Convert.FromBase64String(value);
                }
            }
        }

        [DataMember]
        public bool IsDeleted { get; set; }
    }
}