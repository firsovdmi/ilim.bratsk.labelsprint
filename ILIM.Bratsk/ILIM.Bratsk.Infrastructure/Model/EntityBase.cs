﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.Model
{
    [DataContract(IsReference = true)]
    [Serializable]
    public class EntityBase : IEntity, INotifyPropertyChanged
    {
        [DataMember] private Guid _id;

        #region Implementation of IEntity

        public Guid ID
        {
            get { return _id; }
            set { _id = value; }
        }

        #endregion

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}