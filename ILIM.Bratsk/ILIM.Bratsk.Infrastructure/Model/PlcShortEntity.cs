﻿using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.Model
{
    [DataContract]
    public class PlcShortEntity : ShortEntity
    {
        [DataMember] private int _plcId;

        public int PlcId
        {
            get { return _plcId; }
            set
            {
                if (value.Equals(_plcId)) return;
                _plcId = value;
                OnPropertyChanged("PlcId");
            }
        }
    }
}