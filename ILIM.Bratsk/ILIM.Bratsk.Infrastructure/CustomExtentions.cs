﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ILIM.Bratsk.Infrastructure.Helpers;

namespace ILIM.Bratsk.Infrastructure
{
    public static class CustomExtentions
    {
        public static bool IsNumeric(this string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        public static string GetDescription(this PositionErrorEnum p)
        {
            var type = typeof(PositionErrorEnum);
            var memInfo = type.GetMember(p.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute),
                false);
            return ((DescriptionAttribute)attributes[0]).Description;
        }
    }
}
