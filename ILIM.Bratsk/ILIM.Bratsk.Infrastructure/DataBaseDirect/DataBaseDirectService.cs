﻿using System;
using System.Data;
using System.Data.SqlClient;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Log;

namespace ILIM.Bratsk.Infrastructure.DataBaseDirect
{
    public class DataBaseDirectService : IDataBaseDirectService
    {
        private const string AddPlanStoredProcedureName = "sp_Command";
        private readonly string _connectionString;
        private readonly ILogService _logService;

        public DataBaseDirectService(string connectionString, ILogService logService)
        {
            _logService = logService;
            _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connectionString].ConnectionString;
        }


        public bool AddPlan(string batchCode, int packFirst, int packLast, PrinterModeEnum empt)
        {
            _logService.Log(LogingLevel.Trace, string.Format("Добавлен план. Код {0}, Номер кип {1} - {2}, печать кода {3}", batchCode, packFirst, packLast, empt), "ProcessMessage");
            return RunCommand(batchCode, packFirst, packLast, empt, 1);
        }

        bool RunCommand(string batchCode, int packFirst, int packLast, PrinterModeEnum empt, int commandType)
        {
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    using (var cmd = new SqlCommand(AddPlanStoredProcedureName, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Cmd_Type", commandType);
                        cmd.Parameters.AddWithValue("@Batch_Code", batchCode);
                        cmd.Parameters.AddWithValue("@Pack_First", packFirst);
                        cmd.Parameters.AddWithValue("@Pack_Last", packLast);
                        cmd.Parameters.AddWithValue("@Empt", empt);
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public bool EditPlan(string batchCode, int packFirst, int packLast, PrinterModeEnum empt)
        {
            _logService.Log(LogingLevel.Trace, string.Format("Изменен план. Код {0}, Номер кип {1} - {2}, печать кода {3}", batchCode, packFirst, packLast, empt), "ProcessMessage");
            return RunCommand(batchCode, packFirst, packLast, empt, 2);
        }

        public bool Stop()
        {
            _logService.Log(LogingLevel.Trace, string.Format("Останов"), "ProcessMessage");
            return RunCommand("", 0, 0, 0, 3);
        }
    }
}
