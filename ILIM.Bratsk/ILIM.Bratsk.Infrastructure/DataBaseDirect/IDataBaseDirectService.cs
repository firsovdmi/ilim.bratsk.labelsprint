﻿using System.ServiceModel;
using ILIM.Bratsk.Infrastructure.Helpers;

namespace ILIM.Bratsk.Infrastructure.DataBaseDirect
{
    [ServiceContract]
    public interface IDataBaseDirectService
    {
        [OperationContract]
        bool AddPlan(string batchCode, int packFirst, int packLast, PrinterModeEnum empt);
        [OperationContract]
        bool EditPlan(string batchCode, int packFirst, int packLast, PrinterModeEnum empt);
        [OperationContract]
        bool Stop();
    }
}