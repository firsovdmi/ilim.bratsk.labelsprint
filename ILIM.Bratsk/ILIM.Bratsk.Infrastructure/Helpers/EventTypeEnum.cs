﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.Infrastructure.Helpers
{
    [DataContract]
    public enum EventTypeEnum
    {
        [Description("Команда 'Добавление партии'")]
        [EnumMember]
        Add = 1,
        [Description("Команда 'Изменение партии'")]
        [EnumMember]
        Edit = 2,
        [Description("Команда 'Останов маркировки'")]
        [EnumMember]
        Stop = 3,
        [Description("Исчерпание очереди ПЛАН")]
        [EnumMember]
        PlanEmpty = 7,
        [Description("Прочие ошибки")]
        [EnumMember]
        Error = 8,
    }
}
