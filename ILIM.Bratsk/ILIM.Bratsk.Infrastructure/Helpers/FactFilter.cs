﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.Infrastructure.Helpers
{
    [DataContract]
    public class FactFilter
    {
        [DataMember]
        private TimeFilter _period = new TimeFilter();
        [DataMember]
        public string BatchCode { get; set; }

        [DataMember]
        private List<int> _lineNumbers;
        [DataMember]
        private List<int> _printerNumbers;
        [DataMember]
        private bool _isAllLines = true;
        [DataMember]
        private bool _isAllPrinters = true;
        [DataMember]
        private int _factCount;

        public FactFilter()
        {
            FactCount = 100;
        }
        public int FactCount
        {
            get { return _factCount; }
            set { _factCount = value; }
        }


        public List<int> LinesNumbers
        {
            get { return _lineNumbers ?? (_lineNumbers = new List<int>()); }
            set { _lineNumbers = value; }
        }
        public List<int> PrinterNumbers
        {
            get { return _printerNumbers ?? (_printerNumbers = new List<int>()); }
            set { _printerNumbers = value; }
        }

        public TimeFilter Period
        {
            get { return _period; }
            set { _period = value; }
        }

        public bool IsAllLines
        {
            get { return _isAllLines; }
            set { _isAllLines = value; }
        }

        public bool IsAllPrinters
        {
            get { return _isAllPrinters; }
            set { _isAllPrinters = value; }
        }
        
    }
}
