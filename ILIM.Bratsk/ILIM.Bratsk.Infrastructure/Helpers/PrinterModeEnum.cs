﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.Helpers
{
    public enum PrinterModeEnum
    {
        [Description("Печать кодов")]
        [EnumMember]
        Standart = 0,
        [Description("Пропуск кодов")]
        [EnumMember]
        NoCode = 1,
        [Description("Незапланированная кипа")]
        [EnumMember]
        NoPlaned = 2,
    }
}
