﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.Infrastructure.Helpers
{
    public enum EventSourceEnum
    {
        [Description("Не известно (0)")]
        [EnumMember]
        Uncknow = 0,
        [Description("система Проконт")]
        [EnumMember]
        Procont = 1,
        [Description("панель оператора системы Профипринт")]
        [EnumMember]
        ARM = 2,
        [Description("сервисное сообщение системы Профипринт")]
        [EnumMember]
        Server = 3,
    }
}
