using System.Linq;
using Castle.MicroKernel;
using Castle.Windsor;

namespace ILIM.Bratsk.Infrastructure.Helpers
{
    public static class CastleExtentions
    {
        public static IWindsorContainer AddFacilityIfNotAdded<T>(this IWindsorContainer container)
            where T : IFacility, new()
        {
            if (!container.Kernel.GetFacilities().Any(f => f is T))
            {
                return container.AddFacility<T>();
            }
            return container;
        }
    }
}