﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.Helpers
{
    public enum EventStatusEnum
    {
        [Description("Не известно (0)")]
        [EnumMember]
        Uncknow = 0,
        [Description("Нет ошибки")]
        [EnumMember]
        NoError = 1,
        [Description("Ошибка")]
        [EnumMember]
        Error = 2,
    }
}
