﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.Infrastructure.Helpers
{
    [DataContract]
    public class GroupedPlan
    {
        [DataMember]
        public string   BatchCode       { get; set; }
        [DataMember]
        public DateTime TimeCreate      { get; set; }
        [DataMember]
        public int      PackFirst       { get; set; }
        [DataMember]
        public int      PackLast        { get; set; }
        [DataMember]
        public int      PackCount       { get; set; }
        [DataMember]
        public PrinterModeEnum Empt { get; set; }
        [DataMember]
        public string   CelluloseName { get; set; }
        [DataMember]
        public string      MessageSource { get; set; }
    }
}
