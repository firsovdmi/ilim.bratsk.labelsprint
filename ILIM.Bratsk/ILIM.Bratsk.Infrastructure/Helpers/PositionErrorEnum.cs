﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.Infrastructure.Helpers
{
    public enum PositionErrorEnum
    {
        [Description("Нет ошибки")]
        NoError,
        [Description("Ошибка датчика")]
        PositeonError,
        [Description("Ошибка датчика (этикетка)")]
        PositeonAndPrinterError,
        [Description("Исключение при обработке")]
        Exception
    }
}
