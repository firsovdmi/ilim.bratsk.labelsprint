﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.Infrastructure.Helpers
{
    [DataContract]
    public class EventFilter
    {
        public EventFilter()
        {
            IsAllStatuses =IsAllEventSources= IsAllEventEventTypes = IsAllEventPrinterModeEnums = true;
        }
        [DataMember]
        private TimeFilter _period = new TimeFilter();

        public TimeFilter Period
        {
            get { return _period; }
            set { _period = value; }
        }

        [DataMember]
        public List<int> Statuses { get; set; }
        [DataMember]
        public bool IsAllStatuses { get; set; }

        [DataMember]
        public List<int> EventSources{ get; set; }
        [DataMember]
        public bool IsAllEventSources { get; set; }

        [DataMember]
        public List<int> EventTypes { get; set; }
        [DataMember]
        public bool IsAllEventEventTypes { get; set; }

        [DataMember]
        public List<int> PrinterModeEnums { get; set; }
        [DataMember]
        public bool IsAllEventPrinterModeEnums { get; set; }
        [DataMember]
        public int Count { get; set; }
    }

}
