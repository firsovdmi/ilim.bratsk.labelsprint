﻿namespace ILIM.Bratsk.Infrastructure
{
    public interface IUnitOfWork
    {
        void Save();
    }
}