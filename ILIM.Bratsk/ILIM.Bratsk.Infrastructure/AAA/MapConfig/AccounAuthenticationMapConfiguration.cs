using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.MapConfig
{
    public class AccounAuthenticationMapConfiguration : EntityBaseMapConfiguration<AccounAuthentication>
    {
        private const string TableName = "AccounAuthentication";

        public AccounAuthenticationMapConfiguration()
        {
            HasRequired(a => a.Account).WithRequiredDependent(a => a.AccounAuthentication);
            Map(p => { p.ToTable(TableName); });
        }
    }
}