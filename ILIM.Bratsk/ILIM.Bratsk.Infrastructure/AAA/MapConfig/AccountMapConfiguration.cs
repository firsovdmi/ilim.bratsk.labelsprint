using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.MapConfig
{
    public class AccountMapConfiguration : EntityMapConfiguration<Account>
    {
        private const string TableName = "Account";

        public AccountMapConfiguration()
        {
            Property(p => p.Login)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute {IsUnique = true}));
            HasMany(p => p.AccountInRoles).WithRequired(p => p.Account);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}