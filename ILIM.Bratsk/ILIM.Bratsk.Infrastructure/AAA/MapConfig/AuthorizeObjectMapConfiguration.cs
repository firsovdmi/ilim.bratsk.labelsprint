using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.MapConfig
{
    public class AuthorizeObjectMapConfiguration : EntityBaseMapConfiguration<AuthorizeObject>
    {
        private const string TableName = "AuthorizeObject";

        public AuthorizeObjectMapConfiguration()
        {
            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(255)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute {IsUnique = true}));
            HasMany(p => p.AuthorizeObjectsForRoles).WithRequired(p => p.AuthorizeObject);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}