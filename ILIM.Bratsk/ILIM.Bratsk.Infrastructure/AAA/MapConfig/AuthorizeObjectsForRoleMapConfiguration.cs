using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.MapConfig
{
    public class AuthorizeObjectsForRoleMapConfiguration : EntityMapConfiguration<AuthorizeObjectsForRole>
    {
        private const string TableName = "AuthorizeObjectsForRole";

        public AuthorizeObjectsForRoleMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}