using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Castle.Facilities.WcfIntegration.Behaviors;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.Log;

namespace ILIM.Bratsk.Infrastructure.AAA
{
    public class LifestyleServerMessageAction : AbstractMessageAction
    {
        private readonly IAAAService _aaaService;

        private readonly List<string> _blackListOperationsLog
            = new List<string>
            {
                "PlcBufferService.HandleReciveRecords",
                "PlcBufferService.HandleCipRecords",
                "ReceivingTaskPublisherRemote.Subscribe"
            };

        private readonly ILogService _logService;

        public LifestyleServerMessageAction(IAAAService aaaService, ILogService logService)
            : base(MessageLifecycle.All)
        {
            _aaaService = aaaService;
            _logService = logService;
        }

        public override bool Perform(ref Message message, MessageLifecycle lifecycle, IDictionary state)
        {
            if (lifecycle == MessageLifecycle.IncomingRequest)
            {
                WcfInstanceContext.Current.MessageMem = new MessageMem(message.Headers.MessageId, message.Headers.Action);

                WcfInstanceContext.Current.MessageMem.LoginResult = IsLoginOk(message, GetOperationName());
                if (WcfInstanceContext.Current.MessageMem.LoginResult != AutorizationResult.Success)
                {
                    message = null;
                }
                else
                {
                    WcfInstanceContext.Current.ContextAccount = GetActiveAccountByToken(message);
                }
                return true;
            }

            if (lifecycle == MessageLifecycle.OutgoingResponse)
            {
                if (WcfInstanceContext.Current.MessageMem.LoginResult == AutorizationResult.Success)
                {
                    return true;
                }
                var fault =
                    new FaultException("������ �����������", new FaultCode(FaultsCodes.AutorizationFault.ToString()))
                        .CreateMessageFault();

                message = Message.CreateMessage(message.Version, fault, WcfInstanceContext.Current.MessageMem.Action);
                message.Headers.RelatesTo = WcfInstanceContext.Current.MessageMem.MessageId;
            }
            return true;
        }


        private static string GetOperationName()
        {
            return OperationContext.Current.IncomingMessageHeaders.To.AbsolutePath.Split('/').Last() + "." +
                   OperationContext.Current.IncomingMessageHeaders.Action.Split('/').Last();
        }

        private AutorizationResult IsLoginOk(Message msg, string operationID)
        {
            if (msg == null) return AutorizationResult.Unknown;
            var index = msg.Headers.FindHeader("Token", "http://pagru.com");
            if (index < 0)
            {
                return AutorizationResult.Unknown;
            }
            var contextToken = msg.Headers.GetHeader<Guid>(index);
            return _aaaService.CheckLogin(contextToken, operationID);
        }

        private ActiveAccount GetActiveAccountByToken(Message msg)
        {
            if (msg == null) return null;
            var index = msg.Headers.FindHeader("Token", "http://pagru.com");
            if (index < 0)
            {
                return null;
            }
            var contextToken = msg.Headers.GetHeader<Guid>(index);
            return _aaaService.GetActiveAccountByToken(contextToken);
        }
    }
}