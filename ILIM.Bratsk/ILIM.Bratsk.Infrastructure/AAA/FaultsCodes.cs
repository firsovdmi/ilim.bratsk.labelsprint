namespace ILIM.Bratsk.Infrastructure.AAA
{
    public enum FaultsCodes
    {
        AutorizationFault,
        ConcurencyFault
    }
}