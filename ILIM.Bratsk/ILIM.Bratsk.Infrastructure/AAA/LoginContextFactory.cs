using System.Data.Entity.Infrastructure;

namespace ILIM.Bratsk.Infrastructure.AAA
{
    public class LoginContextFactory : IDbContextFactory<LoginContext>
    {
        #region Implementation of IDbContextFactory<out FactoryContext>

        /// <summary>
        ///     Creates a new instance of a derived <see cref="T:System.Data.Entity.DbContext" /> type.
        /// </summary>
        /// <returns>
        ///     An instance of TContext
        /// </returns>
        public LoginContext Create()
        {
            return new LoginContext("ILIM.Bratsk");
        }

        #endregion
    }
}