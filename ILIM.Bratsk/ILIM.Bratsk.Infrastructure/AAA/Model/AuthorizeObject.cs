using System.Collections.Generic;
using System.Runtime.Serialization;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.Model
{
    [DataContract]
    public class AuthorizeObject : EntityBase
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public List<AuthorizeObjectsForRole> AuthorizeObjectsForRoles { get; set; }
    }
}