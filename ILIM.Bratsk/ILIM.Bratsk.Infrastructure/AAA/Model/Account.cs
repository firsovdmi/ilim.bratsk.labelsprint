using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.Model
{
    [DataContract]
    public class Account : Entity
    {
        [DataMember] private AccounAuthentication _accounAuthentication;
        [DataMember] private List<AccountInRole> _accountInRoles;
        [DataMember] private TimeSpan _autoLogOffTime;
        [DataMember] private long _autoLogOffTimeMinutes;
        [DataMember] private string _email;
        [DataMember] private bool _isChangePassword;
        [DataMember] private bool _isWindowsAuthentification;
        [DataMember] private string _login;

        [Required(AllowEmptyStrings = false, ErrorMessage = "������� �����"),
         StringLength(80, ErrorMessage = "����� �� ����� ���� ������ 80 ��������")]
        public string Login
        {
            get { return _login; }
            set
            {
                if (value == _login) return;
                _login = value;
                OnPropertyChanged("Login");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "������� ����� ������")]
        public long AutoLogOffTimeMinutes
        {
            get { return _autoLogOffTimeMinutes; }
            set
            {
                if (value == _autoLogOffTimeMinutes) return;
                _autoLogOffTimeMinutes = value;
                OnPropertyChanged("AutoLogOffTimeMinutes");
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                if (value == _email) return;
                _email = value;
                OnPropertyChanged("Email");
            }
        }

        public bool IsChangePassword
        {
            get { return _isChangePassword; }
            set
            {
                if (value.Equals(_isChangePassword)) return;
                _isChangePassword = value;
                OnPropertyChanged("IsChangePassword");
            }
        }

        public virtual List<AccountInRole> AccountInRoles
        {
            get { return _accountInRoles; }
            set
            {
                if (Equals(value, _accountInRoles)) return;
                _accountInRoles = value;
                OnPropertyChanged("AccountInRoles");
            }
        }

        public bool IsWindowsAuthentification
        {
            get { return _isWindowsAuthentification; }
            set
            {
                if (value.Equals(_isWindowsAuthentification)) return;
                _isWindowsAuthentification = value;
                OnPropertyChanged("AccountInRoles");
            }
        }

        public AccounAuthentication AccounAuthentication
        {
            get { return _accounAuthentication; }
            set
            {
                if (Equals(value, _accounAuthentication)) return;
                _accounAuthentication = value;
                OnPropertyChanged("AccountInRoles");
            }
        }
    }
}