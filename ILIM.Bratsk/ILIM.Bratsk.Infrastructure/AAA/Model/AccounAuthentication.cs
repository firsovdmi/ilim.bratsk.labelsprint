using System.Runtime.Serialization;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.Model
{
    [DataContract]
    public class AccounAuthentication : EntityBase
    {
        [DataMember]
        public string PasswordHash { get; set; }

        [DataMember]
        public Account Account { get; set; }
    }
}