using System;
using System.Runtime.Serialization;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.Model
{
    [DataContract]
    public class AccountInRole : Entity
    {
        [DataMember]
        public Guid AccountID { get; set; }

        [DataMember]
        public Account Account { get; set; }

        [DataMember]
        public Guid RoleID { get; set; }

        [DataMember]
        public Role Role { get; set; }
    }
}