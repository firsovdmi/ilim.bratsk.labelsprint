using System;
using System.Runtime.Serialization;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.Model
{
    [DataContract]
    public class AuthorizeObjectsForRole : Entity
    {
        [DataMember]
        public Guid AuthorizeObjectID { get; set; }

        [DataMember]
        public AuthorizeObject AuthorizeObject { get; set; }

        [DataMember]
        public Guid RoleID { get; set; }

        [DataMember]
        public Role Role { get; set; }
    }
}