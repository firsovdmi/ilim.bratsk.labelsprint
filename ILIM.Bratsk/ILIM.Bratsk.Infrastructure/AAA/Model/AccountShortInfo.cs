using System;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Infrastructure.AAA.Model
{
    [DataContract]
    public class AccountShortInfo
    {
        [DataMember]
        public Guid ID { get; set; }

        [DataMember]
        public string Login { get; set; }
    }
}