using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using ILIM.Bratsk.Infrastructure.AAA.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<LoginContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"AAA\Migrations";
            this.SetSqlGenerator("System.Data.SqlClient", new SqlServerMigrationSqlGeneratorFixed());
            this.SetSqlGenerator("System.Data.SqlServerCe.4.0", new SqlServerMigrationSqlGeneratorFixed());
        }
        
        internal class SqlServerMigrationSqlGeneratorFixed : SqlServerMigrationSqlGenerator
        {
            protected override string Generate(DateTime defaultValue)
            {
                var value = defaultValue.Ticks != 0
                    ? defaultValue.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture)
                    : "1753-01-01 00:00:00";

                return string.Format("'{0}'", value);
            }

            protected override void Generate(AddColumnOperation addColumnOperation)
            {
                SetCreatedUtcColumn(addColumnOperation.Column);

                base.Generate(addColumnOperation);
            }

            protected override void Generate(CreateTableOperation createTableOperation)
            {
                SetCreatedUtcColumn(createTableOperation.Columns);

                base.Generate(createTableOperation);
            }

            private static void SetCreatedUtcColumn(IEnumerable<ColumnModel> columns)
            {
                foreach (var columnModel in columns)
                {
                    SetCreatedUtcColumn(columnModel);
                }
            }

            private static void SetCreatedUtcColumn(PropertyModel column)
            {
                if (column.Name == "DateTimeCreate" || column.Name == "DateTimeUpdate")
                {
                    column.DefaultValueSql = "GETUTCDATE()";
                }
                if (column.Name == "IsSystem")
                {
                    column.DefaultValueSql = "0";
                }
            }
        }
    }
}