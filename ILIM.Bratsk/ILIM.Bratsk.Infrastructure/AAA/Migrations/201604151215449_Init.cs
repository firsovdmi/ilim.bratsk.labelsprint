namespace ILIM.Bratsk.Infrastructure.AAA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Login = c.String(nullable: false, maxLength: 80),
                        AutoLogOffTimeMinutes = c.Long(nullable: false),
                        Email = c.String(),
                        IsChangePassword = c.Boolean(nullable: false),
                        IsWindowsAuthentification = c.Boolean(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Login, unique: true);
            
            CreateTable(
                "dbo.AccounAuthentication",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PasswordHash = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Account", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.AccountInRoles",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        AccountID = c.Guid(nullable: false),
                        RoleID = c.Guid(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Role", t => t.RoleID, cascadeDelete: true)
                .ForeignKey("dbo.Account", t => t.AccountID, cascadeDelete: true)
                .Index(t => t.AccountID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.AuthorizeObjectsForRole",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        AuthorizeObjectID = c.Guid(nullable: false),
                        RoleID = c.Guid(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuthorizeObject", t => t.AuthorizeObjectID, cascadeDelete: true)
                .ForeignKey("dbo.Role", t => t.RoleID, cascadeDelete: true)
                .Index(t => t.AuthorizeObjectID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.AuthorizeObject",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                        DisplayName = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Name, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountInRoles", "AccountID", "dbo.Account");
            DropForeignKey("dbo.AccountInRoles", "RoleID", "dbo.Role");
            DropForeignKey("dbo.AuthorizeObjectsForRole", "RoleID", "dbo.Role");
            DropForeignKey("dbo.AuthorizeObjectsForRole", "AuthorizeObjectID", "dbo.AuthorizeObject");
            DropForeignKey("dbo.AccounAuthentication", "ID", "dbo.Account");
            DropIndex("dbo.AuthorizeObject", new[] { "Name" });
            DropIndex("dbo.AuthorizeObjectsForRole", new[] { "RoleID" });
            DropIndex("dbo.AuthorizeObjectsForRole", new[] { "AuthorizeObjectID" });
            DropIndex("dbo.Role", new[] { "Name" });
            DropIndex("dbo.AccountInRoles", new[] { "RoleID" });
            DropIndex("dbo.AccountInRoles", new[] { "AccountID" });
            DropIndex("dbo.AccounAuthentication", new[] { "ID" });
            DropIndex("dbo.Account", new[] { "Login" });
            DropTable("dbo.AuthorizeObject");
            DropTable("dbo.AuthorizeObjectsForRole");
            DropTable("dbo.Role");
            DropTable("dbo.AccountInRoles");
            DropTable("dbo.AccounAuthentication");
            DropTable("dbo.Account");
        }
    }
}
