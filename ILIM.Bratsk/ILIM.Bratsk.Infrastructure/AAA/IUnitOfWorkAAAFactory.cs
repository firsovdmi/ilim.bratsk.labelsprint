using System;

namespace ILIM.Bratsk.Infrastructure.AAA
{
    public interface IUnitOfWorkAAAFactory : IDisposable
    {
        IUnitOfWorkAAA Create();
        void Release(IUnitOfWorkAAA unitOfWork);
    }
}