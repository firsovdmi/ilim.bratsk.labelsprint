using System.Xml;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;

namespace ILIM.Bratsk.Infrastructure.AAA
{
    internal sealed class MessageMem
    {
        public MessageMem(UniqueId messageId, string action)
        {
            MessageId = messageId;
            Action = action;
        }

        public AutorizationResult LoginResult { get; set; }
        public UniqueId MessageId { get; set; }
        public string Action { get; set; }
    }
}