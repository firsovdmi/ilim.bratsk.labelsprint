﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using ILIM.Bratsk.Infrastructure.AAA.MapConfig;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.DateTimeManager;
using ILIM.Bratsk.Infrastructure.Model;
using RefactorThis.GraphDiff;

namespace ILIM.Bratsk.Infrastructure.AAA
{
    public class LoginContext : DbContext, IUnitOfWorkAAA
    {
        private readonly IDateTimeManager _dateTimeManager;

        public LoginContext(string connectionString)
            : this(new DateTimeManager.DateTimeManager(), connectionString)
        {
        }

        public LoginContext(IDateTimeManager dateTimeManager, string connectionString)
            : base(connectionString)
        {
            _dateTimeManager = dateTimeManager;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccounAuthentication> AccountsWithCredentials { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<AuthorizeObject> AuthorizeObjects { get; set; }
        public DbSet<AuthorizeObjectsForRole> AuthorizeObjectsForRoles { get; set; }

        #region Implementation of IUnitOfWork

        public void Save()
        {
            SaveChanges();
        }

        #endregion

        #region Overrides of DbContext

        /// <summary>
        ///     This method is called when the model for a derived context has been initialized, but
        ///     before the model has been locked down and used to initialize the context.  The default
        ///     implementation of this method does nothing, but it can be overridden in a derived class
        ///     such that the model can be further configured before it is locked down.
        /// </summary>
        /// <remarks>
        ///     Typically, this method is called only once when the first instance of a derived context
        ///     is created.  The model for that context is then cached and is for all further instances of
        ///     the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///     property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///     More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///     classes directly.
        /// </remarks>
        /// <param name="modelBuilder">The builder that defines the model for the context being created. </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Ignore<Entity>();
            modelBuilder.Ignore<EntityBase>();
            modelBuilder.Configurations.Add(new AccountMapConfiguration());
            modelBuilder.Configurations.Add(new AccounAuthenticationMapConfiguration());
            modelBuilder.Configurations.Add(new RoleMapConfiguration());
            modelBuilder.Configurations.Add(new AuthorizeObjectMapConfiguration());
            modelBuilder.Configurations.Add(new AuthorizeObjectsForRoleMapConfiguration());
        }

        #endregion

        public override int SaveChanges()
        {
            var saveTime = _dateTimeManager.GetDateTimeNow().ToUniversalTime();
            foreach (
                var entry in
                    this.ChangeTracker.Entries()
                        .Where(p => p.Entity is Entity)
                        .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            {
                var entityWithVersion = entry.Entity as Entity;
                if (entityWithVersion != null)
                {
                    entityWithVersion.DateTimeUpdate = saveTime;
                    if (Entry(entityWithVersion).State == EntityState.Added)
                        entityWithVersion.DateTimeCreate = saveTime;
                }
            }
            return base.SaveChanges();
            ;
        }

        #region Implementation of IUnitOfWorkDO

        public IQueryable<T> Get<T>() where T : class, IEntity
        {
            return this.Set<T>().AsNoTracking();
        }

        public T Create<T>(T entity) where T : class, IEntity
        {
            if (Entry(entity).State == EntityState.Detached)
                Set<T>().Add(entity);
            return entity;
        }

        public T Update<T>(T entity) where T : class, IEntity
        {
            Set<T>().Attach(entity);
            var entry = Entry(entity);
            entry.State = EntityState.Modified;

            return entity;
        }

        public T CreateOrUpdate<T>(T entity) where T : class, IEntity
        {
            if (Set<T>().Any(p => p.ID == entity.ID))
            {
                Update(entity);
            }
            else
            {
                Create(entity);
            }
            return entity;
        }

        public void Delete<T>(T entity) where T : class, IEntity
        {
            Set<T>().Attach(entity);
            if (Entry(entity).State != EntityState.Deleted)
                Set<T>().Remove(entity);
        }

        public T Update<T>(T entity, Expression<Func<IUpdateConfiguration<T>, object>> config)
            where T : class, IEntity, new()
        {
            this.UpdateGraph(entity, config);
            return entity;
        }

        #endregion
    }
}