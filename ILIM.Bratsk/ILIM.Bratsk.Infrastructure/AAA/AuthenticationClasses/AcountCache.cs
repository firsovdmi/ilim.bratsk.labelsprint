using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ILIM.Bratsk.Infrastructure.AAA.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses
{
    public class AcountCache : IAcountCache
    {
        public AcountCache(IAAARepository aaaRepository)
        {
            AuthorizeObjects = aaaRepository.GetAuthorizeObjects().Include(p => p.AuthorizeObjectsForRoles).ToList();
            ActiveAccounts = new List<ActiveAccount>
            {
                new AnonimusActiveAccount {Account = new Account {AccountInRoles = new List<AccountInRole>()}}
            };
        }

        #region Implementation of IAcountCache

        public List<ActiveAccount> ActiveAccounts { get; set; }

        public List<AuthorizeObject> AuthorizeObjects { get; set; }

        public AutorizationResult CheckAccess(Guid token, string autorizeObjectID)
        {
            var activeAccount = ActiveAccounts.FirstOrDefault(p => p.Token == token);
            if (activeAccount != null && activeAccount.Account.Login == "Admin1") return AutorizationResult.Success;
            AutorizationResult autorizationResult;
            var authorizeObject = AuthorizeObjects.FirstOrDefault(p => p.Name == autorizeObjectID);
            if (authorizeObject != null)
            {
                if (activeAccount == null)
                {
                    autorizationResult = AutorizationResult.NotLogined;
                }
                else if (activeAccount.IsExpire)
                {
                    ActiveAccounts.Remove(activeAccount);
                    autorizationResult = AutorizationResult.AccountIsExpire;
                }

                else if (
                    activeAccount.Account.AccountInRoles.Select(p => p.RoleID)
                        .Intersect(authorizeObject.AuthorizeObjectsForRoles.Select(p => p.RoleID))
                        .Any())
                {
                    autorizationResult = AutorizationResult.Success;
                }
                else
                {
                    autorizationResult = AutorizationResult.AcessDeny;
                }
            }
            else
            {
                autorizationResult = AutorizationResult.Success;
            }


            return autorizationResult;
        }

        public ActiveAccount GetActiveAccountByToken(Guid token)
        {
            return ActiveAccounts.FirstOrDefault(p => p.Token == token);
        }

        #endregion
    }
}