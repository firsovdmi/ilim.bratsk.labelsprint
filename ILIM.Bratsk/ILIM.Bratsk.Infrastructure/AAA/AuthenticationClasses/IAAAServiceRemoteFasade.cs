﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses

{
    [ServiceContract]
    public interface IAAAServiceRemoteFasade
    {
        [OperationContract]
        AuthenticationResult Login(string login, string password, bool iswWindowsAuth, string machineName);

        [OperationContract]
        AuthenticationResult LoginByToken(Guid token);

        [OperationContract]
        void Logoff(Guid token);

        [OperationContract]
        IEnumerable<Account> GetAllAccounts();

        [OperationContract]
        void SaveAccount(Account account, string password);

        [OperationContract]
        void ChangePassword(string login, string oldPassword, string newPassword);

        [OperationContract]
        IEnumerable<Role> GetRoles();

        [OperationContract]
        Role GetRolesByID(Guid id);

        [OperationContract]
        void DeleteRole(Role role);

        [OperationContract]
        Role CreateRole(Role role);

        [OperationContract]
        void UpdateRole(Role role);

        [OperationContract]
        List<AccountShortInfo> GetAllAccountsShortInfo();

        [OperationContract]
        Account GedAccountByID(Guid id);

        [OperationContract]
        List<ShortEntity> GetRolesShorInfo();

        [OperationContract]
        Account CreateAccount(Account account, string password);

        [OperationContract]
        void DeleteAccount(Account account);

        [OperationContract]
        Account UpdateAccountAndPassword(Account acoount, string password);

        [OperationContract]
        Account UpdateAccount(Account account);

        [OperationContract]
        IEnumerable<ShortEntity> GetAuthorizeObjectsShortInfo();

        [OperationContract]
        ShortEntity GetShortRole(Guid id);

        [OperationContract]
        AccountShortInfo GetShortAccount(Guid id);
    }
}