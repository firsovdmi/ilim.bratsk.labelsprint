﻿using System;
using System.Linq;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using RefactorThis.GraphDiff;

namespace ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses
{
    public class AAARepository : IAAARepository
    {
        protected readonly IUnitOfWorkAAA UnitOfWork;

        public AAARepository(IUnitOfWorkAAA uow)
        {
            UnitOfWork = uow;
        }

        public virtual Account GetUserByLoginAndPassword(string login, string passwordHash)
        {
            return
                UnitOfWork.Get<Account>()
                    .FirstOrDefault(
                        p =>
                            p.Login == login && p.AccounAuthentication != null &&
                            p.AccounAuthentication.PasswordHash == passwordHash);
        }

        public IQueryable<Account> GetAllAccount()
        {
            return UnitOfWork.Get<Account>();
        }

        public void SaveAccount(Account account, string password)
        {
            if (
                UnitOfWork.Get<Account>()
                    .Any(p => p.Login == account.Login && p.AccounAuthentication.PasswordHash == password))
            {
                UnitOfWork.Update(account);
            }
            else
            {
                throw new Exception();
            }
        }

        public void ChangePassword(string login, string oldPasswordHash, string newPasswordHash)
        {
            var account =
                UnitOfWork.Get<Account>()
                    .Where(p => p.Login == login && p.AccounAuthentication.PasswordHash == oldPasswordHash)
                    .Select(p => p.AccounAuthentication)
                    .FirstOrDefault();
            if (account != null)
            {
                account.PasswordHash = newPasswordHash;
            }
            else
            {
                throw new Exception();
            }
            UnitOfWork.Update(account);
        }

        public Account CreateAcoount(Account account, string passwordHash)
        {
            var acc = new Account
            {
                ID = account.ID,
                Login = account.Login,
                AutoLogOffTimeMinutes = account.AutoLogOffTimeMinutes,
                Email = account.Email,
                AccountInRoles = account.AccountInRoles,
                IsChangePassword = account.IsChangePassword,
                IsWindowsAuthentification = account.IsWindowsAuthentification,
                AccounAuthentication = new AccounAuthentication {PasswordHash = passwordHash}
            };
            UnitOfWork.Create(acc);
            return acc;
        }

        public void MarkAsDeleteAccount(Account account)
        {
            var accountToDelete = UnitOfWork.Get<Account>().FirstOrDefault(p => p.ID == account.ID);
            if (account != null && accountToDelete != null)
            {
                accountToDelete.IsDeleted = true;
                UnitOfWork.Update(accountToDelete);
            }
        }

        public Account UpdateAccount(Account account)
        {
            UnitOfWork.Update(account, p => p.OwnedCollection(pp => pp.AccountInRoles));
            UnitOfWork.Save();
            return account;
        }

        public Account UpdateAccountAndPassword(Account account, string passwordHash)
        {
            var acc =
                UnitOfWork.Get<Account>()
                    .Where(p => p.ID == account.ID)
                    .Select(p => p.AccounAuthentication)
                    .FirstOrDefault();
            if (acc != null)
            {
                acc.PasswordHash = passwordHash;
                UnitOfWork.Update(acc);
            }
            else
            {
                acc = new AccounAuthentication {ID = account.ID, PasswordHash = passwordHash};
                UnitOfWork.Create(acc);
            }

            UnitOfWork.Update(account, p => p.OwnedCollection(pp => pp.AccountInRoles));
            UnitOfWork.Save();
            return account;
        }

        public IQueryable<Role> GetRoles()
        {
            return UnitOfWork.Get<Role>();
        }

        public void MarkAsDeleteRole(Role role)
        {
            var roleToDelete = UnitOfWork.Get<Role>().FirstOrDefault(p => p.ID == role.ID);
            if (roleToDelete != null)
            {
                roleToDelete.IsDeleted = true;
                UnitOfWork.Update(roleToDelete);
            }
        }

        public Role CreateRole(Role role)
        {
            return UnitOfWork.Create(role);
        }

        public Role UpdateRole(Role role)
        {
            return UnitOfWork.Update(role, p => p.OwnedCollection(pp => pp.AuthorizeObjectsForRoles));
        }

        public void DeleteRole(Role role)
        {
            UnitOfWork.Delete(role);
        }

        public IQueryable<AuthorizeObject> GetAuthorizeObjects()
        {
            return UnitOfWork.Get<AuthorizeObject>();
        }

        public void CreateOrUpdateByNameAutorizeObject(AuthorizeObject authorizeObject)
        {
            var objToUpdate = UnitOfWork.Get<AuthorizeObject>().FirstOrDefault(p => p.Name == authorizeObject.Name);
            if (objToUpdate != null)
            {
                objToUpdate.DisplayName = authorizeObject.DisplayName;
                objToUpdate.Description = authorizeObject.Description;
                UnitOfWork.Update(objToUpdate);
            }
            else
            {
                UnitOfWork.Create(authorizeObject);
            }
        }
    }
}