using System.Linq;
using ILIM.Bratsk.Infrastructure.AAA.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses
{
    public interface IAAARepository
    {
        Account GetUserByLoginAndPassword(string login, string passwordHash);
        IQueryable<Account> GetAllAccount();
        void SaveAccount(Account account, string password);
        void ChangePassword(string login, string oldPasswordHash, string newPasswordHash);
        Account CreateAcoount(Account account, string passwordHash);
        void MarkAsDeleteAccount(Account account);
        Account UpdateAccount(Account account);
        Account UpdateAccountAndPassword(Account account, string passwordHash);
        IQueryable<Role> GetRoles();
        void MarkAsDeleteRole(Role role);
        Role CreateRole(Role role);
        Role UpdateRole(Role role);
        void DeleteRole(Role role);
        IQueryable<AuthorizeObject> GetAuthorizeObjects();
        void CreateOrUpdateByNameAutorizeObject(AuthorizeObject authorizeObject);
    }
}