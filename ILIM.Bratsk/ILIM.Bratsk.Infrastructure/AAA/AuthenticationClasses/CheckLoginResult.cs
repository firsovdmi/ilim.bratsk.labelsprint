﻿namespace ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses
{
    public class CheckLoginResult
    {
        public ActiveAccount ActiveAccount { get; set; }
        public AutorizationResult AutorizationResult { get; set; }
    }
}