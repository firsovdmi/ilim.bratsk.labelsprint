﻿using System;
using System.Collections.Generic;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses
{
    public interface IAAAService
    {
        AuthenticationResult Login(string login, string password, bool iswWindowsAuth, string machineName);
        AuthenticationResult LoginByToken(Guid token);
        void Logoff(Guid token);
        AutorizationResult CheckLogin(Guid token, string autorizeObjectID);
        IEnumerable<Account> GetAllAccounts();
        void SaveAccount(Account account, string password);
        void ChangePassword(string login, string oldPassword, string newPassword);
        IEnumerable<Role> GetRoles();
        Role GetRolesByID(Guid id);
        void DeleteRole(Role role);
        Role CreateRole(Role role);
        void UpdateRole(Role role);
        List<AccountShortInfo> GetAllAccountsShortInfo();
        Account GedAccountByID(Guid id);
        List<ShortEntity> GetRolesShorInfo();
        Account CreateAccount(Account account, string password);
        void DeleteAccount(Account account);
        Account UpdateAccountAndPassword(Account acoount, string password);
        Account UpdateAccount(Account account);
        IEnumerable<ShortEntity> GetAuthorizeObjectsShortInfo();
        ShortEntity GetShortRole(Guid id);
        AccountShortInfo GetShortAccount(Guid id);
        ActiveAccount GetActiveAccountByToken(Guid token);
    }
}