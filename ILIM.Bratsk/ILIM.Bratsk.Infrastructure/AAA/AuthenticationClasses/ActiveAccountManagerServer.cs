using System.Collections.Generic;

namespace ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses
{
    public class ActiveAccountManagerServer : IActiveAccountManager
    {
        #region Implementation of IAAAManager

        public ActiveAccount ActiveAccount
        {
            get
            {
                if (WcfInstanceContext.Current == null)
                    return null;
                return WcfInstanceContext.Current.ContextAccount;
            }
            set { }
        }

        #endregion

        public List<string> WhiteListObjects { get; set; }
        public List<string> BlackListObjects { get; set; }
    }
}