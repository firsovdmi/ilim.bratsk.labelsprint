using System;
using System.Collections.Generic;
using ILIM.Bratsk.Infrastructure.AAA.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses
{
    public interface IAcountCache
    {
        /// <summary>
        ///     �������� ��������
        /// </summary>
        List<ActiveAccount> ActiveAccounts { get; set; }

        /// <summary>
        ///     ������ ��������, ��� ������� ����������� �����������
        /// </summary>
        List<AuthorizeObject> AuthorizeObjects { get; set; }

        /// <summary>
        ///     �������� ����������� �������� ��� ������������� ��������
        /// </summary>
        /// <param name="token">����� ��������������� ��������</param>
        /// <param name="autorizeObjectID">������������� ��������</param>
        /// <returns></returns>
        AutorizationResult CheckAccess(Guid token, string autorizeObjectID);

        /// <summary>
        ///     ��������� ���������� �� �������� �� ����
        /// </summary>
        /// <param name="token">�����</param>
        /// <returns>���������� �� ��������</returns>
        ActiveAccount GetActiveAccountByToken(Guid token);
    }
}