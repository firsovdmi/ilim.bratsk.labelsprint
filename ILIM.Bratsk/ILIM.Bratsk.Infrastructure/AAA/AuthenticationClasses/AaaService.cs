﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses
{
    public class AAAService : IAAAService
    {
        private readonly IAAARepository _aaaRepository;
        private readonly IAcountCache _acountCache;
        private readonly IUnitOfWorkAAA _unitOfWorkDO;

        public AAAService(IAAARepositoryFactory aaaRepositoryFactory, IAcountCache acountCache,
            IUnitOfWorkAAA unitOfWorkDO)
        {
            _aaaRepository = aaaRepositoryFactory.Create(unitOfWorkDO);
            _acountCache = acountCache;
            _unitOfWorkDO = unitOfWorkDO;
        }

        public AuthenticationResult Login(string login, string password, bool iswWindowsAuth, string machineName)
        {
            var result = new AuthenticationResult();
            Account account = null;
            if (!iswWindowsAuth)
            {
                //Поиск аккаунта по хэшу пароля
                var passwordHash =
                    Convert.ToBase64String(new SHA512Managed().ComputeHash(Encoding.ASCII.GetBytes(password)));
                account = _aaaRepository.GetAllAccount().Include(p => p.AccountInRoles)
                    .FirstOrDefault(p => p.Login == login && p.AccounAuthentication != null
                                         &&
                                         p.AccounAuthentication.PasswordHash == passwordHash
                    );
            }


            //если аккаунт не найден, то ошибка авторизации
            if (account == null)
            {
                result.IsSucces = false;
                result.Message = "Комбинация логина и пароля не найдена";
                return result;
            }
            //если аккаунт найден, то добавление его к кэш
            var activeAccount = _acountCache.ActiveAccounts.FirstOrDefault(p => p.Account.Login == login);
            if (activeAccount == null)
            {
                activeAccount = new ActiveAccount
                {
                    Account = account,
                    LastActionTime = DateTime.Now,
                    Token = Guid.NewGuid(),
                    MachineName = machineName
                };
                _acountCache.ActiveAccounts.Add(activeAccount);
            }
            //если аккаунт уже к кэше, то обновление времени последнего действия
            else
            {
                activeAccount.LastActionTime = DateTime.Now;
            }

            return FinishLogin(result, activeAccount, account);
            //result.AccountInfo = activeAccount;

            //if (result.AccountInfo.Account.Login == "Admin1")
            //{
            //    result.AllowedObjects = _acountCache.AuthorizeObjects.Select(p => p.Name).ToList();
            //    result.DeniedObjects=new List<string>();
            //}
            //else
            //{
            //    result.AllowedObjects =_acountCache.AuthorizeObjects.Where(p =>account.AccountInRoles.Select(pp => pp.RoleID).Intersect(p.AuthorizeObjectsForRoles.Select(ar => ar.RoleID)).Any()).Select(p => p.Name).ToList();
            //    result.DeniedObjects =_acountCache.AuthorizeObjects.Where(p =>!account.AccountInRoles.Select(pp => pp.RoleID).Intersect(p.AuthorizeObjectsForRoles.Select(ar => ar.RoleID)).Any()).Select(p => p.Name).ToList();
            //}
            //result.IsSucces = true;
            //result.Message = "Авторизация выполнена";
            //return result;
        }

        public AuthenticationResult LoginByToken(Guid token)
        {
            var result = new AuthenticationResult();


            var activeAccount = _acountCache.ActiveAccounts.FirstOrDefault(p => p.Token == token);
            if (activeAccount == null)
            {
                result.IsSucces = false;
                result.Message = "Токен не найден";
                return result;
            }
            return FinishLogin(result, activeAccount, activeAccount.Account);
            //    activeAccount.LastActionTime = DateTime.Now;
            //    result.AccountInfo = activeAccount;

            //result.AccountInfo = activeAccount;
            //result.AllowedObjects = _acountCache.AuthorizeObjects.Where(p => activeAccount.Account.AccountInRoles.Select(pp => pp.RoleID).Intersect(p.AuthorizeObjectsForRoles.Select(ar => ar.RoleID)).Any()).Select(p => p.Name).ToList();
            //result.IsSucces = true;
            //result.Message = "Авторизация выполнена";
            //return result;
        }

        public void Logoff(Guid token)
        {
            var account = _acountCache.ActiveAccounts.FirstOrDefault(p => p.Token == token);
            if (account == null)
            {
                return;
            }
            _acountCache.ActiveAccounts.Remove(account);
        }

        public AutorizationResult CheckLogin(Guid token, string autorizeObjectID)
        {
            return _acountCache.CheckAccess(token, autorizeObjectID);
        }

        public IEnumerable<Account> GetAllAccounts()
        {
            return _aaaRepository.GetAllAccount().Where(p => !p.IsDeleted).ToList();
        }

        public ActiveAccount GetActiveAccountByToken(Guid token)
        {
            return _acountCache.GetActiveAccountByToken(token);
        }

        public void SaveAccount(Account account, string password)
        {
            var passwordHash = Convert.ToBase64String(new SHA512Managed().ComputeHash(Encoding.ASCII.GetBytes(password)));
            _aaaRepository.SaveAccount(account, passwordHash);
            _unitOfWorkDO.Save();
        }

        public void ChangePassword(string login, string oldPassword, string newPassword)
        {
            var oldPasswordHash =
                Convert.ToBase64String(new SHA512Managed().ComputeHash(Encoding.ASCII.GetBytes(oldPassword)));
            var newPasswordHash =
                Convert.ToBase64String(new SHA512Managed().ComputeHash(Encoding.ASCII.GetBytes(newPassword)));
            _aaaRepository.ChangePassword(login, oldPasswordHash, newPasswordHash);
            _unitOfWorkDO.Save();
        }

        public IEnumerable<Role> GetRoles()
        {
            return _aaaRepository.GetRoles().Where(p => !p.IsDeleted).ToList();
        }

        public Role GetRolesByID(Guid id)
        {
            return _aaaRepository.GetRoles().Include(p => p.AuthorizeObjectsForRoles).FirstOrDefault(p => p.ID == id);
        }

        public void DeleteRole(Role role)
        {
            _aaaRepository.DeleteRole(role);
            _unitOfWorkDO.Save();
        }

        public Role CreateRole(Role role)
        {
            _aaaRepository.CreateRole(role);
            _unitOfWorkDO.Save();
            return role;
        }

        public void UpdateRole(Role role)
        {
            _aaaRepository.UpdateRole(role);
            _unitOfWorkDO.Save();
        }

        public List<AccountShortInfo> GetAllAccountsShortInfo()
        {
            return
                _aaaRepository.GetAllAccount()
                    .Where(p => !p.IsDeleted)
                    .Select(p => new AccountShortInfo { ID = p.ID, Login = p.Login })
                    .ToList();
        }

        public Account GedAccountByID(Guid id)
        {
            return _aaaRepository.GetAllAccount().Include(p => p.AccountInRoles).FirstOrDefault(p => p.ID == id);
        }

        public List<ShortEntity> GetRolesShorInfo()
        {
            return
                _aaaRepository.GetRoles()
                    .Where(p => !p.IsDeleted)
                    .Select(p => new ShortEntity { ID = p.ID, Name = p.Name })
                    .ToList();
        }

        public Account CreateAccount(Account account, string password)
        {
            _aaaRepository.CreateAcoount(account,
                string.IsNullOrEmpty(password)
                    ? String.Empty
                    : Convert.ToBase64String(new SHA512Managed().ComputeHash(Encoding.ASCII.GetBytes(password))));
            _unitOfWorkDO.Save();
            return account;
        }

        public void DeleteAccount(Account account)
        {
            _aaaRepository.MarkAsDeleteAccount(account);
            _unitOfWorkDO.Save();
        }

        public Account UpdateAccountAndPassword(Account acoount, string password)
        {
            _aaaRepository.UpdateAccountAndPassword(acoount,
                string.IsNullOrEmpty(password)
                    ? String.Empty
                    : Convert.ToBase64String(new SHA512Managed().ComputeHash(Encoding.ASCII.GetBytes(password))));

            _unitOfWorkDO.Save();
            return acoount;
        }

        public Account UpdateAccount(Account account)
        {
            _aaaRepository.UpdateAccount(account);
            return account;
        }

        public IEnumerable<ShortEntity> GetAuthorizeObjectsShortInfo()
        {
            return
                _aaaRepository.GetAuthorizeObjects()
                    .Select(p => new ShortEntity { ID = p.ID, Name = p.DisplayName + "-" + p.Description })
                    .ToList();
        }

        public ShortEntity GetShortRole(Guid id)
        {
            return
                _aaaRepository.GetRoles()
                    .Where(p => !p.IsDeleted && p.ID == id)
                    .Select(p => new ShortEntity { ID = p.ID, Name = p.Name })
                    .FirstOrDefault();
        }

        public AccountShortInfo GetShortAccount(Guid id)
        {
            return
                _aaaRepository.GetAllAccount()
                    .Where(p => !p.IsDeleted && p.ID == id)
                    .Select(p => new AccountShortInfo { ID = p.ID, Login = p.Login })
                    .FirstOrDefault();
        }

        private AuthenticationResult FinishLogin(AuthenticationResult result, ActiveAccount activeAccount,
            Account account)
        {
            result.AccountInfo = activeAccount;

            if (result.AccountInfo.Account.Login == "Admin1")
            {
                result.AllowedObjects = _acountCache.AuthorizeObjects.Select(p => p.Name).ToList();
                result.DeniedObjects = new List<string>();
            }
            else
            {
                result.AllowedObjects =
                    _acountCache.AuthorizeObjects.Where(
                        p =>
                            account.AccountInRoles.Select(pp => pp.RoleID)
                                .Intersect(p.AuthorizeObjectsForRoles.Select(ar => ar.RoleID))
                                .Any()).Select(p => p.Name).ToList();
                result.DeniedObjects =
                    _acountCache.AuthorizeObjects.Where(
                        p =>
                            !account.AccountInRoles.Select(pp => pp.RoleID)
                                .Intersect(p.AuthorizeObjectsForRoles.Select(ar => ar.RoleID))
                                .Any()).Select(p => p.Name).ToList();
            }
            result.IsSucces = true;
            result.Message = "Авторизация выполнена";
            return result;
        }
    }
}