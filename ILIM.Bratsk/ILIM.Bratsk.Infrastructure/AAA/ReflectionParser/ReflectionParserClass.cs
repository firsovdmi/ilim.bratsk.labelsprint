﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ILIM.Bratsk.Infrastructure.AAA.Model;

namespace ILIM.Bratsk.Infrastructure.AAA.ReflectionParser
{
    public class ReflectionParserClass
    {
        public List<AuthorizeObject> GetAllMembersFromAssembly(string assemblyName)
        {
            var result = new List<AuthorizeObject>();
            foreach (
                var type in
                    GetAssembly(assemblyName)
                        .GetTypes()
                        .Where(
                            type => type.GetCustomAttributes(typeof (AuthentificationClassAttribute), true).Length > 0))
            {
                var authentificationClassAttribute =
                    (AuthentificationClassAttribute)
                        type.GetCustomAttributes(typeof (AuthentificationClassAttribute), true)[0];
                var members = type.GetMembers().Where(p => p.IsDefined(typeof (AuthentificationMemberAttribute), false));
                foreach (var member in members)
                {
                    var authentificationMemberAttribute =
                        (AuthentificationMemberAttribute)
                            member.GetCustomAttributes(typeof (AuthentificationMemberAttribute), true)[0];
                    result.Add(new AuthorizeObject
                    {
                        ID = Guid.NewGuid(),
                        Name = type.Name + "." + member.Name,
                        DisplayName = authentificationClassAttribute.Name + "-" + authentificationMemberAttribute.Name,
                        Description =
                            authentificationClassAttribute.Description + "-" +
                            authentificationMemberAttribute.Description
                    });
                }
            }
            return result;
        }

        private static Assembly GetAssembly(string assemblyPath)
        {
            var assemblyName = Path.GetFileName(assemblyPath);
            if (assemblyName != null && assemblyName.IndexOf(",", StringComparison.Ordinal) != -1)
            {
                assemblyName = assemblyName.Split(',')[0].Trim();
            }
            if (assemblyName != null && assemblyName.IndexOf(".dll", StringComparison.Ordinal) == -1)
            {
                assemblyName += ".dll";
            }
            var a =
                AppDomain.CurrentDomain.GetAssemblies().LastOrDefault(p => p.ManifestModule.ScopeName == assemblyName);
            if (a != null) return a;

            var raw = GetTempBin(assemblyPath);
            return Assembly.Load(raw);
        }

        private static byte[] GetTempBin(string assemblyName)
        {
            if (!File.Exists(assemblyName))
            {
                return new byte[] {0};
            }
            using (var fs = new FileStream(assemblyName, FileMode.Open))
            {
                var raw = new byte[fs.Length];
                fs.Read(raw, 0, (int) fs.Length);
                return raw;
            }
        }
    }
}