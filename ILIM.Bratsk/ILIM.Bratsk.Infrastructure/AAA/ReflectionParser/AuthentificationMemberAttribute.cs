﻿using System;

namespace ILIM.Bratsk.Infrastructure.AAA.ReflectionParser
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
    public class AuthentificationMemberAttribute : Attribute
    {
        public AuthentificationMemberAttribute(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public AuthentificationMemberAttribute(string name) : this(name, "")
        {
        }

        public AuthentificationMemberAttribute() : this("", "")
        {
        }

        public string Name { get; set; }
        public string Description { get; set; }
    }
}