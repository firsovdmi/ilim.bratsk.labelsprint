﻿using System;

namespace ILIM.Bratsk.Infrastructure.AAA.ReflectionParser
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class AuthentificationClassAttribute : Attribute
    {
        public AuthentificationClassAttribute(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public AuthentificationClassAttribute(string name) : this(name, "")
        {
        }

        public AuthentificationClassAttribute() : this("", "")
        {
        }

        public string Name { get; set; }
        public string Description { get; set; }
    }
}