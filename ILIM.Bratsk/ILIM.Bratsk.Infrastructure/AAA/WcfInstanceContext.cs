using System.Collections;
using System.ServiceModel;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;

namespace ILIM.Bratsk.Infrastructure.AAA
{
    /// <summary>
    ///     This class incapsulates context information for a service instance
    /// </summary>
    internal sealed class WcfInstanceContext : IExtension<InstanceContext>
    {
        private readonly IDictionary _items;

        private WcfInstanceContext()
        {
            _items = new Hashtable();
        }

        /// <summary>
        ///     <see cref="IDictionary" /> stored in current instance context.
        /// </summary>
        public IDictionary Items
        {
            get { return _items; }
        }

        /// <summary>
        ///     Gets the current instance of <see cref="WcfInstanceContext" />
        /// </summary>
        public static WcfInstanceContext Current
        {
            get
            {
                if (OperationContext.Current == null)
                    return null;
                var context = OperationContext.Current.InstanceContext.Extensions.Find<WcfInstanceContext>();
                if (context == null)
                {
                    context = new WcfInstanceContext();
                    OperationContext.Current.InstanceContext.Extensions.Add(context);
                }
                return context;
            }
        }

        public ActiveAccount ContextAccount
        {
            get { return (ActiveAccount) Items["ActiveAccount"]; }
            set { Items["ActiveAccount"] = value; }
        }

        public MessageMem MessageMem
        {
            get { return (MessageMem) Items["MessageMem"]; }
            set { Items["MessageMem"] = value; }
        }

        /// <summary>
        ///     <see cref="IExtension{T}" /> Attach() method
        /// </summary>
        public void Attach(InstanceContext owner)
        {
        }

        /// <summary>
        ///     <see cref="IExtension{T}" /> Detach() method
        /// </summary>
        public void Detach(InstanceContext owner)
        {
        }
    }
}