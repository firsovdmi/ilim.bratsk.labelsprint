using Castle.DynamicProxy;
using Castle.Windsor;

namespace ILIM.Bratsk.Service
{
    public class RunOnNewResolvedObjectInterceptor : IInterceptor
    {
        private readonly IWindsorContainer _container;

        public RunOnNewResolvedObjectInterceptor(IWindsorContainer container)
        {
            _container = container;
        }

        #region Implementation of IInterceptor

        public void Intercept(IInvocation invocation)
        {
            var obj = _container.Resolve(invocation.TargetType);

            try
            {
                invocation.ReturnValue = invocation.Method.Invoke(obj, invocation.Arguments);
            }
            finally
            {
                _container.Release(obj);
            }
        }

        #endregion

        #region Implementation of IInterceptor

        #endregion
    }
}