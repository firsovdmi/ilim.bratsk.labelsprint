using System;
using Castle.MicroKernel.Registration;

namespace ILIM.Bratsk.Service
{
    public static class WindsorExtensions
    {
        public static ComponentRegistration<T> OverridesExistingRegistration<T>(
            this ComponentRegistration<T> componentRegistration) where T : class
        {
            return componentRegistration
                .Named(Guid.NewGuid().ToString())
                .IsDefault();
        }
    }
}