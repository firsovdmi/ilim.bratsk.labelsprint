using System.Configuration;

namespace ILIM.Bratsk.Service
{
    public sealed class WcfBaseSettingsSection : ConfigurationSection
    {
        [ConfigurationProperty("BaseAddresses")]
        [ConfigurationCollection(typeof (BaseAddressesElementCollection.AddElement), AddItemName = "Add")]
        public BaseAddressesElementCollection BaseAddresses
        {
            get { return ((BaseAddressesElementCollection) (this["BaseAddresses"])); }
        }

        public sealed class BaseAddressesElementCollection : ConfigurationElementCollection
        {
            public AddElement this[int i]
            {
                get { return ((AddElement) (BaseGet(i))); }
            }

            protected override ConfigurationElement CreateNewElement()
            {
                return new AddElement();
            }

            protected override object GetElementKey(ConfigurationElement element)
            {
                return ((AddElement) (element)).BaseAddress;
            }

            public sealed class AddElement : ConfigurationElement
            {
                [ConfigurationProperty("BaseAddress", IsRequired = true)]
                public string BaseAddress
                {
                    get { return ((string) (this["BaseAddress"])); }
                    set { this["BaseAddress"] = value; }
                }
            }
        }
    }
}