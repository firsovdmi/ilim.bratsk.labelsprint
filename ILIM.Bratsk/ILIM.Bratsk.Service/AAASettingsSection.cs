using System.Configuration;

namespace ILIM.Bratsk.Service
{
    public sealed class AAASettingsSection : ConfigurationSection
    {
        [ConfigurationProperty("ReinizializeAutorizeObject", IsRequired = true)]
        public bool ReinizializeAutorizeObject
        {
            get { return ((bool) (this["ReinizializeAutorizeObject"])); }
            set { this["ReinizializeAutorizeObject"] = value; }
        }
    }
}