﻿using AutoMapper;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service
{
    public class MappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return GetType().Name; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Line, ShortEntity>();
            Mapper.CreateMap<Manufacture, ShortEntity>();

            Mapper.CreateMap<Report, ShortEntity>();
        }
    }
}