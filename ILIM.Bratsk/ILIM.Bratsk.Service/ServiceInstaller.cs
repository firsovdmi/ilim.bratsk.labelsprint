using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ILIM.Bratsk.Data;
using ILIM.Bratsk.Data.Repository;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model.Alarms;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.CallBackService.ReceiveData;
using ILIM.Bratsk.Infrastructure.DataBaseDirect;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Log;
using ILIM.Bratsk.Infrastructure.Log.NlogImplementation;
using ILIM.Bratsk.Service.Services;

namespace ILIM.Bratsk.Service
{
    public abstract class ServiceInstaller : IWindsorInstaller
    {
        #region Implementation of IWindsorInstaller

        /// <summary>
        ///     Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public virtual void Install(IWindsorContainer container, IConfigurationStore store)
        {

            container.AddFacilityIfNotAdded<TypedFactoryFacility>();
            container.Register(Component.For<IModule>().ImplementedBy<ApplicationServiceModule>());

            //aaa service
            container.Register(Component.For<IActiveAccountManager>().ImplementedBy<ActiveAccountManagerServer>());
            container.Register(Component.For<IAcountCache>().ImplementedBy<AcountCache>());
            container.Register(Component.For<IAAARepositoryFactory>().AsFactory().LifestyleTransient());
            container.Register(
                Component.For<IUnitOfWorkAAA>()
                    .ImplementedBy<LoginContext>()
                    .LifestyleTransient()
                    .DependsOn(Dependency.OnValue("connectionString", "ILIM.Bratsk")));

            container.Register(Component.For<IAAARepository>().ImplementedBy<AAARepository>().LifestyleTransient());
            container.Register(Component.For<IAAAService>().ImplementedBy<AAAService>().LifestyleTransient());
            container.Register(Component.For<IKipaCash>().ImplementedBy<KipaCash>().LifestyleSingleton());
            container.Register(Component.For<IAlarmCash>().ImplementedBy<AlarmCash>().LifestyleSingleton());
            container.Register(Component.For<IPlcAcessService>().ImplementedBy<PlcAcessService>().LifestyleSingleton());

            //log service
            container.Register(
                Component.For<IUnitOfWorkLog>()
                    .ImplementedBy<LogContext>()
                    .LifestyleTransient()
                    .DependsOn(Dependency.OnValue("connectionString", "ILIM.Bratsk")));
            container.Register(Component.For<ILogRepository>().ImplementedBy<LogRepository>().LifestyleTransient());
            container.Register(Component.For<ILogService>().ImplementedBy<LogService>().LifestyleTransient());
            container.Register(Component.For<IReadLogService>().ImplementedBy<ReadLogService>().LifestyleTransient());
            container.Register(
                Component.For<ILogPublisher>()
                    .ImplementedBy<LogPublisher>()
                    .DependsOn(Dependency.OnValue("pingPeriod", 5000)));
            container.Register(
                Component.For<IReceivingTaskPublisher>()
                    .ImplementedBy<ReceivingTaskPublisher>()
                    .DependsOn(Dependency.OnValue("pingPeriod", 5000)));
            container.Install(new NlogInstaller());


            //Data
            container.Register(Component.For<IRepositoryFactory>().AsFactory().LifestyleTransient());
            container.Register(
                Types.FromAssemblyContaining<ReportRepository>()
                    .BasedOn(typeof (RepositoryEntityWithMarkedAsDelete<>))
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient());
            container.Register(
                Types.FromAssemblyContaining<ReportRepository>()
                    .BasedOn(typeof (RepositoryEntityWithMarkedAsDelete<>))
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient());
            container.Register(
                Types.FromAssemblyContaining<ReportRepository>()
                    .BasedOn(typeof (RepositoryEntity<>))
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient());
            container.Register(
                Component.For<IUnitOfWork, IUnitOfWorkDO>()
                    .ImplementedBy<FactoryContext>()
                    .LifestyleTransient()
                    .DependsOn(Dependency.OnValue("connectionString", "ILIM.Bratsk")));
        }

        #endregion
    }
}