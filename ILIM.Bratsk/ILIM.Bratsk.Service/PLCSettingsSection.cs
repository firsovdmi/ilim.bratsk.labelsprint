using System.Configuration;

namespace ILIM.Bratsk.Service
{
    public sealed class PLCSettingsSection : ConfigurationSection
    {
        [ConfigurationProperty("PLC")]
        public PLCElement PLC
        {
            get { return ((PLCElement) (this["PLC"])); }
        }

        public sealed class PLCElement : ConfigurationElement
        {
            [ConfigurationProperty("Rack", IsRequired = true)]
            public int Rack
            {
                get { return ((int) (this["Rack"])); }
                set { this["Rack"] = value; }
            }

            [ConfigurationProperty("Slot", IsRequired = true)]
            public int Slot
            {
                get { return ((int) (this["Slot"])); }
                set { this["Slot"] = value; }
            }

            [ConfigurationProperty("IP", IsRequired = true)]
            public string IP
            {
                get { return ((string) (this["IP"])); }
                set { this["IP"] = value; }
            }
        }
    }
}