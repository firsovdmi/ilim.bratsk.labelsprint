using System.Configuration;
using AutoMapper;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Infrastructure.AAA;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.AAA.ReflectionParser;
using ILIM.Bratsk.Infrastructure.Log;

namespace ILIM.Bratsk.Service
{
    public class ApplicationServiceModule : IModule
    {
        public ApplicationServiceModule(IAAARepositoryFactory aaaRepositoryFactory, IUnitOfWorkAAA uof,
            ILogService logService)
        {
            Mapper.Initialize(cfg => { cfg.AddProfile<MappingProfile>(); });
            var aaaSettingsSection = ((AAASettingsSection) (ConfigurationManager.GetSection("AAASettings")));
            if (aaaSettingsSection.ReinizializeAutorizeObject)
            {
                var repository = aaaRepositoryFactory.Create(uof);
                var parser = new ReflectionParserClass();
                foreach (var authorizeObject in parser.GetAllMembersFromAssembly(GetType().Assembly.FullName))
                {
                    repository.CreateOrUpdateByNameAutorizeObject(authorizeObject);
                }
                uof.Save();
            }
        }
    }
}