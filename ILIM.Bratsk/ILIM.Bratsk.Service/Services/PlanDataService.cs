﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using AutoMapper.QueryableExtensions;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service.Services
{
    public class PlanDataService : EntityWithMarkAsDeletedService<PlanData, IPlanDataRepository>, IPlanDataService
    {
        private readonly IFactDataRepository _factDataRepository;
        private readonly IPrinterRepository _printerRepository;
        IPlanDataRepository _planDataRepository;
        private ICommandRepository _commandRepository;

        public PlanDataService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
            _factDataRepository = repositoryFactory.Create<IFactDataRepository>(unitOfWork);
            _printerRepository = repositoryFactory.Create<IPrinterRepository>(unitOfWork);
            _commandRepository = repositoryFactory.Create<ICommandRepository>(unitOfWork);
            _planDataRepository = repositoryFactory.Create<IPlanDataRepository>(unitOfWork);
        }

        #region IPlanDataService Members
        public List<PlanData> GetUndeleted()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }

        public List<string> GetDestinctBatchCodes()
        {
            return Repository.Get().Select(p => p.BatchCode).Distinct().ToList();
        }

        public List<string> GetWaitingDestinctBatchCodes()
        {
            //return Repository.Get().GroupBy(p => p.BatchCode).Select(p => new { BatchCode = p.Key, MaxStatus = p.Max(pp => pp.Status) }).Where(p => p.MaxStatus == PlanStatus.Queue).Select(p=>p.BatchCode).ToList();
            return
                Repository.Get()
                          .GroupBy(p => p.BatchCode)
                          .Where(p => p.All(pp => pp.Status == PlanStatus.Queue)).Select(p => p.Key)
                          .ToList();
        }

        public Command GetCommand(string batchCode)
        {
            var ret = new Command();

            var item = Repository.Get().Where(p => p.BatchCode == batchCode).OrderByDescending(p => p.PackNum).FirstOrDefault();
            if (item == null) return ret;
            ret.BatchCode = batchCode;
            ret.CmdID = item.EventID;
            ret.PackLast = item.PackNum;
            item = Repository.Get().Where(p => p.BatchCode == batchCode).OrderBy(p => p.PackNum).FirstOrDefault();
            ret.PackFirst = item.PackNum;
            ret.Empt = item.Empt;

            return ret;
        }

        public CurrentInfo GetCurrentInfo()
        {
            try
            {
                var ret = new CurrentInfo();
                var planBatchcodes = _planDataRepository.Get().Select(p => p.BatchCode).Distinct().ToList();
                var activePrinter =
                    _printerRepository.Get().OrderByDescending(p => p.DateTimeUpdate).FirstOrDefault(p => !string.IsNullOrEmpty(p.StoredBatchCode) && p.StoredBatchCode != "0" && planBatchcodes.Contains(p.StoredBatchCode));
                if (activePrinter == null) return ret;
                var currentBatchCode = activePrinter.StoredBatchCode;
                var currentPlanItems = Repository.Get().Where(p => p.BatchCode == currentBatchCode);
                var currentFactItems = _factDataRepository.Get().Where(p => p.BatchCode == currentBatchCode);

                ret.CurrentBatchCode = currentBatchCode;
                var anyPlan = currentPlanItems.FirstOrDefault();
                ret.CurrentEmpt = anyPlan == null ? 0 : anyPlan.Empt;
                if (currentFactItems.Any())
                {
                    ret.CurrentMinTime = currentFactItems.Min(p => p.PackTime);
                    ret.CurrentMinPuckNum = currentFactItems.Min(p => p.PackNum);
                }
                else
                {
                    ret.CurrentMinTime = null;
                    var t = currentPlanItems.ToArray();
                    ret.CurrentMinPuckNum = currentPlanItems.Min(p => p.PackNum);
                }

                ret.CurrentMaxPuckNum = currentPlanItems.Any() ? currentPlanItems.Max(p => p.PackNum) : currentFactItems.Max(p => p.PackNum);

                ret.CurrentPuckNumCount = ret.CurrentMaxPuckNum - ret.CurrentMinPuckNum + 1;
                ret.CurrentPuckNumComplete = currentFactItems.Count(p => p.BatchCode == currentBatchCode);
                ret.CurrentPuckNumRemain = ret.CurrentPuckNumCount - ret.CurrentPuckNumComplete;
                ret.CurrentProgress = Convert.ToInt32(ret.CurrentPuckNumCount > 0 ? ret.CurrentPuckNumComplete * 100 / ret.CurrentPuckNumCount : 0);
                return ret;
            }
            catch (Exception e)
            {
            }
            return null;
        }

        public List<GroupedPlan> GetGroupedPlan()
        {
            if (!Repository.Get().Any()) return new List<GroupedPlan>();
            //var minPackNumber= _factDataRepository.Get().Where(p=>p)
            var ret = Repository.Get().GroupBy(p => p.BatchCode).Select(pp => new GroupedPlan
            {
                BatchCode = pp.Key,
                TimeCreate = pp.FirstOrDefault().PlanTime,
                PackFirst = pp.Min(p => p.PackNum),
                PackLast = pp.Max(p => p.PackNum),
                PackCount = pp.Max(p => p.PackNum) - pp.Min(p => p.PackNum) + 1,
                Empt = pp.FirstOrDefault().Empt,
                CelluloseName = "",
                MessageSource = pp.FirstOrDefault().CmdSource == 1 ? "система Проконт" : "Оператор"
            }).ToList();
            var batchCodes = ret.Select(p => p.BatchCode).ToList();
            var facts = _factDataRepository.Get().Where(p => batchCodes.Contains(p.BatchCode)).GroupBy(p => p.BatchCode);
            foreach (var fact in facts)
            {
                var plan = ret.First(p => p.BatchCode == fact.Key);
                plan.PackFirst = fact.Min(p => p.PackNum);
                plan.PackCount = plan.PackLast - plan.PackFirst + 1;
            }
            return ret;
        }

        #endregion

    }
}
