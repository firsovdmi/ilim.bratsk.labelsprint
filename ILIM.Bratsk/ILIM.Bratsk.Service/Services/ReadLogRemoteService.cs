using System.Collections.Generic;
using ILIM.Bratsk.Infrastructure.Log;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Service.Services
{
    public class ReadLogRemoteService : IReadLogRemoteService
    {
        private readonly IReadLogService _readLogService;

        public ReadLogRemoteService(IReadLogService readLogService)
        {
            _readLogService = readLogService;
        }

        #region Implementation of IReadLogService

        public List<LogItem> GetLog(LogRequestParameters logRequestParameters)
        {
            return _readLogService.GetLog(logRequestParameters);
        }

        #endregion
    }
}