﻿using System.Collections.Generic;
using System.Linq;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA;
using ILIM.Bratsk.Infrastructure.AAA.ReflectionParser;
using ILIM.Bratsk.Infrastructure.Log;

namespace ILIM.Bratsk.Service.Services
{
    [AuthentificationClass(Name = "PrinterService", Description = "Справочник принтеров")]
    public class PrinterService : DictionaryService<Printer, IPrinterRepository>, IPrinterService
    {
        private ILogService _logService;
        public PrinterService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory, ILogService logService)
            : base(unitOfWork, repositoryFactory)
        {
            _logService = logService;
        }

        public override List<Printer> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }

        public void ApplySetting(Printer printer)
        {
            _logService.Log(LogingLevel.Trace, string.Format("Применение настроек принтера {0}", printer.Number), "ProcessMessage");
            Repository.Update(printer);
            UnitOfWork.Save();
        }
    }
}