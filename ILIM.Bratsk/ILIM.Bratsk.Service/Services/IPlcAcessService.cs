﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;

namespace ILIM.Bratsk.Service.Services
{
    public interface IPlcAcessService
    {
        BatchDirectJobs ProcessJobs(BatchDirectJobs jobs,string ip, int rack, int slot, int pingTimeout = 500);
    }
}
