﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Xml.Linq;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Model.Alarms;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Log;

namespace ILIM.Bratsk.Service.Services
{
    public class MatthewsMperiaService : IMatthewsMperiaService
    {
        static string PrinterPingTimeoutName = "PrinterPingTimeout";
        private const string CelluloseNameParameter = "CelluloseName";
        private const string NoResponseTimeoutName = "NoResponseTimeout";

        private readonly IUnitOfWork _unitOfWork;
        private readonly ILineRepository _lineRepository;
        private readonly IPrinterRepository _printerRepository;
        private readonly IPlanDataRepository _planDataRepository;
        private readonly IManualPlanDataRepository _manualPlanDataRepository;
        private readonly IFactDataRepository _factDataRepository;
        private readonly IPlcAcessRemoteService _plcAcessRemoteService;
        private readonly IKipaCash _kipaCash;
        private readonly IAlarmCash _alarmCash;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private readonly IEventRepository _eventRepository;
        ILogService _logService;

        public MatthewsMperiaService(IUnitOfWork unitOfWork, IPlcAcessRemoteService plcAcessRemoteService, IKipaCash kipaCash,
            IRepositoryFactory repositoryFactory, IAlarmCash alarmCash,
            IApplicationSettingsService applicationSettingsService,
            IManualPlanDataRepository manualPlanDataRepository, ILogService logService)
        {
            _manualPlanDataRepository = manualPlanDataRepository;
            _logService = logService;
            _applicationSettingsService = applicationSettingsService;
            _plcAcessRemoteService = plcAcessRemoteService;
            _kipaCash = kipaCash;
            _unitOfWork = unitOfWork;
            _lineRepository = repositoryFactory.Create<ILineRepository>(unitOfWork);
            _planDataRepository = repositoryFactory.Create<IPlanDataRepository>(unitOfWork);
            _factDataRepository = repositoryFactory.Create<IFactDataRepository>(unitOfWork);
            _printerRepository = repositoryFactory.Create<IPrinterRepository>(unitOfWork);
            _eventRepository = repositoryFactory.Create<IEventRepository>(unitOfWork);
            _alarmCash = alarmCash;
        }

        int PingTimeout
        {
            get
            {
                var plcPingTimeout = _applicationSettingsService.GetByName(PrinterPingTimeoutName);
                return plcPingTimeout.IsNumeric() ? Convert.ToInt32(plcPingTimeout) : 500;
            }
        }
        int NoResponseTimeout
        {
            get
            {
                var noResponseTimeout = _applicationSettingsService.GetByName(NoResponseTimeoutName);
                return noResponseTimeout.IsNumeric() ? Convert.ToInt32(noResponseTimeout) : 1000;
            }
        }

        #region IMatthewsMperiaService Members

        public bool ActivateTriger(int lineNumber, string installationName = null)
        {
            var result = SendRequest(lineNumber, RequestTrigger(installationName));
            return result != null && result.GetAttributeXElement("getCounter", "ok") == "1";
        }

        private static XDocument RequestTrigger(string installationName)
        {
            var element = new XElement("trigger");
            element.AddInstallationName(installationName);
            return GenerateRequestDocument(element);
        }

        public bool MessageSelecting(int lineNumber, string messageName = null, string installationName = null)
        {
            return MessageSelecting(GetPrinterByNumber(lineNumber), messageName, installationName);
        }

        public bool MessageSelecting(Printer printer, string messageName = null, string installationName = null)
        {
            var currentLabel = GetCurrentLabel(printer);
            if (currentLabel.IndexOf("\\") > 0) currentLabel = currentLabel.Split('\\')[1];
            if (currentLabel == printer.RequestLabel) return true;
            var result = SendRequest(printer, RequestMessage(messageName, installationName));
            return result != null && result.GetAttributeXElement("select", "ok") == "1";
        }

        private static XDocument RequestMessage(string messageName, string installationName)
        {
            var element = new XElement("select");
            element.AddInstallationName(installationName);
            element.AddAttribute("msgName", messageName);
            return GenerateRequestDocument(element);
        }

        public List<PrinterErrorItem> GetPrinterStatus(int lineNumber, string installationName)
        {
            var ret = new List<PrinterErrorItem>();
            var result = SendRequest(lineNumber, RequestPrinterStatus(installationName));
            if (result == null) return ret;
            var instalations = result.GetXElements("installation");
            foreach (var instalation in instalations)
            {
                if (!instalation.HasElements) continue;
                var statuses = instalation.GetXElements("status").Elements().ToList();
                if (statuses.Any())
                {
                    ret.Add(new PrinterErrorItem
                    {
                        InstalationName = instalation.Attribute("name").Value,
                        ErrorList = statuses.Select(p => p.Name.LocalName).Select(p => new PrinterMessage { Text = p }).ToList()
                    });
                }
            }

            return ret;
        }

        public string GetPrinterState(int lineNumber, string installationName)
        {
            var result = SendRequest(lineNumber, RequestPrinterStatus(installationName));
            if (result == null || result.GetAttributeXElement("installation", "ok") == "0") return null;
            return result.ToString().Contains("STARTED") ? "STARTED" : result.ToString().Contains("PAUSED") ? "PAUSED" : "N/A";
        }

        private static XDocument RequestPrinterStatus(string installationName)
        {
            var element = new XElement("getInstStatus");
            element.AddInstallationName(installationName);
            return GenerateRequestDocument(element);
        }

        public int GetCounter(int printerNumber)
        {
            var result = SendRequest(printerNumber, RequestCounter());
            if (result == null || result.GetValueXElement("getPrintCount") == null) return -1;
            return Convert.ToInt32(result.GetValueXElement("getPrintCount"));
        }

        string GetCurrentLabel(Printer printer)
        {
            var result = SendRequest(printer.Number, RequestCurrentMessage());
            if (!result.GetXElements("installation").Any()) return null;
            var msg = result.GetAttributeXElement("installation", "msg");
            if (msg == null) return "unknown";
            return msg;
        }

        private static XDocument RequestCounter()
        {
            var element = new XElement("getPrintCount");
            return GenerateRequestDocument(element);
        }

        private static XDocument RequestCurrentMessage()
        {
            var element = new XElement("getSelected");
            return GenerateRequestDocument(element);
        }

        public bool SetVariable(int lineNumber, string variableName, string value)
        {
            var result = SendRequest(lineNumber, RequestSetVariable(variableName, value));
            return ParseSetVariableResult(result);
        }

        public bool SetVariable(Printer printer, string variableName, string value)
        {
            var result = SendRequest(printer, RequestSetVariable(variableName, value));
            return ParseSetVariableResult(result);
        }

        bool ParseSetVariableResult(XDocument result)
        {
            return result != null && result.GetAttributeXElement("setVariable", "ok") == "1";
        }

        private static XDocument RequestSetVariable(string variableName, string value)
        {
            var element = new XElement("setVariable");
            element.AddAttribute("name", variableName);
            element.Value = value;
            return GenerateRequestDocument(element);
        }

        public bool IsCounterChange(int printerNumber)
        {
            var currentCounterValue = GetCounter(printerNumber);
            var printer = _printerRepository.Get().FirstOrDefault(p => p.Number == printerNumber);
            if (printer == null) return false;
            var storedCounterValue = printer.StoredCount;
            if (currentCounterValue == storedCounterValue) return false;
            return true;
        }

        static readonly object PrinterChangeLock = new object();

        public void CheckPrinterState()
        {

            var isNeedSave = false;
            foreach (var printer in _printerRepository.Get().ToArray())
            {
                var cashData = _kipaCash.PlcCommonLinesData.FirstOrDefault(p => p.LineNumber == printer.Number);
                if (cashData == null) continue;
                if (printer.TurnOff)
                {
                    cashData.IsPrinterAlarm = true;
                    continue;
                }
                var line = _lineRepository.Get().FirstOrDefault(p => p.LineNumber == printer.Number);
                if (line == null) continue;
                var currentError = CheckErrorPosition(printer);
                if (cashData.PositionError != currentError)
                {
                    cashData.PositionError = currentError;
                    var message = string.Format("@PR{0}@ Ошибка датчика перед притером с '{1}' на '{2}'", printer.Number, cashData.PositionError.GetDescription(), currentError.GetDescription());
                    DebugMessage(message);
                }

                CheckStatus(printer, cashData);
                if (cashData.PrinterIsDown || cashData.IsNotRespond) continue;
                isNeedSave = CheckCount(printer, line) || isNeedSave;
            }
            if (isNeedSave) _unitOfWork.Save();
            _plcAcessRemoteService.RefreshCashedItems();
            lock (_kipaCash)
            {
                var kipasBeforePrinterPosition = _plcAcessRemoteService.GetCahsedItems().Where(p => p.IsBeforePrinterPosition);

                foreach (var kipa in kipasBeforePrinterPosition)
                {
                    var ret = TryAddNewKipa(kipa.CurrentLineNumber, kipa.InitLineNumber, kipa.Weight, DateTime.UtcNow);
                    if (ret != 0 && ret != 23 && ret != 13)
                    {
                        DebugMessage($"@PR{kipa.CurrentLineNumber}@ !!! Не удалось добавить кипу Code={ret} линия={kipa.CurrentLineNumber}");
                    }
                }
            }
        }

        // Проверяет ситуацию, когда сработал датчик перед принтером, но при этом на 8 позиции в контроллере нет кипы. Если принтер завершил печать, стираем этикетку
        PositionErrorEnum CheckErrorPosition(Printer printer)
        {
            try
            {
                var beforePrinterSensor = _kipaCash.PlcCommonLinesData.Single(p => p.LineNumber == printer.Number).PositionLed6.Value;
                var isPrinterInPrintingProcess = IsPrinterInPrintingProcess(printer);
                var isKipaBeforePrinterPosition = _plcAcessRemoteService.GetCahsedItems().Any(p => p.IsBeforePrinterPosition && p.CurrentLineNumber == printer.Number);
                var commonLine = _kipaCash.PlcCommonLinesData.Single(p => p.LineNumber == printer.Number);
                // если нет датчика перед принтером или кипа на  "8" позиции, значит все ОК
                if (!beforePrinterSensor || isKipaBeforePrinterPosition)
                {
                    // сбрасываем таймер ошибки сенсора, и выходим
                    commonLine.SensorErrorTime = DateTime.MaxValue;
                    return PositionErrorEnum.NoError;
                }

                // Если датчик есть, а кипы нет, засекаем время (если еще не засекли)
                if (commonLine.SensorErrorTime == DateTime.MaxValue)
                {
                    commonLine.SensorErrorTime = DateTime.Now;
                }

                // Если не отсчитали заданное время, выходим
                var sensorErrorTimeoutInMillisecondsSetting = _applicationSettingsService.GetByName("SensorErrorTimeoutInMilliseconds");
                int sensorErrorTimeoutInMillisecondsValue;
                if (!int.TryParse(sensorErrorTimeoutInMillisecondsSetting, out sensorErrorTimeoutInMillisecondsValue)) sensorErrorTimeoutInMillisecondsValue = 1000;
                if ((DateTime.Now - commonLine.SensorErrorTime).TotalMilliseconds < sensorErrorTimeoutInMillisecondsValue) return PositionErrorEnum.NoError;

                if (isPrinterInPrintingProcess)
                {
                    return PositionErrorEnum.PositeonError;
                }
                if (!MessageSelecting(printer, printer.EmptyLabel))
                {
                    var message = string.Format("@PR{1}@ Не удалось установить этикетку {0} на принтере {1}", printer.RequestLabel, printer.Number);
                    DebugMessage(message);
                }
                //SendRequest(printer, RequestMessage(printer.EmptyLabel, null));
                return PositionErrorEnum.PositeonAndPrinterError;

            }
            catch (Exception ex)
            {
                return PositionErrorEnum.Exception;
            }

        }

        bool CheckCount(Printer printer, Line line)
        {
            var currentCounterValue = GetCounter(printer.Number);
            if (printer.StoredCount == currentCounterValue) return false;
            DebugMessage(string.Format("@PR{0}@ Принтер {0} изменился счетчик с {1} на {2}", printer.Number, printer.StoredCount, currentCounterValue));
            printer.StoredCount = currentCounterValue;
            _printerRepository.Update(printer);
            PrintingComplete(printer, line);
            return true;
        }

        void CheckStatus(Printer printer, PlcCommonLineData cashData)
        {
            if (cashData == null) return;
            var printerIsDown = !PlcAcessService.PingHost(printer.IpAdress, PingTimeout);
            if (printerIsDown)
            {
                if (!cashData.IsNotRespond)
                {
                    DebugMessage(string.Format("@PR{0}@ Повышенное время пинга принтера {0} IP='{1}'", printer.Number, printer.IpAdress));
                    cashData.IsNotRespond = true;
                    cashData.NotRespondTime = DateTime.Now;
                    return;
                }
                if ((DateTime.Now - cashData.NotRespondTime).TotalMilliseconds < NoResponseTimeout)
                {
                    return;
                }
                if (cashData.PrinterIsDown) return;
                var message = string.Format("@PR{0}@ Нет связи с принтером {0} IP='{1}'", printer.Number, printer.IpAdress);
                cashData.PrinterIsDown = true;
                cashData.IsPrinterAlarm = true;
                AddErrorAlarm(message);
                AddErrorEvent(message);
                DebugMessage(message);
                return;
            }
            if (cashData.PrinterIsDown)
            {
                var message = string.Format("@PR{0}@ Принтер {0} доступен по сети", printer.Number);
                DebugMessage(message);
            }
            cashData.IsNotRespond = false;
            cashData.PrinterIsDown = false;
            cashData.IsPrinterAlarm = false;

            CheckPrinterErrorList(printer, cashData);

            var currentState = GetPrinterState(printer.Number, null);
            var isNowStarted = currentState == "STARTED";
            if (cashData.IsPrinterStarted && !isNowStarted)
            {
                var msg = string.Format("@PR{0}@ Ошибка статуса принтера {0} статус = {1}", printer.Number, currentState);
                AddErrorEvent(msg);
                DebugMessage(msg);
            }
            if (!cashData.IsPrinterStarted && isNowStarted)
            {
                var msg = string.Format("@PR{0}@ Принтер {0} запущен", printer.Number);
                DebugMessage(msg);
            }
            cashData.IsPrinterStarted = isNowStarted;
        }

        void CheckPrinterErrorList(Printer printer, PlcCommonLineData cashData)
        {
            var instalations = GetPrinterStatus(printer.Number, null);
            var aggregationError = "";
            foreach (var instalation in instalations)
            {
                foreach (var error in instalation.ErrorList)
                {
                    aggregationError += error.Text + "; ";
                    var message = string.Format("Ошибки принтера {0} {1} {2}", printer.Number, instalation.InstalationName, error.Text);
                    _alarmCash.AddItem(new AlarmItem
                    {
                        Id = Guid.NewGuid(),
                        AlarmLevel = error.IsCriticalError ? LogingLevel.Error : LogingLevel.Warn,
                        Text = message,
                        CreateTime = DateTime.UtcNow
                    });
                    if (error.IsCriticalError)
                    {
                        cashData.IsPrinterAlarm = true;
                        var commonLine = _kipaCash.PlcCommonLinesData.FirstOrDefault(p => p.LineNumber == printer.Number);
                        if (commonLine != null)
                        {
                            commonLine.PrinterErrorIn.Value = true;
                            _plcAcessRemoteService.ProcessJobs(
                                new BatchDirectJobs(new[] { commonLine.PrinterErrorIn.WriteJobs }), commonLine.Ip, commonLine.Rack, commonLine.Slot);
                        }
                    }
                }
            }
            if (cashData.ErrorListFromPrinter != aggregationError)
            {
                cashData.ErrorListFromPrinter = aggregationError;
                var message = string.Format("@PR{0}@ Изменение списка ошибок принтера {0} с '{1}' на '{2}'", printer.Number, cashData.ErrorListFromPrinter, aggregationError);
                DebugMessage(message);
            }
        }

        void PrintingComplete(Printer printer, Line line)
        {

            // Скидываем номер позиции в контроллере для этой линии
            ResetPositionInPlc(printer, line, "Штатный режим");

            // SendRequest(printer, RequestMessage(printer.EmptyLabel, null));
            // Если перепечатка, просто убираем запись из ручной очереди
            if (printer.IsManualCodes && printer.SpecModeKipsCount == 0)
            {
                var currentPrinting = _manualPlanDataRepository.Get().FirstOrDefault(p => p.Status == PlanStatus.SendedToPrint);
                if (currentPrinting == null) return;
                _manualPlanDataRepository.Delete(currentPrinting);
                _unitOfWork.Save();
            }
            //Если штатный режим, перекладываем из плана, в факт
            else
            {
                var currentPrinting =
                    _planDataRepository.Get()
                                       .FirstOrDefault(
                                           p => p.Status == PlanStatus.SendedToPrint && p.PrinterNum == printer.Number);
                if (currentPrinting == null) return;
                AddToFact(5,
                        currentPrinting.BatchCode,
                        currentPrinting.PackNum,
                        currentPrinting.Empt,
                        currentPrinting.PrinterNum ?? 0,
                        currentPrinting.WeightNum,
                        currentPrinting.WeightNet ?? 0,
                        currentPrinting.WeightBrut ?? 0,
                        currentPrinting.PlanTime,
                        currentPrinting.WeightTime,
                        currentPrinting.WeightTime,
                        currentPrinting.CmdSource,
                        currentPrinting.EventID,
                        "");
                _planDataRepository.Delete(currentPrinting);
                _unitOfWork.Save();
            }
        }

        private void ResetPositionInPlc(Printer printer, Line line, string msg = "")
        {
            var beforePrinterPositionItem = _kipaCash.PlcKipaItems.FirstOrDefault(p => p.ArrayIndex == 1 && (p.Position.Value == line.BeforePrinterPosition2) && p.CurrentLineNumber == printer.Number);
            if (beforePrinterPositionItem != null)
            {
                beforePrinterPositionItem.Position.Value = 0;
                beforePrinterPositionItem.InitLineNumber.Value = 0;
                var jobs = new BatchDirectJobs(new[] { beforePrinterPositionItem.Position.WriteJobs, beforePrinterPositionItem.InitLineNumber.WriteJobs });
                _plcAcessRemoteService.ProcessJobs(jobs, beforePrinterPositionItem.Ip, beforePrinterPositionItem.Rack, beforePrinterPositionItem.Slot);
                DebugMessage((string.Format("@PR{1}@ Сброс позиции {0} для линии {1} Адрес {2} Причина {3}", line.BeforePrinterPosition2, printer.Number, beforePrinterPositionItem.Position.WriteJobs.ItemAdress, msg)));
            }
        }


        private void AddToFact(int state, string batchCode, int packNum, PrinterModeEnum empt, int printerNum, int weightNum, float weightNet, float weightBrut, DateTime planTime, DateTime? weightTime, DateTime? dateTime, int? cmdSource, int eventId, string name)
        {
            var newItem = new FactData
            {
                State = state,
                BatchCode = batchCode,
                PackNum = packNum,
                Empt = empt,
                PrinterNum = printerNum,
                WeightNum = weightNum,
                WeightNet = weightNet,
                WeightBrut = weightBrut,
                PlanTime = planTime,
                WeightTime = weightTime,
                PackTime = weightTime,
                FactTime = DateTime.UtcNow,
                CelluloseName = _applicationSettingsService.GetByName(CelluloseNameParameter),
                CmdSource = cmdSource,
                EventID = eventId,
                Name = name
            };
            _factDataRepository.Create(newItem);
            _unitOfWork.Save();
        }


        public int TryAddNewKipa(int printerNumber, int lineNumber, float weight, DateTime date)
        {
            lock (_kipaCash)
            {
                var printer = _printerRepository.Get().FirstOrDefault(p => p.Number == printerNumber);
                if (printer == null) return 1;
                var cashData = _kipaCash.PlcCommonLinesData.FirstOrDefault(p => p.LineNumber == printer.Number);
                if (cashData == null) return 2;
                var line = _lineRepository.Get().FirstOrDefault(p => p.LineNumber == lineNumber);
                if (line == null) return 3;
                
                // Если принтер не пингуется, но еще не вышло время ожидания
                if (cashData.IsNotRespond && !cashData.PrinterIsDown)
                {
                    return 6;
                }

                // Если принтер отключен или не доступен, запоминаем кипу в таблицу факт
                if (printer.TurnOff || cashData.PrinterIsDown || !cashData.IsPrinterStarted)
                {
                    var msg = string.Format("TurnOff={0} PrinterIsDown={1} IsPrinterStarted={2}", printer.TurnOff, cashData.PrinterIsDown, cashData.IsPrinterStarted);
                    ResetPositionInPlc(printer, line, msg);
                    var weightNetto = weight;
                    var weightBrutto = (line.UseCustomWeight ? line.CustomWeight : weight) + line.AdditiveForBrutto;
                    AddToFact(0, "0", 0, PrinterModeEnum.NoPlaned, printerNumber, lineNumber, weightNetto, weightBrutto, DateTime.UtcNow, date, DateTime.UtcNow, 0, 0, msg);
                    return 4;
                }



                // Если линия в спец режиме, считаем оставшиеся кипы
                if (printer.IsManualCodes)
                {
                    if (printer.SpecModeKipsCount > 0)
                    {
                        if (TryAddNewKipaAutomaticPlan(weight, date, printer, line) == 0)
                        {
                            printer.SpecModeKipsCount--;
                            _printerRepository.Update(printer);
                            _unitOfWork.Save();
                            return 0;
                        }
                        return 5;
                    }
                    return TryAddNewKipaManualPlan(weight, date, printer, line);
                }
                // Если обычный режим, ищем следующую кипу из плана
                return TryAddNewKipaAutomaticPlan(weight, date, printer, line);
            }
        }

        PlcKipaItem GetKipaByArrayIndex(int line, int index)
        {
            return _kipaCash.PlcKipaItems.FirstOrDefault(p => p.CurrentLineNumber == line && p.ArrayIndex == index);
        }


        private int TryAddNewKipaManualPlan(float weight, DateTime date, Printer printer, Line line)
        {
            if (IsPrinterInPrintingProcess(printer)) return 13;

            // Ищем следующую кипу из ручного плана
            var nextKipa = GetNextKipaFromManualPlan();
            if (nextKipa == null)
            {
                // В плане все закончилось
                printer.IsManualCodes = false;
                _printerRepository.Update(printer);
                _unitOfWork.Save();
                return 11;
            }

            nextKipa.Status = PlanStatus.SendedToPrint;
            if (!SendToPrinterNewManualKipaData(printer, nextKipa, line)) return 12;

            _printerRepository.Update(printer);
            _manualPlanDataRepository.Update(nextKipa);
            _unitOfWork.Save();
            return 0;
        }

        private ManualPlanData GetNextKipaFromManualPlan()
        {
            return _manualPlanDataRepository.Get().Where(p => p.Status == PlanStatus.Queue).OrderByDescending(p => p.PlanTime).ThenBy(p => p.PackNum).FirstOrDefault();
        }

        int TryAddNewKipaAutomaticPlan(float weight, DateTime date, Printer printer, Line line)
        {
            // Проевряем, что на линии нет не напечатанных кип
            if (IsPrinterInPrintingProcess(printer)) return 23;

            // Ищем следующую кипу из плана
            var nextKipa = GetNextKipaFromPlan(printer.StoredBatchCode);
            printer.RequestLabel = printer.MainLabel;
            if (nextKipa == null)
            {
                // В плане все закончилось, генерируем виртуальный план
                PlanEmptyAlarm();
                nextKipa = new PlanData
                {
                    ID = Guid.NewGuid(),
                    PlanTime = DateTime.UtcNow,
                    EventID = 0,
                    BatchCode = "0",
                    PackNum = 0,
                    Empt = PrinterModeEnum.NoPlaned,
                    CmdSource = null
                };
                printer.RequestLabel = printer.EmptyLabel;
                var message = string.Format("@PR{0}@ Закончился план Принтер: {0}", printer.Number);
                DebugMessage(message);
            }

            nextKipa.Status = PlanStatus.SendedToPrint;
            nextKipa.PrinterNum = printer.Number;
            nextKipa.WeightNum = line.LineNumber;
            var weightNetto = weight;
            var weightBrutto = (line.UseCustomWeight ? line.CustomWeight : weight) + line.AdditiveForBrutto;
            nextKipa.WeightNet = weightNetto;
            nextKipa.WeightBrut = weightBrutto;
            nextKipa.WeightTime = date;
            printer.StoredBatchCode = nextKipa.BatchCode;
            if (!SendToPrinterNewAutoKipaData(printer, nextKipa, line))
            {
                DebugMessage(string.Format("@PR{0}@ !!! Не удачная отправка на принтер. Принтер {0}, кипа {1} (№{2})", printer.Number, nextKipa.BatchCode, nextKipa.PackNum));
                return 21;
            }
            DebugMessage(string.Format("@PR{0}@ Кипа отправлена на принтер. Принтер {0}, кипа {1} (№{2})", printer.Number, nextKipa.BatchCode, nextKipa.PackNum));
            _printerRepository.Update(printer);
            _planDataRepository.CreateOrUpdate(nextKipa);
            _unitOfWork.Save();
            return 0;
        }

        bool IsPrinterInPrintingProcess(Printer printer)
        {
            return
                _planDataRepository.Get()
                    .Any(p => p.Status == PlanStatus.SendedToPrint && p.PrinterNum == printer.Number);
        }

        private void PlanEmptyAlarm()
        {
            var message = @"Нет элементов в очереди ""План""";
            AddErrorAlarm(message);
            //Отправляем сообщение Проконту
            AddErrorEvent(message);
        }

        void AddErrorAlarm(string message)
        {
            _alarmCash.AddItem(new AlarmItem
            {
                Text = message,
                AlarmLevel = LogingLevel.Error,
                CreateTime = DateTime.UtcNow
            });
        }

        void AddErrorEvent(string message)
        {
            AddEvent(message, EventTypeEnum.Error, EventStatusEnum.Error);
        }

        void AddEvent(string message, EventTypeEnum eventType, EventStatusEnum eventStatus)
        {
            _eventRepository.Create(new Event
            {
                ID = Guid.NewGuid(),
                BatchCode = "0",
                CmdSource = EventSourceEnum.Server,
                Description = message,
                EventType = eventType,
                Empt = 0,
                EventBegin = DateTime.UtcNow,
                EventEnd = DateTime.UtcNow,
                Status = eventStatus
            });
            _unitOfWork.Save();
        }

        PlanData GetNextKipaFromPlan(string currentBatchCode)
        {
            // Если нет текущей кипы для печати, берем первую (с наименьшим номером) кипу из самого старого задания
            if (currentBatchCode == null)
            {
                var ret = _planDataRepository.Get().Where(p => p.Status == PlanStatus.Queue).OrderBy(p => p.PlanTime).ThenBy(p => p.PackNum).FirstOrDefault();
                return ret;
            }
            // Пробуем найти следующую кипу из текущего задания
            var nextKipa = _planDataRepository.Get().Where(p => currentBatchCode == p.BatchCode && p.Status == PlanStatus.Queue).OrderBy(p => p.PackNum).FirstOrDefault();
            // Текущее задание закончилось ??, тогда берем первую (с наименьшим номером) кипу из самого старого задания
            return nextKipa ?? GetNextKipaFromPlan(null);
        }

        private bool SendToPrinterNewAutoKipaData(Printer printer, PlanData kipa, Line line)
        {
            var weightNettoForLabel = (line.UseCustomWeight ? line.CustomWeight : kipa.WeightNet) + line.AdditiveForNetto;
            return SendToPrinterNewKipaData(printer, weightNettoForLabel, kipa.WeightBrut, kipa.Empt, line, kipa.WeightTime, kipa.PackNum);
        }

        private bool SendToPrinterNewManualKipaData(Printer printer, ManualPlanData kipa, Line line)
        {
            return SendToPrinterNewKipaData(printer, kipa.WeightNet, kipa.WeightBrut, kipa.Empt, line, kipa.WeightTime, kipa.PackNum);
        }

        private bool SendToPrinterNewKipaData(Printer printer, float? kipaWeightNet, float? kipaWeightBrut, PrinterModeEnum mode, Line line, DateTime? kipaWeightTime, int kipaNumber)
        {
            if (!MessageSelecting(printer, printer.RequestLabel))
            {
                var message = string.Format("@PR{1}@ Не удалось установить этикетку {0} на принтере {1}", printer.RequestLabel, printer.Number);
                DebugMessage(message);
                return false;
            }
            var weightNetto = kipaWeightNet == null ? "Ошибка" : mode == PrinterModeEnum.Standart ?
                $"{kipaWeightNet:n0}"
                : "";
            var weightBrutto = kipaWeightBrut == null ? "Ошибка" : mode == PrinterModeEnum.Standart ?
                $"{kipaWeightBrut:n1}"
                : "";
            var batchCode = mode == PrinterModeEnum.Standart ? printer.StoredBatchCode : "";
            var stringKipaNumber = kipaNumber.ToString();
            if (printer.Mode == PrinterModeEnum.NoCode)
            {
                batchCode = "";
                stringKipaNumber = "";
            }
            var notNullableDate = kipaWeightTime ?? DateTime.UtcNow;
            return SetVariable(printer, printer.DateParameterName, notNullableDate.ToString("dd.MM.yyyy hh:mm")) &&
                SetVariable(printer, printer.WeightBruttoParameterName, weightBrutto) && SetVariable(printer, printer.WeightNettoParameterName, weightNetto) &&
                SetVariable(printer, printer.BatchCodeParameterName, batchCode) && SetVariable(printer, printer.KipaNumberParameterName, stringKipaNumber);
        }

        #endregion

        static XDocument GenerateXDocument(XElement rootElement)
        {
            return new XDocument(new XDeclaration("1.0", "utf-8", "yes"), rootElement);
        }

        static XDocument GenerateRequestDocument(XElement children)
        {
            return GenerateXDocument(
                new XElement("request",
                    children));
        }

        XDocument SendRequest(int prinerNumber, XDocument request)
        {
            return SendRequest(GetPrinterByNumber(prinerNumber), request);
        }
        XDocument SendRequest(Printer printer, XDocument request)
        {
            try
            {
                if (printer == null) return null;
                using (var client = new TcpClient(printer.IpAdress, printer.Port))
                {
                    client.SendTimeout = 500;
                    using (var stream = client.GetStream())
                    {
                        using (var writer = new StreamWriter(stream, Encoding.UTF8))
                        {
                            writer.Write(ToStringWithDeclaration(request));
                            writer.Flush();
                            using (var reader = new StreamReader(stream, Encoding.UTF8))
                            {
                                var result = reader.ReadLine();
                                return result == null ? null : XDocument.Parse(result);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }

        static string ToStringWithDeclaration(XDocument doc)
        {
            if (doc == null)
            {
                throw new ArgumentNullException("doc");
            }
            var builder = new StringBuilder();
            builder.Append(doc.Declaration);
            builder.Append(Environment.NewLine);
            builder.Append(doc);
            return builder.ToString();
        }

        Printer GetPrinterByNumber(int number)
        {
            return _printerRepository.Get().FirstOrDefault(p => p.Number == number);
        }

        public void StartSpecMode(int printerNumber)
        {
            var printer = _printerRepository.Get().FirstOrDefault(p => p.Number == printerNumber);
            if (printer == null) return;
            printer.IsManualCodes = true;
            printer.SpecModeKipsCount = _kipaCash.PlcKipaItems.Count(p => p.CurrentLineNumber == printerNumber);
            _unitOfWork.Save();
        }

        public void StopSpecMode(int printerNumber)
        {
            var printer = _printerRepository.Get().FirstOrDefault(p => p.Number == printerNumber);
            if (printer == null) return;
            printer.IsManualCodes = false;
            _unitOfWork.Save();
        }

        public bool CheckIsExistKips(string batchCode, int first, int last)
        {
            var kipaNumbers = Enumerable.Range(first, last).ToArray();
            return _factDataRepository.Get().Any(p => p.BatchCode == batchCode && kipaNumbers.Contains(p.PackNum)) || _planDataRepository.Get().Any(p => p.BatchCode == batchCode && kipaNumbers.Contains(p.PackNum));
        }

        void DebugMessage(string text)
        {
            _logService.Log(LogingLevel.Trace, text, "Debug", GetType().ToString());
        }
    }

    static class Extentions
    {
        public static string GetValueXElement(this XDocument doc, string elementName)
        {
            var element = GetXElement(doc, elementName);
            return element == null ? null : element.Value;
        }

        public static string GetAttributeXElement(this XDocument doc, string elementName, string attributeName)
        {
            var element = GetXElement(doc, elementName);
            if (element == null) return null;
            var attribut = element.Attribute(attributeName);
            return attribut == null ? null : attribut.Value;
        }

        static XElement GetXElement(XDocument doc, string name)
        {
            XNamespace ns = "http://www.matthews.se/longboat/xml/response";
            return doc.Descendants(ns + name).FirstOrDefault();
        }

        public static List<XElement> GetXElements(this XElement element, string name)
        {
            XNamespace ns = "http://www.matthews.se/longboat/xml/response";
            return element.Descendants(ns + name).ToList();
        }

        public static List<XElement> GetXElements(this XDocument doc, string name)
        {
            XNamespace ns = "http://www.matthews.se/longboat/xml/response";
            return doc.Descendants(ns + name).ToList();
        }

        public static void AddInstallationName(this XElement element, string installationName)
        {
            element.AddAttribute("instName", installationName);
        }

        public static void AddAttribute(this XElement element, string attributeName, string attributeValue)
        {
            if (!string.IsNullOrEmpty(attributeName) && !string.IsNullOrEmpty(attributeValue))
            {
                element.Add(new XAttribute(attributeName, attributeValue));
            }
        }

    }
}
