using System.Collections.Generic;
using System.Linq;
using PAG.WBD.Milk.Domain.RemoteFacade;
using PAG.WBD.Milk.Domain.Repository;

namespace PAG.WBD.Milk.Service.Services
{
    public abstract class BaseService<T> : IBaseService<T>
    {
        protected IRepository<T> _repository;
        protected IUnitOfWork _unitOfWork;
        #region Implementation of IBaseService<T>

        public virtual List<T> Get()
        {
            return _repository.Get().ToList();
        }

        public virtual void Create(T entity)
        {
            _repository.Create(entity);
            _unitOfWork.Save();
        }

        public virtual void Update(T entity)
        {
            _repository.Update(entity);
            _unitOfWork.Save();
        }

        public virtual void Delete(T entity)
        {
            _repository.MarkAsDelete(entity);
            _unitOfWork.Save();
        }

        public virtual void Update(List<T> entity)
        {
            foreach (var farm in entity)
            {
                _repository.Update(farm);
            }
            _unitOfWork.Save();
        }

        public virtual void Delete(List<T> entity)
        {
            foreach (var farm in entity)
            {
                _repository.MarkAsDelete(farm);
            }
            _unitOfWork.Save();
        }

        public virtual void Create(List<T> entity)
        {
            foreach (var farm in entity)
            {
                _repository.Create(farm);
            }
            _unitOfWork.Save();
        }

        #endregion
    }
}