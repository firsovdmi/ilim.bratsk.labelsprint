﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;
using ILIM.Bratsk.Domain.RemoteFacade;

namespace ILIM.Bratsk.Service.Services
{
    class PlcAcessService : IPlcAcessService
    {
        readonly List<JobsProcessor> _jobProcessors = new List<JobsProcessor>();

        public BatchDirectJobs ProcessJobs(BatchDirectJobs jobs, string ip, int rack, int slot, int pingTimeout = 500)
        {
            try
            {
                if (!PingHost(ip, pingTimeout))
                {
                    jobs = SetErrors(jobs);
                    return jobs;
                }
                var jobProcessor = _jobProcessors.FirstOrDefault(p => p.Ip == ip && p.Rack == rack && p.Slot == slot);
                if (jobProcessor == null)
                {
                    jobProcessor = new JobsProcessor(ip, rack, slot);
                    _jobProcessors.Add(jobProcessor);
                }
                DateTime memTime = DateTime.Now;
                jobProcessor.ProcessJobs(jobs);
                jobs.ProcessingTime = (DateTime.Now - memTime).TotalMilliseconds;
            }
            catch (Exception e)
            {
                foreach (var job in jobs.Items)
                {
                    job.SetError(e.Message);
                }
            }
            return jobs;
        }

        BatchDirectJobs SetErrors(BatchDirectJobs jobs)
        {
            foreach (BaseDirectJob job in jobs)
            {
                job.SetAsError("No ping");
            }
            return jobs;
        }

        public static bool PingHost(string nameOrAddress, int timeout = 500)
        {
            try
            {
                for (var i = 0; i < 4; i++)
                    if (TryPing(nameOrAddress, timeout)) return true;
            }
            catch (PingException)
            {
                return false;
            }
            return false;
        }

        static bool TryPing(string nameOrAddress, int timeout)
        {
            Ping pinger = new Ping();
            PingReply reply = pinger.Send(nameOrAddress, timeout);
            return reply.Status == IPStatus.Success;
        }
    }
}