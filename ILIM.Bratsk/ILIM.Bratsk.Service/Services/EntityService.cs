using System;
using System.Collections.Generic;
using System.Linq;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service.Services
{
    public abstract class EntityService<T, TK> : IEntityService<T>
        where T : EntityBase
        where TK : class, IRepositoryEntity<T>
    {
        private TK _repository;
        protected readonly IRepositoryFactory RepositoryFactory;
        protected readonly IUnitOfWork UnitOfWork;

        public EntityService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
        {
            UnitOfWork = unitOfWork;
            RepositoryFactory = repositoryFactory;
        }

        protected TK Repository
        {
            get { return _repository ?? (_repository = RepositoryFactory.Create<TK>(UnitOfWork)); }
        }

        #region Implementation of IBaseService<T>

        public virtual List<T> Get()
        {
            return Repository.Get().ToList();
        }

        public virtual T Create(T entity)
        {
            Repository.Create(entity);
            UnitOfWork.Save();
            return entity;
        }

        public virtual T Update(T entity)
        {
            Repository.Update(entity);
            UnitOfWork.Save();
            return entity;
        }

        public virtual void Delete(T entity)
        {
            Repository.Delete(entity);
            UnitOfWork.Save();
        }

        public virtual List<T> UpdateList(List<T> entity)
        {
            foreach (var farm in entity)
            {
                Repository.Update(farm);
            }
            UnitOfWork.Save();
            return entity;
        }

        public virtual void DeleteList(List<T> entity)
        {
            foreach (var farm in entity)
            {
                Repository.Delete(farm);
            }
            UnitOfWork.Save();
        }

        public virtual List<T> CreateList(List<T> entity)
        {
            foreach (var farm in entity)
            {
                Repository.Create(farm);
            }
            UnitOfWork.Save();
            return entity;
        }

        private Guid id = Guid.NewGuid();

        /// <summary>
        ///     �������� �������� �� ��������������
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T GetByID(Guid id)
        {
            return Repository.Get().FirstOrDefault(p => p.ID == id);
        }

        #endregion
    }
}