﻿using System.Linq;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;

namespace ILIM.Bratsk.Service.Services
{
    public class ServiceDataService : EntityWithMarkAsDeletedService<ServiceData, IServiceDataRepository>,
                                      IServiceDataService
    {

        public ServiceDataService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }

        public int GetMaxBatchCode()
        {
            {
                var item = Repository.Get().FirstOrDefault();
                return item == null ? 0 : item.MaxBatchCode;
            }
        }
    }
}
