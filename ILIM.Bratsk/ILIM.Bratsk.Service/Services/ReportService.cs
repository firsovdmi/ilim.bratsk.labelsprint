using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service.Services
{
    //[ServiceMessageInspectorBehavior]
    public class ReportService : EntityWithMarkAsDeletedService<Report, IReportRepository>, IReportService
    {
        private readonly IActiveAccountManager _iaaaManager;

        public ReportService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory,
            IActiveAccountManager iaaaManager = null)
            : base(unitOfWork, repositoryFactory)
        {
            _iaaaManager = iaaaManager;
        }

        #region Implementation of IReportService

        /// <summary>
        ///     �������� ������ ������� � ����������� ����
        /// </summary>
        /// <returns>������ �������</returns>
        public List<ShortEntity> GetShortList()
        {
            return Repository.Get().Where(p => !p.IsDeleted).Project().To<ShortEntity>().ToList();
        }

        /// <summary>
        ///     �������� ����� � ����������� ����
        /// </summary>
        /// <param name="id">������������� ������</param>
        /// <returns>����� � ����������� ����</returns>
        public ShortEntity GetShort(Guid id)
        {
            return Repository.Get().Where(p => !p.IsDeleted && p.ID == id).Project().To<ShortEntity>().FirstOrDefault();
        }

        /// <summary>
        ///     �������� ������ ������� � ����������� ����, ��� ������������� ���� �������
        /// </summary>
        /// <returns>������ �������</returns>
        public List<ShortEntity> GetShortListByRequestType(RequestType type)
        {
            return
                Repository.Get().Where(p => !p.IsDeleted && p.RequestType == type).Project().To<ShortEntity>().ToList();
        }

        #region Implementation of IEntityService<Report>

        /// <summary>
        ///     �������� �������� �� ��������������
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Report GetByID(Guid id)
        {
            string login = null;
            if (_iaaaManager != null)
            {
                login = _iaaaManager.ActiveAccount.Account.Login;
            }

            var result =
                Repository.Get()
                    .Where(p => !p.IsDeleted && p.ID == id)
                    .Select(
                        p =>
                            new
                            {
                                p,
                                UserReportRequestParameter =
                                    p.UserReportRequestParameters.FirstOrDefault(
                                        pp => ((login == null && pp.UserID == null) || (login == pp.UserID)))
                            })
                    .FirstOrDefault();
            if (result != null)
            {
                var userReportRequestParameter = result.UserReportRequestParameter;
                if (userReportRequestParameter == null)
                    userReportRequestParameter = new UserReportRequestParameters
                    {
                        ID = Guid.NewGuid(),
                        ReportID = result.p.ID,
                        UserID = login
                    };
                result.p.UserReportRequestParameter = userReportRequestParameter;
                return result.p;
            }
            return null;
        }

        #endregion

        #region Overrides 

        /// <summary>
        ///     ������� ����� ��������
        /// </summary>
        /// <param name="entity" />
        public override Report Create(Report entity)
        {
            string login = null;
            if (_iaaaManager != null)
            {
                login = _iaaaManager.ActiveAccount.Account.Login;
            }
            var userReportRequestParameters = entity.UserReportRequestParameters.FirstOrDefault();
            if (userReportRequestParameters != null)
                userReportRequestParameters.UserID = login;
            base.Create(entity);


            var userRequestParameters = RepositoryFactory.Create<IUserReportRequestParametersRepository>(UnitOfWork);
            userRequestParameters.Create(entity.UserReportRequestParameter);
            UnitOfWork.Save();
            return entity;
        }


        /// <summary>
        ///     �������� ��������
        /// </summary>
        /// <param name="entity"></param>
        public override Report Update(Report entity)
        {
            base.Update(entity);
            var userRequestParameters = RepositoryFactory.Create<IUserReportRequestParametersRepository>(UnitOfWork);
            userRequestParameters.CreateOrUpdate(entity.UserReportRequestParameter);
            UnitOfWork.Save();
            return entity;
        }

        #endregion

        #endregion
    }
}