using System.Collections.Generic;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service.Services
{
    public abstract class EntityWithMarkAsDeletedService<T, TK> : EntityService<T, TK>
        where T : EntityBase
        where TK : class, IRepositoryWithMarkedAsDelete<T>
    {
        public EntityWithMarkAsDeletedService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }

        public override void Delete(T entity)
        {
            Repository.MarkAsDelete(entity);
            UnitOfWork.Save();
        }

        public override void DeleteList(List<T> entity)
        {
            foreach (var farm in entity)
            {
                Repository.MarkAsDelete(farm);
            }
            UnitOfWork.Save();
        }
    }
}