﻿using System;
using System.Linq;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;

namespace ILIM.Bratsk.Service.Services
{
    public class AccountDataService : EntityService<AccountData, IAccountDataRepository>, IAccountDataService
    {
        private readonly IActiveAccountManager _iaaaManager;

        public AccountDataService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory,
            IActiveAccountManager iaaaManager = null)
            : base(unitOfWork, repositoryFactory)
        {
            _iaaaManager = iaaaManager;
        }

        #region Implementation of IAccountDataService

        public AccountData GetCurrentUserData(string dataID)
        {
            Guid? accountId = null;
            if (_iaaaManager != null)
            {
                accountId = _iaaaManager.ActiveAccount.Account.ID;
            }
            return Repository.Get().FirstOrDefault(p => p.DataID == dataID && p.UserID == accountId);
        }

        public void UpdateOrCreateCurrentUserDataByDataID(AccountData accountData)
        {
            var accountId = new Guid();
            if (_iaaaManager != null)
            {
                accountId = _iaaaManager.ActiveAccount.Account.ID;
            }
            var data = Repository.Get().FirstOrDefault(p => p.DataID == accountData.DataID && p.UserID == accountId);
            if (data != null)
            {
                data.Data = accountData.Data;
                Repository.Update(data);
            }
            else
            {
                accountData.ID = Guid.NewGuid();
                accountData.UserID = accountId;
                Repository.Create(accountData);
            }
            UnitOfWork.Save();
        }

        #endregion
    }
}