﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service.Services
{
    public class EventService : EntityWithMarkAsDeletedService<Event, IEventRepository>, IEventService
    {

        public EventService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }
        public  List<Event> GetUndeleted()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }

        public List<Event> GetByFilter(EventFilter filter)
        {
            return Repository.Get()
                .Where(p => !p.IsDeleted)
                .Where(p => p.EventBegin >= filter.Period.StartTime)
                .Where(p => p.EventBegin <= filter.Period.EndTime)
                .Where(p => filter.IsAllStatuses || filter.Statuses.Contains((int)p.Status))
                .Where(p => filter.IsAllEventEventTypes || filter.EventTypes.Contains((int)p.EventType))
                .Where(p => filter.IsAllEventPrinterModeEnums || filter.PrinterModeEnums.Contains((int)p.Empt))
                .Where(p => filter.IsAllEventSources || filter.EventSources.Contains((int)p.CmdSource))
                .Take(filter.Count)
                .ToList();
        }
    }
}
