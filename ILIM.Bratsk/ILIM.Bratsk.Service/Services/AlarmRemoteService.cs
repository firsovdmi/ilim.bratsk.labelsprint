﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ILIM.Bratsk.Domain.Model.Alarms;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;

namespace ILIM.Bratsk.Service.Services
{
    public class AlarmRemoteService : IAlarmRemoteService
    {
        private readonly IAlarmCash _alarmCash;

        public AlarmRemoteService(IAlarmCash alarmCash)
        {
            _alarmCash = alarmCash;
        }

        public List<AlarmItem> GetAlarmItems()
        {
            return _alarmCash.Items;
        }


        public AlarmItem GetLastAlarm()
        {
            return _alarmCash == null || _alarmCash.Items == null
                       ? null
                       : _alarmCash.Items.OrderBy(p => p.CreateTime).LastOrDefault();
        }


        public void ResetAlarm(AlarmItem item)
        {
            var deleteItem = _alarmCash.Items.FirstOrDefault(p => p.Id == item.Id);
            if (deleteItem == null) return;
            _alarmCash.Items.Remove(deleteItem);
        }
    }
}
