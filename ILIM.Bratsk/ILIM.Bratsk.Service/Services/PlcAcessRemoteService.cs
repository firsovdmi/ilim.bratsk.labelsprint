﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Model.Alarms;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Log;

namespace ILIM.Bratsk.Service.Services
{
    public class PlcAcessRemoteService : IPlcAcessRemoteService
    {
        const string PlcPingTimeoutName = "PlcPingTimeout";
        private readonly IPlcAcessService _plcAcessService;
        private readonly IKipaCash _kipaCash;
        private readonly IEventService _eventService;
        private readonly IAlarmCash _alarmCash;
        private readonly ILogService _logService;
        private readonly IApplicationSettingsService _applicationSettingsService;

        public PlcAcessRemoteService(IKipaCash kipaCash, IPlcAcessService plcAcessService, IEventService eventService, IAlarmCash alarmCash, ILogService logService, IApplicationSettingsService applicationSettingsService)
        {
            _eventService = eventService;
            _alarmCash = alarmCash;
            _logService = logService;
            _applicationSettingsService = applicationSettingsService;
            lock (kipaCash)
            {
                _plcAcessService = plcAcessService;
                _kipaCash = kipaCash;
            }
        }

        int PingTimeout
        {
            get
            {
                var plcPingTimeout= _applicationSettingsService.GetByName(PlcPingTimeoutName);
                return plcPingTimeout.IsNumeric() ?  Convert.ToInt32(plcPingTimeout): 500;
            }
        }

        void AddErrorEvent(string message)
        {
            _eventService.Create(new Event
            {
                ID = Guid.NewGuid(),
                BatchCode = "0",
                CmdSource = EventSourceEnum.Server,
                Description = message,
                EventType = EventTypeEnum.Error,
                Empt = 0,
                EventBegin = DateTime.UtcNow,
                EventEnd = DateTime.UtcNow,
                Status = EventStatusEnum.Error
            });
        }

        public BatchDirectJobs ProcessJobs(BatchDirectJobs jobs, string ip, int rack, int slot)
        {
            lock (_kipaCash)
            {
                return _plcAcessService.ProcessJobs(jobs, ip, rack, slot, PingTimeout);
            }
        }

        #region IPlcAcessRemoteService Members


        public List<PlcKipaItemRemote> GetCahsedItems()
        {
            lock (_kipaCash)
            {
                return _kipaCash.PlcKipaItems.Select(p => new PlcKipaItemRemote(p)).ToList();
            }
        }

        public DebugPlcMonitorRemote GetCahsedDebugPlcMonitor()
        {
            lock (_kipaCash)
            {
                return new DebugPlcMonitorRemote( _kipaCash.DebugPlcMonitor);
            }
        }

        public void RefreshCashedItems()
        {
            lock (_kipaCash)
            {
                foreach (var groupByPlc in _kipaCash.PlcKipaItems.GroupBy(p => new { p.Ip, p.Rack, p.Slot }))
                {
                    var anyItem = groupByPlc.First();
                    var allReadJobs = new BatchDirectJobs(groupByPlc.SelectMany(p => p.AllReadJobs));
                    allReadJobs = _plcAcessService.ProcessJobs(allReadJobs, anyItem.Ip, anyItem.Rack, anyItem.Slot,PingTimeout);
                    foreach (var p in groupByPlc)
                    {
                        p.ReciveData(allReadJobs);
                    }
                }

                foreach (var groupByPlc in _kipaCash.PlcCommonLinesData.GroupBy(p => new { p.Ip, p.Rack, p.Slot }))
                {
                    var anyItem = groupByPlc.First();
                    var allReadJobs = new BatchDirectJobs(_kipaCash.PlcCommonLinesData.SelectMany(p => p.AllReadJobs));
                    allReadJobs = _plcAcessService.ProcessJobs(allReadJobs, anyItem.Ip, anyItem.Rack, anyItem.Slot, PingTimeout);
                    var plcReadError = allReadJobs.Items.FirstOrDefault(p => p.IsError);
                    if (plcReadError != null)
                    {
                        TryAddAlarmAndErrorEvent(string.Format("Ошибки при чтении из контроллера '{0}'", plcReadError.ErrorText));
                    }
                    foreach (var p in groupByPlc)
                    {
                        p.ReciveData(allReadJobs);
                        if (p.SensorError.Value)
                        {
                            TryAddAlarmAndErrorEvent(string.Format("Ошибка датчика на прессе линии '{0}'", p.LineNumber));
                        }
                    }

                }
                var overflowBuffer = _kipaCash.PlcCommonLinesData.FirstOrDefault(p => p.PrinterErrorOut.Value);
                if (overflowBuffer != null)
                {
                    TryAddAlarmAndErrorEvent(string.Format("Переполнение буфера контроллера (не распечатано на принтере) Линия {0}", overflowBuffer.LineNumber));
                }
                if (_kipaCash.ScanCount == int.MaxValue) _kipaCash.ScanCount = 0;

                var allDebugReadJobs = new BatchDirectJobs(_kipaCash.DebugPlcMonitor.AllReadJobs);
                var anyLine= _kipaCash.PlcCommonLinesData.FirstOrDefault();
                if (anyLine != null)
                    allDebugReadJobs = _plcAcessService.ProcessJobs(allDebugReadJobs, anyLine.Ip, anyLine.Rack, anyLine.Slot, PingTimeout);
                _kipaCash.DebugPlcMonitor.ReciveData(allDebugReadJobs);
                if ((DateTime.UtcNow - _kipaCash.DebugPlcMonitor.LastCountLog).TotalMinutes > 10)
                {
                    _logService.Log(LogingLevel.Debug, string.Format("Счетчик 8 позиции = {0}, {1}, {2}, {3}", _kipaCash.DebugPlcMonitor.DeleteCount1.Value, _kipaCash.DebugPlcMonitor.DeleteCount2.Value, _kipaCash.DebugPlcMonitor.DeleteCount3.Value, _kipaCash.DebugPlcMonitor.DeleteCount4.Value), "PlcCounter");
                    _kipaCash.DebugPlcMonitor.LastCountLog=DateTime.UtcNow;
                }
                _kipaCash.ScanCount++;
            }
        }

        void TryAddAlarmAndErrorEvent(string message)
        {
            if (_alarmCash.AddItem(message, LogingLevel.Error, "ProcessMessage"))
            {
                AddErrorEvent(message);
            }
        }

        public List<PlcCommonLineDataRemote> GetCahsedCommonData()
        {
            lock (_kipaCash)
            {
                return _kipaCash.PlcCommonLinesData.Select(p => new PlcCommonLineDataRemote(p)).ToList();
            }
        }

        public void RemoveKipa(PlcKipaItemRemote item)
        {
            _logService.Log(LogingLevel.Trace, string.Format("Удаление кипы Линия {0}, Позиция {1}", item.CurrentLineNumber, item.Position), "ProcessMessage");
            ProcessJobs(new BatchDirectJobs(new[] { item.ResetPositionWriteJob }), item.Ip, item.Rack, item.Slot);
        }

        public void ResetError(PlcCommonLineDataRemote line)
        {
            _logService.Log(LogingLevel.Trace, string.Format("Сброс ошибки (перезапуск линии) {0}", line.LineNumber), "ProcessMessage");
            var resetJob = (WriteObjectValue)line.ResetInErrorJob;
            resetJob.Value = true;
            ProcessJobs(new BatchDirectJobs(new[] { resetJob }), line.Ip, line.Rack, line.Slot);
        }

        public void SetNoTrackingKips(PlcCommonLineDataRemote line)
        {
            SetResetNoTrackingKips(line, true,
                $"Отключение отслеживания кип на линии {line.LineNumber}");
        }

        public void ResetNoTrackingKips(PlcCommonLineDataRemote line)
        {
            SetResetNoTrackingKips(line, false,
                $"Возобновление отслеживания кип на линии {line.LineNumber}");
        }

        public int GetScanCount()
        {
            return _kipaCash.ScanCount;
        }

        void SetResetNoTrackingKips(PlcCommonLineDataRemote line, bool value, string message)
        {
            _logService.Log(LogingLevel.Trace, message, "ProcessMessage");
            var job = (WriteObjectValue)line.NoTrackKips;
            job.Value = value;
            ProcessJobs(new BatchDirectJobs(new[] { job }), line.Ip, line.Rack, line.Slot);
        }

        public void SetWeightSetPointAndDelta(PlcCommonLineDataRemote line)
        {
            _logService.Log(LogingLevel.Trace, string.Format("Изменение уставки веса и дельты веса на весах {0}", line.LineNumber), "ProcessMessage");
            var setWeightSetpointJob = (WriteObjectValue)line.SetWeightSetpoint;
            setWeightSetpointJob.Value = line.WeightSetPoint;
            var setWeightDeltaJob = (WriteObjectValue)line.SetWeightDelta;
            setWeightDeltaJob.Value = line.WeightDelta;
            ProcessJobs(new BatchDirectJobs(new[] { setWeightSetpointJob, setWeightDeltaJob }), line.Ip, line.Rack, line.Slot);
        }

        #endregion
    }
}
