﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA;
using ILIM.Bratsk.Infrastructure.AAA.ReflectionParser;

namespace ILIM.Bratsk.Service.Services
{
    [AuthentificationClass(Name = "ApplicationSettingsService", Description = "Настройки системы")]
    public class ApplicationSettingsService : DictionaryService<ApplicationSettings, IApplicationSettingsRepository>, IApplicationSettingsService
    {
        public ApplicationSettingsService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {

        }

        public override List<ApplicationSettings> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }
        public string GetByName(string name)
        {
            var firstOrDefault = Repository.Get().FirstOrDefault(p => p.SystemName == name);
            return firstOrDefault != null ? firstOrDefault.Value : "";
        }
    }
}