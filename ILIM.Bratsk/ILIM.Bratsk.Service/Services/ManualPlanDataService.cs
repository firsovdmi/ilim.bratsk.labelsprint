﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service.Services
{
    public class ManualPlanDataService : EntityWithMarkAsDeletedService<ManualPlanData, IManualPlanDataRepository>, IManualPlanDataService
    {

        public ManualPlanDataService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
            
        }

        

        public void RemoveAll()
        {
            foreach (var item in Repository.Get())
            {
                Repository.Delete(item);
            }
            UnitOfWork.Save();
        }


        public void SetNewList(List<ManualPlanData> newItems)
        {
            foreach (var item in Repository.Get())
            {
                Repository.Delete(item);
            }
            foreach (var item in newItems)
            {
                Repository.Create(item);
            }
            UnitOfWork.Save();
        }
    }
}
