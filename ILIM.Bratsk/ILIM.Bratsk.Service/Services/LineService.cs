﻿using System.Collections.Generic;
using System.Linq;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA;
using ILIM.Bratsk.Infrastructure.AAA.ReflectionParser;

namespace ILIM.Bratsk.Service.Services
{
    [AuthentificationClass(Name = "LineService", Description = "Справочник линий")]
    public class LineService : DictionaryService<Line, ILineRepository>, ILineService
    {
        public LineService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {

        }

        public override List<Line> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }

        public void SetFixetWeightMode(int lineNumber)
        {
            SetResetFixetWeightMode(lineNumber, true);
        }

        public void ResetFixetWeightMode(int lineNumber)
        {
            SetResetFixetWeightMode(lineNumber, false);
        }

        void SetResetFixetWeightMode(int lineNumber, bool value)
        {
            var line = Repository.Get().FirstOrDefault(p => p.LineNumber == lineNumber);
            if (line == null) return;
            line.UseCustomWeight = value;
            Repository.Update(line);
            UnitOfWork.Save();
        }

    }
}