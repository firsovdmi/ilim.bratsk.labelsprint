﻿using System;
using System.Collections.Generic;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.AAA.ReflectionParser;
using ILIM.Bratsk.Infrastructure.Log;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service.Services
{
    [AuthentificationClass(Name = "AAAServiceRemoteFasade", Description = "Управление аккаунтами")]
    public class AAAServiceRemoteFasade : IAAAServiceRemoteFasade
    {
        private readonly IAAAService _aaaService;
        private readonly ILogService _logService;

        public AAAServiceRemoteFasade(IAAAService aaaService, ILogService logService)
        {
            _aaaService = aaaService;
            _logService = logService;
        }

        #region Implementation of IAAAService

        public AuthenticationResult Login(string login, string password, bool iswWindowsAuth, string machineName)
        {
            _logService.Log(LogingLevel.Trace, string.Format("Вход пользователя {0}, (машина {1})", login, machineName), "Login", machineName);
            return _aaaService.Login(login, password, iswWindowsAuth, machineName);
        }

        public AuthenticationResult LoginByToken(Guid token)
        {
            return _aaaService.LoginByToken(token);
        }

        public void Logoff(Guid token)
        {
            var accaunt=_aaaService.GetActiveAccountByToken(token);
            if (accaunt == null) return;
            _logService.Log(LogingLevel.Trace, string.Format("Выход пользователя {0}, (машина {1})", accaunt.Account.Login, accaunt.MachineName), "Login", accaunt.MachineName);
            _aaaService.Logoff(token);
        }

        public IEnumerable<Account> GetAllAccounts()
        {
            return _aaaService.GetAllAccounts();
        }

        public void SaveAccount(Account account, string password)
        {
            _aaaService.SaveAccount(account, password);
        }

        public void ChangePassword(string login, string oldPassword, string newPassword)
        {
            _aaaService.ChangePassword(login, oldPassword, newPassword);
        }

        public IEnumerable<Role> GetRoles()
        {
            return _aaaService.GetRoles();
        }

        public Role GetRolesByID(Guid id)
        {
            return _aaaService.GetRolesByID(id);
        }

        public void DeleteRole(Role role)
        {
            _aaaService.DeleteRole(role);
        }

        public Role CreateRole(Role role)
        {
            return _aaaService.CreateRole(role);
        }

        public void UpdateRole(Role role)
        {
            _aaaService.UpdateRole(role);
        }

        public List<AccountShortInfo> GetAllAccountsShortInfo()
        {
            return _aaaService.GetAllAccountsShortInfo();
        }

        public Account GedAccountByID(Guid id)
        {
            return _aaaService.GedAccountByID(id);
        }

        public List<ShortEntity> GetRolesShorInfo()
        {
            return _aaaService.GetRolesShorInfo();
        }

        public Account CreateAccount(Account account, string password)
        {
            return _aaaService.CreateAccount(account, password);
        }

        public void DeleteAccount(Account account)
        {
            _aaaService.DeleteAccount(account);
        }

        public Account UpdateAccountAndPassword(Account acoount, string password)
        {
            return _aaaService.UpdateAccountAndPassword(acoount, password);
        }

        public Account UpdateAccount(Account account)
        {
            return _aaaService.UpdateAccount(account);
        }

        public IEnumerable<ShortEntity> GetAuthorizeObjectsShortInfo()
        {
            return _aaaService.GetAuthorizeObjectsShortInfo();
        }

        public ShortEntity GetShortRole(Guid id)
        {
            return _aaaService.GetShortRole(id);
        }

        public AccountShortInfo GetShortAccount(Guid id)
        {
            return _aaaService.GetShortAccount(id);
        }

        #endregion
    }
}