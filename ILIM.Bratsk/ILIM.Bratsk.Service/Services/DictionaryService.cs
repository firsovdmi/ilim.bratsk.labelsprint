using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service.Services
{
    public abstract class DictionaryService<T, TK> : EntityWithMarkAsDeletedService<T, TK>, IDictionaryService<T>
        where T : Entity, IMarkableForDelete
        where TK : class, IRepositoryWithMarkedAsDelete<T>
    {
        protected DictionaryService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }

        public List<ShortEntity> GetShortList()
        {
            return Repository.Get().Where(p => !p.IsDeleted).Project().To<ShortEntity>().ToList();
        }

        public ShortEntity GetShort(Guid id)
        {
            return Repository.Get().Where(p => !p.IsDeleted && p.ID == id).Project().To<ShortEntity>().FirstOrDefault();
        }

        public override List<T> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }
    }
}