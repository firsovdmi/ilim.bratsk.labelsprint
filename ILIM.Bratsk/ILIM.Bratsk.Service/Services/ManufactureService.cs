using System.Collections.Generic;
using System.Linq;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA.ReflectionParser;

namespace ILIM.Bratsk.Service.Services
{
    [AuthentificationClass(Name = "ManufactureService", Description = "���������� �����������")]
    public class ManufactureService : DictionaryService<Manufacture, IManufactureRepository>, IManufactureService
    {
        public ManufactureService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory) : base(unitOfWork, repositoryFactory)
        {
        }
        public override List<Manufacture> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }

    }
}