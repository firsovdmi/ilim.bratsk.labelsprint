﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Service.Services
{
    public class FactDataService : EntityWithMarkAsDeletedService<FactData, IFactDataRepository>, IFactDataService
    {

        public FactDataService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }
        public List<FactData> GetUndeleted(FactFilter filter)
        {
            return Repository.Get()
                .Where(p => !p.IsDeleted)
                .Where(p => filter.BatchCode == null || p.BatchCode.Contains(filter.BatchCode))
                .Where(p => p.PackTime >= filter.Period.StartTime)
                .Where(p => p.PackTime <= filter.Period.EndTime)
                .Where(p => filter.IsAllLines || filter.LinesNumbers.Contains(p.WeightNum))
                .Where(p => filter.IsAllPrinters || filter.PrinterNumbers.Contains(p.PrinterNum))
                .OrderByDescending(p => p.FactTime)
                .Take(filter.FactCount)
                .ToList();
        }


        public int GetMaxCode()
        {
            try
            {
                var startYearDate = new DateTime(DateTime.UtcNow.Year, 1, 1);
                return Convert.ToInt32(Repository.Get().Where(p => p.PlanTime > startYearDate && SqlFunctions.IsNumeric(p.BatchCode.Trim()) == 1).Max(p => p.BatchCode));
            }
            catch (Exception)
            {
                return 0;
            }

        }
    }
}
