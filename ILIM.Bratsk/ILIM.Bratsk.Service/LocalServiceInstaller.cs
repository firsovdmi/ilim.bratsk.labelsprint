using System.Linq;
using System.Reflection;
using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace ILIM.Bratsk.Service
{
    public class LocalServiceInstaller : ServiceInstaller
    {
        #region Implementation of IWindsorInstaller

        /// <summary>
        ///     Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public override void Install(IWindsorContainer container, IConfigurationStore store)
        {
            base.Install(container, store);
            //------------------------------------------
            //�������������� ������������ ��� ������������ ��� wcf �������, � ���������� �������� ������� �� ������� �������
            container.Register(
                Component.For<IInterceptor, RunOnNewResolvedObjectInterceptor>()
                    .ImplementedBy<RunOnNewResolvedObjectInterceptor>());
            foreach (
                var type in
                    Assembly.GetAssembly(GetType())
                        .GetTypes()
                        .Where(p => p.Namespace == "ILIM.Bratsk.Service.Services" && !p.IsAbstract && p.IsPublic
                        ))
            {
                var type1 = type;
                var interfaces = type1.GetInterfaces().Where(p => p.Name == "I" + type1.Name);
                container.Register(
                    Component.For(interfaces).ImplementedBy(type).Interceptors<RunOnNewResolvedObjectInterceptor>());
                container.Register(
                    Component.For(type).ImplementedBy(type).OverridesExistingRegistration().LifestyleTransient());
            }
            container.Register(Component.For<IWindsorContainer>().Instance(container).OnlyNewServices());
            //------------------------------------------
        }

        #endregion
    }
}