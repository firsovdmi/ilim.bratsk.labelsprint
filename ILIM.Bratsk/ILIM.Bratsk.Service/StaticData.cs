﻿namespace ILIM.Bratsk.Service
{
    internal static class StaticData
    {
        public static int SetCipStartTimeWash = 1;
        public static int SetCipEndTimeAndParametersWash = 4;
        public static int SetCipStartTimeStep = 2;
        public static int SetCipEndTimeAndParametersStep = 3;
        public static int SetReciveStartTimeStep = 11;
        public static int SetReciveEndTimeAndParametersStep = 12;
    }
}