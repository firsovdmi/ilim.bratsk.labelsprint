﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using NUnit.Framework;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Log;
using ILIM.Bratsk.Infrastructure.Log.NlogImplementation;

namespace ILIM.Bratsk.IntegrationTest
{
    [TestFixture]
    public class LoggerTest
    {
        protected TransactionScope _transactionScope;

        [SetUp]
        public virtual void Setup()
        {
            _transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew);
        }

        [TearDown]
        public virtual void TearDown()
        {
            _transactionScope.Dispose();
        }

        [Test]
        public void Logger_OnLogCall_ShouldWriteInDatabase()
        {
            var logger = new LogService();
            var message = Guid.NewGuid().ToString();
            logger.Log(LogingLevel.Debug, message, "test type", "par1", "par2", "par3");
            var factory = new LogContextFactory();
            using (var uof = factory.Create())
            {
                Assert.IsTrue(uof.Get().Any(p => p.Message == message));
            }
        }

        [Test]
        public void Logger_OnLogCall_WriteActiveAccountID()
        {
            var aaaManager = new TestAaaManager();
            aaaManager.ActiveAccount.Account.ID = Guid.NewGuid();
            var logger = new LogService(aaaManager);
            var message = Guid.NewGuid().ToString();
            logger.Log(LogingLevel.Debug, message, "test type", "par1", "par2", "par3");
            var factory = new LogContextFactory();
            using (var uof = factory.Create())
            {
                var fromBase = uof.Get().FirstOrDefault(p => p.Message == message);
                Assert.AreEqual(aaaManager.ActiveAccount.Account.ID, fromBase.AccountID);
            }
        }

        [Test]
        public void Logger_OnLogCall_ShouldWriteSystemSource()
        {
            var logger = new LogService();
            var message = Guid.NewGuid().ToString();
            logger.Log(LogingLevel.Debug, message, "test type", "par1", "par2", "par3");
            var factory = new LogContextFactory();
            using (var uof = factory.Create())
            {
                Assert.IsTrue(uof.Get().FirstOrDefault(p => p.Message == message).Source == "Server");
            }
        }
    }

    public class TestAaaManager : IActiveAccountManager
    {
        public TestAaaManager()
        {
            ActiveAccount = new ActiveAccount
            {
                Account = new Account {ID = new Guid("F9D3DE96-186E-4DCC-8ECF-167F6411DE0B")}
            };
        }

        public List<string> WhiteListObjects { get; set; }
        public List<string> BlackListObjects { get; set; }

        #region Implementation of IAAAManager

        public Guid Token { get; private set; }

        public ActiveAccount ActiveAccount { get; set; }

        public AuthenticationResult LoginSession { get; private set; }

        #endregion
    }
}