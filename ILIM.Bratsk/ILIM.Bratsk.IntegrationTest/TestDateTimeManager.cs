﻿using System;
using ILIM.Bratsk.Infrastructure.DateTimeManager;

namespace ILIM.Bratsk.IntegrationTest
{
    public class TestDateTimeManager : IDateTimeManager
    {
        public DateTime DT { get; set; }

        public DateTime GetDateTimeNow()
        {
            return DT;
        }
    }
}