﻿using System.Data.Entity;
using System.Data.SqlClient;
using NUnit.Framework;
using ILIM.Bratsk.Data;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;

namespace ILIM.Bratsk.IntegrationTest
{
    [SetUpFixture]
    public class Global
    {
        [SetUp]
        public static void AssemblyInitialize()
        {
            Database.SetInitializer(new MyDatabaseInitializer());
            var dc = new FactoryContext(new TestDateTimeManager(), new ActiveAccountManagerServer(), "WbdTestDatabase");
            SqlConnection.ClearAllPools();
            dc.Database.Initialize(true);
        }
    }
}