﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;

namespace ILIM.Bratsk.LabelService
{
    public class Loginer
    {
        private IAAAServiceRemoteFasade _aaaServiceRemoteFasade;
        public Loginer(IAAAServiceRemoteFasade aaaServiceRemoteFasade)
        {
            _aaaServiceRemoteFasade = aaaServiceRemoteFasade;
        }

        public bool Login()
        {
            return _aaaServiceRemoteFasade.Login("System", "1", false, "").IsSucces;
        }
    }
}
