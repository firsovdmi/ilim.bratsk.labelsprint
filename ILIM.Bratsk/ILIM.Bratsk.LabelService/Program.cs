﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.LabelService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if (args[0] == "/i")
                {
                    ManagedInstallerClass.InstallHelper(new[] {Assembly.GetExecutingAssembly().Location});
                }
                else if (args[0] == "/u")
                {
                    ManagedInstallerClass.InstallHelper(new[] {"/u", Assembly.GetExecutingAssembly().Location});
                }
                else if (args[0] == "/d")
                {
                    var dbgService = new LabelService();
                    dbgService.OnDebug();
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new LabelService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
