﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Castle.Facilities.TypedFactory;
using Castle.Facilities.WcfIntegration;
using Castle.Facilities.WcfIntegration.Behaviors;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.AAA;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.Log;
using ILIM.Bratsk.Service;
using Component = Castle.MicroKernel.Registration.Component;


namespace ILIM.Bratsk.LabelService
{
    public partial class LabelService : ServiceBase
    {
        private WindsorContainer _container;
        private KipsScaner _kipsScanerHandler;

        public LabelService()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
            Thread.Sleep(Timeout.Infinite);
        }

        protected override void OnStart(string[] args)
        {
            var success = false;
            while (!success)
            {
                try
                {
                    _container = new WindsorContainer();
                    _container.AddFacility<TypedFactoryFacility>();
                    _container.AddFacility<WcfFacility>();

                    _container.Register(Component.For<MessageLifecycleBehavior>());
                    _container.Register(Component.For<LifestyleClientMessageAction>());
                    _container.Register(
                        Component.For<IActiveAccountManager>()
                                 .ImplementedBy<ActiveAcountManagerClient>()
                                 .OverridesExistingRegistration());

                    _container.Register(
                        Component.For(typeof (IChannelFactoryBuilder<>))
                                 .ImplementedBy(typeof (DefaultChannelFactoryBuilder<>))
                                 .IsDefault());

                    _container.Register(
                        Types.FromAssemblyContaining<IMatthewsMperiaService>()
                             .InSameNamespaceAs<IMatthewsMperiaService>()
                             .Configure(p =>
                                 {
                                     p.AsWcfClient(new DefaultClientModel()
                                         {
                                             Endpoint = WcfEndpoint
                                                       .BoundTo(new NetTcpBinding("netTcp") {PortSharingEnabled = true})
                                                       .At(
                                                           string.Format(
                                                               @"net.tcp://" + CommonSettings.HostAddress +
                                                               ":8082/service/{0}", p.Implementation.Name.Substring(1)))
                                         });
                                 }));

                    _container.Register(Component.For<IAAAServiceRemoteFasade>().AsWcfClient(new DefaultClientModel
                        {
                            Endpoint = WcfEndpoint
                                                                                                 .BoundTo(
                                                                                                     new NetTcpBinding(
                                                                                                         "netTcp")
                                                                                                         {
                                                                                                             PortSharingEnabled
                                                                                                                 = true
                                                                                                         })
                                                                                                 .At(
                                                                                                     string.Format(
                                                                                                         @"net.tcp://" +
                                                                                                         CommonSettings
                                                                                                             .HostAddress +
                                                                                                         ":8082/service/AAAServiceRemoteFasade"))
                        }));
                    _container.Register(Component.For<IReadLogRemoteService>().AsWcfClient(new DefaultClientModel
                        {
                            Endpoint = WcfEndpoint
                                                                                               .BoundTo(
                                                                                                   new NetTcpBinding(
                                                                                                       "netTcp")
                                                                                                       {
                                                                                                           PortSharingEnabled
                                                                                                               = true
                                                                                                       })
                                                                                               .At(
                                                                                                   string.Format(
                                                                                                       @"net.tcp://" +
                                                                                                       CommonSettings
                                                                                                           .HostAddress +
                                                                                                       ":8082/service/ReadLogRemoteService"))
                        }));

                    _container.Register(Component.For<Loginer>().LifestyleTransient());
                    var loginer = _container.Resolve<Loginer>();
                    loginer.Login();

                    _container.Register(
                        Component.For<KipsScaner>()
                                 .DependsOn(
                                     (Dependency.OnValue("scanIntervalInMilliseconds",
                                                         CommonSettings.ScanIntervalInMilliseconds))));
                    _kipsScanerHandler = _container.Resolve<KipsScaner>();
                    _kipsScanerHandler.StartScan();
                    success = true; 
                }
                catch
                {
                    
                }
            }
        }

        protected override void OnStop()
        {
        }
    }
}
