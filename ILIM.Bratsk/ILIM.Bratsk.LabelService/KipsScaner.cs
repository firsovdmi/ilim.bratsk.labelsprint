﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.LabelService
{
    class KipsScaner : BaseCyclicHandler
    {
        private readonly IMatthewsMperiaService _matthewsMperiaService;
        private readonly IPlcAcessRemoteService _plcAcessRemoteService;

        public KipsScaner(IMatthewsMperiaService matthewsMperiaService, IPlcAcessRemoteService plcAcessRemoteService)
        {
            _matthewsMperiaService = matthewsMperiaService;
            _plcAcessRemoteService = plcAcessRemoteService;
        }

        protected override void Handler()
        {
            _matthewsMperiaService.CheckPrinterState();
            //_plcAcessRemoteService.RefreshCashedItems();
            //var kipasBeforePrinterPosition =_plcAcessRemoteService.GetCahsedItems().Where(p => p.IsBeforePrinterPosition);
            //foreach (var kipa in kipasBeforePrinterPosition)
            //{
            //    _matthewsMperiaService.TryAddNewKipa(kipa.CurrentLineNumber, kipa.InitLineNumber, kipa.Weight, DateTime.Now);
            //}
            
        }

    }
}
