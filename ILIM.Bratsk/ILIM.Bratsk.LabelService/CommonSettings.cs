﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.LabelService
{
    public static class CommonSettings
    {
        public static string HostAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["HostAddress"];
            }
        }

        public static int ScanIntervalInMilliseconds
        {
            get
            {
                return Convert.ToInt32( ConfigurationManager.AppSettings["ScanIntervalInMilliseconds"])*1000;
            }
        }
    }
}
