﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [Serializable]
    [DataContract]
    public class PlanData : Entity
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PlanID { get; set; }
        [DataMember]
        public DateTime PlanTime { get; set; }
        [DataMember]
        public int EventID { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        [DataMember]
        public string BatchCode { get; set; }
        [DataMember]
        public int PackNum { get; set; }
        [DataMember]
        public PrinterModeEnum Empt { get; set; }
        [DataMember]
        public int? PrinterNum { get; set; }
        [DataMember]
        public int WeightNum { get; set; }
        [DataMember]
        public PlanStatus Status { get; set; }
        [DataMember]
        public float? WeightNet { get; set; }
        [DataMember]
        public float? WeightBrut { get; set; }
        [DataMember]
        public DateTime? WeightTime { get; set; }
        [DataMember]
        public int? CmdSource { get; set; }
    }
}
