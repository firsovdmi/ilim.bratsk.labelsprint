﻿using System;
using System.Runtime.Serialization;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [DataContract]
    public class UserReportRequestParameters : Entity
    {
        [DataMember] private Report _report;
        [DataMember] private Guid _reportID;
        [DataMember] private string _requestParameters;
        [DataMember] private string _userID;

        public string UserID
        {
            get { return _userID; }
            set
            {
                if (value == _userID) return;
                _userID = value;
                OnPropertyChanged("UserID");
            }
        }

        public string RequestParameters
        {
            get { return _requestParameters; }
            set
            {
                if (value == _requestParameters) return;
                _requestParameters = value;
                OnPropertyChanged("RequestParameters");
            }
        }

        public Guid ReportID
        {
            get { return _reportID; }
            set
            {
                if (value.Equals(_reportID)) return;
                _reportID = value;
                OnPropertyChanged("ReportID");
            }
        }

        public Report Report
        {
            get { return _report; }
            set
            {
                if (Equals(value, _report)) return;
                _report = value;
                OnPropertyChanged("Report");
            }
        }
    }
}