﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [Serializable]
    [DataContract]
    public class Printer : Entity
    {
        [DataMember] 
        public int Number { get; set; }
        [DataMember]
        public string IpAdress { get; set; }
        [DataMember]
        public int Port { get; set; }
        [DataMember]
        public int? StoredCount { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        [DataMember]
        public string StoredBatchCode { get; set; }
        [DataMember]
        public int SpecModeKipsCount { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(255)]
        [DataMember]
        public string MainLabel { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(255)]
        [DataMember]
        public string EmptyLabel { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(255)]
        [DataMember]
        public string WeightBruttoParameterName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(255)]
        [DataMember]
        public string WeightNettoParameterName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(255)]
        [DataMember]
        public string DateParameterName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(255)]
        [DataMember]
        public string BatchCodeParameterName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(255)]
        [DataMember]
        public string KipaNumberParameterName { get; set; }

        [DataMember] private PrinterModeEnum _mode;

        [DataMember]
        private bool _isManualCodes;
        public bool IsManualCodes
        {
            get { return _isManualCodes; }
            set { _isManualCodes = value; OnPropertyChanged("IsManualCodes"); }
        }

        [DataMember]
        public string  RequestLabel { get; set; }

        public PrinterModeEnum Mode
        {
            get { return _mode; }
            set { _mode = value; OnPropertyChanged("Mode"); }
        }

        [DataMember]
        public bool TurnOff { get; set; }
    }

}
