﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{

    [Serializable]
    [DataContract]
    public class ProcontPlanItem : Entity
    {
        /// <summary>
        /// Уникальный последовательно возрастающий идентификатор кипы (счетчик)
        /// </summary>
        [DataMember]
        public int ProcontId { get; set; }

        /// <summary>
        /// Номер партии
        /// </summary>
        [DataMember]
        public string BatchCode { get; set; }

        /// <summary>
        /// Номер кипы
        /// </summary>
        [DataMember]
        public int PackNum { get; set; }

        /// <summary>
        /// Время регистрации плана в Системе
        /// </summary>
        [DataMember]
        public DateTime PlanTime { get; set; }

        /// <summary>
        /// Номер линии
        /// </summary>
        [DataMember]
        public int LineNum { get; set; }

        /// <summary>
        /// Признак печати партии без номера(0-печатаем с номеров, 1-печатаем без номера)
        /// </summary>
        [DataMember]
        public int Empt { get; set; }

    }
}
