﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ILIM.Bratsk.Domain.Model
{
    [DataContract]
    public class ReportParameterPeriodSelect : INotifyPropertyChanged
    {
        [DataMember] private PeriodSets _selectedPeriod;
        [DataMember] private DateTime _userEndTime;
        [DataMember] private DateTime _userStartTime;

        private readonly ObservableCollection<PeriodSets> _periodSetses = new ObservableCollection<PeriodSets>
        {
            new PeriodSets {Id = "Day", Name = "День"},
            new PeriodSets {Id = "TenDays", Name = "10 дней"},
            new PeriodSets {Id = "Mount", Name = "Месяц"},
            new PeriodSets {Id = "ThreeMounts", Name = "3 месяца"},
            new PeriodSets {Id = "Year", Name = "Год"},
            new PeriodSets {Id = "UserDefinition", Name = "Пользовательский"}
        };

        public ReportParameterPeriodSelect()
        {
            UserEndTime = DateTime.Now;
            UserStartTime = DateTime.Now.AddDays(-1);
            SelectedPeriod = PeriodSetses.FirstOrDefault(p => p.Id == "Day");
        }

        [XmlIgnore]
        public DateTime StartTime
        {
            get
            {
                if (_selectedPeriod == null) return DateTime.Now;
                switch (_selectedPeriod.Id)
                {
                    case "Day":
                        return DateTime.Now.AddDays(-1);
                    case "TenDays":
                        return DateTime.Now.AddDays(-10);
                    case "Mount":
                        return DateTime.Now.AddMonths(-1);
                    case "ThreeMounts":
                        return DateTime.Now.AddMonths(-3);
                    case "Year":
                        return DateTime.Now.AddYears(-1);
                    case "UserDefinition":
                        return UserStartTime;
                }
                return DateTime.Now;
            }
        }

        [XmlIgnore]
        public DateTime EndTime
        {
            get
            {
                if (_selectedPeriod == null) return DateTime.Now;
                return _selectedPeriod.Id == "UserDefinition" ? UserEndTime : DateTime.Now;
            }
        }

        public DateTime UserStartTime
        {
            get { return _userStartTime; }
            set
            {
                if (value.Equals(_userStartTime)) return;
                _userStartTime = value;
                OnPropertyChanged("UserStartTime");
            }
        }

        public DateTime UserEndTime
        {
            get { return _userEndTime; }
            set
            {
                if (value.Equals(_userEndTime)) return;
                _userEndTime = value;
                OnPropertyChanged("UserEndTime");
            }
        }

        [XmlIgnore]
        public ObservableCollection<PeriodSets> PeriodSetses
        {
            get { return _periodSetses; }
        }

        public PeriodSets SelectedPeriod
        {
            get
            {
                if (_selectedPeriod == null) return null;
                return PeriodSetses.FirstOrDefault(p => p.Id == _selectedPeriod.Id);
            }
            set
            {
                if (value.Equals(_selectedPeriod)) return;
                _selectedPeriod = value;
                OnPropertyChanged("SelectedPeriod");
                OnPropertyChanged("IsUserDefinition");
            }
        }

        [XmlIgnore]
        public bool IsUserDefinition
        {
            get
            {
                if (_selectedPeriod == null) return false;
                return _selectedPeriod.Id == "UserDefinition";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    [DataContract]
    public class PeriodSets
    {
        [DataMember] private string _id = "";
        [DataMember] private string _name = "";

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}