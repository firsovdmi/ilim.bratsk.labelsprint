﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Domain.Model
{
    [DataContract]
    public enum RequestType
    {
        [Description("Мойка")] [EnumMember] Cip,
        [Description("Приемка молока")] [EnumMember] Receive,
        [Description("Акт по приемке")] [EnumMember] Act
    }
}