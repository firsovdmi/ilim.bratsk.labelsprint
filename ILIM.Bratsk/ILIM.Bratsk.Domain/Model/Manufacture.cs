﻿using System.Runtime.Serialization;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [DataContract]
    public class Manufacture : Entity
    {
        [DataMember] private string _addres;
        [DataMember] private string _contract;

        public string Addres
        {
            get { return _addres; }
            set
            {
                if (value != null && value.Equals(_addres)) return;
                _addres = value;
                OnPropertyChanged("Addres");
            }
        }

        public string Contract
        {
            get { return _contract; }
            set
            {
                if (value != null && value.Equals(_contract)) return;
                _contract = value;
                OnPropertyChanged("Contract");
            }
        }
    }
}