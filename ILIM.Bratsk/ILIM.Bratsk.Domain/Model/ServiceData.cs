﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [Serializable]
    [DataContract]
    public class ServiceData : Entity
    {
        [DataMember]
        public int MaxBatchCode { get; set; }
        [DataMember]
        public int MaxBatchCodeYear { get; set; }
    }
}
