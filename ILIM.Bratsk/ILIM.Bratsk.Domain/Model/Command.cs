﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [Serializable]
    [DataContract]
    public class Command : Entity
    {
        [DataMember] 
        public int CmdID { get; set; }
        [DataMember]
        public DateTime CmdTime { get; set; }
        [DataMember]
        public int CmdSource { get; set; }
        [DataMember]
        public int CmdType { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        [DataMember]
        public string BatchCode { get; set; }
        [DataMember]
        public int PackFirst { get; set; }
        [DataMember]
        public int PackLast { get; set; }
        [DataMember]
        public PrinterModeEnum Empt { get; set; }

    }
}
