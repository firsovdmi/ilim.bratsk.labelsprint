﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ILIM.Bratsk.Domain.Model
{
    [DataContract]
    public enum PlanStatus
    {
        
        [Description("Печать выполнена")]
        [EnumMember]
        Queue,
        [Description("Отправлена на печать")]
        [EnumMember]
        SendedToPrint
    }
}
