﻿using System;
using System.Data.SqlTypes;

namespace PAG.WBD.Milk.Domain.Model
{
    public interface IEntity
    {
        Guid ID { get; set; }
        string Name { get; set; }
        bool IsSystem { get; set; }
    }

    public class Entity : IEntity
    {
        public bool IsDeleted { get; set; }

        public Guid ID { get; set; }

        public string Name { get; set; }


        public Guid? UserCreated { get; set; }

        public Guid? UserUpdated { get; set; }

        public bool IsSystem { get; set; }

        private DateTime _dateTimeCreate;
        private DateTime _dateTimeUpdate;

        /// <summary>
        /// Дата и время создания
        /// </summary>
        public DateTime DateTimeCreate
        {
            get
            {
                if (_dateTimeCreate < SqlDateTime.MinValue.Value)
                {
                    _dateTimeCreate = SqlDateTime.MinValue.Value;
                }
                return _dateTimeCreate;
            }
            set { _dateTimeCreate = value; }
        }

        /// <summary>
        /// Дата и время обновления
        /// </summary>
        public DateTime DateTimeUpdate
        {
            get
            {

                if (_dateTimeUpdate < SqlDateTime.MinValue.Value)
                {
                    _dateTimeUpdate = DateTime.Now;
                }
                return _dateTimeUpdate;
            }
            set { _dateTimeUpdate = value; }
        }

        public byte[] TimeStamp { get; set; }

        public string RowVersion
        {
            get
            {
                if (this.TimeStamp != null)
                {
                    return Convert.ToBase64String(this.TimeStamp);
                }

                return string.Empty;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this.TimeStamp = null;
                }
                else
                {
                    this.TimeStamp = Convert.FromBase64String(value);
                }
            }
        }
    }
}