﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [Serializable]
    [DataContract]
    public class FactData : Entity, INotifyPropertyChanged
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FactID { get; set; }
        [DataMember]
        public int State { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        [DataMember]
        public string BatchCode { get; set; }
        [DataMember]
        public int PackNum { get; set; }
        [DataMember]
        public PrinterModeEnum Empt { get; set; }
        [DataMember]
        public int PrinterNum { get; set; }
        [DataMember]
        public int WeightNum { get; set; }
        [DataMember]
        public float WeightNet { get; set; }
        [DataMember]
        public float WeightBrut { get; set; }
        [DataMember]
        public DateTime PlanTime { get; set; }
        [DataMember]
        public DateTime? WeightTime { get; set; }
        [DataMember]
        public DateTime? PackTime { get; set; }
        [DataMember]
        public DateTime FactTime { get; set; }
        [DataMember]
        public int EventID { get; set; }
        [DataMember]
        public string CelluloseName { get; set; }
        [DataMember]
        public int ReprintingCount { get; set; }
        [DataMember]
        public int? CmdSource { get; set; }
        [NotMapped]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        [NotMapped]
        private bool _isSelected;
        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
