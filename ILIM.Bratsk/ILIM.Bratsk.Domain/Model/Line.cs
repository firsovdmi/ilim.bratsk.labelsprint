﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [Serializable]
    [DataContract]
    public class Line : Entity
    {
        [DataMember]
        public int LineNumber { get; set; }
        [DataMember]
        public string PlcIpAdress { get; set; }
        [DataMember]
        public int PlcSlot { get; set; }
        [DataMember]
        public int PlcRack { get; set; }
        [DataMember]
        public int PlcDbn { get; set; }
        [DataMember]
        public int PlcStartByte { get; set; }
        [DataMember]
        public int BeforePrinterPosition1 { get; set; }
        [DataMember]
        public int BeforePrinterPosition2 { get; set; }
        [DataMember]
        public bool HasPrinter { get; set; }
        [DataMember]
        public int PlcOffsetCurrentWeight { get; set; }
        [DataMember]
        public int PlcWeightSetPoint { get; set; }
        [DataMember]
        public int PrinterErrorIn { get; set; }
        [DataMember]
        public int PrinterErrorOut { get; set; }
        [DataMember]
        public int PrinterResetErrorIn { get; set; }
        [DataMember]
        public int PlcCrossroadLeft { get; set; }
        [DataMember]
        public int PlcCrossroadRight { get; set; }
        [DataMember]
        public int NoTrackKips { get; set; }
        [DataMember]
        private float _additiveForNetto;
        public float AdditiveForNetto { get { return _additiveForNetto; } set { _additiveForNetto = value; OnPropertyChanged("AdditiveForNetto"); } }

        [DataMember]
        private float _additiveForBrutto;
        public float AdditiveForBrutto
        {
            get { return _additiveForBrutto; }
            set
            {
                _additiveForBrutto = value;
                OnPropertyChanged("AdditiveForBrutto");
            }
        }
        [DataMember]
        public int PlcWeightDelta { get; set; }
        [DataMember]
        public int PlcWeightFlag { get; set; }

        [DataMember] private float _customWeight;
        public float CustomWeight { get { return _customWeight; } set { _customWeight = value; OnPropertyChanged("CustomWeight"); } }
        [DataMember] private bool _useCustomWeight;
        public bool UseCustomWeight { get { return _useCustomWeight; } set { _useCustomWeight = value; OnPropertyChanged("UseCustomWeight"); } }
        [DataMember]
        public int SensorError { get; set; }
    }

}
