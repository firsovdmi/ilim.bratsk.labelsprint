﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses
{
    public class PlcStringProperty
    {
        public string Value { get; set; }
        public int MaxStringLenght { get; set; }
        public Adress StartAdress { get; set; }
        public int Dbn { get; set; }

        public PlcStringProperty(Adress startAdress, int dbn, int maxStringLenght)
        {
            StartAdress = startAdress;
            Dbn = dbn;
            MaxStringLenght = maxStringLenght;
        }

        public IEnumerable<WriteDirectJob> WriteJobs
        {
            get
            {
                var bytesOfString = new List<PlcProperty<byte>>();
                var tmpAdress = new Adress(StartAdress);
                foreach (var p in GetPlcByteArray())
                {
                    bytesOfString.Add(new PlcProperty<byte>(tmpAdress, Dbn) { Value = p });
                    tmpAdress += 8;
                }
                return bytesOfString.Select(p => p.WriteJobs);
            }
        }

        private IEnumerable<byte> GetPlcByteArray()
        {
            var bs = new List<byte>();
            if (Value == null) return bs;
            if (Value.Length > MaxStringLenght)
            {
                Value = Value.Substring(0, MaxStringLenght);
            }
            bs.Add((byte)MaxStringLenght);
            bs.Add((byte)Value.Length);
            return bs.Concat(Encoding.GetEncoding("windows-1251").GetBytes(Value.ToArray()).ToList());
        }
    }
}
