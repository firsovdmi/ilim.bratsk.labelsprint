﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.KipaData
{
    [Serializable]
    [DataContract]
    public class PlcCommonLineDataRemote
    {
        [DataMember]
        public string Ip { get; set; }
        [DataMember]
        public int Rack { get; set; }
        [DataMember]
        public int Slot { get; set; }

        public PlcCommonLineDataRemote(PlcCommonLineData source)
        {
            CurrentWeight = source.CurrentWeight.Value;
            WeightSetPoint = source.WeightSetPoint.Value;
            WeightDelta = source.WeightDelta.Value;
            LineNumber = source.LineNumber;
            PrinterErrorOut = source.PrinterErrorOut.Value;
            PrinterErrorIn = source.PrinterErrorIn.Value;
            ResetInErrorJob = source.PrinterResetErrorIn.WriteJobs;
            SetWeightSetpoint = source.WeightSetPoint.WriteJobs;
            SetWeightDelta = source.WeightDelta.WriteJobs;
            NoTrackKips = source.NoTrackKips.WriteJobs;
            IsPrinterAlarm = source.IsPrinterAlarm;
            IsPrinterStarted = source.IsPrinterStarted;
            WeightFlag = source.WeightFlag.Value;
            IsNoTrackKips = source.NoTrackKips.Value;
            Ip = source.Ip;
            Rack = source.Rack;
            Slot = source.Slot;
            Name = source.Name;
            CrossroadLeft = source.PlcCrossroadLeft.Value;
            CrossroadRight = source.PlcCrossroadRight.Value;
            PositionLed1 = source.PositionLed1.Value;
            PositionLed2 = source.PositionLed2.Value;
            PositionLed3 = source.PositionLed3.Value;
            PositionLed4 = source.PositionLed4.Value;
            PositionLed5 = source.PositionLed5.Value;
            PositionLed6 = source.PositionLed6.Value;
            SensorError = source.SensorError.Value;
            IsEdit = false;
        }

        [DataMember]
        public bool CrossroadRight { get; set; }
        [DataMember]
        public bool CrossroadLeft { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public float CurrentWeight { get; set; }

        [DataMember]
        public bool IsNoTrackKips { get; set; }

        [DataMember]
        private float _weightSetPoint;
        public float WeightSetPoint
        {
            get { return _weightSetPoint; }
            set
            {
                _weightSetPoint = value;
                IsEdit = true;
            }
        }

        [DataMember]
        private float _weightDelta;
        public float WeightDelta
        {
            get { return _weightDelta; }
            set
            {
                _weightDelta = value;
                IsEdit = true;
            }
        }
        [DataMember]
        public int LineNumber { get; set; }
        [DataMember]
        public bool PrinterErrorOut { get; set; }
        [DataMember]
        public bool PrinterErrorIn { get; set; }
        [DataMember]
        public WriteDirectJob ResetInErrorJob { get; set; }
        [DataMember]
        public WriteDirectJob SetWeightSetpoint { get; set; }
        [DataMember]
        public WriteDirectJob SetWeightDelta { get; set; }
        [DataMember]
        public WriteDirectJob NoTrackKips { get; set; }
        [DataMember]
        public bool IsPrinterStarted { get; set; }
        [DataMember]
        public bool IsPrinterAlarm { get; set; }
        [DataMember]
        public bool WeightFlag { get; set; }
        [DataMember]
        public bool IsEdit { get; set; }
        [DataMember]
        public bool PositionLed1 { get; set; }
        [DataMember]
        public bool PositionLed2 { get; set; }
        [DataMember]
        public bool PositionLed3 { get; set; }
        [DataMember]
        public bool PositionLed4 { get; set; }
        [DataMember]
        public bool PositionLed5 { get; set; }
        [DataMember]
        public bool PositionLed6 { get; set; }
        [DataMember]
        public bool SensorError { get; set; }

    }
}
