﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.KipaData
{
    public  interface IKipaCash
    {
        List<PlcKipaItem> PlcKipaItems { get; }
        List<PlcCommonLineData> PlcCommonLinesData { get; }
        DebugPlcMonitor DebugPlcMonitor { get; }
        int ScanCount { get; set; }
    }
}
