﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;
using ILIM.Bratsk.Infrastructure.Helpers;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.KipaData
{
    [Serializable]
    [DataContract]
    public class PlcCommonLineData
    {
        public string Ip { get; set; }
        public int Rack { get; set; }
        public int Slot { get; set; }
        public int LineNumber { get; set; }

        public PlcCommonLineData(Line source)
        {
            CurrentWeight       = new PlcProperty<float>(new Adress(source.PlcOffsetCurrentWeight, 0), source.PlcDbn);
            WeightSetPoint      = new PlcProperty<float>(new Adress(source.PlcWeightSetPoint, 0), source.PlcDbn);
            WeightDelta         = new PlcProperty<float>(new Adress(source.PlcWeightDelta,0), source.PlcDbn);
            PrinterErrorIn      = new PlcProperty<bool>(new Adress(source.PrinterErrorIn ), source.PlcDbn);
            PrinterErrorOut     = new PlcProperty<bool>(new Adress(source.PrinterErrorOut), source.PlcDbn);
            WeightFlag          = new PlcProperty<bool>(new Adress(source.PlcWeightFlag), source.PlcDbn);
            PrinterResetErrorIn = new PlcProperty<bool>(new Adress(source.PrinterResetErrorIn), source.PlcDbn);
            PlcCrossroadLeft    = new PlcProperty<bool>(new Adress(source.PlcCrossroadLeft), source.PlcDbn);
            PlcCrossroadRight   = new PlcProperty<bool>(new Adress(source.PlcCrossroadRight), source.PlcDbn);
            PositionLed1        = new PlcProperty<bool>(new Adress(1), source.PlcDbn);
            PositionLed2        = new PlcProperty<bool>(new Adress(3), source.PlcDbn);
            PositionLed3        = new PlcProperty<bool>(new Adress(5), source.PlcDbn);
            PositionLed4        = new PlcProperty<bool>(new Adress(7), source.PlcDbn);
            PositionLed5        = new PlcProperty<bool>(new Adress(8+1), source.PlcDbn);
            PositionLed6        = new PlcProperty<bool>(new Adress(8+2), source.PlcDbn);
            NoTrackKips = new PlcProperty<bool>(new Adress(source.NoTrackKips), source.PlcDbn);
            SensorError = new PlcProperty<bool>(new Adress(source.SensorError), source.PlcDbn);
            Name = source.Name;
            SensorErrorTime=DateTime.MaxValue;
        }

        [DataMember]
        public PlcProperty<float> CurrentWeight { get; set; }
        [DataMember]
        public PlcProperty<float> WeightSetPoint { get; set; }
        [DataMember]
        public PlcProperty<bool> PrinterErrorIn { get; set; }
        [DataMember]
        public PlcProperty<bool> PrinterResetErrorIn { get; set; }
        [DataMember]
        public PlcProperty<bool> PrinterErrorOut { get; set; }
        [DataMember]
        public PlcProperty<bool> PositionLed1 { get; set; }
        [DataMember]
        public PlcProperty<bool> PositionLed2 { get; set; }
        [DataMember]
        public PlcProperty<bool> PositionLed3 { get; set; }
        [DataMember]
        public PlcProperty<bool> PositionLed4 { get; set; }
        [DataMember]
        public PlcProperty<bool> PositionLed5 { get; set; }
        [DataMember]
        public PlcProperty<bool> PositionLed6 { get; set; }
        [DataMember]
        public bool PrinterIsDown { get; set;}
        private IEnumerable<ReadDirectJob> _allReadJobs;
        public IEnumerable<ReadDirectJob> AllReadJobs
        {
            get
            {
                if (_allReadJobs == null)
                {
                    var rJobs = new List<ReadDirectJob>
                    {
                        CurrentWeight          .ReadJobs,
                        WeightSetPoint         .ReadJobs,
                        WeightDelta            .ReadJobs,
                        PrinterErrorIn         .ReadJobs,
                        PrinterErrorOut        .ReadJobs,
                        WeightFlag             .ReadJobs,
                        PlcCrossroadLeft       .ReadJobs,
                        PlcCrossroadRight      .ReadJobs,
                        NoTrackKips            .ReadJobs,
                        PositionLed1           .ReadJobs,
                        PositionLed2           .ReadJobs,
                        PositionLed3           .ReadJobs,
                        PositionLed4           .ReadJobs,
                        PositionLed5           .ReadJobs,
                        PositionLed6           .ReadJobs,
                        SensorError            .ReadJobs,
                    };
                    _allReadJobs = rJobs;
                }
                return _allReadJobs;
            }
        }

        private IEnumerable<WriteDirectJob> _allWriteobs;
        public IEnumerable<WriteDirectJob> AllWriteJobs
        {
            get
            {
                if (_allWriteobs == null)
                {
                    var rJobs = new List<WriteDirectJob>
                    {
                        PrinterErrorOut.WriteJobs
                    };
                    _allWriteobs = rJobs;
                }
                ((WriteObjectValue)PrinterErrorOut.WriteJobs).Value = PrinterErrorOut.Value;
                return _allWriteobs;
            }
        }

        public bool IsPrinterAlarm { get; set; }

        public bool IsPrinterStarted { get; set; }

        public PlcProperty<float> WeightDelta { get; set; }

        public PlcProperty<bool> WeightFlag { get; set; }

        public void ReciveData(BatchDirectJobs rJobs)
        {
            if (rJobs != null)
            {                        
                CurrentWeight        .FindValue(rJobs);
                WeightSetPoint       .FindValue(rJobs);
                WeightDelta          .FindValue(rJobs);
                PrinterErrorIn       .FindValue(rJobs);
                PrinterErrorOut      .FindValue(rJobs);
                WeightFlag           .FindValue(rJobs);
                PlcCrossroadLeft     .FindValue(rJobs);
                PlcCrossroadRight    .FindValue(rJobs);
                NoTrackKips          .FindValue(rJobs);
                PositionLed1         .FindValue(rJobs);
                PositionLed2         .FindValue(rJobs);
                PositionLed3         .FindValue(rJobs);
                PositionLed4         .FindValue(rJobs);
                PositionLed5         .FindValue(rJobs);
                PositionLed6         .FindValue(rJobs);
                PositionLed6         .FindValue(rJobs);
            }
        }
        public PlcProperty<bool> PlcCrossroadLeft { get; set; }
        public PlcProperty<bool> PlcCrossroadRight { get; set; }
        public PlcProperty<bool> NoTrackKips { get; set; }
        public PlcProperty<bool> SensorError { get; set; }
        public string Name { get; set; }
        public DateTime SensorErrorTime { get; set; }


        public PositionErrorEnum PositionError { get; set; }
        public string ErrorListFromPrinter { get; set; }
        public bool IsNotRespond { get; set; }
        public DateTime NotRespondTime { get; set; }
    }
}
