﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.RemoteFacade;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.KipaData
{
    public class KipaCash : IKipaCash
    {
        private const int MaxKipsPerLine = 15;

        public KipaCash(ILineService lineService)
        {
            foreach (var line in lineService.Get().OrderBy(p=>p.LineNumber))
            {
                var adress = new Adress(line.PlcStartByte, 0);
                for (var i = 0; i < MaxKipsPerLine; i++)
                {
                    _plcKipaItems.Add(new PlcKipaItem(line, adress, i + 1)
                    {
                        Ip = line.PlcIpAdress,
                        Slot = line.PlcSlot,
                        Rack = line.PlcRack
                    });
                    adress += 160;
                }

                _plcCommonLinesData.Add(new PlcCommonLineData(line)
                {
                    Ip = line.PlcIpAdress,
                    Slot = line.PlcSlot,
                    Rack = line.PlcRack,
                    LineNumber=line.LineNumber
                });
            }
        }

        readonly List<PlcKipaItem> _plcKipaItems = new List<PlcKipaItem>();

        public List<PlcKipaItem> PlcKipaItems
        {
            get { return  _plcKipaItems; }
        }

        private readonly List<PlcCommonLineData> _plcCommonLinesData = new List<PlcCommonLineData>();

        public List<PlcCommonLineData> PlcCommonLinesData
        {
            get { return _plcCommonLinesData; }
        }

        public DebugPlcMonitor DebugPlcMonitor { get; } = new DebugPlcMonitor();

        public int ScanCount { get; set; }
    }
}
