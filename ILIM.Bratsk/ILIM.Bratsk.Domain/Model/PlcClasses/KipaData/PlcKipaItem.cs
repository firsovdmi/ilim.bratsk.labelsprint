﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.KipaData
{
    [Serializable]
    [DataContract]
    public class PlcKipaItem
    {
        [DataMember]
        public string Ip { get; set; }
        [DataMember]
        public int Rack { get; set; }
        [DataMember]
        public int Slot { get; set; }
        [DataMember]
        public int ArrayIndex { get; set; }
        [DataMember]
        public int BeforePrinterPosition1 { get; set; }
        [DataMember]
        public int BeforePrinterPosition2 { get; set; }

        public PlcKipaItem(Line source, Adress adress, int arrayIndex)
        {
            Position = new PlcProperty<Int16>(adress, source.PlcDbn);
            adress += 2 * 8; // 16
            InitLineNumber = new PlcProperty<Int16>(adress, source.PlcDbn);
            adress += 2 * 8; // 32
            Weight = new PlcProperty<float>(adress, source.PlcDbn);
            adress += 4 * 8; // 64
            Year = new PlcProperty<Int16>(adress, source.PlcDbn);
            adress += 2 * 8;
            Mount = new PlcProperty<byte>(adress, source.PlcDbn);
            adress += 1 * 8;
            Day = new PlcProperty<byte>(adress, source.PlcDbn);
            adress += 1 * 8;
            WeekDay = new PlcProperty<byte>(adress, source.PlcDbn);
            adress += 1 * 8;
            Hour = new PlcProperty<byte>(adress, source.PlcDbn);
            adress += 1 * 8;
            Minute = new PlcProperty<byte>(adress, source.PlcDbn);
            adress += 1 * 8;
            Second = new PlcProperty<byte>(adress, source.PlcDbn);
            adress += 1 * 8;
            Nanosecond = new PlcProperty<byte>(adress, source.PlcDbn);
            adress += 1 * 8;
            ArrayIndex = arrayIndex;
            BeforePrinterPosition1 = source.BeforePrinterPosition1;
            BeforePrinterPosition2 = source.BeforePrinterPosition2;
            CurrentLineNumber = source.LineNumber;
        }

        public bool IsBeforePrinterPosition
        {
            get { return (BeforePrinterPosition1 == Position.Value || BeforePrinterPosition2 == Position.Value) && ArrayIndex == 1; }
        }

        [DataMember]
        public PlcProperty<Int16> InitLineNumber { get; set; }
        [DataMember]
        public int CurrentLineNumber { get; set; }
        [DataMember]
        public PlcProperty<float> Weight { get; set; }
        [DataMember]
        public DateTime WeightTime
        {
            get
            {
                try
                {
                    if (Year.Value>0 && Mount.Value>0 && Day.Value>0)
                    return new DateTime(Year.Value, Mount.Value, Day.Value, Hour.Value, Minute.Value, Second.Value, Nanosecond.Value);
                }
                catch (Exception)
                {
                    return DateTime.MinValue;
                }
                return DateTime.MinValue;
            }
        }
        public PlcProperty<Int16> Year { get; set; }
        public PlcProperty<byte> Mount { get; set; }
        public PlcProperty<byte> Day { get; set; }
        public PlcProperty<byte> WeekDay { get; set; }
        public PlcProperty<byte> Hour { get; set; }
        public PlcProperty<byte> Minute { get; set; }
        public PlcProperty<byte> Second { get; set; }
        public PlcProperty<byte> Nanosecond { get; set; }
        [DataMember]
        public PlcProperty<Int16> Position { get; set; }

        private IEnumerable<ReadDirectJob> _allReadJobs;
        public IEnumerable<ReadDirectJob> AllReadJobs
        {
            get
            {
                if (_allReadJobs == null)
                {
                    var rJobs = new List<ReadDirectJob>
                    {
                        Weight.ReadJobs,
                         Year                .ReadJobs,
                         Mount               .ReadJobs,
                         Day                 .ReadJobs,
                         WeekDay             .ReadJobs,
                         Hour                .ReadJobs,
                         Minute              .ReadJobs,
                         Second              .ReadJobs,
                         Nanosecond          .ReadJobs,
                        Position.ReadJobs,
                        InitLineNumber.ReadJobs
                    };
                    _allReadJobs = rJobs;
                }
                return _allReadJobs;
            }
        }

        public WriteDirectJob ResetPositionWriteJob
        {
            get
            {
                var ret = (WriteObjectValue)Position.WriteJobs;
                ret.Value = 0;
                return ret;
            }
        }

        private IEnumerable<WriteDirectJob> _allWriteobs;
        public IEnumerable<WriteDirectJob> AllWriteJobs
        {
            get
            {
                if (_allWriteobs == null)
                {
                    var rJobs = new List<WriteDirectJob>
                    {
                        Weight.WriteJobs,
                        Position.WriteJobs
                    };
                    _allWriteobs = rJobs;
                }
                ((WriteObjectValue)Weight.WriteJobs).Value = Weight.Value;
                ((WriteObjectValue)Position.WriteJobs).Value = Position.Value;
                return _allWriteobs;
            }
        }

        public void ReciveData(BatchDirectJobs rJobs)
        {
            if (rJobs != null)
            {
                Weight.FindValue(rJobs);
                Year.FindValue(rJobs);
                Mount.FindValue(rJobs);
                Day.FindValue(rJobs);
                WeekDay.FindValue(rJobs);
                Hour.FindValue(rJobs);
                Minute.FindValue(rJobs);
                Second.FindValue(rJobs);
                Nanosecond.FindValue(rJobs);

                Position.FindValue(rJobs);
                InitLineNumber.FindValue(rJobs);
            }
        }
    }
}
