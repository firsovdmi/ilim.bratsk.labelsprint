﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.KipaData
{
    [Serializable]
    [DataContract]
    public class DebugPlcMonitorRemote
    {
        public DebugPlcMonitorRemote(DebugPlcMonitor source)
        {
            TurnLeft           =        source    .TurnLeft             .Value;
            TurnRight = source.TurnRight.Value;
            TurnRight2 = source.TurnRight2.Value;
            DeleteCount1       =        source    .DeleteCount1         .Value;
            DeleteCount2 = source.DeleteCount2.Value;
            DeleteCount3 = source.DeleteCount3.Value;
            DeleteCount4 = source.DeleteCount4.Value;
        }
        [DataMember]
        public bool      TurnLeft       { get; set; }
        [DataMember]
        public bool     TurnRight       { get; set; }
        [DataMember]
        public bool     TurnRight2      { get; set; }
        [DataMember]
        public int      DeleteCount1 { get; set; }
        [DataMember]
        public int DeleteCount2 { get; set; }
        [DataMember]
        public int DeleteCount3 { get; set; }
        [DataMember]
        public int      DeleteCount4 { get; set; }

    }
}
