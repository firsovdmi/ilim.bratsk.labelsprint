﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.KipaData
{
    [Serializable]
    [DataContract]
    public class PlcKipaItemRemote
    {
        public PlcKipaItemRemote(PlcKipaItem source)
        {
            ArrayIndex = source.ArrayIndex;
            CurrentLineNumber = source.CurrentLineNumber;
            InitLineNumber = source.InitLineNumber.Value;
            Weight = source.Weight.Value;
            WeightTime = source.WeightTime;
            Position = source.Position.Value;
            BeforePrinterPosition1 = source.BeforePrinterPosition1;
            BeforePrinterPosition2 = source.BeforePrinterPosition2;
            ResetPositionWriteJob = source.ResetPositionWriteJob;
            Ip = source.Ip;
            Rack = source.Rack;
            Slot = source.Slot;
        }
        [DataMember]
        public int CurrentLineNumber { get; set; }
        [DataMember]
        public int InitLineNumber { get; set; }
        [DataMember]
        public float Weight { get; set; }
        [DataMember]
        public DateTime WeightTime { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int BeforePrinterPosition1 { get; set; }
        [DataMember]
        public int BeforePrinterPosition2 { get; set; }
        [DataMember]
        public int ArrayIndex { get; set; }
        [DataMember]
        public string Ip { get; set; }
        [DataMember]
        public int Rack { get; set; }
        [DataMember]
        public int Slot { get; set; }
        [DataMember]
        public WriteDirectJob ResetPositionWriteJob { get; set; }
        public bool IsBeforePrinterPosition
        {
            get { return (BeforePrinterPosition1 == Position || BeforePrinterPosition2 == Position) && ArrayIndex==1; }
        }
    }
}
