﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.KipaData
{
    [Serializable]
    [DataContract]
    public class DebugPlcMonitor
    {
        public DebugPlcMonitor()
        {
            TurnLeft = new PlcProperty<bool>(new Adress(3), 50);
            TurnRight = new PlcProperty<bool>(new Adress(4), 50);
            TurnRight2 = new PlcProperty<bool>(new Adress(2), 50);
            DeleteCount1 = new PlcProperty<Int32>(new Adress(320, 0), 50);
            DeleteCount2 = new PlcProperty<Int32>(new Adress(324, 0), 50);
            DeleteCount3 = new PlcProperty<Int32>(new Adress(328, 0), 50);
            DeleteCount4 = new PlcProperty<Int32>(new Adress(332, 0), 50);
            LastCountLog=DateTime.UtcNow;
        }
        [DataMember]
        public PlcProperty<bool> TurnLeft { get; set; }
        [DataMember]
        public PlcProperty<bool> TurnRight { get; set; }
        [DataMember]
        public PlcProperty<bool> TurnRight2 { get; set; }
        [DataMember]
        public PlcProperty<Int32> DeleteCount1 { get; set; }
        [DataMember]
        public PlcProperty<Int32> DeleteCount2 { get; set; }
        [DataMember]
        public PlcProperty<Int32> DeleteCount3 { get; set; }
        [DataMember]
        public PlcProperty<Int32> DeleteCount4 { get; set; }

        [DataMember]
        public DateTime LastCountLog { get; set; }

        private IEnumerable<ReadDirectJob> _allReadJobs;
        public IEnumerable<ReadDirectJob> AllReadJobs
        {
            get
            {
                if (_allReadJobs == null)
                {
                    var rJobs = new List<ReadDirectJob>
                    {
                        TurnLeft          .ReadJobs,
                        TurnRight         .ReadJobs,
                        TurnRight2         .ReadJobs,
                        DeleteCount1      .ReadJobs,
                        DeleteCount2      .ReadJobs,
                        DeleteCount3      .ReadJobs,
                        DeleteCount4      .ReadJobs
                    };
                    _allReadJobs = rJobs;
                }
                return _allReadJobs;
            }
        }

        public void ReciveData(BatchDirectJobs rJobs)
        {
            if (rJobs == null) return;
            TurnLeft        .FindValue(rJobs);
            TurnRight.FindValue(rJobs);
            TurnRight2.FindValue(rJobs);
            DeleteCount1    .FindValue(rJobs);
            DeleteCount2.FindValue(rJobs);
            DeleteCount3.FindValue(rJobs);
            DeleteCount4    .FindValue(rJobs);
        }

    }
}
