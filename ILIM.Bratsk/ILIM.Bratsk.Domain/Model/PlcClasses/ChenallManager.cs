﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using OpcRcw.Da;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses
{
    class ChanellManager
    {
        public const int LOCALE_ID = 0x409;

        int _hClient = 1;

        readonly Object _pobjGroup1;
        readonly IOPCSyncIO _pIopcSyncIo;
        readonly IOPCServer _pIopcServer;

        readonly string _opcitemdeDefaultFHeader;

        public ChanellManager(string ip, int rack, int slot)
        {
            if (ip == "")
            {
                return;
            }
            //конвертируем комовский ProgID в дотнетовский CLSID
            Guid iidRequiredInterface = typeof(IOPCItemMgt).GUID;
            Type svrComponenttyp = Type.GetTypeFromProgID("OPC.SimaticNET");


            // Подключаемся к серверу.
            _pIopcServer = (IOPCServer)Activator.CreateInstance(svrComponenttyp);

            #region Добавление группы
            int pSvrGroupHandle;
            Int32 pRevUpdateRate;
            GCHandle hTimeBias = GCHandle.Alloc(0, GCHandleType.Pinned);
            GCHandle hDeadband = GCHandle.Alloc(0, GCHandleType.Pinned);

            string groupName = String.Format("PAG_DC{0}", Math.Abs(string.Format("{0}", ip).GetHashCode()));// RandomString(15);

            _pIopcServer.AddGroup(groupName,   // Название группы
                0,                              // Группа активна (включить калбаки)
                250,                            // Время проверки обновления переменных (мс)
                1,                              // Указатель на клиентскую группу
                hTimeBias.AddrOfPinnedObject(), // (разобраца)
                hDeadband.AddrOfPinnedObject(), // Гистерезис вызова калбаков
                LOCALE_ID,                      // Английский язык
                out pSvrGroupHandle,            // Указатель на группу (не используется)
                out pRevUpdateRate,             // Фактическое время опроса
                ref iidRequiredInterface,       // CLSID объекта
                out _pobjGroup1);                // Ссылка на интерфейс
            #endregion

            #region Инициализация интерфейсов для чтения/записи.
            _pIopcSyncIo = (IOPCSyncIO)_pobjGroup1;

            #endregion

            _opcitemdeDefaultFHeader = String.Format("S7:[{2}9|VFD1|S7ONLINE|01.00,{0},02.{1:00},1]", ip, (rack << 5) | slot, groupName);


        }

        public void ReadJobs(List<BaseDirectJob> jobs)
        {
            IntPtr pErrors;
            IntPtr pItemValues;

            int[] hSrv = jobs.Where(p => p != null).Select(p => GetHServer(p.ItemAdress, p.InteropOpcType)).ToArray();
            DateTime mem = DateTime.Now;
            _pIopcSyncIo.Read(OPCDATASOURCE.OPC_DS_DEVICE, jobs.Count, hSrv, out pItemValues, out pErrors);
            jobs[0].ErrorText = string.Format("{0}", (DateTime.Now - mem).TotalMilliseconds);
            var errors = new int[jobs.Count];
            Marshal.Copy(pErrors, errors, 0, jobs.Count);

            IntPtr pos = pItemValues;
            int k = 0;
            foreach (BaseDirectJob item in jobs)
            {

                var state = ((OPCITEMSTATE)Marshal.PtrToStructure(pos, typeof(OPCITEMSTATE)));
                if (errors[k] != 0 || state.wQuality != 0xC0)
                {
                    item.IsError = true;
                    string errorText;
                    _pIopcServer.GetErrorString(errors[k], 0x409, out errorText);
                    item.ErrorText = errorText;
                }
                else
                {
                    item.IsError = false;
                    item.ErrorText = "OK";
                    item.SetValue(state.vDataValue);
                }
                pos = new IntPtr(pos.ToInt32() + Marshal.SizeOf(typeof(OPCITEMSTATE)));
                k++;
            }

            Marshal.FreeCoTaskMem(pItemValues);
            Marshal.FreeCoTaskMem(pErrors);
        }

        public void WriteJobs(List<BaseDirectJob> jobs)
        {
            IntPtr pErrors;
            IntPtr pItemValues = IntPtr.Zero;

            int[] hSrv = jobs.Select(p => GetHServer(p.ItemAdress, p.InteropOpcType)).ToArray();

            _pIopcSyncIo.Write(jobs.Count, hSrv, jobs.Select(p => p.GetValueAsObject()).ToArray(), out pErrors);

            int[] errors = new int[jobs.Count];
            Marshal.Copy(pErrors, errors, 0, jobs.Count);


            int k = 0;
            foreach (BaseDirectJob Item in jobs)
            {
                if (errors[k] != 0)
                {
                    Item.IsError = true;
                    string errorText;
                    _pIopcServer.GetErrorString(errors[k], 0x409, out errorText);
                    Item.ErrorText = errorText;
                }
                k++;
            }

            Marshal.FreeCoTaskMem(pItemValues);
            Marshal.FreeCoTaskMem(pErrors);
        }

        private readonly Dictionary<string, int> _hServers = new Dictionary<string, int>();
        private int GetHServer(string s, VarEnum type)
        {
            if (!_hServers.ContainsKey(s))
            {
                AddChanel(s, type);
            }
            return _hServers[s];
        }

        private void AddChanel(string s, VarEnum type)
        {
            IntPtr pResults;
            IntPtr pErrors;

            // Добавления итемов в группу
            var item = new OPCITEMDEF
            {
                szAccessPath = "",
                szItemID = _opcitemdeDefaultFHeader + s,
                bActive = 1, //ХЗ, может важно
                hClient = _hClient,
                dwBlobSize = 0,
                pBlob = IntPtr.Zero,
                vtRequestedDataType = Convert.ToByte(type)
            };
            _hClient++;
            ((IOPCItemMgt)_pobjGroup1).AddItems(1, new[] { item }, out pResults, out pErrors);

            // Получаем доступ к неуправляемому интерфейсу ком объекта (указатели на созданные итемы)
            var errors = new int[1];
            Marshal.Copy(pErrors, errors, 0, 1);

            // Получение указателей на созданные элементы и проверка на ошибки


            if (errors[0] != 0)
            {
                string errorText;
                _pIopcServer.GetErrorString(errors[0], 0x409, out errorText);
            }
            else
            {
                var oc = (OPCITEMRESULT)Marshal.PtrToStructure(pResults, typeof(OPCITEMRESULT));
                _hServers[s] = oc.hServer;
            }

        }

        private static readonly Random Random = new Random((int)DateTime.Now.Ticks);
        private string RandomString(int size)
        {
            var builder = new StringBuilder();
            for (int i = 0; i < size; i++)
            {
                char ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * Random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}
