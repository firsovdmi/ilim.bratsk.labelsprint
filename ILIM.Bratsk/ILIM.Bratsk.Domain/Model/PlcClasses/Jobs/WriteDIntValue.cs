﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    [KnownType(typeof(WriteDirectJob))]
    public class WriteDIntValue : WriteDirectJob
    {
        public WriteDIntValue(Adress adres, int dbn) : base(adres, dbn) { }
        public WriteDIntValue(Adress adres, int dbn, string id) : base(adres, dbn, id) { }

        [DataMember]
        public Int32 Value { get; set; }

        internal override VarEnum InteropOpcType
        {
            get { return VarEnum.VT_I4; }
        }

        internal override void SetValue(object value)
        {
            Value = (Int32)value;
        }

        public override object GetValueAsObject()
        {
            return Value;
        }
    }
}
