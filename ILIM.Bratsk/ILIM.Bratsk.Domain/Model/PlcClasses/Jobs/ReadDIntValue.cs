﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    [KnownType(typeof(ReadDirectJob))]
    public class ReadDIntValue : ReadDirectJob
    {
        public ReadDIntValue(Adress adres, int dbn) : base(adres, dbn) { }
        public ReadDIntValue(Adress adres, int dbn, string id) : base(adres, dbn, id) { }

        [DataMember]
        public Int32 Value { get; internal set; }

        internal override VarEnum InteropOpcType
        {
            get { return VarEnum.VT_I4; }
        }

        internal override void SetValue(object value)
        {
            Value = Convert.ToInt32(value);
        }

        public override object GetValueAsObject()
        {
            return Value;
        }
    }
}
