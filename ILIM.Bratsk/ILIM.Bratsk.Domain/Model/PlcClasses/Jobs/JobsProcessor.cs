﻿using System.Collections.Generic;
using System.Linq;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.Jobs
{
   public class JobsProcessor
    {
        readonly ChanellManager chenalManager;
        public string Ip { get; set; }
        public int Rack { get; set; }
        public int Slot { get; set; }

        public JobsProcessor(string ip, int rack, int slot)
        {
            Ip = ip;
            Rack = rack;
            Slot = slot;
            chenalManager = new ChanellManager(ip, rack, slot);
        }

        public void ProcessJobs(BatchDirectJobs jobs)
        {
            ReadJobsProcess(jobs.Items.Where(p => p is ReadDirectJob).ToList());
            WriteJobProcess(jobs.Items.Where(p => p is WriteDirectJob).ToList());

        }

        private void ReadJobsProcess(List<BaseDirectJob> jobs)
        {
            if (jobs.Count > 0)
            {
                chenalManager.ReadJobs(jobs);
            }
        }

        private void WriteJobProcess(List<BaseDirectJob> jobs)
        {
            if (jobs.Count > 0)
            {
                chenalManager.WriteJobs(jobs);
            }
        }
    }

    class JobAndhServer
    {
        public BaseDirectJob Job { get; set; }
        public int HServer { get; set; }
    }
}
