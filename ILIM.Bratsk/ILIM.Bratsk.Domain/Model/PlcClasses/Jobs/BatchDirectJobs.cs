﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    public class BatchDirectJobs : IEnumerable
    {
        public BatchDirectJobs(IEnumerable<BaseDirectJob> jobs)
        {
            items=new List<BaseDirectJob>(jobs);
        }

        [DataMember]
        protected List<BaseDirectJob> items;

        [DataMember]
        public double ProcessingTime { get; set; }

        
        public List<BaseDirectJob> Items
        {
            get
            {
                return items;
            }
            set
            {
                items = value;
            }
        }

        public BaseDirectJob this[int index]
        {
            get
            {
                return items[index];
            }
            set
            {
                items[index] = value;
            }
        }

        public int IndexOf(BaseDirectJob item)
        {
            return items.IndexOf(item);
        }

        public void Add(BaseDirectJob item)
        {
            items.Add(item);
        }

        public void AddReadObjectValue(Adress adres, int dbn, string type, string id = null)
        {
            items.Add(new ReadObjectValue(adres, dbn, type, id));
        }

        public void AddReadBoolValue(Adress adres, int dbn, string id = null)
        {
            items.Add(new ReadBoolValue(adres, dbn, id));
        }

        public void AddReadIntValue(Adress adres, int dbn, string id = null)
        {
            items.Add(new ReadIntValue(adres, dbn, id));
        }

        public void AddReadDIntValue(Adress adres, int dbn, string id = null)
        {
            items.Add(new ReadDIntValue(adres, dbn, id));
        }

        public void AddReadRealValue(Adress adres, int dbn, string id = null)
        {
            items.Add(new ReadRealValue(adres, dbn, id));
        }

        public BaseDirectJob GetElementById(string id)
        {
            if (items == null) return null;
            return items.Where(p => p != null).FirstOrDefault(p => p.Id == id);
        }


        public void AddWriteObjectValue(Adress adres, int dbn, string type, string id = null)
        {
            items.Add(new WriteObjectValue(adres, dbn, type, id));
        }

        public void AddWriteBoolValue(Adress adres, int dbn, string id = null)
        {
            items.Add(new WriteBoolValue(adres, dbn, id));
        }

        public void AddWriteIntValue(Adress adres, int dbn, string id = null)
        {
            items.Add(new WriteIntValue(adres, dbn, id));
        }

        public void AddWriteDIntValue(Adress adres, int dbn, string id = null)
        {
            items.Add(new WriteDIntValue(adres, dbn, id));
        }

        public void AddWriteRealValue(Adress adres, int dbn, string id = null)
        {
            items.Add(new WriteRealValue(adres, dbn, id));
        }



        public void AddRange(List<BaseDirectJob> item)
        {
            items.AddRange(item);
        }

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }


    }
}
