﻿using System.Runtime.Serialization;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases
{
    [DataContract]
    [KnownType(typeof(WriteBoolValue))]
    [KnownType(typeof(WriteDIntValue))]
    [KnownType(typeof(WriteIntValue))]
    [KnownType(typeof(WriteRealValue))]
    [KnownType(typeof(WriteObjectValue))]
    public abstract class WriteDirectJob : BaseDirectJob
    {
        protected WriteDirectJob(Adress adres, int dbn) : base(adres, dbn) { }
        protected WriteDirectJob(Adress adres, int dbn, string id) : base(adres, dbn, id) { }
    }
}
