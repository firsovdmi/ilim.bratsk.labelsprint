﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases
{
    [DataContract]
    [KnownType(typeof(ReadBoolValue))]
    [KnownType(typeof(ReadDateTimeValue))]
    [KnownType(typeof(ReadDIntValue))]
    [KnownType(typeof(ReadIntValue))]
    [KnownType(typeof(ReadRealValue))]
    [KnownType(typeof(ReadObjectValue))]
    [KnownType(typeof(WriteBoolValue))]
    [KnownType(typeof(WriteDIntValue))]
    [KnownType(typeof(WriteIntValue))]
    [KnownType(typeof(WriteRealValue))]
    [KnownType(typeof(WriteObjectValue))]
    [KnownType(typeof(ReadDirectJob))]
    [KnownType(typeof(WriteDirectJob))]
    public abstract class BaseDirectJob
    {
        protected BaseDirectJob(Adress adress, int dbn, string id)
        {
            Adress = adress;
            Dbn = dbn;
            Id = id;
        }

        protected BaseDirectJob(Adress adress, int dbn)
            : this(adress, dbn, null)
        {
        }

        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int Dbn { get; set; }
        [DataMember]
        internal Adress Adress { get; set; }
        public string ItemAdress
        {
            get
            {
                string szItemId = String.Format("DB{0},{1}{2}", Dbn, GetFormatName(InteropOpcType), Adress.Byte);

                if (InteropOpcType == VarEnum.VT_BOOL)
                {
                    szItemId += String.Format(".{0}", Adress.Bit);
                }
                return szItemId;
            }
        }
        internal abstract VarEnum InteropOpcType { get; }
        internal abstract void SetValue(object value);
        public abstract object GetValueAsObject();

        public void SetAsError(string message = "")
        {
            IsError = true;
            ErrorText = message;
        }

        [DataMember]
        public bool IsError { get; internal set; }
        [DataMember]
        public string ErrorText { get; internal set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", Adress, GetValueAsObject());
        }

        private string GetFormatName(VarEnum vtRequestedDataType)
        {
            switch (vtRequestedDataType)
            {
                case VarEnum.VT_BOOL: return "X";
                case VarEnum.VT_UI1: return "BYTE";
                case VarEnum.VT_I1: return "CHAR";
                case VarEnum.VT_UI2: return "WORD";
                case VarEnum.VT_I2: return "INT";
                case VarEnum.VT_UI4: return "DWORD";
                case VarEnum.VT_I4: return "DINT";
                case VarEnum.VT_R4: return "REAL";
                case VarEnum.VT_DATE: return "DT";
                default: throw new Exception("Тип не поддерживается");
            }
        }

        public void SetError(string errorText)
        {
            IsError = true;
            ErrorText = errorText;
        }
    }
}
