﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    [KnownType(typeof(WriteDirectJob))]
    public class WriteRealValue : WriteDirectJob
    {
        public WriteRealValue(Adress adres, int dbn) : base(adres, dbn) { }
        public WriteRealValue(Adress adres, int dbn, string id) : base(adres, dbn, id) { }

        [DataMember]
        public double Value { get; set; }

        internal override VarEnum InteropOpcType
        {
            get { return VarEnum.VT_R4; }
        }

        internal override void SetValue(object value)
        {
            Value = Convert.ToDouble(value);
        }

        public override object GetValueAsObject()
        {
            return Value;
        }
    }
}
