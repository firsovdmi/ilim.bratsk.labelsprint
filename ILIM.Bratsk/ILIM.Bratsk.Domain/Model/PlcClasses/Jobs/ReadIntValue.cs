﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    [KnownType(typeof(ReadDirectJob))]
    public class ReadIntValue : ReadDirectJob
    {
        public ReadIntValue(Adress adres, int dbn) : base(adres, dbn) { }
        public ReadIntValue(Adress adres, int dbn, string id) : base(adres, dbn, id) { }
        [DataMember]
        public Int16 Value { get; internal set; }

        internal override VarEnum InteropOpcType
        {
            get { return VarEnum.VT_I2; }
        }

        internal override void SetValue(object value)
        {
            Value = Convert.ToInt16(value);
        }

        public override object GetValueAsObject()
        {
            return Value;
        }
    }
}
