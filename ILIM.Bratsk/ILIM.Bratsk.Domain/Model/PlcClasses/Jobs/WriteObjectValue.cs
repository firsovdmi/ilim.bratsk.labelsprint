﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    [KnownType(typeof(WriteDirectJob))]
    public class WriteObjectValue : WriteDirectJob
    {
        public WriteObjectValue(Adress adres, int dbn, string type)
            : base(adres, dbn)
        {
            InitValues(type);
        }


        public WriteObjectValue(Adress adres, int dbn, string type, string id)
            : base(adres, dbn, id)
        {
            InitValues(type);
        }

        private void InitValues(string type)
        {
            PLCObjectType = type;
        }
        [DataMember]
        public object Value { get; set; }

        internal override void SetValue(object value)
        {
            Value = value;
        }

        internal override VarEnum InteropOpcType
        {
            get
            {
                switch (PLCObjectType)
                {
                    case "BOOL":
                        return VarEnum.VT_BOOL;

                    case "BYTE":
                        return VarEnum.VT_UI1;

                    case "CHAR":
                        return VarEnum.VT_I1;

                    case "WORD":
                        return VarEnum.VT_UI2;

                    case "INT":
                        return VarEnum.VT_I2;

                    case "DWORD":
                        return VarEnum.VT_UI4;

                    case "DINT":
                        return VarEnum.VT_I4;

                    case "REAL":
                        return VarEnum.VT_R4;

                    case "DATE_AND_TIME":
                        return VarEnum.VT_DATE;

                    default: throw new Exception(string.Format("Не известный формат {0}", PLCObjectType));
                }
            }
        }

        [DataMember]
        public string PLCObjectType { get; set; }


        public override object GetValueAsObject()
        {
            return Value;
        }
    }
}
