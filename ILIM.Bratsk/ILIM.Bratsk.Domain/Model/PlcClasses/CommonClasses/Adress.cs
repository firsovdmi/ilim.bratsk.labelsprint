﻿using System;
using System.Runtime.Serialization;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses
{
     [DataContract]
    public struct Adress : IComparable<Adress>
    {
        public Adress(long bitOffset)
        {
            _bitOffset = bitOffset;
        }

        public Adress(long Byte, long bit)
            : this(Byte * MemorySpan.BitPerByte + bit)
        {

        }

        public void RoundToByte()
        {
            long ostatok = _bitOffset % 8;
            _bitOffset = _bitOffset + 8 - (ostatok == 0 ? 8 : ostatok);
        }

        public static bool operator ==(Adress a1, Adress a2)
        {
            return a1.Equals(a2);
        }

        public static bool operator !=(Adress a1, Adress a2)
        {
            return !a1.Equals(a2);
        }

        public static bool operator >(Adress a1, Adress a2)
        {
            return a1.BitOffset > a2.BitOffset;
        }

        public static bool operator >=(Adress a1, Adress a2)
        {
            return a1.BitOffset >= a2.BitOffset;
        }

        public static bool operator <(Adress a1, Adress a2)
        {
            return a1.BitOffset < a2.BitOffset;
        }

        public static bool operator <=(Adress a1, Adress a2)
        {
            return a1.BitOffset <= a2.BitOffset;
        }

        public static Adress operator +(Adress a1, long BitCount)
        {
            BitCount += a1.BitOffset;
            return new Adress(BitCount);
        }

        public static Adress operator +(Adress a1, MemorySpan ms)
        {
            return new Adress(a1.BitOffset + ms.TotalBits);
        }

        public static Adress operator -(Adress a1, MemorySpan ms)
        {
            return new Adress(a1.BitOffset - ms.TotalBits);
        }

        public static Adress operator -(Adress a1, long bitCount)
        {
            bitCount = (a1.BitOffset) - bitCount;
            return new Adress(bitCount);
        }
        public static Adress operator -(Adress a1, Adress a2)
        {
            long BitCount = a1.BitOffset - a2.BitOffset;
            return new Adress(BitCount);
        }
        public static implicit operator int(Adress x)
        {
            return (int)x.BitOffset;
        }

        public override int GetHashCode()
        {
            return Bit.GetHashCode() + Byte.GetHashCode();
        }
        public override string ToString()
        {
            return string.Format("{0}.{1}", Byte, Bit);
        }

        public override bool Equals(object obj)
        {
            return obj is Adress && ((Adress)obj).BitOffset == BitOffset;
        }
        [DataMember]
        private long _bitOffset;
        public long BitOffset
        {
            get { return _bitOffset; }
            set { _bitOffset = value; }
        }

        public long Byte
        {
            get
            {
                return BitOffset / MemorySpan.BitPerByte;
            }
        }

        public long Bit
        {
            get
            {
                return BitOffset % MemorySpan.BitPerByte;
            }
        }

        public bool IsNegative { get { return BitOffset < 0; } }



        public int CompareTo(Adress other)
        {
            if (BitOffset > other.BitOffset)
                return 1;
            if (BitOffset < other.BitOffset)
                return -1;
            else
                return 0;
        }
    }
}
