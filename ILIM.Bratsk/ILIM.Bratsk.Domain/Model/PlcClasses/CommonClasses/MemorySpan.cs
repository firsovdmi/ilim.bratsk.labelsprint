﻿using System;

namespace ILIM.Bratsk.Domain.Model.PlcClasses.CommonClasses
{
    [Serializable]
    public struct MemorySpan : IComparable, IComparable<MemorySpan>, IEquatable<MemorySpan>, IFormattable
    {
        public const long BitPerByte = 0x000000008;
        public const long BitPerKilobyte = 0x000002000;
        public const long BitPerMegabyte = 0x000800000;
        public const long BitPerGigabyte = 0x200000000;

        public MemorySpan(long totalBits)
        {
            _sizeInBit = totalBits;
        }

        public MemorySpan(long gigabytes, long megabytes, long kilobytes, long bytes, long bits)
            : this(gigabytes * BitPerGigabyte + megabytes * BitPerMegabyte + kilobytes * BitPerKilobyte + bytes * BitPerByte + bits)
        {
        }
        public MemorySpan(long bytes, long bits)
            : this(bytes * BitPerByte + bits)
        {
        }

        readonly long _sizeInBit;
        public long Bytes
        {
            get
            {
                return (_sizeInBit % BitPerKilobyte) / BitPerByte;
            }
        }
        public long Kilobytes
        {
            get
            {
                return (_sizeInBit % BitPerMegabyte) / BitPerKilobyte;
            }
        }
        public long Megabytes
        {
            get
            {
                return (_sizeInBit % BitPerGigabyte) / BitPerMegabyte;
            }
        }
        public long Gigabytes
        {
            get
            {
                return _sizeInBit / BitPerGigabyte;
            }
        }

        public long Bits
        {
            get
            {
                return _sizeInBit % BitPerByte;
            }
        }

        public double TotalBytes
        {
            get
            {
                return _sizeInBit / (double)BitPerByte;
            }
        }

        public double TotalKiloBytes
        {
            get
            {
                return _sizeInBit / (double)BitPerKilobyte;
            }
        }

        public double TotalMegaBytes
        {
            get
            {
                return _sizeInBit / (double)BitPerMegabyte;
            }
        }

        public double TotalGifaBytes
        {
            get
            {
                return _sizeInBit / (double)BitPerGigabyte;
            }
        }

        public long TotalBits
        {
            get
            {
                return _sizeInBit;
            }
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return string.Format("{0}GB {1}MB {2}KB {3}B {4}b", Gigabytes, Megabytes, Kilobytes, Bytes, Bits);
        }

        public override string ToString()
        {
            return ToString("GB MB KB B b", null);
        }


        public override int GetHashCode()
        {
            return TotalBits.GetHashCode();
        }

        #region Утилитные расширения бинарных операций


        public override bool Equals(object obj)
        {
            return (obj is MemorySpan) && ((MemorySpan)obj).TotalBits == TotalBits;
        }

        public bool Equals(MemorySpan other)
        {
            return this.Equals((object)other);
        }

        public static bool operator ==(MemorySpan ms1, MemorySpan ms2)
        {
            return ms1.Equals(ms2);
        }

        public static bool operator !=(MemorySpan ms1, MemorySpan ms2)
        {
            return !ms1.Equals(ms2);
        }

        public static bool operator >(MemorySpan ms1, MemorySpan ms2)
        {
            return ms1.TotalBits > ms2.TotalBits;
        }

        public static bool operator >=(MemorySpan ms1, MemorySpan ms2)
        {
            return ms1.TotalBits >= ms2.TotalBits;
        }

        public static bool operator <(MemorySpan ms1, MemorySpan ms2)
        {
            return ms1.TotalBits < ms2.TotalBits;
        }

        public static bool operator <=(MemorySpan ms1, MemorySpan ms2)
        {
            return ms1.TotalBits <= ms2.TotalBits;
        }

        public static MemorySpan operator +(MemorySpan ms1, MemorySpan ms2)
        {
            return new MemorySpan(ms1.TotalBits + ms2.TotalBits);
        }

        public static MemorySpan operator -(MemorySpan ms1, MemorySpan ms2)
        {
            return new MemorySpan(ms1.TotalBits - ms2.TotalBits);
        }
        public static implicit operator int(MemorySpan x)
        {
            return (int)x.TotalBits;
        }

        #endregion

        #region Утилитные расширения унарных операций

        public static MemorySpan operator -(MemorySpan ms1)
        {
            return new MemorySpan(-ms1.TotalBits);
        }

        #endregion



        public int CompareTo(MemorySpan other)
        {
            long diff = this.TotalBits - other.TotalBits;
            if (diff == 0) return 0;
            if (diff > 0) return 1;
            return -1;
        }

        public int CompareTo(object obj)
        {
            if (!(obj is MemorySpan))
            {
                throw new System.ArgumentException();
            }
            return this.CompareTo((MemorySpan)obj);
        }
    }
}
