﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ILIM.Bratsk.Infrastructure.Log;

namespace ILIM.Bratsk.Domain.Model.Alarms
{
    public class AlarmCash : IAlarmCash
    {
        private readonly ILogService _logService;
        public AlarmCash(ILogService logService)
        {
            _logService = logService;
        }
        private List<AlarmItem> _items = new List<AlarmItem>();

        public List<AlarmItem> Items
        {
            get
            {
                return _items;
            }
            set { _items = value; }
        }



        public bool AddItem(AlarmItem newItem)
        {
            if (Items.Any(p =>
                          p.Text == newItem.Text &&
                          p.MessageSource == newItem.MessageSource
                )) return false;
            _logService.Log(newItem.AlarmLevel, newItem.Text, "ProcessMessage", newItem.MessageSource);
            Items.Add(newItem);
            return true;
        }

        public void RemoveItem(AlarmItem removeItem)
        {
            Items.Remove(removeItem);
        }


        public bool AddItem(string text, LogingLevel level, string messageSource)
        {
            return AddItem(new AlarmItem
                  {
                      Id = Guid.NewGuid(),
                      AlarmLevel = level,
                      Text = text,
                      MessageSource = messageSource,
                      CreateTime = DateTime.UtcNow
            });
        }
    }
}
