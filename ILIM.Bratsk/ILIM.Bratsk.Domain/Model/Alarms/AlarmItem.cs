﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ILIM.Bratsk.Infrastructure.Log;

namespace ILIM.Bratsk.Domain.Model.Alarms
{
    [Serializable]
    [DataContract]
    public class AlarmItem
    {
        public AlarmItem()
        {
            Id = Guid.NewGuid();
        }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public DateTime CreateTime { get; set; }
        [DataMember]
        public string MessageSource { get; set; }
        [DataMember]
        public LogingLevel AlarmLevel { get; set; }
        [DataMember]
        public bool IsAcknowledge { get; set; }
    }
}
