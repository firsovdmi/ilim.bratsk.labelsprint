﻿using System;
using System.Runtime.Serialization;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [DataContract]
    public class AccountData : EntityBase
    {
        [DataMember]
        public Guid UserID { get; set; }

        [DataMember]
        public string DataID { get; set; }

        [DataMember]
        public string Data { get; set; }
    }
}