﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [Serializable]
    [DataContract]
    public class Event : Entity
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventID { get; set; }
        [DataMember]
        public DateTime EventBegin { get; set; }
        [DataMember]
        public DateTime EventEnd { get; set; }
        [DataMember]
        public EventStatusEnum Status { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(1000)]
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public EventSourceEnum CmdSource { get; set; }
        [DataMember]
        public EventTypeEnum EventType { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        [DataMember]
        public string BatchCode { get; set; }
        [DataMember]
        public int PackFirst { get; set; }
        [DataMember]
        public int PackLast { get; set; }
        [DataMember]
        public PrinterModeEnum Empt { get; set; }
    

    }
}
