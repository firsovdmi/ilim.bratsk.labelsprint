﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Model
{
    [DataContract]
    public class Report : Entity
    {
        [DataMember] private byte[] _reportAssembly;
        [DataMember] private byte[] _reportTemplate;
        [DataMember] private RequestType _requestType;
        [DataMember] private UserReportRequestParameters _userReportRequestParameter;
        [DataMember] private List<UserReportRequestParameters> _userReportRequestParameters;
        [DataMember] private Guid? _userReportRequestParametersID;

        /// <summary>
        ///     Ссылка на запрос
        /// </summary>
        public RequestType RequestType
        {
            get { return _requestType; }
            set
            {
                if (value == _requestType) return;
                _requestType = value;
                OnPropertyChanged("RequestType");
            }
        }

        /// <summary>
        ///     Сборка отчета
        /// </summary>
        public byte[] ReportAssembly
        {
            get { return _reportAssembly; }
            set
            {
                if (Equals(value, _reportAssembly)) return;
                _reportAssembly = value;
                OnPropertyChanged("ReportAssembly");
            }
        }

        /// <summary>
        ///     Шаблон отчета
        /// </summary>
        public byte[] ReportTemplate
        {
            get { return _reportTemplate; }
            set
            {
                if (Equals(value, _reportTemplate)) return;
                _reportTemplate = value;
                OnPropertyChanged("ReportTemplate");
            }
        }

        /// <summary>
        ///     Параметры запроса
        /// </summary>
        public UserReportRequestParameters UserReportRequestParameter
        {
            get { return _userReportRequestParameter; }
            set
            {
                if (Equals(value, _userReportRequestParameter)) return;
                _userReportRequestParameter = value;
                OnPropertyChanged("UserReportRequestParameter");
            }
        }

        /// <summary>
        ///     Параметры запроса
        /// </summary>
        public virtual List<UserReportRequestParameters> UserReportRequestParameters
        {
            get { return _userReportRequestParameters; }
            set
            {
                if (Equals(value, _userReportRequestParameters)) return;
                _userReportRequestParameters = value;
                OnPropertyChanged("UserReportRequestParameters");
            }
        }
    }
}