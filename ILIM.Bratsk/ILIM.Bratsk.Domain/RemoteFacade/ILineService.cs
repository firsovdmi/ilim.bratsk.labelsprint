﻿using System.ServiceModel;
using ILIM.Bratsk.Domain.Model;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface ILineService : IDictionaryService<Line>
    {
        [OperationContract]
        void SetFixetWeightMode(int lineNumber);
        [OperationContract]
        void ResetFixetWeightMode(int lineNumber);
    }
}