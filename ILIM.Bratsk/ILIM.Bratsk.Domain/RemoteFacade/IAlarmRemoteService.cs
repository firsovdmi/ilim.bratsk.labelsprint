﻿using System.Collections.Generic;
using System.ServiceModel;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Model.Alarms;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IAlarmRemoteService
    {
        [OperationContract]
        List<AlarmItem> GetAlarmItems();
        [OperationContract]
        AlarmItem GetLastAlarm();
        [OperationContract]
        void ResetAlarm(AlarmItem item);
    }
}