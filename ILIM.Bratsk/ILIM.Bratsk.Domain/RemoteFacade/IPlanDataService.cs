﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Helpers;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IPlanDataService : IEntityService<PlanData>
    {
        [OperationContract]
        List<PlanData> GetUndeleted();

        [OperationContract]
        List<string> GetDestinctBatchCodes();

        [OperationContract]
        List<string> GetWaitingDestinctBatchCodes();

        [OperationContract]
        Command GetCommand(string batchCode);

        [OperationContract]
        CurrentInfo GetCurrentInfo();

        [OperationContract]
        List<GroupedPlan> GetGroupedPlan();
    }
}
