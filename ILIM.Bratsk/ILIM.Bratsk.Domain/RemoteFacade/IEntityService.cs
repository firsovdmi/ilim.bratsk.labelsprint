﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IEntityService<T> //where T : IEntity
    {
        /// <summary>
        ///     Получить список сущностей
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<T> Get();

        /// <summary>
        ///     Создать новую сущность
        /// </summary>
        /// <param name="entity"></param>
        [OperationContract]
        T Create(T entity);

        /// <summary>
        ///     обновить сущность
        /// </summary>
        /// <param name="entity"></param>
        [OperationContract]
        T Update(T entity);

        /// <summary>
        ///     Удалить сущность
        /// </summary>
        /// <param name="entity"></param>
        [OperationContract]
        void Delete(T entity);

        /// <summary>
        ///     Обновить список сущностей
        /// </summary>
        /// <param name="entity"></param>
        [OperationContract]
        List<T> UpdateList(List<T> entity);

        /// <summary>
        ///     Удалить список сущностей
        /// </summary>
        /// <param name="entity"></param>
        [OperationContract]
        void DeleteList(List<T> entity);

        /// <summary>
        ///     Создать список сущностей
        /// </summary>
        /// <param name="entity"></param>
        [OperationContract]
        List<T> CreateList(List<T> entity);

        /// <summary>
        ///     Получить сущность по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        T GetByID(Guid id);
    }
}