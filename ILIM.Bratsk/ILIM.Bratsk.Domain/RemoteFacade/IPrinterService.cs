﻿using System.ServiceModel;
using ILIM.Bratsk.Domain.Model;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IPrinterService : IDictionaryService<Printer>
    {
        [OperationContract]
        void ApplySetting(Printer printer);
    }
}