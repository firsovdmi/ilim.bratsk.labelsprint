using System;
using System.Collections.Generic;
using System.ServiceModel;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IDictionaryService<T> : IEntityService<T> where T : IEntity
    {
        [OperationContract]
        List<ShortEntity> GetShortList();

        [OperationContract]
        ShortEntity GetShort(Guid id);
    }
}