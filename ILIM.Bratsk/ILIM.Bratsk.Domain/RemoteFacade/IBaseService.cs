﻿using System.Collections.Generic;

namespace PAG.WBD.Milk.Domain.RemoteFacade
{
    public interface IBaseService<T>
    {
        List<T> Get();
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Update(List<T> entity);
        void Delete(List<T> entity);
        void Create(List<T> entity);
    }
}