﻿using System.Collections.Generic;
using System.ServiceModel;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IPlcAcessRemoteService
    {
        [OperationContract]
        BatchDirectJobs ProcessJobs(BatchDirectJobs jobs, string ip, int rack, int slot);
        [OperationContract]
        List<PlcKipaItemRemote> GetCahsedItems();
        [OperationContract]
        List<PlcCommonLineDataRemote> GetCahsedCommonData();
        [OperationContract]
        DebugPlcMonitorRemote GetCahsedDebugPlcMonitor();
        [OperationContract]
        void RefreshCashedItems();
        [OperationContract]
        void RemoveKipa(PlcKipaItemRemote item);
        [OperationContract]
        void ResetError(PlcCommonLineDataRemote line);
        [OperationContract]
        void SetWeightSetPointAndDelta(PlcCommonLineDataRemote line);
        [OperationContract]
        void SetNoTrackingKips(PlcCommonLineDataRemote line);
        [OperationContract]
        void ResetNoTrackingKips(PlcCommonLineDataRemote line);
        [OperationContract]
        int GetScanCount();
    }
}