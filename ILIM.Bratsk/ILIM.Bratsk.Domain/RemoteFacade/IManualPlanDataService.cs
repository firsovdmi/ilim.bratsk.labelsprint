﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Helpers;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IManualPlanDataService : IEntityService<ManualPlanData>
    {
        [OperationContract]
        void RemoveAll();
        [OperationContract]
        void SetNewList(List<ManualPlanData> newItems);
    }
}
