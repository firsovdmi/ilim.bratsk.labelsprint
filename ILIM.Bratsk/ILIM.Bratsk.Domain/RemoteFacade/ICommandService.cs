﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ILIM.Bratsk.Domain.Model;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface ICommandService : IEntityService<Command>
    {
    }
}
