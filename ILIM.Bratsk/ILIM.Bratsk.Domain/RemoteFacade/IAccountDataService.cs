using System.ServiceModel;
using ILIM.Bratsk.Domain.Model;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IAccountDataService : IEntityService<AccountData>
    {
        [OperationContract]
        AccountData GetCurrentUserData(string dataID);

        [OperationContract]
        void UpdateOrCreateCurrentUserDataByDataID(AccountData accountData);
    }
}