﻿using System.ServiceModel;
using ILIM.Bratsk.Domain.Model;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IApplicationSettingsService : IDictionaryService<ApplicationSettings>
    {
        [OperationContract]
        string GetByName(string name);
    }
}