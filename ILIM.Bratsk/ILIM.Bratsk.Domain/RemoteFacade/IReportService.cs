using System;
using System.Collections.Generic;
using System.ServiceModel;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    /// <summary>
    ///     ������ ������� � �������
    /// </summary>
    [ServiceContract]
    public interface IReportService : IEntityService<Report>
    {
        /// <summary>
        ///     �������� ������ ������� � ����������� ����
        /// </summary>
        /// <returns>������ �������</returns>
        [OperationContract]
        List<ShortEntity> GetShortList();

        /// <summary>
        ///     �������� ����� � ����������� ����
        /// </summary>
        /// <param name="id">������������� ������</param>
        /// <returns>����� � ����������� ����</returns>
        [OperationContract]
        ShortEntity GetShort(Guid id);

        /// <summary>
        ///     �������� ������ ������� � ����������� ����, ��� ������������� ���� �������
        /// </summary>
        /// <returns>������ �������</returns>
        [OperationContract]
        List<ShortEntity> GetShortListByRequestType(RequestType type);
    }
}