﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ILIM.Bratsk.Infrastructure.Helpers;

namespace ILIM.Bratsk.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IMatthewsMperiaService
    {
        /// <summary>
        /// Запускает печать на принтере
        /// </summary>
        /// <param name="lineNumber">Номер линии принтера</param>
        /// <param name="installationName">Имя печатающей головки (Если не указывать, применяется ко всем головкам)</param>
        /// <returns>В случае успешной отправки комманды, возвращает "True"</returns>
        [OperationContract]
        bool ActivateTriger(int lineNumber, string installationName = null);

        /// <summary>
        /// Изменяет шаблон этикетки
        /// </summary>
        /// <param name="lineNumber">Номер линии принтера</param>
        /// <param name="messageName">Название новой этикетки (если не указано, то снимается назначение этикетки)</param>
        /// <param name="installationName">Имя печатающей головки (Если не указывать, применяется ко всем головкам)</param>
        /// <returns>В случае успешной отправки комманды, возвращает "True"</returns>ы
        [OperationContract]
        bool MessageSelecting(int lineNumber, string messageName = null, string installationName = null);

        /// <summary>
        /// Запрашивает статус печатающей головки "STARTED", "PAUSED" и т.д.
        /// </summary>
        /// <param name="lineNumber">Номер линии принтера</param>
        /// <param name="installationName">Имя печатающей головки</param>
        /// <returns>Статус печатающей головки "STARTED", "PAUSED" и т.д.</returns>
        [OperationContract]
        List<PrinterErrorItem> GetPrinterStatus(int lineNumber, string installationName);

        /// <summary>
        /// Получает значение счетчика
        /// </summary>
        /// <param name="lineNumber">Номер линии принтера</param>
        /// <param name="counterName">Имя счетчика</param>
        /// <returns>Показанич счетчика или -1 в случае ошибки</returns>
        [OperationContract]
        int GetCounter(int lineNumber);

        /// <summary>
        /// Устанавливает значение переменной по имени
        /// </summary>
        /// <param name="lineNumber">Номер линии принтера</param>
        /// <param name="variableName">Имя переменной</param>
        /// <param name="value">Значение переменной</param>
        /// <returns>В случае успешной отправки комманды, возвращает "True"</returns>
        [OperationContract]
        bool SetVariable(int lineNumber, string variableName, string value);
        
        /// <summary>
        /// Проверяет, изменилось ли значение счетчика с момента последнего вызова или с момента запуска сервиса
        /// </summary>
        /// <param name="printerNumber">Номер линии принтера</param>
        /// <param name="counterName">Название счетчика</param>
        /// <returns>Если значение изменилось, возвращается "True"</returns>
        [OperationContract]
        bool IsCounterChange(int printerNumber);

        /// <summary>
        /// Проверяет изменение счетчика по всем линиям
        /// </summary>
        [OperationContract]
        void CheckPrinterState();

        /// <summary>
        /// Запрос на отправку в принтер новой этикетки
        /// </summary>
        /// <param name="printerNumber">Номер линии принтера</param>
        /// <param name="weight">Вес новой кипы</param>
        /// <param name="date">Дата и время взвешивания</param>
        /// <returns>Если принтер уже напчатал предыдущее задание, новая кипа добавляется в ожидание и вовращается "True". Если предыдущая кипа еще не напечатана (слот ожидания занят), возвращается "False"</returns>
        [OperationContract]
        int TryAddNewKipa(int printerNumber, int lineNumber, float weight, DateTime date);

        /// <summary>
        /// Проверяет, существуют ли заданные кипы в таблице Факт
        /// </summary>
        /// <param name="batchCode">Код партии</param>
        /// <param name="first">Первая кипа в партии</param>
        /// <param name="last">Последняя кипа в партии</param>
        /// <returns>True, если найдена хоть одна кипа в факте</returns>
        [OperationContract]
        bool CheckIsExistKips(string batchCode, int first, int last);

        [OperationContract]
        void StartSpecMode(int printerNumber);
        [OperationContract]
        void StopSpecMode(int printerNumber);
    }
}
