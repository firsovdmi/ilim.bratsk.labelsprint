using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Domain.Repository
{
    public interface IRepositoryWithMarkedAsDelete<T> : IRepositoryEntity<T> where T : IEntity
    {
        T MarkAsDelete(T entity);
    }

    public interface IRepository
    {
    }
}