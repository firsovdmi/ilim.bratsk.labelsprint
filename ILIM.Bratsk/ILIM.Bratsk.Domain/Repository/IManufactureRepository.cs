using ILIM.Bratsk.Domain.Model;

namespace ILIM.Bratsk.Domain.Repository
{
    public interface IManufactureRepository : IRepositoryWithMarkedAsDelete<Manufacture>
    {

    }
}