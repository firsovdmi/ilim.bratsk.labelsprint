using ILIM.Bratsk.Domain.Model;

namespace ILIM.Bratsk.Domain.Repository
{
    public interface IAccountDataRepository : IRepositoryEntity<AccountData>
    {
    }
}