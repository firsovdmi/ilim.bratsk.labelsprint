using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;

namespace ILIM.Bratsk.Domain
{
    public interface IModule
    {
    }

    public interface IRepositoryFactory
    {
        T Create<T>(IUnitOfWork unitOfWork) where T : IRepository;
        void Release(object obj);
    }
}