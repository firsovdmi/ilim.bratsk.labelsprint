﻿using System.Windows;
using System.Windows.Controls;
using ILIM.Bratsk.Visualization.ViewModels;

namespace ILIM.Bratsk.Visualization.Views
{
    /// <summary>
    ///     Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            InitializeComponent();
        }

        private void LoginView_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldValue = e.OldValue as LoginViewModel;
            if (oldValue != null)
                oldValue.RefreshPasswordEvent -= logiViewModel_RefreshPasswordEvent;
            var newValue = e.NewValue as LoginViewModel;
            if (newValue != null)
                newValue.RefreshPasswordEvent += logiViewModel_RefreshPasswordEvent;
        }

        private void logiViewModel_RefreshPasswordEvent(object sender, RefreshPasswordEventArgs e)
        {
            Password.Password = e.RefreshPassword;
        }

        private void LoginView_OnUnloaded(object sender, RoutedEventArgs e)
        {
            var logiViewModel = DataContext as LoginViewModel;
            if (logiViewModel != null)
                logiViewModel.RefreshPasswordEvent -= logiViewModel_RefreshPasswordEvent;
        }
    }
}