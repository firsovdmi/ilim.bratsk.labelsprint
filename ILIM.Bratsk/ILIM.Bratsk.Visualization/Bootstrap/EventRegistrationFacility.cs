using Caliburn.Micro;
using Castle.Core;
using Castle.MicroKernel.Facilities;

namespace ILIM.Bratsk.Visualization.Bootstrap
{
    internal class EventRegistrationFacility : AbstractFacility
    {
        private IEventAggregator _eventAggregator;

        protected override void Init()
        {
            Kernel.ComponentCreated += ComponentCreated;
            Kernel.ComponentDestroyed += ComponentDestroyed;
        }

        private void ComponentCreated(ComponentModel model, object instance)
        {
            if (!(instance is IHandle)) return;
            if (_eventAggregator == null) _eventAggregator = Kernel.Resolve<IEventAggregator>();
            _eventAggregator.Subscribe(instance);
        }

        private void ComponentDestroyed(ComponentModel model, object instance)
        {
            if (!(instance is IHandle)) return;
            if (_eventAggregator == null) return;
            _eventAggregator.Unsubscribe(instance);
        }
    }
}