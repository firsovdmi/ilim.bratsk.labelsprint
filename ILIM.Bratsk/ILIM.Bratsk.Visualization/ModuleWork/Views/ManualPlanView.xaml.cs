﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ILIM.Bratsk.Visualization.ModuleWork.Views
{
    /// <summary>
    /// Interaction logic for ManualPlanView.xaml
    /// </summary>
    public partial class ManualPlanView : UserControl
    {
        public ManualPlanView()
        {
            InitializeComponent();
        }
    }
}
