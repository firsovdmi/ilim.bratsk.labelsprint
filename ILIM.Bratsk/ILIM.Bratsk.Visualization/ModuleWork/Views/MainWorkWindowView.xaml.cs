﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ModuleWork.Views
{
    /// <summary>
    /// Interaction logic for MainWorkWindowView.xaml
    /// </summary>
    public partial class MainWorkWindowView : UserControl
    {
        public MainWorkWindowView()
        {
            InitializeComponent();
        }
    }
}
