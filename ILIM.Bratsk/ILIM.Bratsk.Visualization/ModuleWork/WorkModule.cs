﻿
using System.Collections.Generic;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleWork.MenuAction;

namespace ILIM.Bratsk.Visualization.ModuleWork
{
    public class WorkModule : IModule
    {
        public WorkModule(IMenuManager menuManager,
            IEnumerable<WorkMenuAction> actionItems
            )
        {
            menuManager.ShowItem(new ActionItem("Работа", null), 1);
            ShowMainWorkWindowMenuAction topTab=null;
            foreach (var actionItem in actionItems)
            {
                menuManager.WithParent("Работа")
                    .ShowItem(actionItem);
                if (actionItem is ShowScadaMenuAction)
                {
                    actionItem.Execute();
                }
                if (actionItem is ShowMainWorkWindowMenuAction )
                {
                    topTab = actionItem as ShowMainWorkWindowMenuAction;
                }
            }
            if (topTab != null) topTab.Execute();
        }
    }
}