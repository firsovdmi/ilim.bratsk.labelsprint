﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.MenuAction;
using ILIM.Bratsk.Visualization.ModuleManager;
using ILIM.Bratsk.Visualization.ModuleWork.MenuAction;
using ILIM.Bratsk.Visualization.ModuleWork.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleWork
{
    public class WorkModuleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IModule>().ImplementedBy<WorkModule>());
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof(WorkMenuAction<>))
                    .If(p => p != typeof(WorkMenuAction<>))
                    .WithServices(typeof(WorkMenuAction)));

            container.Register(
        Component.For<MainWorkWindowViewModel>()
            .ImplementedBy<MainWorkWindowViewModel>()
            .LifestyleCustom<RealisableSingletoneLifestyleManager>());

            container.Register(
        Component.For<ScadaViewModel>()
            .ImplementedBy<ScadaViewModel>()
            .LifestyleCustom<RealisableSingletoneLifestyleManager>());
        }
    }
}