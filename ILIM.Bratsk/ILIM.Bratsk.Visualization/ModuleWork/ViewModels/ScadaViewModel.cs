﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.Model;
using System.Threading;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Service.Services;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class ScadaViewModel : Screen
    {
        private const int ScanIntervalInMilliseconds = 500;
        private ScadaScaner _scadaScaner;
        public ConveerItemsViewModel Line1 { get; set; }
        public ConveerItemsViewModel Line2 { get; set; }
        public ConveerItemsViewModel Line3 { get; set; }
        public ConveerItemsViewModel Line4 { get; set; }
        public bool TurnLeft { get; set; }
        public bool TurnRight { get; set; }
        public bool TurnRight2 { get; set; }
        public int DeleteCount1 { get; set; }
        public int DeleteCount3 { get; set; }
        public int DeleteCount4 { get; set; }

        private readonly IPlcAcessRemoteService _plcAcessRemoteService;
        private readonly IWindowManager _windowManager;
        ILineService _lineService;
        public int PlcScanCount { get; set; }
        public int ScadaScanCount { get; set; }

        public ScadaViewModel(IPlcAcessRemoteService plcAcessRemoteService, IWindowManager windowManager, ILineService lineService)
        {
            _windowManager = windowManager;
            _lineService = lineService;
            _plcAcessRemoteService = plcAcessRemoteService;
            Line1 = new ConveerItemsViewModel();
            Line2 = new ConveerItemsViewModel();
            Line3 = new ConveerItemsViewModel();
            Line4 = new ConveerItemsViewModel();
        }

        ScadaScaner Scaner
        {
            get { return _scadaScaner ?? (_scadaScaner = new ScadaScaner(ScanIntervalInMilliseconds)); }
            set { _scadaScaner = value; }
        }

        void _scadaScaner_UpdateScadaHandler()
        {
            var newItems = _plcAcessRemoteService.GetCahsedItems();
            Line1.UpdateItems(newItems.Where(p => p.CurrentLineNumber == 1).Select(p => new PlcKipaViewModel(_plcAcessRemoteService, _windowManager) { Item = p }));
            Line2.UpdateItems(newItems.Where(p => p.CurrentLineNumber == 2).Select(p => new PlcKipaViewModel(_plcAcessRemoteService, _windowManager) { Item = p }));
            Line3.UpdateItems(newItems.Where(p => p.CurrentLineNumber == 3).Select(p => new PlcKipaViewModel(_plcAcessRemoteService, _windowManager) { Item = p }));
            Line4.UpdateItems(newItems.Where(p => p.CurrentLineNumber == 4).Select(p => new PlcKipaViewModel(_plcAcessRemoteService, _windowManager) { Item = p }));

            var commonDatas = _plcAcessRemoteService.GetCahsedCommonData();
            PlcScanCount = _plcAcessRemoteService.GetScanCount();

            Line1.GetCommonData(1, commonDatas);
            Line2.GetCommonData(2, commonDatas);
            Line3.GetCommonData(3, commonDatas);
            Line4.GetCommonData(4, commonDatas);

            NotifyOfPropertyChange(() => Line1);
            NotifyOfPropertyChange(() => Line2);
            NotifyOfPropertyChange(() => Line3);
            NotifyOfPropertyChange(() => Line4);
            NotifyOfPropertyChange(() => PlcScanCount);
            if (ScadaScanCount == int.MaxValue) ScadaScanCount = 0;
            ScadaScanCount++;
            NotifyOfPropertyChange(() => ScadaScanCount);

            var debugPlc = _plcAcessRemoteService.GetCahsedDebugPlcMonitor();
            TurnLeft = debugPlc.TurnLeft;
            TurnRight = debugPlc.TurnRight;
            TurnRight2 = debugPlc.TurnRight2;
            NotifyOfPropertyChange(() => TurnLeft);
            NotifyOfPropertyChange(() => TurnRight);
            NotifyOfPropertyChange(() => TurnRight2);
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            DisplayName = "Визуализация";
            Scaner.UpdateScadaHandler += _scadaScaner_UpdateScadaHandler;
            Scaner.StartScan();
        }

        protected override void OnDeactivate(bool close)
        {
            Scaner.StopScan();
            Scaner.UnSubscribe();
            base.OnDeactivate(close);
        }

        private RelayCommand _stopTrackingKipsLine1Command;
        public RelayCommand StopTrackingKipsLine1Command
        {
            get
            {
                return _stopTrackingKipsLine1Command ??
                       (_stopTrackingKipsLine1Command =
                        new RelayCommand(ExecuteStopTrackingKipsLine1Command, CanStopTrackingKipsLine1Command, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private bool CanStopTrackingKipsLine1Command(object obj)
        {
            return true;
        }

        private void ExecuteStopTrackingKipsLine1Command(object obj)
        {
            SetNoTrackingKips(Line1);
        }


        private RelayCommand _startTrackingKipsLine1Command;
        public RelayCommand StartTrackingKipsLine1Command
        {
            get
            {
                return _startTrackingKipsLine1Command ??
                       (_startTrackingKipsLine1Command =
                        new RelayCommand(ExecuteStartTrackingKipsLine1Command, CanStartTrackingKipsLine1Command, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private bool CanStartTrackingKipsLine1Command(object obj)
        {
            return true;
        }

        private void ExecuteStartTrackingKipsLine1Command(object obj)
        {
            ResetNoTrackingKips(Line1);
        }







        private RelayCommand _stopTrackingKipsLine2Command;
        public RelayCommand StopTrackingKipsLine2Command
        {
            get
            {
                return _stopTrackingKipsLine2Command ??
                       (_stopTrackingKipsLine2Command =
                        new RelayCommand(ExecuteStopTrackingKipsLine2Command, CanStopTrackingKipsLine2Command, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private bool CanStopTrackingKipsLine2Command(object obj)
        {
            return true;
        }

        private void ExecuteStopTrackingKipsLine2Command(object obj)
        {
            SetNoTrackingKips(Line2);
        }


        private RelayCommand _startTrackingKipsLine2Command;
        public RelayCommand StartTrackingKipsLine2Command
        {
            get
            {
                return _startTrackingKipsLine2Command ??
                       (_startTrackingKipsLine2Command =
                        new RelayCommand(ExecuteStartTrackingKipsLine2Command, CanStartTrackingKipsLine2Command, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private bool CanStartTrackingKipsLine2Command(object obj)
        {
            return true;
        }

        private void ExecuteStartTrackingKipsLine2Command(object obj)
        {
            ResetNoTrackingKips(Line2);
        }






        private RelayCommand _stopTrackingKipsLine3Command;
        public RelayCommand StopTrackingKipsLine3Command
        {
            get
            {
                return _stopTrackingKipsLine3Command ??
                       (_stopTrackingKipsLine3Command =
                        new RelayCommand(ExecuteStopTrackingKipsLine3Command, CanStopTrackingKipsLine3Command, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private bool CanStopTrackingKipsLine3Command(object obj)
        {
            return true;
        }

        private void ExecuteStopTrackingKipsLine3Command(object obj)
        {
            SetNoTrackingKips(Line3);
        }


        private RelayCommand _startTrackingKipsLine3Command;
        public RelayCommand StartTrackingKipsLine3Command
        {
            get
            {
                return _startTrackingKipsLine3Command ??
                       (_startTrackingKipsLine3Command =
                        new RelayCommand(ExecuteStartTrackingKipsLine3Command, CanStartTrackingKipsLine3Command, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private bool CanStartTrackingKipsLine3Command(object obj)
        {
            return true;
        }

        private void ExecuteStartTrackingKipsLine3Command(object obj)
        {
            ResetNoTrackingKips(Line3);
        }







        private RelayCommand _stopTrackingKipsLine4Command;
        public RelayCommand StopTrackingKipsLine4Command
        {
            get
            {
                return _stopTrackingKipsLine4Command ??
                       (_stopTrackingKipsLine4Command =
                        new RelayCommand(ExecuteStopTrackingKipsLine4Command, CanStopTrackingKipsLine4Command, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private bool CanStopTrackingKipsLine4Command(object obj)
        {
            return true;
        }

        private void ExecuteStopTrackingKipsLine4Command(object obj)
        {
            SetNoTrackingKips(Line4);
        }


        private RelayCommand _startTrackingKipsLine4Command;
        public RelayCommand StartTrackingKipsLine4Command
        {
            get
            {
                return _startTrackingKipsLine4Command ??
                       (_startTrackingKipsLine4Command =
                        new RelayCommand(ExecuteStartTrackingKipsLine4Command, CanStartTrackingKipsLine4Command, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private bool CanStartTrackingKipsLine4Command(object obj)
        {
            return true;
        }

        private void ExecuteStartTrackingKipsLine4Command(object obj)
        {
            ResetNoTrackingKips(Line4);
        }




        void SetNoTrackingKips(ConveerItemsViewModel line)
        {
            var confirmModel = new YesNoDialogViewModel
            {
                DisplayName = "Подтверждение отключения отслеживания",
                Message = "Вы действительно хотите отменить отслеживание кип и печатать фиксированный вес?"
            };
            if (_windowManager.ShowDialog(confirmModel) != true) return;
            _plcAcessRemoteService.SetNoTrackingKips(line.CommonData);
            _lineService.SetFixetWeightMode(line.CommonData.LineNumber);
        }

        void ResetNoTrackingKips(ConveerItemsViewModel line)
        {
            _lineService.ResetFixetWeightMode(line.CommonData.LineNumber);
            _plcAcessRemoteService.ResetNoTrackingKips(line.CommonData);
        }

    }

    class ScadaScaner : BaseCyclicHandler
    {
        public delegate void UpdateScada();

        public event UpdateScada UpdateScadaHandler;

        public ScadaScaner(int scanIntervalInMilliseconds)
        {
            ScanIntervalInMilliseconds = scanIntervalInMilliseconds;
        }

        protected override void Handler()
        {
            OnUpdateScadaHandler();
        }

        protected virtual void OnUpdateScadaHandler()
        {
            var handler = UpdateScadaHandler;
            if (handler != null) handler();
        }

        public void UnSubscribe()
        {
            var handler = UpdateScadaHandler;
            if (handler == null) return;
            Delegate[] clientList = handler.GetInvocationList();

            foreach (var d in clientList)
            {
                var subscriber = (d as UpdateScada);
                if (subscriber != null)
                    handler -= subscriber;
            }
        }


    }
}