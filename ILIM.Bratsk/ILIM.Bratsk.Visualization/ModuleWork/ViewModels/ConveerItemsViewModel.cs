﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs.BaseClases;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Visualization.Infrastructure;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class ConveerItemsViewModel
    {
        private readonly IPlcAcessRemoteService _plcAcessRemoteService;
        public ConveerItemsViewModel(IPlcAcessRemoteService plcAcessRemoteService=null)
        {
            _plcAcessRemoteService = plcAcessRemoteService;
        }
        private List<PlcKipaViewModel> _items = new List<PlcKipaViewModel>();

        public PlcCommonLineDataRemote CommonData { get; set; }

        public PlcKipaViewModel this[int i]
        {
            get
            {
                var plcItem = _items.FirstOrDefault(p => p.Item.Position == i+1);
                return plcItem;
            }
        }

        public void UpdateItems(IEnumerable<PlcKipaViewModel> newItems)
        {
            _items = newItems.ToList();
        }

        public void GetCommonData(int lineNumber, IEnumerable<PlcCommonLineDataRemote> source)
        {
            CommonData = source.FirstOrDefault(p => p.LineNumber == lineNumber);
        }


        private RelayCommand _resetErrorCommand;
        public RelayCommand ResetErrorCommand
        {
            get
            {
                return _resetErrorCommand ??
                       (_resetErrorCommand =
                        new RelayCommand(ExecuteResetErrorCommand, CanResetErrorCommand, CommandType.Delete,
                                         "Сброс", "Сброс"));
            }
        }

        private void ExecuteResetErrorCommand(object obj)
        {
            if (_plcAcessRemoteService == null) return;
            _plcAcessRemoteService.ResetError(CommonData);
        }

        private bool CanResetErrorCommand(object obj)
        {
            return true;
        }

        private RelayCommand _setWeightSetPointCommand;
        public RelayCommand SetWeightSetPointCommand
        {
            get
            {
                return _setWeightSetPointCommand ??
                       (_setWeightSetPointCommand =
                        new RelayCommand(ExecuteSetWeightSetPointCommand, CanSetWeightSetPointCommand, CommandType.Delete,
                                         "Сброс", "Сброс"));
            }
        }

        private void ExecuteSetWeightSetPointCommand(object obj)
        {
            if (_plcAcessRemoteService == null) return;
            _plcAcessRemoteService.SetWeightSetPointAndDelta(CommonData);
            CommonData.IsEdit = false;
        }

        private bool CanSetWeightSetPointCommand(object obj)
        {
            return true;
        }
    }
}
