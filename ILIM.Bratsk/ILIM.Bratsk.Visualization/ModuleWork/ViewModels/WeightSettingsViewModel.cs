﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ViewModels;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class WeightSettingsViewModel : Screen
    {

        private readonly IWindowManager _windowManager;
        private readonly ILineService _lineService;
        private readonly IPlcAcessRemoteService _plcAcessRemoteService;

        public WeightSettingsViewModel(
            IPlcAcessRemoteService plcAcessRemoteService, IWindowManager windowManager, ILineService lineService)
        {
            _plcAcessRemoteService = plcAcessRemoteService;
            _windowManager = windowManager;
            _lineService = lineService;

            Initialization();
        }
        public ObservableCollection<ConveerItemsViewModel> Lines { get; set; }
        private void Initialization()
        {
            var commonDatas = _plcAcessRemoteService.GetCahsedCommonData();
            Lines = new ObservableCollection<ConveerItemsViewModel>(_lineService.Get().OrderBy(p => p.Name).Select(p => new ConveerItemsViewModel(_plcAcessRemoteService) { CommonData = commonDatas.FirstOrDefault(pp => pp.LineNumber == p.LineNumber) }).ToList());
        }

        private RelayCommand _applyCommand;
        public RelayCommand ApplyCommand
        {
            get
            {
                return _applyCommand ??
                       (_applyCommand =
                        new RelayCommand(ExecuteApplyCommand, CanApplyCommand, CommandType.Delete,
                                         "Применить", "Применить"));
            }
        }

        private void ExecuteApplyCommand(object obj)
        {
            TryClose(true);
        }

        private bool CanApplyCommand(object obj)
        {
            return true;
        }


        private RelayCommand _closeCommand;
        public RelayCommand CloseCommand
        {
            get
            {
                return _closeCommand ??
                       (_closeCommand =
                        new RelayCommand(ExecuteCloseCommand, CanCloseCommand, CommandType.Delete,
                                         "Отмена", "Отмена"));
            }
        }

        private void ExecuteCloseCommand(object obj)
        {

            if (Lines.Any(p => p.CommonData.IsEdit))
            {
                var yesNoModel = new YesNoDialogViewModel();
                yesNoModel.DisplayName = "Предупреждение";
                yesNoModel.Message = "Некоторые данные не записаны в контроллер. \r\nВы действительно хотите закрыть окно?";
                if (_windowManager.ShowDialog(yesNoModel) != true) return;

            }
            TryClose(true);
        }

        private bool CanCloseCommand(object obj)
        {
            return true;
        }

        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView() as RadWindow;
            if (window != null)
                window.Close();
        }
    }
}
