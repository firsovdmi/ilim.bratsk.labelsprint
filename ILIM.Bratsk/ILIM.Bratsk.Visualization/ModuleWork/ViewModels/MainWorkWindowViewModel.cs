﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Model.Alarms;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.DataBaseDirect;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Log;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels;
using ILIM.Bratsk.Visualization.ViewModels;
using PAG.Controls.Wpf.Utils.Localization;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class MainWorkWindowViewModel : Screen, IHandle<AccountDataChanged>
    {
        private const string PlanFactScanIntervalInMillisecondsName = "PlanFactScanIntervalInMilliseconds";
        private const string AlarmScanIntervalInMillisecondsName = "AlarmScanIntervalInMilliseconds";
        private readonly IPlanDataService _planDataService;
        private readonly IFactDataService _factDataService;
        private readonly IApplicationSettingsService _applicationSettingsService;
        private readonly IAlarmRemoteService _alarmRemoteService;
        private readonly IWindowManager _windowManager;
        private readonly IDataBaseDirectService _dataBaseDirectService;
        private readonly IPrinterService _printerService;
        private readonly IManualPlanDataService _manualPlanDataService;
        private BindableCollection<SelectableViewModel<Line>> _lines;
        private BindableCollection<SelectableViewModel<Printer>> _printers;
        private FactFilter _factFilter;
        private readonly ILineService _lineService;
        private readonly IMatthewsMperiaService _matthewsMperiaService;
        private readonly IPlcAcessRemoteService _plcAcessRemoteService;
        private readonly IActiveAccountManager _activeAccountManager;
        private readonly AAAManager _aaaManager;
        private readonly ILoginViewModelFactory _loginViewModelFactory;
        private IServiceDataService _serviceDataService;

        public MainWorkWindowViewModel(
            IPlanDataService planDataService,
            IFactDataService factDataService,
            IApplicationSettingsService applicationSettingsService,
            IAlarmRemoteService alarmRemoteService,
            IWindowManager windowManager,
            IDataBaseDirectService dataBaseDirectService,
            IPrinterService printerService,
            IManualPlanDataService manualPlanDataService,
            ILineService lineService,
            IMatthewsMperiaService matthewsMperiaService,
             IPlcAcessRemoteService plcAcessRemoteService,
             IActiveAccountManager activeAccountManager,
            AAAManager aaaManager, ILoginViewModelFactory loginViewModelFactory,
             IServiceDataService serviceDataService)
        {
            _serviceDataService = serviceDataService;
            _aaaManager = aaaManager;
            _loginViewModelFactory = loginViewModelFactory;
            _activeAccountManager = activeAccountManager;
            _plcAcessRemoteService = plcAcessRemoteService;
            _matthewsMperiaService = matthewsMperiaService;
            _lineService = lineService;
            _manualPlanDataService = manualPlanDataService;
            _printerService = printerService;
            _dataBaseDirectService = dataBaseDirectService;
            _windowManager = windowManager;
            _planDataService = planDataService;
            _factDataService = factDataService;
            _alarmRemoteService = alarmRemoteService;
            _applicationSettingsService = applicationSettingsService;
            Initialization();
        }

        public void Initialization()
        {
            Lines = new BindableCollection<SelectableViewModel<Line>>(_lineService.Get().Select(p => new SelectableViewModel<Line>(p)));
            foreach (var line in Lines)
            {
                line.PropertyChanged += line_PropertyChanged;
            }
            Printers = new BindableCollection<SelectableViewModel<Printer>>(_printerService.Get().Select(p => new SelectableViewModel<Printer>(p)));
            foreach (var Printer in Printers)
            {
                Printer.PropertyChanged += Printer_PropertyChanged;
            }
        }

        void Printer_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            NotifyOfPropertyChange(() => SelectedPrinters);
            FactFilter.PrinterNumbers = Printers.Where(p => p.IsSelected).Select(p => p.Content.Number).ToList();
        }

        void line_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            NotifyOfPropertyChange(() => SelectedLines);
            FactFilter.LinesNumbers = Lines.Where(p => p.IsSelected).Select(p => p.Content.LineNumber).ToList();
        }

        public bool IsCanEditSystemSettings { get { return _activeAccountManager.WhiteListObjects.Any(p => p == "SystemSettings"); } }

        public CurrentInfo CurrentInfo { get; set; }

        public List<GroupedPlan> PlanDatas
        { get { return _planDataService.GetGroupedPlan().OrderBy(p => p.BatchCode).ThenBy(p => p.TimeCreate).ToList(); } }
        //{ get { return _planDataService.Get().Where(p => p.Status != PlanStatus.SendedToPrint).OrderBy(p => p.BatchCode).ThenBy(p => p.PackNum).ToList(); } }

        readonly ObservableCollection<FactData> _factDatas = new ObservableCollection<FactData>();
        public ObservableCollection<FactData> FactDatas
        {
            get
            {

                //return new ObservableCollection<FactData>( _factDataService.GetUndeleted(FactFilter));

                return _factDatas;
            }
        }
        public List<PlanData> CurrentDatas
        { get { return _planDataService.Get().Where(p => p.Status == PlanStatus.SendedToPrint).OrderBy(p => p.BatchCode).ThenBy(p => p.PackNum).ToList(); } }

        private PlanFactScaner _planFactScaner;
        PlanFactScaner PlanFactScaner
        {
            get
            {
                if (_planFactScaner == null)
                {
                    var planFactScanIntervalInMilliseconds = _applicationSettingsService.GetByName(PlanFactScanIntervalInMillisecondsName);
                    _planFactScaner = planFactScanIntervalInMilliseconds.IsNumeric() ? new PlanFactScaner(Convert.ToInt32(planFactScanIntervalInMilliseconds)) : new PlanFactScaner(2000);
                }
                return _planFactScaner;
            }
        }


        public string SelectedLines
        {
            get
            {
                if (Lines != null && Lines.Any(p => p.IsSelected))
                    return String.Join(", ", Lines.Where(p => p.IsSelected).Select(p => p.Content.Name));
                return "";
            }

        }

        public string SelectedPrinters
        {
            get
            {
                if (Printers != null && Printers.Any(p => p.IsSelected))
                    return String.Join(", ", Printers.Where(p => p.IsSelected).Select(p => p.Content.Name));
                return "";
            }

        }

        private AlarmScaner _alarmScaner;
        AlarmScaner AlarmScaner
        {
            get
            {
                if (_alarmScaner == null)
                {
                    var alarmtScanIntervalInMilliseconds = _applicationSettingsService.GetByName(AlarmScanIntervalInMillisecondsName);
                    _alarmScaner = alarmtScanIntervalInMilliseconds.IsNumeric() ? new AlarmScaner(Convert.ToInt32(alarmtScanIntervalInMilliseconds)) : new AlarmScaner(2000);
                }
                return _alarmScaner;
            }
        }


        protected override void OnActivate()
        {
            base.OnActivate();
            DisplayName = "Главное окно";
            PlanFactScaner.UpdateScadaHandler += PlanFactScaner_UpdateScadaHandler;
            PlanFactScaner.StartScan();
            AlarmScaner.UpdateScadaHandler += AlarmScaner_UpdateScadaHandler;
            AlarmScaner.StartScan();
        }

        private AlarmItem _message;

        public AlarmItem Message
        {
            get { return _message; }
            set { _message = value; NotifyOfPropertyChange(() => Message); }
        }

        void AlarmScaner_UpdateScadaHandler()
        {
            Message = _alarmRemoteService.GetLastAlarm();
        }

        void PlanFactScaner_UpdateScadaHandler()
        {
            NotifyOfPropertyChange(() => PlanDatas);
            UpdateFact();
            //NotifyOfPropertyChange(() => FactDatas);
            NotifyOfPropertyChange(() => CurrentDatas);
            CurrentInfo = _planDataService.GetCurrentInfo();
            NotifyOfPropertyChange(() => CurrentInfo);
            //_aAAManager.
        }

        void UpdateFact()
        {

            Application.Current.Dispatcher.BeginInvoke(
DispatcherPriority.Background,
new System.Action(() =>
{
    var serverItems = _factDataService.GetUndeleted(FactFilter);



    foreach (var serverItem in serverItems)
    {
        if (FactDatas.Any(p => p.ID == serverItem.ID)) continue;

        var nextItem = FactDatas.OrderByDescending(p => p.FactTime).FirstOrDefault(p => p.FactTime > serverItem.FactTime);
        if (nextItem != null)
        {
            FactDatas.Insert(FactDatas.IndexOf(nextItem), serverItem);
            continue;
        }
        FactDatas.Insert(0,serverItem);
    }

    foreach (var localItem in FactDatas.ToArray())
    {
        if (serverItems.All(p => p.ID != localItem.ID)) FactDatas.Remove(localItem);
    }
}));
        }

        protected override void OnDeactivate(bool close)
        {
            PlanFactScaner.StopScan();
            PlanFactScaner.UnSubscribe();
            AlarmScaner.StopScan();
            AlarmScaner.UnSubscribe();
            base.OnDeactivate(close);
        }

        private RelayCommand _resetAlarmCommand;
        public RelayCommand ResetAlarmCommand
        {
            get
            {
                return _resetAlarmCommand ??
                       (_resetAlarmCommand =
                        new RelayCommand(ExecuteResetAlarmCommand, CanResetAlarmCommand, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private void ExecuteResetAlarmCommand(object obj)
        {
            _alarmRemoteService.ResetAlarm(Message);
        }

        private bool CanResetAlarmCommand(object obj)
        {
            return Message != null;
        }

        private RelayCommand _createPlanCommand;
        public RelayCommand CreatePlanCommand
        {
            get
            {
                return _createPlanCommand ??
                       (_createPlanCommand =
                        new RelayCommand(ExecuteCreatePlanCommand, CanCreatePlanCommand, CommandType.Delete,
                                         "Создать", "Создать план"));
            }
        }

        private void ExecuteCreatePlanCommand(object obj)
        {
            _windowManager.ShowDialog(new AddNewPlanViewModel(_dataBaseDirectService, _factDataService, _serviceDataService, _matthewsMperiaService, _windowManager) { DisplayName = "Создание плана вручную" }, null, new Dictionary<string, object>() { { "ResizeMode", ResizeMode.NoResize } });
        }

        private bool CanCreatePlanCommand(object obj)
        {
            return true;
        }

        private RelayCommand _editPlanCommand;
        public RelayCommand EditPlanCommand
        {
            get
            {
                return _editPlanCommand ??
                       (_editPlanCommand =
                        new RelayCommand(ExecuteEditPlanCommand, CanEditPlanCommand, CommandType.Delete,
                                         "Редактировать", "Редактировать план"));
            }
        }

        private void ExecuteEditPlanCommand(object obj)
        {
            _windowManager.ShowDialog(new EditPlanViewModel(_dataBaseDirectService, _planDataService) { DisplayName = "Изменить партию" }, null, new Dictionary<string, object>() { { "ResizeMode", ResizeMode.NoResize } });
        }

        private bool CanEditPlanCommand(object obj)
        {
            return true;
        }


        private RelayCommand _logoffCommand;
        public RelayCommand LogoffCommand
        {
            get
            {
                return _logoffCommand ??
                       (_logoffCommand =
                        new RelayCommand(ExecuteLogoffCommand, CanLogoffCommand, CommandType.Delete,
                                         "Выход из системы", "Выход из системы"));
            }
        }

        private void ExecuteLogoffCommand(object obj)
        {
            _aaaManager.Logoff();
            _activeAccountManager.ActiveAccount.Account = null;
            _activeAccountManager.BlackListObjects.Clear();
            _activeAccountManager.WhiteListObjects.Clear();
            LoginChange();
            var loginViewModel = _loginViewModelFactory.Create(LoginType.FirstTime);
            while (
                _windowManager.ShowDialog(loginViewModel, null,
                    new Dictionary<string, object> { { "ResizeMode", ResizeMode.NoResize }, { "CanClose", false } }) != true)
            {
            }
            _loginViewModelFactory.Release(loginViewModel);
        }

        private bool CanLogoffCommand(object obj)
        {
            return true;
        }


        private RelayCommand _stopPlanCommand;
        public RelayCommand StopPlanCommand
        {
            get
            {
                return _stopPlanCommand ??
                       (_stopPlanCommand =
                        new RelayCommand(ExecuteStopPlanCommand, CanStopPlanCommand, CommandType.Delete,
                                         "Остановить", "Остановить"));
            }
        }

        private void ExecuteStopPlanCommand(object obj)
        {
            var dialogModel = new YesNoDialogViewModel();
            dialogModel.Message = "Действительно удалить всю очередь 'План'?";
            dialogModel.DisplayName = "Подтверждение";
            if (_windowManager.ShowDialog(dialogModel) == true)
            {
                _dataBaseDirectService.Stop();
            }
        }

        private bool CanStopPlanCommand(object obj)
        {
            return true;
        }



        private RelayCommand _manualPlanCommand;
        public RelayCommand ManualPlanCommand
        {
            get
            {
                return _manualPlanCommand ??
                       (_manualPlanCommand =
                        new RelayCommand(ExecuteManualPlanCommand, CanManualPlanCommand, CommandType.Delete,
                                         "Ручная очередь", "Ручная очередь"));
            }
        }

        private void ExecuteManualPlanCommand(object obj)
        {
            _windowManager.ShowWindow(new ManualPlanViewModel(_printerService, _factDataService, _manualPlanDataService, _lineService, _matthewsMperiaService, _windowManager) { DisplayName = "Настройка перемаркировки" });
        }

        private bool CanManualPlanCommand(object obj)
        {
            return true;
        }



        private RelayCommand _printerSettingsCommand;
        public RelayCommand PrinterSettingsCommand
        {
            get
            {
                return _printerSettingsCommand ??
                       (_printerSettingsCommand =
                        new RelayCommand(ExecutePrinterSettingsCommand, CanPrinterSettingsCommand, CommandType.Delete,
                                         "Редактировать", "Редактировать план"));
            }
        }

        private void ExecutePrinterSettingsCommand(object obj)
        {
            _windowManager.ShowDialog(new PrinterSettingsViewModel(_printerService, _plcAcessRemoteService) { DisplayName = "Настройка принтеров" }, null, new Dictionary<string, object>() { { "ResizeMode", ResizeMode.NoResize }, { "CanClose", false } });
        }

        private bool CanPrinterSettingsCommand(object obj)
        {
            return true;
        }

        private RelayCommand _weightSettingsCommand;
        public RelayCommand WeightSettingsCommand
        {
            get
            {
                return _weightSettingsCommand ??
                       (_weightSettingsCommand =
                        new RelayCommand(ExecuteWeightSettingsCommand, CanWeightSettingsCommand, CommandType.Delete,
                                         "Редактировать", "Редактировать план"));
            }
        }

        private void ExecuteWeightSettingsCommand(object obj)
        {
            _windowManager.ShowDialog(new WeightSettingsViewModel(_plcAcessRemoteService, _windowManager, _lineService) { DisplayName = "Настройка весов" }, null, new Dictionary<string, object>() { { "ResizeMode", ResizeMode.NoResize } });
        }

        private bool CanWeightSettingsCommand(object obj)
        {
            return true;
        }




        private RelayCommand _lineSettingsCommand;
        public RelayCommand LineSettingsCommand
        {
            get
            {
                return _lineSettingsCommand ??
                       (_lineSettingsCommand =
                        new RelayCommand(ExecuteLineSettingsCommand, CanLineSettingsCommand, CommandType.Delete,
                                         "Редактировать", "Редактировать план"));
            }
        }

        private void ExecuteLineSettingsCommand(object obj)
        {
            _windowManager.ShowDialog(new LineSettingsViewModel(_lineService) { DisplayName = "Настройка упаковки" }, null, new Dictionary<string, object>() { { "ResizeMode", ResizeMode.NoResize } });
        }

        private bool CanLineSettingsCommand(object obj)
        {
            return true;
        }

        public BindableCollection<SelectableViewModel<Line>> Lines
        {
            get { return _lines; }
            set
            {
                _lines = value;
                NotifyOfPropertyChange(() => Lines);
            }
        }

        public BindableCollection<SelectableViewModel<Printer>> Printers
        {
            get { return _printers; }
            set
            {
                _printers = value;
                NotifyOfPropertyChange(() => Printers);
            }
        }

        public FactFilter FactFilter
        {
            get { return _factFilter ?? (_factFilter = new FactFilter()); }
            set { _factFilter = value; NotifyOfPropertyChange(() => FactFilter); }
        }

        private TranslationObject _loginName;
        public TranslationObject LoginName
        {
            get
            {
                if (_loginName == null) _loginName = new TranslationObject();
                try
                {
                    _loginName.Value = _activeAccountManager.ActiveAccount.Account.Login;
                    return _loginName;
                }
                catch
                {
                    _loginName.Value = "";
                    return _loginName;
                }
            }
        }

        void LoginChange()
        {
            NotifyOfPropertyChange(() => IsCanEditSystemSettings);
            NotifyOfPropertyChange(() => LoginName);
        }

        public void Handle(AccountDataChanged message)
        {
            LoginChange();
        }
    }

    class PlanFactScaner : BaseCyclicHandler
    {
        public delegate void UpdateScada();

        public event UpdateScada UpdateScadaHandler;

        public PlanFactScaner(int scanIntervalInMilliseconds)
        {
            ScanIntervalInMilliseconds = scanIntervalInMilliseconds;
        }

        protected override void Handler()
        {
            OnUpdateScadaHandler();
        }

        protected virtual void OnUpdateScadaHandler()
        {
            var handler = UpdateScadaHandler;
            if (handler != null) handler();
        }

        public void UnSubscribe()
        {
            var handler = UpdateScadaHandler;
            if (handler == null) return;
            Delegate[] clientList = handler.GetInvocationList();

            foreach (var d in clientList)
            {
                var subscriber = (d as UpdateScada);
                if (subscriber != null)
                    handler -= subscriber;
            }
        }
    }

    class AlarmScaner : BaseCyclicHandler
    {
        public delegate void UpdateScada();

        public event UpdateScada UpdateScadaHandler;

        public AlarmScaner(int scanIntervalInMilliseconds)
        {
            ScanIntervalInMilliseconds = scanIntervalInMilliseconds;
        }

        protected override void Handler()
        {
            OnUpdateScadaHandler();
        }

        protected virtual void OnUpdateScadaHandler()
        {
            var handler = UpdateScadaHandler;
            if (handler != null) handler();
        }

        public void UnSubscribe()
        {
            var handler = UpdateScadaHandler;
            if (handler == null) return;
            Delegate[] clientList = handler.GetInvocationList();

            foreach (var d in clientList)
            {
                var subscriber = (d as UpdateScada);
                if (subscriber != null)
                    handler -= subscriber;
            }
        }
    }

}