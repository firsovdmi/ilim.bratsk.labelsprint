﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Visualization.Infrastructure;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class LineSettingViewModel : Screen, IEditStatus
    {
        private bool _isEdited;
        private bool _isDeleted;
        private bool _isAdded;
        private Line _content;

        public LineSettingViewModel(Line content)
        {
            Content = content;
            WeighTypes = new ObservableCollection<WeighType>
                {
                    new WeighType {Value = false, Description = "Фактич. вес"},
                    new WeighType{Value=true, Description="Фикс. вес"}
                };
        }

        public ObservableCollection<WeighType> WeighTypes { get; set; }

        public Line Content
        {
            get { return _content; }
            set
            {
                if (_content != null) _content.PropertyChanged -= _content_PropertyChanged;
                _content = value;
                if (_content != null) _content.PropertyChanged += _content_PropertyChanged;
                NotifyOfPropertyChange(() => Content);
            }
        }

        private void _content_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsEdited = true;
        }

        #region Implementation of IEditStatus

        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                if (value.Equals(_isEdited)) return;
                _isEdited = value;
                NotifyOfPropertyChange("IsEdited");
            }
        }

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                if (value.Equals(_isDeleted)) return;
                _isDeleted = value;
                NotifyOfPropertyChange("IsDeleted");
            }
        }

        public bool IsAdded
        {
            get { return _isAdded; }
            set
            {
                if (value.Equals(_isAdded)) return;
                _isAdded = value;
                NotifyOfPropertyChange("IsAdded");
            }
        }

        #endregion
    }

    public class WeighType
    {
        public bool Value { get; set; }
        public string Description { get; set; }
    }
}