﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model.PlcClasses.Jobs;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class PlcKipaViewModel
    {
        public PlcKipaItemRemote Item { get; set; }
        private IPlcAcessRemoteService _plcAcessRemoteService;
        private readonly IWindowManager _windowManager;

        public PlcKipaViewModel(IPlcAcessRemoteService plcAcessRemoteService, IWindowManager windowManager)
        {
            _windowManager = windowManager;
            _plcAcessRemoteService = plcAcessRemoteService;
        }

        private RelayCommand _deleteFromPlc;
        public RelayCommand DeleteFromPlc
        {
            get
            {
                return _deleteFromPlc ??
                       (_deleteFromPlc =
                        new RelayCommand(ExecuteDeleteFromPlcCommand, CanDeleteFromPlcCommand, CommandType.Delete,
                                         "Убрать", "Убрать кипу"));
            }
        }

        private bool CanDeleteFromPlcCommand(object obj)
        {
            return true;
        }

        private void ExecuteDeleteFromPlcCommand(object obj)
        {
            var confirmModel = new YesNoDialogViewModel();
            confirmModel.DisplayName = "Подтверждение удаления";
            confirmModel.Message = "Вы действительно хотите удалить кипу?";
            if (_windowManager.ShowDialog(confirmModel) == true)
                _plcAcessRemoteService.RemoveKipa(Item);
                
        }
    }
}
