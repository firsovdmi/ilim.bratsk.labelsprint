﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.DataBaseDirect;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Infrastructure.Log;
using ILIM.Bratsk.Visualization.Infrastructure;
using Telerik.Windows.Controls;
using ILIM.Bratsk.Domain.Model;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class AddNewPlanViewModel : Screen
    {
        public Command Item { get; set; }
        public string Message { get; set; }

        private IDataBaseDirectService _dataBaseDirectService;
        private readonly IFactDataService _factDataService;
        private IServiceDataService _serviceDataService;
        private IMatthewsMperiaService _matthewsMperiaService;
        private IWindowManager _windowManager;

        public AddNewPlanViewModel(IDataBaseDirectService dataBaseDirectService, IFactDataService factDataService, IServiceDataService serviceDataService, IMatthewsMperiaService matthewsMperiaService,
            IWindowManager windowManager)
        {
            _windowManager = windowManager;
            _matthewsMperiaService = matthewsMperiaService;
            _serviceDataService = serviceDataService;
            _factDataService = factDataService;
            _dataBaseDirectService = dataBaseDirectService;
            var maxCode = _serviceDataService.GetMaxBatchCode() + 1;
            Item = new Command
            {
                BatchCode = @"2/" + DateTime.Now.ToString("yy") + "-" + maxCode.ToString("D5"),
                PackFirst = 1,
                PackLast = 1
            };
        }

        public List<EmptType> EmptTypes
        {
            get
            {
                return _emptTypes ?? (_emptTypes = new List<EmptType>
                    {
                        new EmptType {Name = "Печатать", Value = PrinterModeEnum.Standart},
                        new EmptType {Name = "Не печатать", Value = PrinterModeEnum.NoCode}
                    });
            }
            set { _emptTypes = value; }
        }

        private List<EmptType> _emptTypes;

        private RelayCommand _applyPlanCommand;
        public RelayCommand ApplyPlanCommand
        {
            get
            {
                return _applyPlanCommand ??
                       (_applyPlanCommand =
                        new RelayCommand(ExecuteApplyPlanCommand, CanApplyPlanCommand, CommandType.Delete,
                                         "Создать", "Создать план"));
            }
        }

        private void ExecuteApplyPlanCommand(object obj)
        {
            if (_matthewsMperiaService.CheckIsExistKips(Item.BatchCode, Item.PackFirst, Item.PackLast))
            {
                _windowManager.Alert(@"Ошибка","Кипы с таким кодом уже существуют!");
                return;
            }
            if (_dataBaseDirectService.AddPlan(Item.BatchCode, Item.PackFirst, Item.PackLast, Item.Empt))
            {
                TryClose(true);
                return;
            }
            Message = "Не удалось применить";
            NotifyOfPropertyChange(() => Message);
        }

        private bool CanApplyPlanCommand(object obj)
        {
            return Item.BatchCode != null && Item.PackFirst <= Item.PackLast;
        }


        private RelayCommand _closeCommand;
        public RelayCommand CloseCommand
        {
            get
            {
                return _closeCommand ??
                       (_closeCommand =
                        new RelayCommand(ExecuteCloseCommand, CanCloseCommand, CommandType.Delete,
                                         "Отмена", "Отмена"));
            }
        }

        private void ExecuteCloseCommand(object obj)
        {
            TryClose(true);
        }

        private bool CanCloseCommand(object obj)
        {
            return true;
        }


        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView() as RadWindow;
            if (window != null)
                window.Close();
        }

    }
    public class EmptType
    {
        public string Name { get; set; }
        public PrinterModeEnum Value { get; set; }
    }
}
