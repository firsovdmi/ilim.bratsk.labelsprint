﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ViewModels;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class ManualPlanViewModel : Screen
    {
        private readonly IPrinterService _printerService;
        private FactFilter _factFilter;
        private readonly IFactDataService _factDataService;
        private ObservableCollection<FactData> _factSource;
        private ObservableCollection<ManualPlanData> _manualPlan;
        private IList _factSourceSelected;
        private IList _manualPlanSelected;
        private readonly IManualPlanDataService _manualPlanDataService;
        private BindableCollection<SelectableViewModel<Line>> _lines;
        private readonly ILineService _lineService;
        private readonly IMatthewsMperiaService _matthewsMperiaService;
        private IWindowManager _windowManager;

        public ManualPlanViewModel(
            IPrinterService printerService,
            IFactDataService factDataService,
            IManualPlanDataService manualPlanDataService,
            ILineService lineService,
            IMatthewsMperiaService matthewsMperiaService,
            IWindowManager windowManager
            )
        {
            _windowManager = windowManager;
            _matthewsMperiaService = matthewsMperiaService;
            _lineService = lineService;
            _manualPlanDataService = manualPlanDataService;
            _factDataService = factDataService;
            _printerService = printerService;
            UpdateFactSource();
            
            Initialization();
        }

        public PrinterSettingViewModel Printer4 { get; set; }

        private void InitManualPlan()
        {
            ManualPlan = new ObservableCollection<ManualPlanData>(_manualPlanDataService.Get().ToList());
        }

        public void Initialization()
        {
            InitManualPlan();
            Printer4 = new PrinterSettingViewModel(_printerService.Get().FirstOrDefault(p => p.Number == 4));
            Lines = new BindableCollection<SelectableViewModel<Line>>(_lineService.Get().Select(p => new SelectableViewModel<Line>(p)));
            foreach (var line in Lines)
            {
                line.PropertyChanged += line_PropertyChanged;
            }
        }

        void line_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            NotifyOfPropertyChange(() => SelectedLines);
            FactFilter.LinesNumbers = Lines.Where(p => p.IsSelected).Select(p => p.Content.LineNumber).ToList();
        }

        void UpdateFactSource()
        {
            var serverItems = _factDataService.GetUndeleted(FactFilter);
            foreach (var serverItem in serverItems)
            {
                if (FactSource.Any(p => p.ID == serverItem.ID)) continue;
                if (ManualPlan.Any(p => p.PackNum == serverItem.PackNum && p.BatchCode == serverItem.BatchCode)) continue;
                FactSource.Add(serverItem);
            }

            foreach (var localItem in FactSource.ToArray())
            {
                if (serverItems.All(p => p.ID != localItem.ID)) FactSource.Remove(localItem);
            }
        }

        public string SelectedLines
        {
            get
            {
                if (Lines != null && Lines.Any(p => p.IsSelected))
                    return String.Join(", ", Lines.Where(p => p.IsSelected).Select(p => p.Content.Name));
                return "";
            }

        }
        public BindableCollection<SelectableViewModel<Line>> Lines
        {
            get { return _lines; }
            set
            {
                _lines = value;
                NotifyOfPropertyChange(() => Lines);
            }
        }


        public FactFilter FactFilter
        {
            get { return _factFilter ?? (_factFilter = new FactFilter()); }
            set { _factFilter = value; }
        }

        public ObservableCollection<FactData> FactSource
        {
            get { return _factSource ?? (_factSource = new ObservableCollection<FactData>()); }
            set { _factSource = value; }
        }

        public ObservableCollection<ManualPlanData> ManualPlan
        {
            get { return _manualPlan ?? (_manualPlan = new ObservableCollection<ManualPlanData>()); }
            set { _manualPlan = value; }
        }

        public IList FactSourceSelected
        {
            get { return _factSourceSelected; }
            set { _factSourceSelected = value; NotifyOfPropertyChange(() => FactSourceSelected); }
        }

        public IList ManualPlanSelected
        {
            get { return _manualPlanSelected; }
            set { _manualPlanSelected = value; NotifyOfPropertyChange(() => ManualPlanSelected); }
        }

        private RelayCommand _applyFactFilterCommand;
        public RelayCommand ApplyFactFilterCommand
        {
            get
            {
                return _applyFactFilterCommand ??
                       (_applyFactFilterCommand =
                        new RelayCommand(ExecuteApplyFactFilterCommand, CanApplyFactFilterCommand, CommandType.Delete,
                                         "Редактировать", "Редактировать план"));
            }
        }

        private void ExecuteApplyFactFilterCommand(object obj)
        {
            UpdateFactSource();
        }

        private bool CanApplyFactFilterCommand(object obj)
        {
            return true;
        }


        private RelayCommand _ApplyCommand;
        public RelayCommand ApplyCommand
        {
            get
            {
                return _ApplyCommand ??
                       (_ApplyCommand =
                        new RelayCommand(ExecuteApplyCommand, CanApplyCommand, CommandType.Delete,
                                         "Создать", "Создать план"));
            }
        }

        private void ExecuteApplyCommand(object obj)
        {
            if (Printer4.IsEdited) _printerService.Update(Printer4.Content);
            _manualPlanDataService.SetNewList(ManualPlan.Select(p => new ManualPlanData
                {
                    FactID = p.FactID,
                    State = p.State,
                    BatchCode = p.BatchCode,
                    PackNum = p.PackNum,
                    Empt = p.Empt,
                    LineNum = p.LineNum,
                    WeightNet = p.WeightNet,
                    WeightBrut = p.WeightBrut,
                    PlanTime = p.PlanTime,
                    WeightTime = p.WeightTime,
                    PackTime = p.PackTime,
                    FactTime = p.FactTime,
                    CmdID = p.CmdID
                }).ToList());
            TryClose(true);
        }

        private bool CanApplyCommand(object obj)
        {
            return true;
        }


        private RelayCommand _closeCommand;
        public RelayCommand CloseCommand
        {
            get
            {
                return _closeCommand ??
                       (_closeCommand =
                        new RelayCommand(ExecuteCloseCommand, CanCloseCommand, CommandType.Delete,
                                         "Отмена", "Отмена"));
            }
        }

        private void ExecuteCloseCommand(object obj)
        {
            TryClose(true);
        }

        private bool CanCloseCommand(object obj)
        {
            return true;
        }



        private RelayCommand _addToManualCommand;
        public RelayCommand AddToManualCommand
        {
            get
            {
                return _addToManualCommand ??
                       (_addToManualCommand =
                        new RelayCommand(ExecuteAddToManualCommand, CanAddToManualCommand, CommandType.Delete,
                                         "Создать", "Создать план"));
            }
        }

        private void ExecuteAddToManualCommand(object obj)
        {
            var memSelected = FactSourceSelected.Cast<FactData>().ToList();
            foreach (var selectedFact in memSelected)
            {
                ManualPlan.Add(new ManualPlanData
                    {
                        FactID = selectedFact.FactID,
                        State = selectedFact.State,
                        BatchCode = selectedFact.BatchCode,
                        PackNum = selectedFact.PackNum,
                        Empt = selectedFact.Empt,
                        LineNum = selectedFact.PrinterNum,
                        WeightNet = selectedFact.WeightNet,
                        WeightBrut = selectedFact.WeightBrut,
                        PlanTime = selectedFact.PlanTime,
                        WeightTime = selectedFact.WeightTime,
                        PackTime = selectedFact.PackTime,
                        FactTime = selectedFact.FactTime,
                        CmdID = selectedFact.EventID
                    });
                FactSource.Remove(selectedFact);
            }
        }

        private bool CanAddToManualCommand(object obj)
        {
            return FactSourceSelected != null;
        }



        private RelayCommand _removeFromManualCommand;
        public RelayCommand RemoveFromManualCommand
        {
            get
            {
                return _removeFromManualCommand ??
                       (_removeFromManualCommand =
                        new RelayCommand(ExecuteRemoveFromManualCommand, CanRemoveFromManualCommand, CommandType.Delete,
                                         "Создать", "Создать план"));
            }
        }

        private void ExecuteRemoveFromManualCommand(object obj)
        {
            var memSelected = ManualPlanSelected.Cast<ManualPlanData>().ToList();
            foreach (var selectedItem in memSelected)
            {
                ManualPlan.Remove(selectedItem);
            }
            UpdateFactSource();
        }

        private bool CanRemoveFromManualCommand(object obj)
        {
            return ManualPlanSelected != null;
        }


        private RelayCommand _startSpecModeCommand;
        public RelayCommand StartSpecModeCommand
        {
            get
            {
                return _startSpecModeCommand ??
                       (_startSpecModeCommand =
                        new RelayCommand(ExecuteStartSpecModeCommand, CanStartSpecModeCommand, CommandType.Delete,
                                         "Спец режим", "Спец режим"));
            }
        }

        private void ExecuteStartSpecModeCommand(object obj)
        {
            var dialogModel = new YesNoDialogViewModel {Message = "Переключить линию в режим \"Перепечатка\"?"};
            if (_windowManager.ShowDialog(dialogModel) == true)
            {
                _matthewsMperiaService.StartSpecMode(4);
            }
        }

        private bool CanStartSpecModeCommand(object obj)
        {
            return true;
        }


        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView() as RadWindow;
            if (window != null)
                window.Close();
        }
    }
}
