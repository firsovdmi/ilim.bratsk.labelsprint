﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.DataBaseDirect;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Visualization.Infrastructure;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class EditPlanViewModel : Screen
    {
        private string _batchCode;
        public Command Item { get; set; }
        public string Message { get; set; }
        public string WarringMessage { get; set; }
        public ObservableCollection<string> AvailableBatchCode { get; set; }

        private readonly IDataBaseDirectService _dataBaseDirectService;
        private readonly IPlanDataService _planDataService;

        public EditPlanViewModel(IDataBaseDirectService dataBaseDirectService, IPlanDataService planDataService)
        {
            _planDataService = planDataService;
            _dataBaseDirectService = dataBaseDirectService;
            Item = new Command
                {
                    PackFirst = 1,
                    PackLast = 1
                };
            AvailableBatchCode = new ObservableCollection<string>(_planDataService.GetWaitingDestinctBatchCodes());
            if (AvailableBatchCode.Any()) BatchCode = AvailableBatchCode.First();
            NotifyOfPropertyChange(() => AvailableBatchCode);
        }

        private EmptType _selectedEmptType;

        public EmptType SelectedEmptType
        {
            get { return _selectedEmptType; }
            set
            {
                _selectedEmptType = value;
                if (SelectedEmptType != null) Item.Empt = SelectedEmptType.Value;
            }
        }

        public List<EmptType> EmptTypes
        {
            get
            {
                if (_emptTypes == null)
                {
                    _emptTypes = new List<EmptType>
                    {
                        new EmptType {Name = "Печатать", Value = PrinterModeEnum.Standart},
                        new EmptType{Name="Не печатать", Value=PrinterModeEnum.NoCode}

                    };
                }
                return _emptTypes;
            }
            set { _emptTypes = value; }
        }

        private List<EmptType> _emptTypes;

        private RelayCommand _applyPlanCommand;
        public RelayCommand ApplyPlanCommand
        {
            get
            {
                return _applyPlanCommand ??
                       (_applyPlanCommand =
                        new RelayCommand(ExecuteApplyPlanCommand, CanApplyPlanCommand, CommandType.Delete,
                                         "Применить", "Применить"));
            }
        }

        private void ExecuteApplyPlanCommand(object obj)
        {
            if (_dataBaseDirectService.EditPlan(Item.BatchCode, Item.PackFirst, Item.PackLast, Item.Empt))
            {
                TryClose(true);
                return;
            }
            WarringMessage = "Не удалось применить";
            NotifyOfPropertyChange(() => WarringMessage);
        }

        private bool CanApplyPlanCommand(object obj)
        {
            return BatchCode != null && Item.PackFirst <= Item.PackLast;
        }


        private RelayCommand _closeCommand;
        public RelayCommand CloseCommand
        {
            get
            {
                return _closeCommand ??
                       (_closeCommand =
                        new RelayCommand(ExecuteCloseCommand, CanCloseCommand, CommandType.Delete,
                                         "Отмена", "Отмена"));
            }
        }

        public string BatchCode
        {
            get { return _batchCode; }
            set
            {
                if (_batchCode == value) return;
                _batchCode = value;
                UpdateCommand();
            }
        }

        private void UpdateCommand()
        {
            Item = _planDataService.GetCommand(BatchCode);
            if (Item!=null) SelectedEmptType = EmptTypes.FirstOrDefault(p => p.Value == Item.Empt);
            NotifyOfPropertyChange(() => Item);
            NotifyOfPropertyChange(() => SelectedEmptType);
        }

        private void ExecuteCloseCommand(object obj)
        {
            TryClose(true);
        }

        private bool CanCloseCommand(object obj)
        {
            return true;
        }


        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView() as RadWindow;
            if (window != null)
                window.Close();
        }

    }

}
