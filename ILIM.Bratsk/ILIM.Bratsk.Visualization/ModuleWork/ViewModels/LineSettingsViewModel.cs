﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Visualization.Infrastructure;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class LineSettingsViewModel : Screen
    {
        private readonly ILineService _lineService;
        public LineSettingsViewModel(ILineService lineService)
        {
            _lineService = lineService;
            InitLines();
        }

        

        private void InitLines()
        {
            Lines = new ObservableCollection<LineSettingViewModel>(_lineService.Get().OrderBy(p=>p.Name).Select(p => new LineSettingViewModel(p)).ToList());
        }


        public ObservableCollection< LineSettingViewModel> Lines { get; set; }


        private RelayCommand _applyCommand;
        public RelayCommand ApplyCommand
        {
            get
            {
                return _applyCommand ??
                       (_applyCommand =
                        new RelayCommand(ExecuteApplyCommand, CanApplyCommand, CommandType.Delete,
                                         "Сохранить", "Сохранить"));
            }
        }

        private void ExecuteApplyCommand(object obj)
        {
            foreach (var line in Lines.Where(p=>p.IsEdited))
            {
                _lineService.Update(line.Content);
            }
            TryClose(true);
        }

        private bool CanApplyCommand(object obj)
        {
            return true;// Line1.IsEdited || Line2.IsEdited || Line3.IsEdited || Line4.IsEdited;
        }


        private RelayCommand _closeCommand;
        public RelayCommand CloseCommand
        {
            get
            {
                return _closeCommand ??
                       (_closeCommand =
                        new RelayCommand(ExecuteCloseCommand, CanCloseCommand, CommandType.Delete,
                                         "Отмена", "Отмена"));
            }
        }

        private void ExecuteCloseCommand(object obj)
        {
            TryClose(true);
        }

        private bool CanCloseCommand(object obj)
        {
            return true;
        }

        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView() as RadWindow;
            if (window != null)
                window.Close();
        }
    }
}
