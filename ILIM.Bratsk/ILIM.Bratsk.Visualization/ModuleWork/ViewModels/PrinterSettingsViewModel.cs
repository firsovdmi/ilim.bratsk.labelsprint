﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Model.PlcClasses.KipaData;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ViewModels;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ModuleWork.ViewModels
{
    public class PrinterSettingsViewModel : Screen
    {
        private readonly IPrinterService _printerService;
        public ConveerItemsViewModel Line1 { get; set; }
        public ConveerItemsViewModel Line2 { get; set; }
        public ConveerItemsViewModel Line3 { get; set; }
        public ConveerItemsViewModel Line4 { get; set; }
        private readonly IPlcAcessRemoteService _plcAcessRemoteService;

        public PrinterSettingsViewModel(
            IPrinterService printerService,
            IPlcAcessRemoteService plcAcessRemoteService
            )
        {
            _plcAcessRemoteService = plcAcessRemoteService;
            _printerService = printerService;

            Initialization();
        }

        private void Initialization()
        {
            Printer1 = new PrinterSettingViewModel(_printerService.Get().FirstOrDefault(p => p.Number == 1));
            Printer3 = new PrinterSettingViewModel(_printerService.Get().FirstOrDefault(p => p.Number == 3));
            Printer4 = new PrinterSettingViewModel(_printerService.Get().FirstOrDefault(p => p.Number == 4));
            var commonDatas = _plcAcessRemoteService.GetCahsedCommonData();
            Line1 = new ConveerItemsViewModel(_plcAcessRemoteService);
            Line2 = new ConveerItemsViewModel(_plcAcessRemoteService);
            Line3 = new ConveerItemsViewModel(_plcAcessRemoteService);
            Line4 = new ConveerItemsViewModel(_plcAcessRemoteService);
            Line1.GetCommonData(1, commonDatas);
            Line2.GetCommonData(2, commonDatas);
            Line3.GetCommonData(3, commonDatas);
            Line4.GetCommonData(4, commonDatas);
        }

        public PrinterSettingViewModel Printer1 { get; set; }
        public PrinterSettingViewModel Printer3 { get; set; }
        public PrinterSettingViewModel Printer4 { get; set; }

        private RelayCommand _applyPlanCommand;
        public RelayCommand ApplyPlanCommand
        {
            get
            {
                return _applyPlanCommand ??
                       (_applyPlanCommand =
                        new RelayCommand(ExecuteApplyPlanCommand, CanApplyPlanCommand, CommandType.Delete,
                                         "Создать", "Создать план"));
            }
        }

        private void ExecuteApplyPlanCommand(object obj)
        {
            UpdatePrinterSettings(Printer1);
            UpdatePrinterSettings(Printer3);
            UpdatePrinterSettings(Printer4);
            TryClose(true);
        }

        void UpdatePrinterSettings(PrinterSettingViewModel printer)
        {
            if (!printer.IsEdited) return;
            _printerService.Update(printer.Content);
        }

        private bool CanApplyPlanCommand(object obj)
        {
            return true;
        }


        private RelayCommand _closeCommand;
        public RelayCommand CloseCommand
        {
            get
            {
                return _closeCommand ??
                       (_closeCommand =
                        new RelayCommand(ExecuteCloseCommand, CanCloseCommand, CommandType.Delete,
                                         "Отмена", "Отмена"));
            }
        }

        private void ExecuteCloseCommand(object obj)
        {
            TryClose(true);
        }

        private bool CanCloseCommand(object obj)
        {
            return true;
        }

        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView() as RadWindow;
            if (window != null)
                window.Close();
        }
    }
}
