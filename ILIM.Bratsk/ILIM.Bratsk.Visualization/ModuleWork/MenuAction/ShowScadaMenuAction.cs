﻿using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleWork.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleWork.MenuAction
{
    public class ShowScadaMenuAction : WorkMenuAction<ScadaViewModel>
    {
        public ShowScadaMenuAction(string displayName)
            : base(displayName)
        {
        }

        public ShowScadaMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager)
            : base(radDockingManager, screenFactory, "Визуализация", eventAggregator, activeAccountManager)
        {
        }

        protected override string AuthorizeObjectName
        {
            get { return "GetRolesShorInfo"; }
        }
    }
}