﻿using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleWork.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleWork.MenuAction
{
    public class ShowMainWorkWindowMenuAction : WorkMenuAction<MainWorkWindowViewModel>
    {
        public ShowMainWorkWindowMenuAction(string displayName)
            : base(displayName)
        {
        }

        public ShowMainWorkWindowMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager)
            : base(radDockingManager, screenFactory, "Главное окно", eventAggregator, activeAccountManager)
        {
        }

        protected override string AuthorizeObjectName
        {
            get { return "GetRolesShorInfo"; }
        }
    }
}