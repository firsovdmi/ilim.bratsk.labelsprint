﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels;
using ILIM.Bratsk.Visualization.ModuleWork.ViewModels;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleWork.MenuAction
{
    public abstract class WorkMenuAction<T> : WorkMenuAction, IHandle<AccountDataChanged>
        where T : Screen
    {
        private readonly object _executeLock = new object();
        private readonly IActiveAccountManager ActiveAccountManager;
        protected readonly IEventAggregator EventAggregator;
        protected readonly IRadDockingManager RadDockingManager;
        protected readonly IScreenFactory ScreenFactory;

        protected WorkMenuAction(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public WorkMenuAction(string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
        }

        public WorkMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            string displayName, IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager)
            : base(displayName)
        {
            RadDockingManager = radDockingManager;
            ScreenFactory = screenFactory;
            EventAggregator = eventAggregator;
            ActiveAccountManager = activeAccountManager;
            CheckAcess(ActiveAccountManager.BlackListObjects);
        }

        protected abstract string AuthorizeObjectName { get; }

        public void Handle(AccountDataChanged message)
        {
            CheckAcess(message.AuthenticationResult.DeniedObjects);
        }

        /// <summary>
        ///     The action associated to the ActionItem
        /// </summary>
        public override void Execute()
        {
            lock (_executeLock)
            {
                var viewmodel = ScreenFactory.Create<T>();
                //Anti-double subscribe hack
                viewmodel.Deactivated -= Release;
                viewmodel.Deactivated += Release;

                RadDockingManager.ShowWindow(viewmodel, null);
            }
        }

        protected void Release(object sender, DeactivationEventArgs e)
        {
            var model = (Screen)sender;
            model.Deactivated -= Release;
            ScreenFactory.Release(sender);
        }

        private void CheckAcess(IEnumerable<string> blackListObjects)
        {
            var objectName = "";
            try
            {
                // objectName =typeof(T).GetField("_aaaService", BindingFlags.Instance | BindingFlags.NonPublic).FieldType.Name.TrimStart('I') + "." + AuthorizeObjectName;
            }
            catch
            {
            }
            IsEnable = blackListObjects.All(p => p != objectName);
        }
    }

    public abstract class WorkMenuAction : ActionItem
    {
        protected WorkMenuAction(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public WorkMenuAction(string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
        }
    }
}