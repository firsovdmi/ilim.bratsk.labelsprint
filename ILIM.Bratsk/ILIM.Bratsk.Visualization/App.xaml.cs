﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows;

namespace ILIM.Bratsk.Visualization
{
    /// <summary>
    ///     Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)

        {
            var success = false;
            while (!success)
            {
                try
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU");
                    base.OnStartup(e);
                    success = true;
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}