﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Data.Model;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Visualization.ViewModelClasses
{
    public class TtnHeadViewModel : ITtnHead
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string RawCode { get; set; }
        public string Client { get; set; }
        public string Invoice { get; set; }
        public IContract Contract { get; set; }
        public IManufacturer Manufacturer { get; set; }
        public bool IsSelf { get; set; }
        public IFioDriver FioDrivers { get; set; }
        public IStaff Master { get; set; }
        public IStaff Operator { get; set; }
        public IEnumerable<ISection> Section { get; set; }
    }
}
