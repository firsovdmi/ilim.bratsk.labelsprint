﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Data.Model;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Visualization.ViewModelClasses
{
    public class TankViewModel : ITank
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ISort Sort { get; set; }
        public IThermalResistance ThermalResistance { get; set; }
        public float AcidLab { get; set; }
        public float DensityLab { get; set; }
        public float FatLab { get; set; }
        public float ProteinLab { get; set; }
    }
}
