namespace PAG.WBD.Milk.Visualization.Model
{
    public interface IDictionaryStatus
    {
        bool IsEdited { get; set; }
        bool IsDeleted { get; set; }
        bool IsAdded { get; set; }
    }
}