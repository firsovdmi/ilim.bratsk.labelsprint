using System;
using System.ComponentModel;
using System.Windows.Media;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Behaviour;
using ILIM.Bratsk.Visualization.Infrastructure;

namespace ILIM.Bratsk.Visualization.Model
{
    public class EntityWithEditStatusModel<T> : PropertyChangedBase, IEditStatus where T : EntityBase, new()
    {
        private T _content;
        private bool _isAdded;
        private bool _isDeleted;
        private bool _isEdited;

        public EntityWithEditStatusModel(T model)
        {
            Content = model;
        }

        public EntityWithEditStatusModel()
        {
            Content = new T();
            Content.ID = Guid.NewGuid();
            IsAdded = true;
        }

        public T Content
        {
            get { return _content; }
            set
            {
                if (Equals(value, _content)) return;
                if (_content != null) _content.PropertyChanged -= _content_PropertyChanged;
                _content = value;
                if (_content != null) _content.PropertyChanged += _content_PropertyChanged;

                NotifyOfPropertyChange("Content");
            }
        }

        private void _content_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsEdited = true;
        }

        #region Implementation of IEditStatus

        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                _isEdited = value;
                NotifyOfPropertyChange(() => IsEdited);
                NotifyOfPropertyChange(() => ModeValue);
            }
        }

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                NotifyOfPropertyChange(() => IsDeleted);
                NotifyOfPropertyChange(() => ModeValue);
            }
        }

        public bool IsAdded
        {
            get { return _isAdded; }
            set
            {
                _isAdded = value;
                NotifyOfPropertyChange(() => IsAdded);
                NotifyOfPropertyChange(() => ModeValue);
            }
        }

        public ImageSource ModeValue
        {
            get
            {
                if (IsDeleted) return ImageSources.DictionaryStatusDeleted;
                if (IsEdited || IsAdded) return ImageSources.DictionaryStatusEdited;
                return null;
            }
        }

        #endregion
    }
}