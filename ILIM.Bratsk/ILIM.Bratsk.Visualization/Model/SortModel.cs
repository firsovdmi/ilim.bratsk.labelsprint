using System;
using System.ComponentModel;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Visualization.Model
{
    public class SortModel : IDictionaryStatus, INotifyPropertyChanged
    {
        public SortModel()
        {
            Content = new Sort() { ID = Guid.NewGuid() };
            IsAdded = true;
        }

        public SortModel(Sort content)
        {
            Content = content;
        }
        public Sort Content { get; set; }

        private bool _isEdited;
        private bool _isAdded;
        private bool _isDeleted;

        #region Implementation of IDictionaryStatus

        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                _isEdited = value;
                RaisePropertyChanged("IsEdited");
            }
        }

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                RaisePropertyChanged("IsDeleted");
            }
        }

        public bool IsAdded
        {
            get { return _isAdded; }
            set
            {
                _isAdded = value;
                RaisePropertyChanged("IsAdded");
            }
        }

        #endregion

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}