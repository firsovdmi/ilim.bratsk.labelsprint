using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels;

namespace ILIM.Bratsk.Visualization.ViewModels
{
    public class AAAManager
    {
        private static readonly object _fileLock = new Object();
        private readonly IAAAServiceRemoteFasade _aaaServiceRemoteFasade;
        private readonly IActiveAccountManager _activeAccountManager;
        private readonly IWindowManager _windowManager;
        protected readonly IEventAggregator EventAggregator;

        public AAAManager(IAAAServiceRemoteFasade aaaServiceRemoteFasade, IActiveAccountManager activeAccountManager,
            IEventAggregator eventAggregator, IWindowManager windowManager)
        {
            _aaaServiceRemoteFasade = aaaServiceRemoteFasade;
            _activeAccountManager = activeAccountManager;
            EventAggregator = eventAggregator;
            SavedAccountsCache = new AccountsCache();
            _windowManager = windowManager;
            LoadAccountsCache();
        }

        public AccountsCache SavedAccountsCache { get; set; }

        public AuthenticationResult Login(string login, string password, bool iswWindowsAuth, bool rememberPassword,
            bool stayLoggedIn, string machineName)
        {
            var loginSession = _aaaServiceRemoteFasade.Login(login, password, iswWindowsAuth, machineName);
            return LogiCommon(loginSession, login, password, iswWindowsAuth, rememberPassword, stayLoggedIn);

            //if (!loginSession.IsSucces)
            //{
            //    _windowManager.Alert(@"������ ��� ����� � �������", loginSession.Message);
            //}

            //if (loginSession != null && loginSession.IsSucces)
            //{
            //    _activeAccountManager.ActiveAccount = loginSession.AccountInfo;
            //    _activeAccountManager.WhiteListObjects = loginSession.AllowedObjects;
            //    _activeAccountManager.BlackListObjects = loginSession.DeniedObjects;
            //    SavedAccountsCache.LastAccount = loginSession.AccountInfo;
            //    SavedAccountsCache.StayLoggedIn = stayLoggedIn;
            //    var existAccount = SavedAccountsCache.AccountSavedDatas.FirstOrDefault(p => p.Account == login);
            //    if (existAccount != null)
            //    {
            //        existAccount.Password = (rememberPassword ? password : null);
            //        existAccount.RememberPassword = rememberPassword;
            //        existAccount.StayLoggedIn = stayLoggedIn;
            //    }
            //    else
            //    {
            //        SavedAccountsCache.AccountSavedDatas.Add(new AccountsCache.AccountSavedData(login, (rememberPassword ? password : null), rememberPassword, stayLoggedIn));
            //    }

            //    SaveAccountsCache();
            //    EventAggregator.PublishOnCurrentThread(new AccountDataChanged { AuthenticationResult = loginSession });
            //}

            //return loginSession;
        }

        private AuthenticationResult LogiCommon(AuthenticationResult loginSession, string login, string password,
            bool iswWindowsAuth, bool rememberPassword, bool stayLoggedIn)
        {
            if (!loginSession.IsSucces)
            {
                _windowManager.Alert(@"������ ��� ����� � �������", loginSession.Message);
            }

            if (loginSession != null && loginSession.IsSucces)
            {
                _activeAccountManager.ActiveAccount = loginSession.AccountInfo;
                _activeAccountManager.WhiteListObjects = loginSession.AllowedObjects;
                _activeAccountManager.BlackListObjects = loginSession.DeniedObjects;
                SavedAccountsCache.LastAccount = loginSession.AccountInfo;
                SavedAccountsCache.StayLoggedIn = stayLoggedIn;
                var existAccount = SavedAccountsCache.AccountSavedDatas.FirstOrDefault(p => p.Account == login);
                if (existAccount != null)
                {
                    existAccount.Password = (rememberPassword ? password : null);
                    existAccount.RememberPassword = rememberPassword;
                    existAccount.StayLoggedIn = stayLoggedIn;
                }
                else
                {
                    SavedAccountsCache.AccountSavedDatas.Add(new AccountsCache.AccountSavedData(login,
                        (rememberPassword ? password : null), rememberPassword, stayLoggedIn));
                }

                SaveAccountsCache();
                EventAggregator.PublishOnCurrentThread(new AccountDataChanged {AuthenticationResult = loginSession});
            }

            return loginSession;
        }

        public AuthenticationResult LoginAsLast()
        {
            //  _activeAccountManager.ActiveAccount = SavedAccountsCache.LastAccount;
            //   var result = _aaaServiceRemoteFasade.LoginByToken(_activeAccountManager.ActiveAccount.Token);
            var loginSession = _aaaServiceRemoteFasade.LoginByToken(SavedAccountsCache.LastAccountToken);
            if (!loginSession.IsSucces) return loginSession;
            var password = "";
            var login = "";
            if (SavedAccountsCache != null && SavedAccountsCache.AccountSavedDatas != null &&
                loginSession.AccountInfo != null && loginSession.AccountInfo.Account != null)
            {
                password = SavedAccountsCache.AccountSavedDatas.FirstOrDefault(
                    p => p.Account == loginSession.AccountInfo.Account.Login).Password;
                login = loginSession.AccountInfo.Account.Login;
            }
            return LogiCommon(loginSession, login, password, false, true, true);
            //EventAggregator.PublishOnCurrentThread(new AccountDataChanged { AuthenticationResult = loginSession });
            //return loginSession;
        }

        public void Logoff()
        {
            _aaaServiceRemoteFasade.Logoff(_activeAccountManager.ActiveAccount.Token);
        }

        private void SaveAccountsCache()
        {
            lock (_fileLock)
            {
                var filePath = Path.GetTempPath() + @"\logins.sys";
                var serializer = new XmlSerializer(typeof (AccountsCache));
                using (TextWriter tw = new StreamWriter(filePath))
                {
                    serializer.Serialize(tw, SavedAccountsCache);
                }
            }
        }

        private void LoadAccountsCache()
        {
            lock (_fileLock)
            {
                var filePath = Path.GetTempPath() + @"\logins.sys";
                if (File.Exists(filePath))
                {
                    var serializer = new XmlSerializer(typeof (AccountsCache));
                    using (TextReader tr = new StreamReader(filePath))
                    {
                        SavedAccountsCache = (AccountsCache) serializer.Deserialize(tr);
                    }
                }
            }
        }

        [Serializable]
        public class AccountsCache
        {
            [XmlIgnore] [NonSerialized] private ActiveAccount _lastAccount;

            public AccountsCache()
            {
                AccountSavedDatas = new List<AccountSavedData>();
            }

            [XmlIgnore]
            public ActiveAccount LastAccount
            {
                get { return _lastAccount; }
                set
                {
                    _lastAccount = value;
                    LastAccountToken = _lastAccount.Token;
                    LastAccauntLogin = _lastAccount.Account.Login;
                }
            }

            public string LastAccauntLogin { get; set; }
            public Guid LastAccountToken { get; set; }
            public bool StayLoggedIn { get; set; }
            public List<AccountSavedData> AccountSavedDatas { get; set; }

            [Serializable]
            public class AccountSavedData
            {
                public AccountSavedData(string account, string password, bool rememberPassword, bool stayLoggedIn)
                {
                    Password = password;
                    RememberPassword = rememberPassword;
                    StayLoggedIn = stayLoggedIn;
                    Account = account;
                }

                public AccountSavedData()
                {
                }

                public String Account { get; set; }
                public String Password { get; set; }
                public bool RememberPassword { get; set; }
                public bool StayLoggedIn { get; set; }
            }
        }
    }
}