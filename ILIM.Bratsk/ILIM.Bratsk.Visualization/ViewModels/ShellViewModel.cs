using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using ILIM.Bratsk.Visualization.Infrastructure;

namespace ILIM.Bratsk.Visualization.ViewModels
{
    public class ShellViewModel : Conductor<Screen>, IShell
    {
        private readonly AAAManager _aaaManager;
        private readonly ILoginViewModelFactory _loginViewModelFactory;
        private readonly MainViewModel _mainViewModel;
        private readonly IWindowManager _windowManager;

        public ShellViewModel(MainViewModel mainViewModel, IWindowManager windowManager,
            ILoginViewModelFactory loginViewModelFactory, AAAManager aaaManager)
        {
            _mainViewModel = mainViewModel;
            _windowManager = windowManager;
            _loginViewModelFactory = loginViewModelFactory;
            _aaaManager = aaaManager;
        }

        #region Overrides of ViewAware

        /// <summary>
        ///     Called when an attached view's Loaded event fires.
        /// </summary>
        /// <param name="view" />
        protected override void OnViewLoaded(object view)
        {
            DisplayName = "������� ��������� ���������� �� ������� ���������";
            if (TryAutoLogin() || TryLoginDialog())
            {
                ActivateItem(_mainViewModel);
            }
            else
            {
                Application.Current.Shutdown();
            }

            base.OnViewLoaded(view);
        }

        private bool TryAutoLogin()
        {
            if (!_aaaManager.SavedAccountsCache.StayLoggedIn) return false;
            return _aaaManager.LoginAsLast().IsSucces;
        }

        private bool TryLoginDialog()
        {
            var loginViewModel = _loginViewModelFactory.Create(LoginType.FirstTime);
            while (
                _windowManager.ShowDialog(loginViewModel, null,
                    new Dictionary<string, object> {{"ResizeMode", ResizeMode.NoResize}, {"CanClose", false}}) != true)
            {
            }

            _loginViewModelFactory.Release(loginViewModel);
            return true; //(result.HasValue && result.Value);
        }

        #endregion
    }
}