using System;
using Caliburn.Micro;

namespace PAG.WBD.Milk.Visualization.ViewModels
{
    public abstract class DictionaryViewModelBase : Screen
    {
        private RelayCommand _saveItemCommandCommand;
        private RelayCommand _saveItemsCommandCommand;
        private RelayCommand _deleteItemCommandCommand;
        private RelayCommand _endEditHandlingCommand;
        
        public RelayCommand SaveItemCommand
        {
            get
            {
                if (_saveItemCommandCommand == null)
                {
                    _saveItemCommandCommand = new RelayCommand(ExecuteSaveItemCommand, CanExecuteSaveItemCommand);
                }
                return _saveItemCommandCommand;
            }
        }

        public RelayCommand SaveItemsCommand
        {
            get
            {
                if (_saveItemsCommandCommand == null)
                {
                    _saveItemsCommandCommand = new RelayCommand(ExecuteSaveItemsCommand, CanExecuteSaveItemsCommand);
                }
                return _saveItemsCommandCommand;
            }
        }

        public RelayCommand DeleteItemCommand
        {
            get
            {
                if (_deleteItemCommandCommand == null)
                {
                    _deleteItemCommandCommand = new RelayCommand(ExecuteDeleteItemCommand, CanExecuteDeleteItemCommand);
                }
                return _deleteItemCommandCommand;
            }
        }

        public RelayCommand EndEditHandlingCommand
        {
            get
            {
                if (_endEditHandlingCommand == null)
                {
                    _endEditHandlingCommand = new RelayCommand(ExecuteEndEditHandlingCommand, CanExecuteEndEditHandlingCommand);
                }
                return _endEditHandlingCommand;
            }
        }

        #region SaveItemCommand
        protected virtual bool CanExecuteSaveItemCommand(object obj)
        {
            throw new NotImplementedException();
        }

        protected virtual void ExecuteSaveItemCommand(object obj)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region SaveItemsCommand
        protected virtual bool CanExecuteSaveItemsCommand(object obj)
        {
            throw new NotImplementedException();
        }

        protected virtual void ExecuteSaveItemsCommand(object obj)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region DeleteItemCommand
        protected virtual bool CanExecuteDeleteItemCommand(object obj)
        {
            throw new NotImplementedException();
        }

        protected virtual void ExecuteDeleteItemCommand(object obj)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region EndEditHandlingCommand
        protected virtual bool CanExecuteEndEditHandlingCommand(object obj)
        {
            return true;
        }

        protected virtual void ExecuteEndEditHandlingCommand(object obj)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}