using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model.Alarms;
using ILIM.Bratsk.Domain.RemoteFacade;
using System.Linq;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels;

namespace ILIM.Bratsk.Visualization.ViewModels
{
    public class MainViewModel : Screen, IHandle<AccountDataChanged>
    {
        private readonly IAlarmRemoteService _alarmRemoteService;
        private IActiveAccountManager _activeAccountManager;
        public MainViewModel(IAlarmRemoteService alarmRemoteServic, IActiveAccountManager activeAccountManager)
        {
            _alarmRemoteService = alarmRemoteServic;
            _activeAccountManager = activeAccountManager;
        }

        public AlarmItem Message
        {
            get
            {
                var alarmItem = _alarmRemoteService.GetAlarmItems().OrderBy(p => p.AlarmLevel).FirstOrDefault();
                return alarmItem;
            }
        } 
        public ToolBarViewModel ToolBarRegion { get; set; }
        public DockViewModel DockRegion { get; set; }
        public MenuViewModel MenuRegion { get; set; }

        public bool IsCanEditSystemSettings { get { return _activeAccountManager.WhiteListObjects.Any(p => p == "SystemSettings"); } }
        public string LoginName{get { return _activeAccountManager.ActiveAccount.Account.Login; }}
        public void Handle(AccountDataChanged message)
        {
            NotifyOfPropertyChange(() => IsCanEditSystemSettings);
            NotifyOfPropertyChange(() => LoginName);
        }
    }
}