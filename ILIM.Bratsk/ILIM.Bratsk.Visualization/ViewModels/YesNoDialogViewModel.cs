﻿using Caliburn.Micro;
using ILIM.Bratsk.Visualization.Infrastructure;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ViewModels
{
    public class YesNoDialogViewModel : Screen
    {
        public string Message { get; set; }
        private RelayCommand _applyPlanCommand;
        public RelayCommand ApplyPlanCommand
        {
            get
            {
                return _applyPlanCommand ??
                       (_applyPlanCommand =
                        new RelayCommand(ExecuteApplyPlanCommand, CanApplyPlanCommand, CommandType.Delete,
                                         "Да", "Да"));
            }
        }

        private void ExecuteApplyPlanCommand(object obj)
        {
            TryClose(true);
        }

        private bool CanApplyPlanCommand(object obj)
        {
            return true;
        }


        private RelayCommand _closeCommand;
        public RelayCommand CloseCommand
        {
            get
            {
                return _closeCommand ??
                       (_closeCommand =
                        new RelayCommand(ExecuteCloseCommand, CanCloseCommand, CommandType.Delete,
                                         "Отмена", "Отмена"));
            }
        }

        private void ExecuteCloseCommand(object obj)
        {
            TryClose(false);
        }

        private bool CanCloseCommand(object obj)
        {
            return true;
        }


        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView() as RadWindow;
            if (window != null)
                window.Close();
        }
    }
}
