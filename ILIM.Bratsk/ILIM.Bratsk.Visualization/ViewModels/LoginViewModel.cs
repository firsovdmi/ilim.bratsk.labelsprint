using System;
using System.Linq;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ViewModels
{
    public class LoginViewModel : Screen
    {
        private const string DummyPassword = "1111111111111";
        private string _account;
        private AAAManager.AccountsCache _accountsCache;
        private RelayCommand _exitCommand;
        private RelayCommand<Func<string>> _loginCommand;
        private bool _rememberPassword;
        private bool _stayLoggedIn;
        private readonly AAAManager _aaaManager;
        private readonly IAAAServiceRemoteFasade _aaaServiceRemoteFasade;
        private readonly LoginType _loginType;

        #region Constructor

        public LoginViewModel(AAAManager aaaManager, LoginType loginType = LoginType.Relogin)
        {
            _aaaManager = aaaManager;
            _loginType = loginType;
        }

        #endregion

        #region Private methods

        private void RefreshPassword(string password)
        {
            var refreshPasswordEvent = RefreshPasswordEvent;
            if (refreshPasswordEvent != null)
                refreshPasswordEvent.Invoke(this, new RefreshPasswordEventArgs(password));
        }

        #endregion

        #region Public proprties

        public String Account
        {
            get { return _account; }
            set
            {
                if (value == _account) return;
                //���� ������ ����� � ����, � ����� ���, �� ��������� ����������
                if (AccountsCache.AccountSavedDatas.Select(p => p.Account).Contains(_account) &&
                    !AccountsCache.AccountSavedDatas.Select(p => p.Account).Contains(value))
                {
                    RememberPassword = false;
                    StayLoggedIn = false;
                    RefreshPassword("");
                }
                _account = value;
                NotifyOfPropertyChange("Account");
                //���� ����� ����� � ����, �� ���������� ���������� �� ����
                if (AccountsCache.AccountSavedDatas.Select(p => p.Account).Contains(_account))
                {
                    var cache = AccountsCache.AccountSavedDatas.FirstOrDefault(p => p.Account == _account);
                    RememberPassword = cache.RememberPassword;
                    StayLoggedIn = cache.StayLoggedIn;
                    if (RememberPassword)
                        RefreshPassword(DummyPassword);
                }
            }
        }

        public event EventHandler<RefreshPasswordEventArgs> RefreshPasswordEvent;

        public bool RememberPassword
        {
            get { return _rememberPassword; }
            set
            {
                if (value.Equals(_rememberPassword)) return;
                _rememberPassword = value;
                NotifyOfPropertyChange("RememberPassword");
            }
        }

        public bool StayLoggedIn
        {
            get { return _stayLoggedIn; }
            set
            {
                if (value.Equals(_stayLoggedIn)) return;
                _stayLoggedIn = value;
                NotifyOfPropertyChange("StayLoggedIn");
            }
        }

        public AAAManager.AccountsCache AccountsCache
        {
            get { return _accountsCache; }
            set
            {
                if (Equals(value, _accountsCache)) return;
                _accountsCache = value;
                NotifyOfPropertyChange("AccountsCache");
            }
        }

        public RelayCommand<Func<string>> LoginCommand
        {
            get
            {
                if (_loginCommand == null)
                {
                    _loginCommand = new RelayCommand<Func<string>>(ExecuteLoginCommand, CanExecuteLoginCommand,
                        CommandType.Login, "�����", "����� � �������");
                }
                return _loginCommand;
            }
        }

        public RelayCommand ExitCommand
        {
            get
            {
                if (_exitCommand == null)
                {
                    _exitCommand = new RelayCommand(ExecuteExitCommand, CanExecuteExitCommand, CommandType.Exit,
                        "������", "������");
                }
                return _exitCommand;
            }
        }

        #endregion

        #region ExitCommand

        private bool CanExecuteExitCommand(object obj)
        {
            return true;
        }

        private void ExecuteExitCommand(object obj)
        {
            TryClose(false);
        }

        #endregion

        #region LoginCommand

        private bool CanExecuteLoginCommand(Func<string> obj)
        {
            return true;
        }

        private void ExecuteLoginCommand(Func<string> passwordGetter)
        {
            var password = passwordGetter.Invoke();
            LoginAction(password);
        }

        private void LoginAction(string password)
        {
            if (password == DummyPassword && AccountsCache.AccountSavedDatas.Select(p => p.Account).Contains(_account))
            {
                password = AccountsCache.AccountSavedDatas.FirstOrDefault(p => p.Account == _account).Password;
            }

            var loginResult = _aaaManager.Login(Account, password, false, RememberPassword, StayLoggedIn, System.Environment.MachineName);
            if (loginResult.IsSucces)
            {
                TryClose(true);
            }
        }

        #endregion

        #region Overrides of Screen

        /// <summary>
        ///     Called when initializing.
        /// </summary>
        protected override void OnInitialize()
        {
            DisplayName = "����";
            AccountsCache = _aaaManager.SavedAccountsCache;

            Account = _aaaManager.SavedAccountsCache == null ? null : _aaaManager.SavedAccountsCache.LastAccauntLogin;

            base.OnInitialize();
            if (_aaaManager.SavedAccountsCache.StayLoggedIn && _loginType == LoginType.FirstTime)
            {
                var loginResult = _aaaManager.LoginAsLast();
                if (loginResult.IsSucces)
                    TryClose(true);
            }
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            if (_aaaManager.SavedAccountsCache.StayLoggedIn && _loginType == LoginType.FirstTime)
            {
                var loginResult = _aaaManager.LoginAsLast();
                if (loginResult.IsSucces)
                {
                    TryClose(true);
                }
            }
        }

        /// <summary>
        ///     Tries to close this instance by asking its Parent to initiate shutdown or by asking its corresponding view to
        ///     close.
        ///     Also provides an opportunity to pass a dialog result to it's corresponding view.
        /// </summary>
        /// <param name="dialogResult">The dialog result.</param>
        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView() as RadWindow;
            if (window != null)
                window.Close();
        }

        #endregion
    }

    public enum LoginType
    {
        FirstTime,
        Relogin
    }


    public class RefreshPasswordEventArgs : EventArgs
    {
        public RefreshPasswordEventArgs(string refreshPassword)
        {
            RefreshPassword = refreshPassword;
        }

        public string RefreshPassword { get; private set; }
    }
}