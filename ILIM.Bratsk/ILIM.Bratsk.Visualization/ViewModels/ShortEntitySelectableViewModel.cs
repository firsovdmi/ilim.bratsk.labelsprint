using Caliburn.Micro;

namespace ILIM.Bratsk.Visualization.ViewModels
{
    public class SelectableViewModel<T> : PropertyChangedBase
    {
        private T _content;
        private bool _isSelected;

        public SelectableViewModel(T content)
        {
            Content = content;
        }

        public T Content
        {
            get { return _content; }
            set
            {
                if (Equals(value, _content)) return;
                _content = value;
                NotifyOfPropertyChange("Content");
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value.Equals(_isSelected)) return;
                _isSelected = value;
                NotifyOfPropertyChange("IsSelected");
            }
        }
    }
}