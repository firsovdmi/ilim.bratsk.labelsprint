﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace ILIM.Bratsk.Visualization.ErrorMessage
{
    /// <summary>
    ///     Interaction logic for ErrorWindow.xaml
    /// </summary>
    public partial class ErrorWindow : Window
    {
        private const int GWL_STYLE = -16;
        private const uint WS_SYSMENU = 0x80000;

        public ErrorWindow()
        {
            InitializeComponent();
        }

        [DllImport("user32.dll")]
        private static extern uint GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, uint dwNewLong);

        protected override void OnSourceInitialized(EventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE,
                GetWindowLong(hwnd, GWL_STYLE) & (0xFFFFFFFF ^ WS_SYSMENU));

            base.OnSourceInitialized(e);
        }

        public static bool ShowError(string ErrorCaption, string ErrorMessage, string ErrorStack)
        {
            var EW = new ErrorWindow();
            EW.tbErrorCaption.Text = ErrorCaption;
            EW.tbErrorMessage.Text = ErrorMessage;
            EW.tbStack.Text = ErrorStack;

            return (bool) EW.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}