﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Caliburn.Micro;
using ILIM.Bratsk.Visualization.Converters;
using ILIM.Bratsk.Visualization.Infrastructure;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Docking;
using Telerik.Windows.Controls.TabControl;

namespace ILIM.Bratsk.Visualization
{
    public class RadDockingManager : IRadDockingManager
    {
        private RadDocking _radDocking;

        public RadDocking RadDocking
        {
            get
            {
                if (_radDocking == null)
                    Init(null);
                return _radDocking;
            }
        }

        public IScreen RootViewModel { get; set; }

        public void Init(IViewAware parentViewModel)
        {
            _radDocking = GetRadDocking(parentViewModel);
        }

        public void ShowWindow(IViewAware viewModel,
            object context,
            Action<RadPaneEx> configRadPane = null,
            Action<RadPaneGroup> configRadPaneGroup = null,
            Action<RadSplitContainer> configSplitContainer = null)
        {
            var radPane = CreateRadPane(viewModel, context);
            if (configRadPane != null) configRadPane(radPane);

            if (RadDocking.GetRadPane(radPane) != null)
            {
                RadDocking.ActivePane = radPane;
                return;
            }

            AttachPane(radPane, configRadPaneGroup, configSplitContainer);
        }

        public void ShowDockedWindow(IViewAware viewModel,
            object context,
            DockPosition dockPosition = DockPosition.Left,
            Action<RadPaneEx> configRadPane = null,
            Action<RadPaneGroup> configRadPaneGroup = null,
            Action<RadSplitContainer> configSplitContainer = null)
        {
            var radPane = CreateRadPane(viewModel, context);
            if (configRadPane != null) configRadPane(radPane);

            if (RadDocking.GetRadPane(radPane) != null) return;

            AttachDockedPane(radPane, configRadPaneGroup, configSplitContainer, dockPosition);
        }

        public void ShowFloatingDockableWindow(IViewAware viewModel,
            object context,
            Action<RadPaneEx> configRadPane = null,
            Action<RadPaneGroup> configRadPaneGroup = null,
            Action<RadSplitContainer> configSplitContainer = null)
        {
            var radPane = CreateRadPane(viewModel, context);
            if (configRadPane != null) configRadPane(radPane);

            if (RadDocking.GetRadPane(radPane) != null) return;

            AttachFloatingDockablePane(radPane, configRadPaneGroup, configSplitContainer);
        }

        public void ShowFloatingWindow(IViewAware viewModel,
            object context,
            Action<RadPaneEx> configRadPane = null,
            Action<RadPaneGroup> configRadPaneGroup = null,
            Action<RadSplitContainer> configSplitContainer = null)
        {
            var radPane = CreateRadPane(viewModel, context);
            if (configRadPane != null) configRadPane(radPane);

            if (RadDocking.GetRadPane(radPane) != null) return;

            AttachFloatingPane(radPane, configRadPaneGroup, configSplitContainer);
        }

        public void CloseAllWindows()
        {
            foreach (var radPane1 in RadDocking.Panes.ToList())
            {
                var radPane = radPane1 as RadPaneEx;
                if (radPane != null)
                {
                    radPane.Close();
                    radPane.Content = null;
                }
            }
        }

        public void LoadLayout()
        {
            // Load your layot from the isolated storage.
            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var isoStream = storage.OpenFile("RadDocking_Layout.xml", FileMode.Open))
                {
                    RadDocking.LoadLayout(isoStream);
                }
            }
        }

        public string SaveLayout()
        {
            string xml;
            // SaveCommand your layout for example in the isolated storage.
            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var isoStream = storage.OpenFile("RadDocking_Layout.xml", FileMode.OpenOrCreate))
                {
                    RadDocking.SaveLayout(isoStream);
                    isoStream.Seek(0, SeekOrigin.Begin);
                    var reader = new StreamReader(isoStream);
                    xml = reader.ReadToEnd();
                }
            }
            // Return the generated XML
            return xml;
        }

        private static RadDocking GetRadDocking(IViewAware parentViewModel)
        {
            if (parentViewModel == null)
                return Application.Current.MainWindow.FindChildByType<RadDocking>();

            return FindVisualChild<RadDocking>(parentViewModel.GetView() as DependencyObject);
        }

        private RadPaneEx CreateRadPane(object viewModel, object context)
        {
            var view = EnsureRadPane(viewModel, ViewLocator.LocateForModel(viewModel, null, context));
            ViewModelBinder.Bind(viewModel, view, context);

            var haveDisplayName = viewModel as IHaveDisplayName;
            if (haveDisplayName != null && !ConventionManager.HasBinding(view, RadPane.TitleProperty))
            {
                var haveStatus = viewModel as IEditStatus;
                {
                    if (haveStatus != null)
                    {
                        var binding1 = new Binding("IsAdded") {Mode = BindingMode.TwoWay};
                        var binding2 = new Binding("IsDeleted") {Mode = BindingMode.TwoWay};
                        var binding3 = new Binding("IsEdited") {Mode = BindingMode.TwoWay};
                        var binding4 = new Binding("DisplayName") {Mode = BindingMode.TwoWay};
                        var multibinding = new MultiBinding {Converter = new DisplayNameConverter()};
                        multibinding.Bindings.Add(binding1);
                        multibinding.Bindings.Add(binding2);
                        multibinding.Bindings.Add(binding3);
                        multibinding.Bindings.Add(binding4);
                        view.SetBinding(HeaderedContentControl.HeaderProperty, multibinding);
                    }
                    else
                    {
                        var binding = new Binding("DisplayName") {Mode = BindingMode.TwoWay};
                        view.SetBinding(HeaderedContentControl.HeaderProperty, binding);
                    }
                }
            }

            new DockableWindowConductor(viewModel, view, RadDocking);

            return view;
        }

        private RadPaneEx EnsureRadPane(object viewModel, object view)
        {
            var radPane = view as RadPaneEx;

            if (radPane == null)
            {
                radPane = new RadPaneEx {DataContext = viewModel, Content = view};
                radPane.SetValue(View.IsGeneratedProperty, true);
            }

            return radPane;
        }

        private RadSplitContainer GetRadSplitContainer(Action<RadSplitContainer> configSplitContainer)
        {
            var radSplitContainer = RadDocking.DocumentHost as RadSplitContainer;
            if (radSplitContainer == null)
            {
                radSplitContainer = new RadSplitContainer();
                radSplitContainer.SetValue(View.IsGeneratedProperty, true);
                RadDocking.DocumentHost = radSplitContainer;
            }

            if (configSplitContainer != null) configSplitContainer(radSplitContainer);
            return radSplitContainer;
        }

        private void AddPaneToSplitAndPaneGroup(Action<RadPaneGroup> configRadPaneGroup,
            RadSplitContainer radSplitContainer, RadPaneEx radPane)
        {
            var radPaneGroup = radSplitContainer.Items.OfType<RadPaneGroup>().FirstOrDefault();
            if (radPaneGroup == null)
            {
                radPaneGroup = new RadPaneGroup();
                radPaneGroup.SetValue(View.IsGeneratedProperty, true);
                radPaneGroup.SelectedItemRemoveBehaviour = SelectedItemRemoveBehaviour.SelectNone;
                radSplitContainer.Items.Add(radPaneGroup);
            }

            if (configRadPaneGroup != null) configRadPaneGroup(radPaneGroup);
            try
            {
                radPaneGroup.Items.Add(radPane);
            }
            catch
            {
            }

        }

        private void AttachPane(RadPaneEx radPane, Action<RadPaneGroup> configRadPaneGroup,
            Action<RadSplitContainer> configSplitContainer)
        {
            var radSplitContainer = GetRadSplitContainer(configSplitContainer);
            AddPaneToSplitAndPaneGroup(configRadPaneGroup, radSplitContainer, radPane);
        }

        private void AttachDockedPane(RadPaneEx radPane, Action<RadPaneGroup> configRadPaneGroup,
            Action<RadSplitContainer> configSplitContainer, DockPosition dockPosition)
        {
            var radSplitContainer = GetRadSplitContainer(configSplitContainer);
            AddPaneToSplitAndPaneGroup(configRadPaneGroup, radSplitContainer, radPane);
        }

        private void AttachFloatingDockablePane(RadPaneEx radPane, Action<RadPaneGroup> configRadPaneGroup,
            Action<RadSplitContainer> configSplitContainer)
        {
            var radSplitContainer = GetRadSplitContainer(configSplitContainer);
            AddPaneToSplitAndPaneGroup(configRadPaneGroup, radSplitContainer, radPane);


            radPane.MakeFloatingDockable();
            var window = radPane.GetParentToolWindow();
            if (window != null)
            {
                var content = radPane.Content as FrameworkElement;
                if (content != null)
                {
                    window.Left = 100;
                    window.Top = 100;
                }
            }
        }

        private void AttachFloatingPane(RadPaneEx radPane, Action<RadPaneGroup> configRadPaneGroup,
            Action<RadSplitContainer> configSplitContainer)
        {
            var radSplitContainer = GetRadSplitContainer(configSplitContainer);
            AddPaneToSplitAndPaneGroup(configRadPaneGroup, radSplitContainer, radPane);
            radPane.MakeFloatingOnly();
        }

        private static TChildItem FindVisualChild<TChildItem>(DependencyObject obj)
            where TChildItem : DependencyObject
        {
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                var child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is TChildItem)
                    return (TChildItem) child;

                var childOfChild = FindVisualChild<TChildItem>(child);
                if (childOfChild != null)
                    return childOfChild;
            }
            return null;
        }

        private class DockableWindowConductor
        {
            private static bool _eventsRegistered;
            private static bool _selectEventsRegistered;

            public DockableWindowConductor(object viewModel, RadPaneEx view, RadDocking radDocking)
            {
                RadDocking = radDocking;
                ViewModel = viewModel;
                View = view;

                var activatable = viewModel as IActivate;
                if (activatable != null)
                    activatable.Activate();
                var selectable = viewModel as ISelect;
                if (selectable != null)
                {
                    //selectable.Select();
                    if (!_selectEventsRegistered)
                    {
                        _selectEventsRegistered = true;
                        radDocking.ActivePaneChanged += OnPaneChanges;
                        radDocking.Close += radDocking_Close;
                        radDocking.PaneStateChange += radDocking_PaneStateChange;
                    }
                }
                var deactivatable = viewModel as IDeactivate;
                if (deactivatable != null)
                {
                    //only want these registered once
                    if (!_eventsRegistered)
                    {
                        _eventsRegistered = true;
                        radDocking.PreviewClose += OnClosing;
                        radDocking.Close += OnClosed;
                    }

                    view.OnClose += OnClosed;
                    deactivatable.Deactivated += OnDeactivated;
                }

                var guard = viewModel as IGuardClose;
                if (guard == null)
                {
                    //only want these registered once
                    if (!_eventsRegistered)
                    {
                        _eventsRegistered = true;
                        radDocking.Close += OnClosed;
                    }
                }
            }

            private RadDocking RadDocking { get; set; }
            private object ViewModel { get; set; }
            private RadPaneEx View { get; set; }
            private bool IsClosing { get; set; }
            private bool IsDeactivatingFromView { get; set; }
            private bool IsDeactivatingFromViewModel { get; set; }

            private void radDocking_PaneStateChange(object sender, RadRoutedEventArgs e)
            {
            }

            private void radDocking_Close(object sender, StateChangeEventArgs e)
            {
                if (e.Panes.FirstOrDefault() != null)
                {
                    var oldViewModel = e.Panes.FirstOrDefault().DataContext as ISelect;
                    if (oldViewModel != null)
                        oldViewModel.Unselect();
                }
            }

            private void OnPaneChanges(object sender, ActivePangeChangedEventArgs e)
            {
                if (e.OldPane != null)
                {
                    var oldViewModel = e.OldPane.DataContext as ISelect;
                    if (oldViewModel != null)
                        oldViewModel.Unselect();
                }
                if (e.NewPane != null)
                {
                    var newViewModel = e.NewPane.DataContext as ISelect;
                    if (newViewModel != null)
                        newViewModel.Select();
                }
            }

            private void OnClosed(object sender, StateChangeEventArgs e)
            {
                var tempRadPaneEx = sender as RadPaneEx;

                //if RadPaneEx initiated the close, un-registered OnClosed
                if (tempRadPaneEx != null)
                    View.OnClose -= OnClosed;

                var radPane = e.Panes.FirstOrDefault();
                if (radPane == null) return;

                var deactivatable = radPane.DataContext as IDeactivate;
                if (deactivatable == null) return;

                if (IsDeactivatingFromViewModel)
                    return;

                IsDeactivatingFromView = true;
                RemovePane(radPane);
                deactivatable.Deactivate(true);
                IsDeactivatingFromView = false;
            }

            private void OnDeactivated(object sender, DeactivationEventArgs e)
            {
                ((IDeactivate) ViewModel).Deactivated -= OnDeactivated;

                if (!e.WasClosed)
                    return;

                IsDeactivatingFromViewModel = true;
                IsClosing = true;
                RemovePane(FindPane(sender));
                IsClosing = false;
                IsDeactivatingFromViewModel = false;
            }

            private void OnClosing(object sender, StateChangeEventArgs e)
            {
                var radPane = e.Panes.FirstOrDefault();
                if (radPane == null)
                    return;

                if (IsClosing)
                {
                    IsClosing = false;
                    return;
                }

                var guard = radPane.DataContext as IGuardClose;
                if (guard == null)
                    return;

                if (e.Handled)
                    return;

                if (IsClosing)
                {
                    IsClosing = false;
                    return;
                }

                bool running = false, shouldEnd = false;

                var tempRunning = running;
                guard.CanClose(canClose => Execute.OnUIThread(() =>
                {
                    if (tempRunning && canClose)
                    {
                        IsClosing = true;
                        radPane.IsHidden = true;
                    }
                    else
                        e.Handled = !canClose;

                    shouldEnd = true;
                }));

                if (shouldEnd)
                    return;
                //radPane.RemoveFromParent();
                running = e.Handled = true;
            }

            private RadPane FindPane(object viewModel)
            {
                return RadDocking.Panes.FirstOrDefault(p => p.DataContext == viewModel);
            }

            private void RemovePane(RadPane pane)
            {
                //return;
                if (pane == null)
                    return;

                var paneGroup = pane.PaneGroup;
                pane.RemoveFromParent();
                return;
                if (paneGroup == null || paneGroup.HasItems)
                    return;

                var splitContainer = paneGroup.ParentContainer;
                paneGroup.RemoveFromParent();
                if (splitContainer == null || splitContainer.HasItems)
                    return;

                if (splitContainer.IsInDocumentHost)
                    RadDocking.DocumentHost = null;
                RadDocking.Items.Remove(splitContainer);
            }
        }

        #region RadDocking events

        //PaneStateChanged - This event is fired whenever the state of the RadPane is changed (e.g. pin, unpin, close, show, etc.).
        //Pin\Unpin Events
        //PreviewUnpin - Occurs before the RadPane is unpinned. The type of the passed event arguments is StateChangeEventArgs.
        //Unpin - Occurs when the RadPane is unpinned. The type of the passed event arguments is StateChangeEventArgs.
        //PreviewPin - Occurs before the RadPane is pinned. The type of the passed event arguments is StateChangeEventArgs.
        //Pin - Occurs when the RadPane is pinned. The type of the passed event arguments is StateChangeEventArgs.

        //Show\Hide Events
        //PreviewShow - Occurs before the RadPane is shown.
        //Show - Occurs when the RadPane is shown.
        //PreviewClose - Occurs before the RadPane is closed.
        //Close - Occurs when the RadPane is closed.
        //PreviewWindowClose - Occurs before the ToolWindow is closed.
        //WindowClose - Occurs when the ToolWindow is closed.

        //The PreviewClose and Close events are fired, whenever you press the Close button.
        //The PreviewShow and Show events are fired, when you invoke the ShowAllPanes method of the RadPaneGroup class. This method will show all hidden panes.

        #endregion
    }


    //public class DockManager : IDockManager
    //{
    //    private IConductActiveItem _conductor;
    //    private RadDocking _dock;

    //    private bool _activatedFromViewModel;
    //    private bool _actuallyClosing;
    //    private bool _deactivateFromViewModel;
    //    private bool _deactivatingFromView;


    //    public void Link(IConductor conductor, FrameworkElement dock = null)
    //    {
    //        if (_conductor != null || _dock != null)
    //        {
    //            throw new InvalidOperationException("Dock manager is already linked");
    //        }

    //        _conductor = conductor as IConductActiveItem;
    //        _dock = dock as RadDocking ?? FindDock();

    //        if (_conductor == null || _dock == null)
    //        {
    //            throw new InvalidOperationException("Invalid conductor or docking control");
    //        }

    //        _conductor.ActivationProcessed += OnActivationProcessed;
    //        _dock.ActivePaneChanged += OnActivePaneChanged;
    //        _dock.PreviewClose += OnPreviewClose;
    //        _dock.Close += OnClose;
    //    }


    //    protected virtual void OnActivationProcessed(object s, ActivationProcessedEventArgs e)
    //    {
    //        if (!e.Success)
    //        {
    //            return;
    //        }

    //        object viewModel = e.Item;
    //        RadPane pane = FindPane(viewModel);
    //        if (pane == null)
    //        {
    //            _activatedFromViewModel = true;

    //            pane = CreatePane(viewModel);
    //            AttachPane(viewModel, pane);

    //            var deactivatable = viewModel as IDeactivate;
    //            if (deactivatable != null)
    //            {
    //                deactivatable.Deactivated += OnDeactivated;
    //            }
    //        }

    //        _dock.ActivePane = pane;
    //    }


    //    protected virtual void OnActivePaneChanged(object s, ActivePangeChangedEventArgs e)
    //    {
    //        if (_activatedFromViewModel)
    //        {
    //            _activatedFromViewModel = false;
    //            return;
    //        }

    //        RadPane pane = e.NewPane;
    //        if (pane == null)
    //        {
    //            return;
    //        }

    //        _conductor.ActivateItem(pane.DataContext);
    //    }


    //    protected virtual void OnDeactivated(object s, DeactivationEventArgs e)
    //    {
    //        if (!e.WasClosed)
    //        {
    //            return;
    //        }

    //        ((IDeactivate)s).Deactivated -= OnDeactivated;

    //        if (_deactivatingFromView)
    //        {
    //            return;
    //        }

    //        _deactivateFromViewModel = true;
    //        RemovePane(FindPane(s));
    //        _deactivateFromViewModel = false;
    //    }


    //    protected virtual void OnPreviewClose(object s, StateChangeEventArgs e)
    //    {
    //        RadPane pane = e.Panes.FirstOrDefault();
    //        if (pane == null)
    //        {
    //            return;
    //        }

    //        var guard = pane.DataContext as IGuardClose;
    //        if (guard == null)
    //        {
    //            return;
    //        }

    //        if (e.Handled)
    //        {
    //            return;
    //        }

    //        if (_actuallyClosing)
    //        {
    //            _actuallyClosing = false;
    //            return;
    //        }

    //        bool runningAsync = false;
    //        bool shouldEnd = false;

    //        guard.CanClose(canClose =>
    //        {
    //            Execute.OnUIThread(() =>
    //            {
    //                if (runningAsync && canClose)
    //                {
    //                    _actuallyClosing = true;
    //                    pane.IsHidden = true;
    //                }
    //                else
    //                {
    //                    e.Handled = !canClose;
    //                }

    //                shouldEnd = true;
    //            });
    //        });

    //        if (shouldEnd)
    //        {
    //            return;
    //        }

    //        e.Handled = true;
    //        runningAsync = true;
    //    }


    //    protected virtual void OnClose(object s, StateChangeEventArgs e)
    //    {
    //        RadPane pane = e.Panes.FirstOrDefault();
    //        if (pane == null)
    //        {
    //            return;
    //        }

    //        var deactivatable = pane.DataContext as IDeactivate;
    //        if (deactivatable == null)
    //        {
    //            return;
    //        }

    //        if (_deactivateFromViewModel)
    //        {
    //            return;
    //        }

    //        _deactivatingFromView = true;
    //        RemovePane(pane);
    //        deactivatable.Deactivate(true);
    //        _deactivatingFromView = false;
    //    }


    //    protected virtual RadDocking FindDock()
    //    {
    //        return Application.Current.MainWindow.FindChildByType<RadDocking>();
    //    }


    //    protected virtual RadPane FindPane(object viewModel)
    //    {
    //        return _dock.Panes.FirstOrDefault(p => p.DataContext == viewModel);
    //    }


    //    protected virtual RadPane CreatePane(object viewModel)
    //    {
    //        RadPane pane = EnsurePane(viewModel, ViewLocator.LocateForModel(viewModel, null, null));
    //        ViewModelBinder.Bind(viewModel, pane, null);

    //        var haveDisplayName = viewModel as IHaveDisplayName;
    //        if (haveDisplayName != null && !ConventionManager.HasBinding(pane, HeaderedContentControl.HeaderProperty))
    //        {
    //            pane.Header = haveDisplayName.name;
    //        }

    //        return pane;
    //    }


    //    protected virtual RadPane EnsurePane(object viewModel, object view)
    //    {
    //        var pane = view as RadPane;

    //        if (pane == null)
    //        {
    //            pane = new RadPane { DataContext = viewModel, Content = view };
    //            pane.SetValue(Caliburn.Micro.View.IsGeneratedProperty, true);
    //        }

    //        return pane;
    //    }


    //    protected virtual void AttachPane(object viewModel, RadPane pane)
    //    {
    //        DockState? dockState = GetDockState(viewModel);

    //        RadSplitContainer splitContainer;
    //        if (dockState == null)
    //        {
    //            splitContainer = _dock.DocumentHost as RadSplitContainer;
    //            if (splitContainer == null)
    //            {
    //                splitContainer = new RadSplitContainer();
    //                splitContainer.SetValue(Caliburn.Micro.View.IsGeneratedProperty, true);
    //                _dock.DocumentHost = splitContainer;
    //            }
    //        }
    //        else
    //        {
    //            splitContainer = _dock.Items.OfType<RadSplitContainer>().FirstOrDefault(
    //                x => x.GetValue(RadDocking.DockStateProperty) as DockState? == dockState);
    //            if (splitContainer == null)
    //            {
    //                splitContainer = new RadSplitContainer { InitialPosition = dockState.Value };
    //                splitContainer.SetValue(Caliburn.Micro.View.IsGeneratedProperty, true);
    //                _dock.Items.Add(splitContainer);
    //            }
    //        }

    //        RadPaneGroup paneGroup = splitContainer.Items.OfType<RadPaneGroup>().FirstOrDefault();
    //        if (paneGroup == null)
    //        {
    //            paneGroup = new RadPaneGroup { IsContentPreserved = true };
    //            paneGroup.SetValue(Caliburn.Micro.View.IsGeneratedProperty, true);
    //            paneGroup.SelectedItemRemoveBehaviour = SelectedItemRemoveBehaviour.SelectNone;
    //            splitContainer.Items.Add(paneGroup);
    //        }

    //        paneGroup.AddItem(pane, DockPosition.Center);
    //    }


    //    protected virtual void RemovePane(RadPane pane)
    //    {
    //        if (pane == null)
    //        {
    //            return;
    //        }

    //        RadPaneGroup paneGroup = pane.PaneGroup;
    //        pane.RemoveFromParent();
    //        if (paneGroup == null || paneGroup.HasItems)
    //        {
    //            return;
    //        }

    //        RadSplitContainer splitContainer = paneGroup.ParentContainer;
    //        paneGroup.RemoveFromParent();
    //        if (splitContainer == null || splitContainer.HasItems)
    //        {
    //            return;
    //        }

    //        if (splitContainer.IsInDocumentHost)
    //        {
    //            _dock.DocumentHost = null;
    //        }
    //        else
    //        {
    //            _dock.Items.Remove(splitContainer);
    //        }
    //    }


    //    protected virtual DockState? GetDockState(object viewModel)
    //    {
    //        if (viewModel is IHavePaneSettings)
    //        {
    //            var paneSettings = (IHavePaneSettings)viewModel;

    //            switch (paneSettings.InitialState)
    //            {
    //                case PaneState.DockedLeft:
    //                    return DockState.DockedLeft;
    //                case PaneState.DockedTop:
    //                    return DockState.DockedTop;
    //                case PaneState.DockedRight:
    //                    return DockState.DockedRight;
    //                case PaneState.DockedBottom:
    //                    return DockState.DockedBottom;
    //            }
    //        }
    //        return null;
    //    }
    //}
    //public interface IHavePaneSettings
    //{
    //    PaneState InitialState { get; }
    //}


    //public enum PaneState
    //{
    //    DockedCenter,
    //    DockedLeft,
    //    DockedTop,
    //    DockedRight,
    //    DockedBottom
    //}

    //public interface IDockManager
    //{
    //    void Link(IConductor conductor, FrameworkElement dock = null);
    //}
}