using System;

namespace PAG.WBD.Milk.Visualization.ViewModels
{
    public class ShowDictionaryAction : ActionItem
    {
        protected IRadDockingManager _radDockingManager;
        

        protected ShowDictionaryAction(string displayName) : base(displayName)
        {
        }

        /// <summary>
        /// Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public ShowDictionaryAction(string displayName, Action execute, Func<bool> canExecute = null) : base(displayName, execute, canExecute)
        {
        }


    }
}