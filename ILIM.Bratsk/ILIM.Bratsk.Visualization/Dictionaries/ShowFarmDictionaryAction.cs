using System;
using System.Data;
using Caliburn.Micro;
using StateChangeEventArgs = Telerik.Windows.Controls.Docking.StateChangeEventArgs;

namespace PAG.WBD.Milk.Visualization.ViewModels
{
    public class ShowFarmDictionaryAction : ShowDictionaryAction
    {
        protected IFarmDictionaryViewModelFactory _viewAwareFactory;
        public ShowFarmDictionaryAction(IRadDockingManager radDockingManager, IFarmDictionaryViewModelFactory viewAwareFactory):base("�����")
        {
            _radDockingManager = radDockingManager;
            _viewAwareFactory = viewAwareFactory;
        }
        /// <summary>
        /// The action associated to the ActionItem
        /// </summary>
        public override void Execute()
        {
            var viewmodel = _viewAwareFactory.Create();
            viewmodel.Deactivated +=  Release;
            _radDockingManager.ShowWindow(_viewAwareFactory.Create(), null);
        }

        private void Release(object sender, DeactivationEventArgs e)
        {
            var model = (FarmDictionaryViewModel) sender;
            model.Deactivated -= Release;
            _viewAwareFactory.Release((FarmDictionaryViewModel)sender);
        }




    }


    public class SaveDictionaryAction : ActionItem, IToolBarAction
    {
        private readonly Func<bool> canExecuteInToolBar;
        protected IFarmDictionaryViewModelFactory _viewAwareFactory;
        public SaveDictionaryAction(IRadDockingManager radDockingManager, IFarmDictionaryViewModelFactory viewAwareFactory)
            : base("���������")
        {
            this.canExecuteInToolBar = canExecuteInToolBar ?? (() => IsActive);
        }
        /// <summary>
        /// The action associated to the ActionItem
        /// </summary>
        public override void Execute()
        {
            var viewmodel = _viewAwareFactory.Create();
            viewmodel.Deactivated += Release;

        }

        private void Release(object sender, DeactivationEventArgs e)
        {
            var model = (FarmDictionaryViewModel)sender;
            model.Deactivated -= Release;
            _viewAwareFactory.Release((FarmDictionaryViewModel)sender);
        }

        #region Overrides of ActionItem

        /// <summary>
        /// Calls the underlying canExecute function.
        /// </summary>
        public override bool CanExecute
        {
            get { canExecuteInToolBar(); }
        }

        #endregion
    }

    public interface IToolBarAction
    {
        
    }
}