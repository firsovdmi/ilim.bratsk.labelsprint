﻿using System.Configuration;

namespace ILIM.Bratsk.Visualization
{
    internal static class Settings
    {
        /// <summary>
        ///     Строка подключения
        /// </summary>
        public static string ConnectionString
        {
            get { return ConfigurationSettings.AppSettings["ConnectionString"]; }
        }
    }
}