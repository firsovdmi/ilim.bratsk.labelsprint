﻿using System.Configuration;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public sealed class CommonSettings
    {
        private CommonSettings()
        {
        }

        static CommonSettings()
        {
            Config = ((CommonSettingsSection) (ConfigurationManager.GetSection("CommonSettings")));
        }

        public static CommonSettingsSection Config { get; private set; }
    }

    public sealed class CommonSettingsSection : ConfigurationSection
    {
        [ConfigurationProperty("LocalMachine")]
        public LocalMachineElement LocalMachine
        {
            get { return ((LocalMachineElement) (this["LocalMachine"])); }
        }

        public sealed class LocalMachineElement : ConfigurationElement
        {
            [ConfigurationProperty("Name", IsRequired = true)]
            public string Name
            {
                get { return ((string) (this["Name"])); }
                set { this["Name"] = value; }
            }
        }
    }

    public sealed class PLCSettings
    {
        private PLCSettings()
        {
        }

        static PLCSettings()
        {
            Config = ((PLCSettingsSection) (ConfigurationManager.GetSection("PLCSettings")));
        }

        public static PLCSettingsSection Config { get; private set; }
    }

    public sealed class PLCSettingsSection : ConfigurationSection
    {
        [ConfigurationProperty("LoadBuffer")]
        public LoadBufferElement LoadBuffer
        {
            get { return ((LoadBufferElement) (this["LoadBuffer"])); }
        }

        public sealed class LoadBufferElement : ConfigurationElement
        {
            [ConfigurationProperty("startAdress", IsRequired = true)]
            public long StartAdress
            {
                get { return ((long) (this["startAdress"])); }
                set { this["startAdress"] = value; }
            }

            [ConfigurationProperty("dbN", IsRequired = true)]
            public long DbN
            {
                get { return ((long) (this["dbN"])); }
                set { this["dbN"] = value; }
            }

            [ConfigurationProperty("bufferSize", IsRequired = true)]
            public long BufferSize
            {
                get { return ((long) (this["bufferSize"])); }
                set { this["bufferSize"] = value; }
            }
        }
    }
}