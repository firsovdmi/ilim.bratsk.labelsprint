using System;
using Caliburn.Micro;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public class SelectableScreen : Screen, ISelect
    {
        private bool _isSelect;

        public SelectableScreen()
        {
            Selected += OnSelected;
            Unselected += OnUnselected;
        }

        protected virtual void OnUnselected(object sender, SelectionEventArgs e)
        {
        }

        protected virtual void OnSelected(object sender, SelectionEventArgs e)
        {
        }

        #region Implementation of ISelect

        /// <summary>
        ///     Activates this instance.
        /// </summary>
        public void Select()
        {
            if (IsSelect)
            {
                return;
            }

            IsSelect = true;
            if (Selected != null)
                Selected(this, new SelectionEventArgs());
        }

        /// <summary>
        ///     Indicates whether or not this instance is active.
        /// </summary>
        public bool IsSelect
        {
            get { return _isSelect; }
            private set
            {
                _isSelect = value;
                NotifyOfPropertyChange("IsSelect");
            }
        }

        public event EventHandler<SelectionEventArgs> Selected;

        #endregion

        #region Implementation of IUnselect

        /// <summary>
        ///     Unselect this instance.
        /// </summary>
        public void Unselect()
        {
            if (!IsSelect)
            {
                return;
            }

            IsSelect = false;
            if (Unselected != null)
                Unselected(this, new SelectionEventArgs());
        }

        public event EventHandler<SelectionEventArgs> Unselected;

        #endregion
    }
}