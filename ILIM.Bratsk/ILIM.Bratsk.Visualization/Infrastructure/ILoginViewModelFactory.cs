using ILIM.Bratsk.Visualization.ViewModels;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public interface ILoginViewModelFactory
    {
        LoginViewModel Create(LoginType loginType);
        void Release(LoginViewModel obj);
    }
}