using System;
using Caliburn.Micro;
using Telerik.Windows.Controls;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public static class WindowManagerExtensions
    {
        /// <summary>
        ///     Opens an Alert modal window
        /// </summary>
        public static void Alert(this IWindowManager windowManager, string title, string message)
        {
            TelerikWindowManager.Alert(title, message);
        }

        /// <summary>
        ///     Opens an Alert modal window
        /// </summary>
        public static void Alert(this IWindowManager windowManager, DialogParameters dialogParameters)
        {
            TelerikWindowManager.Alert(dialogParameters);
        }

        /// <summary>
        ///     Opens a Confirm modal window
        /// </summary>
        public static void Confirm(this IWindowManager windowManager, string title, string message, Action onOK,
            Action onCancel = null)
        {
            TelerikWindowManager.Confirm(title, message, onOK, onCancel);
        }

        /// <summary>
        ///     Opens a Confirm modal window
        /// </summary>
        public static void Confirm(this IWindowManager windowManager, DialogParameters dialogParameters)
        {
            TelerikWindowManager.Confirm(dialogParameters);
        }

        /// <summary>
        ///     Opens a Prompt modal window
        /// </summary>
        public static void Prompt(this IWindowManager windowManager, string title, string message,
            string defaultPromptResultValue, Action<string> onOK)
        {
            TelerikWindowManager.Prompt(title, message, defaultPromptResultValue, onOK);
        }

        /// <summary>
        ///     Opens a Prompt modal window
        /// </summary>
        public static void Prompt(this IWindowManager windowManager, DialogParameters dialogParameters)
        {
            TelerikWindowManager.Prompt(dialogParameters);
        }
    }
}