﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public class EmptType
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
