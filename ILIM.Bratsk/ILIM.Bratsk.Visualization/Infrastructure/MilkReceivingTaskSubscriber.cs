﻿using System;
using System.Collections.Generic;
using ILIM.Bratsk.Infrastructure.CallBackService.ReceiveData;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public class ReceivingTaskSubscriber : IReceivingTaskSubscriber
    {
        #region Implementation of IReceivingTaskSubscriber

        public void Publish(List<ReceivingTaskItem> ReceivingTaskitems)
        {
            OnPublish(ReceivingTaskitems);
        }

        public void CheckSubscriber()
        {
        }

        public event EventHandler<ReceivingTaskPublishedEventArgs> Published;


        private void OnPublish(List<ReceivingTaskItem> ReceivingTaskitems)
        {
            var handler = Published;
            if (handler != null)
                handler.Invoke(this, new ReceivingTaskPublishedEventArgs(ReceivingTaskitems));
        }

        #endregion
    }


    public class ReceivingTaskPublishedEventArgs : EventArgs
    {
        public ReceivingTaskPublishedEventArgs(List<ReceivingTaskItem> published)
        {
            Published = published;
        }

        public List<ReceivingTaskItem> Published { get; private set; }
    }
}