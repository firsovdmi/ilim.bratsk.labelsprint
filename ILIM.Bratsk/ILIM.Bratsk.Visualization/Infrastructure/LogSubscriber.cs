using System;
using System.Collections.Generic;
using ILIM.Bratsk.Infrastructure.Log;
using ILIM.Bratsk.Infrastructure.Log.Model;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public class LogSubscriber : ILogSubscriber
    {
        #region Implementation of ILogSubscriber

        public void Publish(List<LogItem> logitems)
        {
            OnPublish(logitems);
        }

        public void CheckSubscriber()
        {
        }

        public event EventHandler<PublishedEventArgs> Published;


        private void OnPublish(List<LogItem> logitems)
        {
            var handler = Published;
            if (handler != null)
                handler.Invoke(this, new PublishedEventArgs(logitems));
        }

        #endregion
    }


    public class PublishedEventArgs : EventArgs
    {
        public PublishedEventArgs(List<LogItem> published)
        {
            Published = published;
        }

        public List<LogItem> Published { get; private set; }
    }
}