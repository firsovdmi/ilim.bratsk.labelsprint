using System;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    /// <summary>
    ///     Denotes an instance which requires selection.
    /// </summary>
    public interface ISelect
    {
        /// <summary>
        ///     Indicates whether or not this instance is selected.
        /// </summary>
        bool IsSelect { get; }

        /// <summary>
        ///     Select this instance.
        /// </summary>
        void Select();

        /// <summary>
        ///     Raised after selection occurs.
        /// </summary>
        event EventHandler<SelectionEventArgs> Selected;

        /// <summary>
        ///     Unselect this instance.
        /// </summary>
        void Unselect();

        /// <summary>
        ///     Raised after unselection occurs.
        /// </summary>
        event EventHandler<SelectionEventArgs> Unselected;
    }
}