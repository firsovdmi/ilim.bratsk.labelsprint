﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ILIM.Bratsk.Visualization.Infrastructure.Controls
{
    /// <summary>
    /// Interaction logic for TimeControl.xaml
    /// </summary>
    public partial class TimeControl : UserControl
    {
        public TimeControl()
        {
            InitializeComponent();
        }

        public Orientation PanelOrientation
        {
            get { return (Orientation)GetValue(PanelOrientationProperty); }
            set
            {
                SetValue(PanelOrientationProperty, value);
                MainStackPanel.Orientation = value;
            }
        }

        // Using a DependencyProperty as the backing store for Property1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PanelOrientationProperty =
             DependencyProperty.Register("PanelOrientation", typeof(Orientation),  typeof(TimeControl), new PropertyMetadata(Orientation.Vertical));


    }
}
