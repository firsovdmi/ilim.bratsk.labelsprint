using System;
using System.Collections.Generic;
using System.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Docking;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public class RadPaneEx : RadPane
    {
        public void Close()
        {
            var stateChangeEventArgs = new StateChangeEventArgs(null, this, new List<RadPane> {this});
            OnClose(this, stateChangeEventArgs);
        }

        public void Release()
        {
            if (OnRelease != null)
                OnRelease.Invoke(this, new RoutedEventArgs());
        }

        public event EventHandler<RoutedEventArgs> OnRelease = delegate { };
        public event EventHandler<StateChangeEventArgs> OnClose = delegate { };
    }
}