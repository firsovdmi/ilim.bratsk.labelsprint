namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public interface IScreenFactory
    {
        T Create<T>();
        void Release(object viewAware);
    }
}