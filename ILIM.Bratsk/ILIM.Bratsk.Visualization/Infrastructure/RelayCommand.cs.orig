﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using PAG.WBD.Milk.Infrastructure.Properties;

namespace PAG.WBD.Milk.Visualization.Infrastructure
{
    public interface ICommandExt : ICommand
    {
        CommandType CommandType { get; }
        string ShortName { get; }
        string ToolTipText { get; }
    }

    public class RelayCommand<T> : INotifyPropertyChanged,ICommandExt
    {


        #region Fields

        readonly Action<T> _execute = null;
        readonly Predicate<T> _canExecute = null;
        readonly CommandType _commandType;
        readonly string _shortName;
        readonly string _tooltipText;
        private bool _isEnable;

        public CommandType CommandType { get { return _commandType; } }
        public string ShortName { get { return _shortName; } }
        public string ToolTipText { get { return _tooltipText; } }

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        /// <param name="commandType">Type of command</param>
        /// <param name="shortName">Text</param>
        /// <param name="tooltipText">ToolTip text</param>
        /// <param name="isEnable">Enable to action</param>
        public RelayCommand(Action<T> execute, Predicate<T> canExecute = null, CommandType commandType = CommandType.None, string shortName = "text", string tooltipText = "",bool isEnable=true)
        {
            IsEnable = isEnable;
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;

            _commandType = commandType;
            _shortName = shortName;
            _tooltipText = tooltipText;
        }

        #endregion // Constructors

        #region Properties
        public bool IsEnable
        {
            get { return _isEnable; }
            set
            {
                if (value.Equals(_isEnable)) return;
                _isEnable = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region ICommand Members

        [DebuggerStepThrough]
        public virtual bool CanExecute(object parameter)
        {
            if (IsEnable)
                return _canExecute == null ? true : _canExecute((T)parameter);
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if(IsEnable)
            _execute((T)parameter);
        }

        #endregion // ICommand Members

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class RelayCommand : RelayCommand<Object> 
    {
        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        /// <param name="commandType">Type of command</param>
        /// <param name="shortName">Text</param>
        /// <param name="tooltipText">ToolTip text</param>
        /// <param name="isEnable">Enable to action</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute = null, CommandType commandType = CommandType.None, string shortName = "button", string tooltipText = "", bool isEnable = true)
            : base(execute, canExecute, commandType, shortName, tooltipText, isEnable)
        {
        }
    }

    public class RelayCommandWithCheckAccess<T> : RelayCommand<T>
    {
        private readonly Func<bool> _checkAccess;


        public RelayCommandWithCheckAccess(Action<T> execute, Predicate<T> canExecute = null,
            Func<bool> checkAccess = null,
            CommandType commandType = CommandType.None, string shortName = "text", string tooltipText = "", bool isEnable = true) 
            : base(execute, canExecute, commandType, shortName, tooltipText, isEnable)
        {
            _checkAccess = checkAccess;
        }

        public override bool CanExecute(object parameter)
        {
            return _checkAccess == null ? base.CanExecute(parameter) : _checkAccess.Invoke() && base.CanExecute(parameter);
        }
    }
    public class RelayCommandWithCheckAccess : RelayCommandWithCheckAccess<Object>
    {
        public RelayCommandWithCheckAccess(Action<object> execute, Predicate<object> canExecute = null, Func<bool> checkAccess = null, CommandType commandType = CommandType.None, string shortName = "text", string tooltipText = "", bool isEnable = true) : base(execute, canExecute, checkAccess, commandType, shortName, tooltipText, isEnable)
        {
        }
    }
    public enum CommandType
    {
        None,
        Save,
        SaveAll,
        Add,
        Act,
        Delete,
        Upload,
        AddTTN,
        GoTo,
        AddSubTask,
        Connect,
        Diconect,
        Down,
        Up,
        Refresh,
        Edit,
        ExportPdf,
        ExportXls,
        Print,
        AddAccount,
        MoveRight,
        MoveLeft,
        Roles,
        RoleAdd,
        BatchEdit,
        BatchAdd,
        Login,
        Exit,
<<<<<<< HEAD
        Copy
=======
        Clear,
        Stop,
        Start
>>>>>>> 897d78cbf08bd0581d36506ab8fd47852471a020
    }
}
