﻿using System;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public class DefaultValues
    {
        public Guid? RawMaterial { get; set; }
        public Guid? StateOfThePackaging { get; set; }
        public Guid? TasteAndSmell { get; set; }
        public Guid? Master { get; set; }
        public Guid? Operator { get; set; }
        public Guid? Laborant { get; set; }
        public int ReceivingAnalysisPurity { get; set; }
        public int ShippedAnalysisPurity { get; set; }
    }
}