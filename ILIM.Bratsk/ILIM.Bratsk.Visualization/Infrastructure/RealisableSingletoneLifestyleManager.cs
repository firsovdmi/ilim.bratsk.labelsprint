using Castle.Core.Internal;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;
using Castle.MicroKernel.Lifestyle;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public class RealisableSingletoneLifestyleManager : AbstractLifestyleManager
    {
        private Burden _cachedBurden;
        private readonly ThreadSafeInit _init = new ThreadSafeInit();

        public override object Resolve(CreationContext context, IReleasePolicy releasePolicy)
        {
            // 1. read from cache

            if (_cachedBurden != null)
            {
                return _cachedBurden.Instance;
            }
            var instanceFromContext = context.GetContextualProperty(ComponentActivator);
            if (instanceFromContext != null)
            {
                //we've been called recursively, by some dependency from base.Resolve call
                return instanceFromContext;
            }

            var initializing = false;
            try
            {
                initializing = _init.ExecuteThreadSafeOnce();
                if (_cachedBurden != null)
                {
                    return _cachedBurden.Instance;
                }
                var burden = CreateInstance(context, false);
                _cachedBurden = burden;
                Track(burden, releasePolicy);
                return burden.Instance;
            }
            finally
            {
                if (initializing)
                {
                    _init.EndThreadSafeOnceSection();
                }
            }
        }

        public override void Dispose()
        {
            if (_cachedBurden != null)
            {
                _cachedBurden.Release();
                _cachedBurden = null;
            }
        }

        public override bool Release(object instance)
        {
            if (_cachedBurden != null)
            {
                ComponentActivator.Destroy(instance);
                _cachedBurden = null;
                return true;
            }
            return false;
        }

        protected override void Track(Burden burden, IReleasePolicy releasePolicy)
        {
            burden.RequiresDecommission = true;
            releasePolicy.Track(burden.Instance, burden);
        }
    }
}