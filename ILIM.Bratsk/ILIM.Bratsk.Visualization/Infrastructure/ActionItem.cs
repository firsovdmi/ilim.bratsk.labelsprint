using System;
using System.Collections.ObjectModel;
using Caliburn.Micro;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    /// <summary>
    ///     View model for an action item.
    /// </summary>
    public class ActionItem : PropertyChangedBase, IActionItem
    {
        protected string _displayName;
        protected string _displayNameShort;
        protected string _name;
        protected string _toolTip;
        private readonly ObservableCollection<IActionItem> items = new ObservableCollection<IActionItem>();

        protected ActionItem(string name)
        {
            _displayName = name;
            _displayNameShort = name;
            _name = name;
            _execute = (() => { });
            _canExecute = (() => IsActive);
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="name">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public ActionItem(string name, Action execute, Func<bool> canExecute = null)
            : this(name)
        {
            _execute = execute ?? (() => { });
            _canExecute = canExecute ?? (() => IsActive);
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public string DisplayName
        {
            get { return _displayName; }
            set
            {
                _displayName = value;
                NotifyOfPropertyChange(() => DisplayName);
            }
        }

        public string DisplayNameShort
        {
            get { return _displayNameShort; }
            set
            {
                _displayNameShort = value;
                NotifyOfPropertyChange(() => DisplayNameShort);
            }
        }

        public string ToolTip
        {
            get { return _toolTip; }
            set
            {
                _toolTip = value;
                NotifyOfPropertyChange(() => ToolTip);
            }
        }

        public ObservableCollection<IActionItem> Items
        {
            get { return items; }
        }

        #region Execution

        protected Action _execute;

        /// <summary>
        ///     The action associated to the ActionItem
        /// </summary>
        public virtual void Execute()
        {
            _execute();
        }

        protected Func<bool> _canExecute;

        /// <summary>
        ///     Calls the underlying canExecute function.
        /// </summary>
        public virtual bool CanExecute
        {
            get { return _canExecute() && IsEnable; }
        }

        #endregion

        #region Activation & Deactivation

        public event EventHandler<ActivationEventArgs> Activated;
        public event EventHandler<DeactivationEventArgs> AttemptingDeactivation;
        public event EventHandler<DeactivationEventArgs> Deactivated;

        private bool _isActive = true;

        public bool IsActive
        {
            get { return _isActive; }
            protected set
            {
                _isActive = value;
                NotifyOfPropertyChange(() => IsActive);
            }
        }

        private bool _isEnable = true;

        public bool IsEnable
        {
            get { return _isEnable; }
            set
            {
                _isEnable = value;
                NotifyOfPropertyChange(() => CanExecute);
            }
        }

        public void Activate()
        {
            if (IsActive)
                return;

            IsActive = true;
            OnActivate();
            if (Activated != null)
                Activated(this, new ActivationEventArgs {WasInitialized = false});
            NotifyOfPropertyChange(() => CanExecute);
        }

        protected virtual void OnActivate()
        {
        }

        public virtual void Deactivate(bool close)
        {
            if (!IsActive)
                return;

            if (AttemptingDeactivation != null)
                AttemptingDeactivation(this, new DeactivationEventArgs {WasClosed = close});

            IsActive = false;
            OnDeactivate(close);
            NotifyOfPropertyChange(() => CanExecute);
            if (Deactivated != null)
                Deactivated(this, new DeactivationEventArgs {WasClosed = close});
        }

        protected virtual void OnDeactivate(bool close)
        {
        }

        #endregion


        public int Index { get; set; }
    }


    /// <summary>
    ///     View model for an action item.
    /// </summary>
    public class CommandActionItem : ActionItem
    {
        private readonly ICommandExt _command;

        protected CommandActionItem(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="name">The display name.</param>
        /// <param name="command"></param>
        public CommandActionItem(string name, ICommandExt command)
            : base(name)
        {
            _displayName = command.ShortName;
            _displayNameShort = command.ShortName;
            _name = name;
            _toolTip = command.ToolTipText;
            _execute = (() => { });
            _canExecute = (() => IsActive);
        }
    }
}