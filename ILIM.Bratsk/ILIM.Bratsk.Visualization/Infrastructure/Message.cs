using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public class Message<T>
    {
        private T Content { get; set; }
    }


    public abstract class EntityChanges<T> : Message<T> where T : IEntity
    {
        public EntityChanges(T content, object sender)
        {
            Content = content;
            Sender = sender;
        }

        public T Content { get; set; }
        public object Sender { get; set; }
    }

    public class EntityAdded<T> : EntityChanges<T> where T : IEntity
    {
        public EntityAdded(T content, object sender)
            : base(content, sender)
        {
        }
    }

    public class EntityDeleted<T> : EntityChanges<T> where T : IEntity
    {
        public EntityDeleted(T content, object sender)
            : base(content, sender)
        {
        }
    }

    public class EntityEdited<T> : EntityChanges<T> where T : IEntity
    {
        public EntityEdited(T content, object sender)
            : base(content, sender)
        {
        }
    }
}