using System;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public interface IShell
    {
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ViewAttribute : Attribute
    {
        public ViewAttribute(Type viewType, object context = null)
        {
            ViewType = viewType;
            Context = context;
        }

        public object Context { get; set; }
        public Type ViewType { get; private set; }
    }
}