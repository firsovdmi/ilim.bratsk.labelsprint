using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels
{
    public class AccountDataChanged
    {
        public AuthenticationResult AuthenticationResult { get; set; }
    }
}