namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public interface IEditStatus
    {
        bool IsEdited { get; set; }
        bool IsDeleted { get; set; }
        bool IsAdded { get; set; }
    }
}