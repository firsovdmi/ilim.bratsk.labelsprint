﻿using System;

namespace ILIM.Bratsk.Visualization.Infrastructure
{
    public static class StaticData
    {
        private static Guid? _allowedLaboratoryGuid;

        public static Guid AllowedLaboratoryGuid
        {
            get
            {
                if (_allowedLaboratoryGuid == null)
                {
                    _allowedLaboratoryGuid = new Guid("FFAB9C0C-078C-4689-A340-2601E6C1A786");
                }
                return _allowedLaboratoryGuid.Value;
            }
        }
    }
}