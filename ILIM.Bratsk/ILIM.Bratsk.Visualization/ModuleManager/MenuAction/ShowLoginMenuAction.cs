﻿using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.MenuAction
{
    public class ShowLoginMenuAction : ManagerMenuAction<LoginViewModel>
    {
        private readonly ILoginViewModelFactory _loginViewModelFactory;
        private readonly IWindowManager _windowManager;

        public ShowLoginMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            IEventAggregator eventAggregator,
            ILoginViewModelFactory loginViewModelFactory, IWindowManager windowManager) :
                base(radDockingManager, screenFactory, "Смена пользователя", eventAggregator)
        {
            _loginViewModelFactory = loginViewModelFactory;
            _windowManager = windowManager;
        }

        public override void Execute()
        {
            var loginViewModel = _loginViewModelFactory.Create(LoginType.Relogin);
            try
            {
                _windowManager.ShowDialog(loginViewModel, null,
                    new Dictionary<string, object> {{"ResizeMode", ResizeMode.NoResize}, {"CanClose", false}});
            }
            finally
            {
                _loginViewModelFactory.Release(loginViewModel);
            }
        }
    }
}