﻿using System;
using Caliburn.Micro;
using ILIM.Bratsk.Visualization.Infrastructure;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.MenuAction
{
    public abstract class ManagerMenuAction<T> : ManagerMenuAction where T : Screen
    {
        private readonly object _executeLock = new object();
        protected readonly IEventAggregator EventAggregator;
        protected readonly IRadDockingManager RadDockingManager;
        protected readonly IScreenFactory ScreenFactory;

        protected ManagerMenuAction(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public ManagerMenuAction(string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
        }

        public ManagerMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory, string displayName,
            IEventAggregator eventAggregator)
            : base(displayName)
        {
            RadDockingManager = radDockingManager;
            ScreenFactory = screenFactory;
            EventAggregator = eventAggregator;
        }

        /// <summary>
        ///     The action associated to the ActionItem
        /// </summary>
        public override void Execute()
        {
            lock (_executeLock)
            {
                var viewmodel = ScreenFactory.Create<T>();
                //Anti-double subscribe hack
                viewmodel.Deactivated -= Release;
                viewmodel.Deactivated += Release;

                RadDockingManager.ShowWindow(viewmodel, null);
            }
        }

        protected void Release(object sender, DeactivationEventArgs e)
        {
            var model = (Screen) sender;
            model.Deactivated -= Release;
            ScreenFactory.Release(sender);
        }
    }

    public abstract class ManagerMenuAction : ActionItem
    {
        protected ManagerMenuAction(string displayName) : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public ManagerMenuAction(string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
        }
    }
}