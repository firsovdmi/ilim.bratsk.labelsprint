﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Visualization.ModuleAdministration.MenuAction;

namespace ILIM.Bratsk.Visualization.ModuleManager
{
    public class ManagerModuleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IModule>().ImplementedBy<ManagerModule>());
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof (ManagerMenuAction<>))
                    .If(p => p != typeof (ManagerMenuAction<>))
                    .WithServices(typeof (ManagerMenuAction)));
        }
    }
}