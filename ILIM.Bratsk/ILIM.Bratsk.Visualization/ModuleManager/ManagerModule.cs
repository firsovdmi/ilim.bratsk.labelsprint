﻿using System.Collections.Generic;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.MenuAction;

namespace ILIM.Bratsk.Visualization.ModuleManager
{
    public class ManagerModule : IModule
    {
        public ManagerModule(IMenuManager menuManager,
            IToolBarManager toolBarManager,
            IEnumerable<ManagerMenuAction> actionItems,
            ManagerMenuAction administrationActionItems
            )
        {
            menuManager.ShowItem(new ActionItem("Управление", null), 0);
            foreach (var actionItem in actionItems)
                menuManager.WithParent("Управление")
                    .ShowItem(actionItem);
        }
    }
}