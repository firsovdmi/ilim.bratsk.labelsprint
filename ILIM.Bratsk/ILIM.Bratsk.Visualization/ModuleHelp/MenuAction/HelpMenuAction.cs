﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleHelp.MenuAction
{
    public abstract class HelpMenuAction<T> : HelpMenuAction, IHandle<AccountDataChanged>
        where T : Screen
    {
        private readonly object _executeLock = new object();
        private readonly IActiveAccountManager _activeAccountManager;
        protected readonly IEventAggregator EventAggregator;
        protected readonly IRadDockingManager RadDockingManager;
        protected readonly IScreenFactory ScreenFactory;

        protected HelpMenuAction(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public HelpMenuAction(string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
        }

        public HelpMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            string displayName, IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager)
            : base(displayName)
        {
            RadDockingManager = radDockingManager;
            ScreenFactory = screenFactory;
            EventAggregator = eventAggregator;
            _activeAccountManager = activeAccountManager;
            CheckAcess(_activeAccountManager.BlackListObjects);
        }

        protected abstract string AuthorizeObjectName { get; }

        public void Handle(AccountDataChanged message)
        {
            CheckAcess(message.AuthenticationResult.DeniedObjects);
        }

        /// <summary>
        ///     The action associated to the ActionItem
        /// </summary>
        public override void Execute()
        {
            lock (_executeLock)
            {
                var viewmodel = ScreenFactory.Create<T>();
                //Anti-double subscribe hack
                viewmodel.Deactivated -= Release;
                viewmodel.Deactivated += Release;

                RadDockingManager.ShowWindow(viewmodel, null);
            }
        }

        protected void Release(object sender, DeactivationEventArgs e)
        {
            var model = (Screen)sender;
            model.Deactivated -= Release;
            ScreenFactory.Release(sender);
        }

        private void CheckAcess(IEnumerable<string> blackListObjects)
        {
            var objectName = "";
            try
            {
                // objectName =typeof(T).GetField("_aaaService", BindingFlags.Instance | BindingFlags.NonPublic).FieldType.Name.TrimStart('I') + "." + AuthorizeObjectName;
            }
            catch
            {
            }
            IsEnable = blackListObjects.All(p => p != objectName);
        }
    }

    public abstract class HelpMenuAction : ActionItem
    {
        protected HelpMenuAction(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public HelpMenuAction(string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
        }
    }
}