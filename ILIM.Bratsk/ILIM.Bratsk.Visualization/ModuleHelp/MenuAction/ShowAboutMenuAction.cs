﻿using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleHelp.ViewModels;
using ILIM.Bratsk.Visualization.ModuleHelp.ViewModels;
using ILIM.Bratsk.Visualization.ModuleWork.MenuAction;

namespace ILIM.Bratsk.Visualization.ModuleHelp.MenuAction
{
    public class ShowAboutMenuAction : HelpMenuAction<AboutViewModel>
    {
        private readonly IWindowManager _windowManager;
        public ShowAboutMenuAction(string displayName, IWindowManager windowManager)
            : base(displayName)
        {
            _windowManager = windowManager;
        }

        public ShowAboutMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager,IWindowManager windowManager)
            : base(radDockingManager, screenFactory, "О программе", eventAggregator, activeAccountManager)
        {
            _windowManager = windowManager;
        }

        protected override string AuthorizeObjectName
        {
            get { return ""; }
        }

        public override void Execute()
        {
            _windowManager.ShowDialog(new AboutViewModel() { DisplayName = "О программе" }, null, new Dictionary<string, object>() { { "ResizeMode", ResizeMode.NoResize } });
            
        }
    }
}