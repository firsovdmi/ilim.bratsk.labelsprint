﻿using System.Reflection;
using Caliburn.Micro;

namespace ILIM.Bratsk.Visualization.ModuleHelp.ViewModels
{
    public class AboutViewModel : Screen
    {
        public AboutViewModel()
        {

        }

        public string Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version.ToString(); }
        }
    }
}