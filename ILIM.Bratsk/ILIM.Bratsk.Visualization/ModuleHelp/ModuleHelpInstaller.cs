﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleHelp.MenuAction;
using ILIM.Bratsk.Visualization.ModuleHelp.ViewModels;
using ILIM.Bratsk.Visualization.ModuleHelp.ViewModels;
using ILIM.Bratsk.Visualization.ModuleWork;
using ILIM.Bratsk.Visualization.ModuleWork.MenuAction;
using ILIM.Bratsk.Visualization.ModuleWork.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleHelp
{
    public class ModuleHelpInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IModule>().ImplementedBy<ModuleHelp>());
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof(HelpMenuAction<>))
                    .If(p => p != typeof(HelpMenuAction<>))
                    .WithServices(typeof(HelpMenuAction)));

            container.Register(
        Component.For<AboutViewModel>()
            .ImplementedBy<AboutViewModel>()
            .LifestyleCustom<RealisableSingletoneLifestyleManager>());

        }
    }
}