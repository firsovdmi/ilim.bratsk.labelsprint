﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleHelp.MenuAction;
using ILIM.Bratsk.Visualization.ModuleWork.MenuAction;

namespace ILIM.Bratsk.Visualization.ModuleHelp
{
    public class ModuleHelp: IModule
    {
        public ModuleHelp(IMenuManager menuManager,
            IEnumerable<HelpMenuAction> actionItems
            )
        {
            menuManager.ShowItem(new ActionItem("Помощь", null), 4);
            foreach (var actionItem in actionItems)
            {
                menuManager.WithParent("Помощь").ShowItem(actionItem);
            }
        }
    }
}