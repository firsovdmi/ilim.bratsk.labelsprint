using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Win32;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Docking;

namespace PAG.WBD.Milk.Visualization
{
    public class RadPaneEx : RadPane
    {
        public RadPaneEx()
        {
            
        }
        public void Close()
        {
            var stateChangeEventArgs = new StateChangeEventArgs(null, this, new List<RadPane> { this });
            OnClose(this, stateChangeEventArgs);
        }

        public void Release()
        {
            if (OnRelease != null)
                OnRelease.Invoke(this, new RoutedEventArgs());
        }
        public event EventHandler<RoutedEventArgs> OnRelease = delegate { }; 
        public event EventHandler<StateChangeEventArgs> OnClose = delegate { };       
    }
}