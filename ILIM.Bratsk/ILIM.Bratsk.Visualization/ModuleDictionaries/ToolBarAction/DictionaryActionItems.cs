using System.Collections.Generic;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.ToolBarAction
{
    public class DictionaryActionItems : DictionaryActionItem
    {
        protected DictionaryActionItems(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        public DictionaryActionItems(IEnumerable<DictionaryActionItem> items)
            : base("справочники")
        {
            foreach (var item in items)
                Items.Add(item);
        }
    }
}