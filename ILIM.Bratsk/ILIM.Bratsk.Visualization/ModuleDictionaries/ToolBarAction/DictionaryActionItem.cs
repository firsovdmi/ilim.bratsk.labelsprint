using System;
using System.Windows.Input;
using ILIM.Bratsk.Visualization.Infrastructure;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.ToolBarAction
{
    public class DictionaryActionItem : ActionItem
    {
        private ICommand _command;

        protected DictionaryActionItem(string displayName) : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public DictionaryActionItem(string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
        }

        public ICommand Command
        {
            get { return _command; }
            set
            {
                _command = value;
                NotifyOfPropertyChange(() => Command);
            }
        }
    }
}