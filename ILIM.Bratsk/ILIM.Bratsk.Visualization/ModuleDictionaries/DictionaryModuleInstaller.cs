using Castle.Core;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.Model;
using ILIM.Bratsk.Visualization.ModuleDictionaries.MenuAction;
using ILIM.Bratsk.Visualization.ModuleDictionaries.Model;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ToolBarAction;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries
{
    public class DictionaryModuleInstaller : IWindsorInstaller
    {
        #region Implementation of IWindsorInstaller

        /// <summary>
        ///     Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IDictionaryModelFactory>().AsFactory().LifestyleTransient());
            //container.Register(Types.FromThisAssembly().BasedOn(typeof(DictionaryViewModelBase<>)).If(p => p != typeof(DictionaryViewModelBase<>)).WithServiceSelf().LifestyleCustom<RealisableSingletoneLifestyleManager>());
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof (DictionaryViewModelBase<>))
                    .If(p => p != typeof (DictionaryViewModelBase<>))
                    .WithServiceBase()
                    .Configure(p => p.Properties(PropertyFilter.IgnoreAll))
                    .LifestyleCustom<RealisableSingletoneLifestyleManager>());
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof (ShowListDictionaryItemViewModel<>))
                    .WithServiceSelf()
                    .LifestyleCustom<RealisableSingletoneLifestyleManager>());


            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof (EntityWithEditStatusModel<>))
                    .If(p => p != typeof (EntityWithEditStatusModel<>))
                    .WithServiceBase()
                    .LifestyleTransient());
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof (AddDictionaryItemViewModel<>))
                    .If(p => p != typeof (AddDictionaryItemViewModel<>))
                    .WithServiceSelf()
                    .LifestyleTransient());
            container.Register(Component.For<IModule>().ImplementedBy<DictionaryModule>());
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof (DictionaryMenuAction<>))
                    .If(p => p != typeof (DictionaryMenuAction<>))
                    .WithServices(typeof (DictionaryMenuAction)));
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof (DictionaryActionItem))
                    .If(p => p != typeof (DictionaryActionItem) && p != typeof (DictionaryActionItems))
                    .WithServices(typeof (DictionaryActionItem)));
            container.Register(Component.For<DictionaryActionItems>().ImplementedBy<DictionaryActionItems>());
        }

        #endregion
    }
}