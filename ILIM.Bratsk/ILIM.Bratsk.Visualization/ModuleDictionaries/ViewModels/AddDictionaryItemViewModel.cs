using System;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.Views;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels
{
    [View(typeof(AddDictionaryItemView), Context = "AddDictionary")]
    public class AddDictionaryItemViewModel<T> : Screen
        where T : Entity
    {
        public T Content { get; set; }
        protected readonly IDictionaryService<T> _dictionaryService;
        private readonly IEventAggregator _eventAggregator;

        public AddDictionaryItemViewModel(IDictionaryService<T> dictionaryService, IEventAggregator eventAggregator)
        {
            _dictionaryService = dictionaryService;
            _eventAggregator = eventAggregator;
        }


        private RelayCommand _SaveCommand;
        private RelayCommand _CancelCommand;

        public RelayCommand SaveCommand
        {
            get
            {
                if (_SaveCommand == null)
                {
                    _SaveCommand = new RelayCommand(ExecuteSaveCommand, CanExecuteSaveCommand, CommandType.None, "��������", "��������");
                }
                return _SaveCommand;
            }
        }





        public RelayCommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand(ExecuteCancelCommand, CanExecuteCancelCommand, CommandType.None, "������", "������");
                }
                return _CancelCommand;
            }
        }

        #region CancelCommand

        protected virtual bool CanExecuteCancelCommand(object obj)
        {

            return true;
        }

        protected virtual void ExecuteCancelCommand(object obj)
        {
            TryClose(false);
        }

        #endregion

        #region SaveCommand

        protected virtual bool CanExecuteSaveCommand(object obj)
        {
            return true;
        }

        protected virtual void ExecuteSaveCommand(object obj)
        {
            Content.ID = Guid.NewGuid();
            Content = _dictionaryService.Create(Content);
            _eventAggregator.PublishOnCurrentThread(new DictAdded<T>(Content, this));
            TryClose(true);
        }

        #endregion

        #region Overrides of Scree

        /// <summary>
        /// Tries to close this instance by asking its Parent to initiate shutdown or by asking its corresponding view to close.
        ///             Also provides an opportunity to pass a dialog result to it's corresponding view.
        /// </summary>
        /// <param name="dialogResult">The dialog result.</param>
        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView("AddDictionary") as RadWindow;
            if (window != null)
                window.Close();
        }

        #endregion
    }
}