﻿using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.Model;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ToolBarAction;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels
{
    public sealed class ApplicationSettingsDictionaryViewModel : DictionaryViewModelBase<ApplicationSettings>
    {

        public ApplicationSettingsDictionaryViewModel(IToolBarManager toolBarManager, DictionaryActionItems dictionaryActionItems,
            IApplicationSettingsService dictionaryService, IEventAggregator eventAggregator,
            IDictionaryModelFactory dictionaryModelFactory,
            IWindowManager windowManager, IAccountDataService accountDataService, 
            IActiveAccountManager activeAccountManager)
            : base(
                toolBarManager, dictionaryActionItems, dictionaryService, eventAggregator, dictionaryModelFactory,
                windowManager, accountDataService, activeAccountManager)
        {
            DisplayName = "Настройки системы";
            Initialize();
        }


        private void Initialize()
        {
           
        }

        #region Implementation of IHandle<AddNewDictionaryElement<Driver>>

        /// <summary>
        ///     Handles the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(DictChanges<Manufacture> message)
        {

        }

        #endregion
    }
}