using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Extentions;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.Model;
using ILIM.Bratsk.Visualization.ModuleDictionaries.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.Model;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ToolBarAction;
using ILIM.Bratsk.Visualization.ModuleDictionaries.Views;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels
{
    [View(typeof (ToolBarDictionaryView), Context = "ToolBar")]
    public abstract class DictionaryViewModelBase<T> : SelectableScreen, IHandle<DictChanges<T>>, IAccountDataGiver,
        IHandle<AccountDataChanged>
        where T : Entity, new()
    {
        private RelayCommand _addItemCommand;
        private RelayCommand _deleteItemCommandCommand;
        private RelayCommand _endEditHandlingCommand;
        private ObservableCollection<EntityWithEditStatusModel<T>> _items;
        private RelayCommand _refreshCommand;
        private RelayCommand _saveItemCommandCommand;
        private RelayCommand _saveItemsCommandCommand;
        private EntityWithEditStatusModel<T> _selectedItem;
        private readonly IAccountDataService _accountDataService;
        private readonly IActiveAccountManager _activeAccountManager;
        private readonly IWindowManager _windowManager;
        protected readonly DictionaryActionItems DictionaryActionItems;
        protected readonly IDictionaryModelFactory DictionaryModelFactory;
        protected readonly IDictionaryService<T> DictionaryService;
        protected readonly IEventAggregator EventAggregator;
        protected readonly IToolBarManager ToolBarManager;

        public DictionaryViewModelBase(IToolBarManager toolBarManager, DictionaryActionItems dictionaryActionItems,
            IDictionaryService<T> dictionaryService, IEventAggregator eventAggregator,
            IDictionaryModelFactory dictionaryModelFactory, IWindowManager windowManager,
            IAccountDataService accountDataService
            , IActiveAccountManager activeAccountManager)
        {
            ToolBarManager = toolBarManager;
            DictionaryActionItems = dictionaryActionItems;
            DictionaryService = dictionaryService;
            EventAggregator = eventAggregator;
            DictionaryModelFactory = dictionaryModelFactory;
            _windowManager = windowManager;
            _accountDataService = accountDataService;
            _activeAccountManager = activeAccountManager;
            Items =
                new ObservableCollection<EntityWithEditStatusModel<T>>(
                    DictionaryService.Get().OrderBy(p => p.Name).Select(p => DictionaryModelFactory.Create<T>(p)));
        }

        public EntityWithEditStatusModel<T> SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (Equals(value, _selectedItem)) return;
                _selectedItem = value;
                NotifyOfPropertyChange("SelectedItem");
            }
        }

        public ObservableCollection<EntityWithEditStatusModel<T>> Items
        {
            get { return _items; }
            set
            {
                if (Equals(value, _items)) return;
                _items = value;
                NotifyOfPropertyChange("Items");
            }
        }

        public bool IsAdminRole
        {
            get
            {
                return _activeAccountManager.WhiteListObjects.Contains("Administartor") ||
                       _activeAccountManager.ActiveAccount.Account.Login.ToUpper() == "ADMIN1";
            }
        }

        public RelayCommand SaveItemCommand
        {
            get
            {
                if (_saveItemCommandCommand == null)
                {
                    _saveItemCommandCommand = new RelayCommand(ExecuteSaveItemCommand, CanExecuteSaveItemCommand,
                        CommandType.Save, "���������", "��������� � ���� ������");
                }
                return _saveItemCommandCommand;
            }
        }

        public RelayCommand SaveItemsCommand
        {
            get
            {
                if (_saveItemsCommandCommand == null)
                {
                    _saveItemsCommandCommand = new RelayCommand(ExecuteSaveItemsCommand, CanExecuteSaveItemsCommand,
                        CommandType.SaveAll, "��������� ���", "��������� ��� � ���� ������");
                }
                return _saveItemsCommandCommand;
            }
        }

        public RelayCommand DeleteItemCommand
        {
            get
            {
                if (_deleteItemCommandCommand == null)
                {
                    _deleteItemCommandCommand = new RelayCommand(ExecuteDeleteItemCommand, CanExecuteDeleteItemCommand,
                        CommandType.Delete, "�������", "�������");
                }
                return _deleteItemCommandCommand;
            }
        }

        public RelayCommand EndEditHandlingCommand
        {
            get
            {
                if (_endEditHandlingCommand == null)
                {
                    _endEditHandlingCommand = new RelayCommand(ExecuteEndEditHandlingCommand,
                        CanExecuteEndEditHandlingCommand);
                }
                return _endEditHandlingCommand;
            }
        }

        public RelayCommand AddItemCommand
        {
            get
            {
                if (_addItemCommand == null)
                {
                    _addItemCommand = new RelayCommand(ExecuteAddItemCommand, CanExecuteAddItemCommand, CommandType.Add,
                        "��������", "�������� ����� �������");
                }
                return _addItemCommand;
            }
        }

        public RelayCommand RefreshCommand
        {
            get
            {
                if (_refreshCommand == null)
                {
                    _refreshCommand = new RelayCommand(ExecuteRefreshCommand, CanExecuteRefreshCommand,
                        CommandType.Refresh, "��������", "�������� ������");
                }
                return _refreshCommand;
            }
        }

        public IAccountDataService AccountDataService
        {
            get { return _accountDataService; }
        }

        public void Handle(AccountDataChanged message)
        {
            //var autherizationObject = DictionaryService.GetType().Name + ".Get";
            //if (!message.AuthenticationResult.AllowedObjects.Any(p => p == autherizationObject))
            //{
            //    DictionaryActionItems.Deactivate(true);
            //}
        }

        #region Implementation of IHandle<DictChanges<T>>

        /// <summary>
        ///     Handles the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(DictChanges<T> message)
        {
            if (message != null && message.Sender != this && message.Content != null)
            {
                var itemToChange = Items.Where(p => p.Content.ID == message.Content.ID).FirstOrDefault();
                if (message is DictDeleted<T> && itemToChange != null)
                    Items.Remove(itemToChange);
                if (message is DictAdded<T>)
                    Items.Add(DictionaryModelFactory.Create<T>(message.Content));
                if (message is DictEdited<T> && itemToChange != null)
                    itemToChange.Content = message.Content;
            }
        }

        #endregion

        public void ExecuteAction(ActionExecutionContext context)
        {
            var eventArgs = (KeyEventArgs) context.EventArgs;
            // if (eventArgs.Key == Key.Delete && CanExecuteDeleteItemCommand(null)) ExecuteDeleteItemCommand(null);
        }

        #region AddItemCommand

        protected virtual bool CanExecuteAddItemCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddItemCommand(object obj)
        {
            var entity = Activator.CreateInstance<T>();
            entity.ID = Guid.NewGuid();
            var model = DictionaryModelFactory.Create<T>(entity);
            model.IsAdded = true;
            Items.Add(model);
        }

        #endregion

        #region SaveCommand

        protected virtual bool CanExecuteSaveItemCommand(object obj)
        {
            return SelectedItem != null && (SelectedItem.IsEdited || SelectedItem.IsDeleted || SelectedItem.IsAdded);
        }

        protected virtual void ExecuteSaveItemCommand(object obj)
        {
            SaveItem(SelectedItem);
        }

        private void SaveItem(EntityWithEditStatusModel<T> itemForSave)
        {
            if (itemForSave.IsDeleted)
            {
                if (!itemForSave.IsAdded)
                    DictionaryService.Delete(itemForSave.Content);
                EventAggregator.PublishOnCurrentThread(new DictDeleted<T>(itemForSave.Content, this));
                Items.Remove(itemForSave);
                return;
            }
            if (itemForSave.IsAdded)
            {
                var newDict = DictionaryService.Create(itemForSave.Content);
                itemForSave.Content.RowVersion = newDict.RowVersion;
                itemForSave.Content.ID = newDict.ID;
                EventAggregator.PublishOnCurrentThread(new DictAdded<T>(itemForSave.Content, this));
            }
            else if (itemForSave.IsEdited)
            {
                itemForSave.Content.RowVersion = DictionaryService.Update(itemForSave.Content).RowVersion;
                EventAggregator.PublishOnCurrentThread(new DictEdited<T>(itemForSave.Content, this));
            }
            itemForSave.IsAdded = false;
            itemForSave.IsDeleted = false;
            itemForSave.IsEdited = false;
        }

        #endregion

        #region SaveAllCommand

        protected virtual bool CanExecuteSaveItemsCommand(object obj)
        {
            return Items.Any(p => p.IsEdited || p.IsDeleted || p.IsAdded);
        }

        protected void ExecuteSaveItemsCommand(object obj)
        {
            var changetItems = Items.Where(p => p.IsAdded || p.IsDeleted || p.IsEdited).ToList();
            foreach (var item in changetItems)
            {
                SaveItem(item);
            }
            return;
            var itemsToDelete = Items.Where(p => p.IsDeleted).ToList();
            DictionaryService.DeleteList(itemsToDelete.Where(p => !p.IsAdded).Select(p => p.Content).ToList());
            foreach (var item in itemsToDelete)
            {
                Items.Remove(item);
                EventAggregator.PublishOnCurrentThread(new DictDeleted<T>(item.Content, this));
            }

            var itemsToAdd = Items.Where(p => p.IsAdded).ToList();
            InitGuids(itemsToAdd);

            var createdItems = DictionaryService.CreateList(itemsToAdd.Select(p => p.Content).ToList());

            foreach (var item in itemsToAdd)
            {
                var createdItem = createdItems.FirstOrDefault(p => p.ID == item.Content.ID);
                if (createdItem != null) item.Content.RowVersion = createdItem.RowVersion;
                EventAggregator.PublishOnCurrentThread(new DictAdded<T>(item.Content, this));
                item.IsAdded = false;
                item.IsEdited = false;
            }

            var itemsToUpdate = Items.Where(p => p.IsEdited).ToList();
            InitGuids(itemsToUpdate);
            var updatesItems = DictionaryService.UpdateList(itemsToUpdate.Select(p => p.Content).ToList());
            foreach (var item in itemsToUpdate)
            {
                var updatedItem = updatesItems.FirstOrDefault(p => p.ID == item.Content.ID);
                if (updatedItem != null) item.Content.RowVersion = updatedItem.RowVersion;
                EventAggregator.PublishOnCurrentThread(new DictEdited<T>(item.Content, this));
                item.IsEdited = false;
            }
        }

        private void InitGuids(IEnumerable<EntityWithEditStatusModel<T>> items)
        {
            foreach (var itemToAdd in items.Where(itemToAdd => itemToAdd.Content.ID == Guid.Empty))
            {
                itemToAdd.Content.ID = Guid.NewGuid();
            }
        }

        #endregion

        #region DeleteCommand

        protected virtual bool CanExecuteDeleteItemCommand(object obj)
        {
            return SelectedItem != null && IsAdminRole;
        }

        protected virtual void ExecuteDeleteItemCommand(object obj)
        {
            SelectedItem.IsDeleted = true;
        }

        #endregion

        #region RefreshCommand

        protected virtual bool CanExecuteRefreshCommand(object obj)
        {
            return true;
        }

        protected virtual void ExecuteRefreshCommand(object obj)
        {
            var newItems =
                new ObservableCollection<EntityWithEditStatusModel<T>>(
                    DictionaryService.Get().Select(p => DictionaryModelFactory.Create<T>(p)));
            Items.RefreshItems(newItems, _windowManager);
        }

        #endregion

        #region EndEditHandlingCommand

        protected virtual bool CanExecuteEndEditHandlingCommand(object obj)
        {
            return true;
        }

        protected virtual void ExecuteEndEditHandlingCommand(object obj)
        {
            var model = obj as EntityWithEditStatusModel<T>;
            if (model != null)
            {
                if (model.IsAdded != true)
                    model.IsEdited = true;
            }
        }

        #endregion

        #region Override

        public override void CanClose(Action<bool> callback)
        {
            if (Items.Any(p => p.IsAdded || p.IsDeleted || p.IsEdited))
                _windowManager.Confirm("�� �������?",
                    "���� �������� ������������� ������.\n�� �������, ��� ������ ��� �������?",
                    () => { callback(true); },
                    () => { callback(false); });
            else
            {
                callback(true);
            }
        }

        protected override void OnUnselected(object sender, SelectionEventArgs e)
        {
            //if (Items.Any(p => p.IsEdited || p.IsDeleted || p.IsAdded))
            //    return;
            //Items.Clear();
            //SelectedItem = null;
        }

        protected override void OnSelected(object sender, SelectionEventArgs e)
        {
            //if (Items == null || !Items.Any())
            //    Items = new ObservableCollection<EntityWithEditStatusModel<T>>(DictionaryService.Get().Select(p => DictionaryModelFactory.Create<T>(p))); 
        }

        #endregion
    }
}