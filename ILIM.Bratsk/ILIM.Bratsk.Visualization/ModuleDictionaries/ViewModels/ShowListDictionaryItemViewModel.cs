using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels
{
    //[View(typeof(ShowListDictionaryItemView), Context = "ShowDictionary")]
    public class ShowListDictionaryItemViewModel<T> : Conductor<DictionaryViewModelBase<T>> where T : Entity, new()
    {
        private RelayCommand _cancelCommand;
        private RelayCommand _selectCommand;
        private DictionaryViewModelBase<T> _viewModel;
        private readonly IScreenFactory _screenFactory;

        public ShowListDictionaryItemViewModel(IScreenFactory screenFactory)
        {
            _screenFactory = screenFactory;
        }

        public DictionaryViewModelBase<T> ViewModel
        {
            get { return _viewModel; }
            set
            {
                if (Equals(value, _viewModel)) return;
                _viewModel = value;
                NotifyOfPropertyChange("ViewModel");
            }
        }

        public RelayCommand SelectCommand
        {
            get
            {
                if (_selectCommand == null)
                {
                    _selectCommand = new RelayCommand(ExecuteSelectCommand, CanExecuteSaveCommand, CommandType.None,
                        "�������", "�������");
                }
                return _selectCommand;
            }
        }

        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(ExecuteCancelCommand, CanExecuteCancelCommand, CommandType.None,
                        "������", "������");
                }
                return _cancelCommand;
            }
        }

        #region CancelCommand

        protected virtual bool CanExecuteCancelCommand(object obj)
        {
            return true;
        }

        protected virtual void ExecuteCancelCommand(object obj)
        {
            TryClose(false);
        }

        #endregion

        #region SaveCommand

        protected virtual bool CanExecuteSaveCommand(object obj)
        {
            return true;
        }

        protected virtual void ExecuteSelectCommand(object obj)
        {
            TryClose(true);
        }

        #endregion

        #region Overrides of Screen

        /// <summary>
        ///     Tries to close this instance by asking its Parent to initiate shutdown or by asking its corresponding view to
        ///     close.
        ///     Also provides an opportunity to pass a dialog result to it's corresponding view.
        /// </summary>
        /// <param name="dialogResult">The dialog result.</param>
        public override void TryClose(bool? dialogResult = null)
        {
            base.TryClose(dialogResult);
            var window = GetView() as RadWindow;
            if (window != null)
                window.Close();
        }


        /// <summary>
        ///     Called when activating.
        /// </summary>
        protected override void OnActivate()
        {
            var dictionaryViewModelBase = _screenFactory.Create<DictionaryViewModelBase<T>>();
            ActivateItem(dictionaryViewModelBase);
            DisplayName = dictionaryViewModelBase.DisplayName;
        }

        #endregion
    }
}