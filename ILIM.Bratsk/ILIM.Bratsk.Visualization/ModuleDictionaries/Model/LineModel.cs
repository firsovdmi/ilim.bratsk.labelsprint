using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Visualization.Model;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.Model
{
    public class LineModel : EntityWithEditStatusModel<Line>
    {
        public LineModel()
        {
            Content = new Line {ID = Guid.NewGuid()};
        }

        public LineModel(Line model)
            : base(model)
        {
        }
    }
}