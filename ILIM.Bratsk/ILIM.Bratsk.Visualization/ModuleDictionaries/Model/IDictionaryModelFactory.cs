using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Model;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.Model
{
    public interface IDictionaryModelFactory
    {
        EntityWithEditStatusModel<T> Create<T>(T model) where T : EntityBase, new();
        void Release(object model);
    }
}