using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Visualization.Model;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.Model
{
    public class ApplicationSettingsModel : EntityWithEditStatusModel<ApplicationSettings>
    {
        public ApplicationSettingsModel()
        {
            Content = new ApplicationSettings {ID = Guid.NewGuid()};
        }

        public ApplicationSettingsModel(ApplicationSettings model)
            : base(model)
        {
        }
    }
}