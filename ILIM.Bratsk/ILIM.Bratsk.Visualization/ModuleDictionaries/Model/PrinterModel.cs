using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Visualization.Model;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.Model
{
    public class PrinterModel : EntityWithEditStatusModel<Printer>
    {
        public PrinterModel()
        {
            Content = new Printer {ID = Guid.NewGuid()};
        }

        public PrinterModel(Printer model)
            : base(model)
        {
        }
    }
}