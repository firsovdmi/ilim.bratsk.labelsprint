using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.MenuAction
{
    public abstract class DictionaryMenuAction<T> : DictionaryMenuAction, IHandle<AccountDataChanged> where T : Screen
    {
        private readonly object _executeLock = new object();
        private readonly IActiveAccountManager ActiveAccountManager;
        protected readonly IEventAggregator EventAggregator;
        protected readonly IRadDockingManager RadDockingManager;
        protected readonly IScreenFactory ScreenFactory;

        protected DictionaryMenuAction(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public DictionaryMenuAction(string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
        }

        public DictionaryMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            string displayName, IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager)
            : base(displayName)
        {
            RadDockingManager = radDockingManager;
            ScreenFactory = screenFactory;
            EventAggregator = eventAggregator;
            ActiveAccountManager = activeAccountManager;
            CheckAcess(ActiveAccountManager.BlackListObjects);
        }

        public void Handle(AccountDataChanged message)
        {
            CheckAcess(message.AuthenticationResult.DeniedObjects);
        }

        /// <summary>
        ///     The action associated to the ActionItem
        /// </summary>
        public override void Execute()
        {
            lock (_executeLock)
            {
                var viewmodel = ScreenFactory.Create<T>();
                //Anti-double subscribe hack
                viewmodel.Deactivated -= Release;
                viewmodel.Deactivated += Release;

                RadDockingManager.ShowWindow(viewmodel, null);
            }
        }

        protected void Release(object sender, DeactivationEventArgs e)
        {
            var model = (Screen) sender;
            model.Deactivated -= Release;
            ScreenFactory.Release(sender);
        }

        private void CheckAcess(IEnumerable<string> blackListObjects)
        {
            var objectName = "";
            try
            {
                objectName =
                    typeof (T).GetField("DictionaryService", BindingFlags.Instance | BindingFlags.NonPublic)
                        .ReflectedType.GetGenericArguments()[0].Name + "Service.Get";
            }
            catch
            {
            }
            IsEnable = blackListObjects.All(p => p != objectName);
        }
    }

    public class DictionaryMenuAction : ActionItem
    {
        protected DictionaryMenuAction(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        ///     Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public DictionaryMenuAction(string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
        }
    }
}