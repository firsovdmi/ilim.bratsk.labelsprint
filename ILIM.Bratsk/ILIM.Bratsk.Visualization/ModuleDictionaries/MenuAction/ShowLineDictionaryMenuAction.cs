using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.MenuAction
{
    public class ShowLineDictionaryMenuAction : DictionaryMenuAction<DictionaryViewModelBase<Line>>
    {
        public ShowLineDictionaryMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager)
            : base(radDockingManager, screenFactory, "���������� �����", eventAggregator, activeAccountManager)
        {
        }
    }
}