﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Visualization.ModuleDictionaries.Model;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.Views
{
    /// <summary>
    /// Interaction logic for ApplicationSettingsDictionaryView.xaml
    /// </summary>
    public partial class ApplicationSettingsDictionaryView : UserControl
    {
        public ApplicationSettingsDictionaryView()
        {
            InitializeComponent();
        }

        private void RaiseEdit(object sender, object e)
        {
            try
            {
                ((ApplicationSettingsModel)((FrameworkElement)sender).DataContext).IsEdited = true;
            }
            catch (Exception ex)
            {
            }
        }
    }
}
