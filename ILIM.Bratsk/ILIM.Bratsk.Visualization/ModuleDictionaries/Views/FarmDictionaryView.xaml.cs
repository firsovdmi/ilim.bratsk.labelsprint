﻿using System.Windows.Controls;
using ILIM.Bratsk.Visualization.Extentions;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.Views
{
    /// <summary>
    ///     Логика взаимодействия для DictionaryEdit.xaml
    /// </summary>
    public partial class FarmDictionaryView : UserControl
    {
        public FarmDictionaryView()
        {
            InitializeComponent();
            GridView.ClipboardPasteMode = GridViewClipboardPasteMode.SkipHiddenColumns |
                                          GridViewClipboardPasteMode.InsertNewRows;
            GridView.Pasting += GridView_Pasting;
            GridView.Pasted += GridView_Pasted;
            new PersistenceManagerTool(this, GridView);
        }

        private void GridView_Pasting(object sender, GridViewClipboardEventArgs e)
        {
            if (GridView.Columns == null || GridView.Columns.Count < 1) return;
            GridView.Columns[0].IsVisible = false;
        }

        private void GridView_Pasted(object sender, RadRoutedEventArgs e)
        {
            if (GridView.Columns == null || GridView.Columns.Count < 1) return;
            GridView.Columns[0].IsVisible = true;
        }
    }
}