using System.Collections.Generic;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleDictionaries.MenuAction;
using ILIM.Bratsk.Visualization.ModuleDictionaries.ToolBarAction;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries
{
    public class DictionaryModule : IModule
    {
        public DictionaryModule(IMenuManager menuManager,
            IToolBarManager toolBarManager,
            IEnumerable<DictionaryMenuAction> actionItems,
            DictionaryActionItems dictionaryActionItems
            )
        {
            menuManager.ShowItem(new ActionItem("Справочники", null),2);
            foreach (var actionItem in actionItems)
                menuManager.WithParent("Справочники")
                    .ShowItem(actionItem);

            toolBarManager.ShowItem(dictionaryActionItems);
            dictionaryActionItems.Deactivate(false);
        }
    }
}