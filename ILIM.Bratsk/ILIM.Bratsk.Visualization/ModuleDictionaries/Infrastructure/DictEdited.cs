using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.Infrastructure
{
    public class DictEdited<T> : DictChanges<T> where T : IEntity
    {
        public DictEdited(T content, object sender)
            : base(content, sender)
        {
        }
    }
}