using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.Infrastructure
{
    public class DictAdded<T> : DictChanges<T> where T : IEntity
    {
        public DictAdded(T content, object sender)
            : base(content, sender)
        {
        }
    }
}