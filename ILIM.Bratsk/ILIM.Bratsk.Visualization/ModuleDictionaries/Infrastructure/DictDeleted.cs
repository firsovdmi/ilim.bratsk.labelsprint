using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Visualization.ModuleDictionaries.Infrastructure
{
    public class DictDeleted<T> : DictChanges<T> where T : IEntity
    {
        public DictDeleted(T content, object sender)
            : base(content, sender)
        {
        }
    }
}