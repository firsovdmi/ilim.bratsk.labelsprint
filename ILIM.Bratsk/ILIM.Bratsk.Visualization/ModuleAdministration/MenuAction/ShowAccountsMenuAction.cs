using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.MenuAction
{
    public class ShowAccountsMenuAction : AdministrationMenuAction<AccountsViewModel>
    {
        public ShowAccountsMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager) :
                base(radDockingManager, screenFactory, "������� ������", eventAggregator, activeAccountManager)
        {
        }

        protected override string AuthorizeObjectName
        {
            get { return "GetRolesShorInfo"; }
        }
    }
}