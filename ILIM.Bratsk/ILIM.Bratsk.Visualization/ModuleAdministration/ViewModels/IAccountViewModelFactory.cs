using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels
{
    //public class AdministrationAccountsViewModel : Conductor<AdministrationAccountViewModel>
    //{
    //    private readonly IAAAServiceRemoteFasade _remoteFacade;
    //    private readonly IWindowManager _windowManager;


    //    private EntityWithEditStatusModel<Content> _selectedAccount;

    //    //public ObservableCollection<LoginInfoItemViewModel> Logins { get; set; }

    //    //public ObservableCollection<RoleViewModel> AcessGroups { get; set; }
    //    //public IEnumerable<RoleViewModel> SelectedLoginAcessGroups
    //    //{
    //    //    get { return AcessLoginsGroups.Where(p => p.IsSelected); }
    //    //}

    //    //public ObservableCollection<RoleViewModel> AcessLoginsGroups
    //    //{
    //    //    get { return SelectedLogin == null ? null : new ObservableCollection<RoleViewModel>(AcessGroups.Where(p => !SelectedLogin.AcessGroups.Contains(p))); }
    //    //}

    //    //public ObservableCollection<AcessGroupViewModel> AssignedAcessLoginsGroups
    //    //{
    //    //    get
    //    //    {
    //    //        return SelectedLogin == null ? null : SelectedLogin.AcessGroups;
    //    //    }
    //    //}
    //    //public IEnumerable<RoleViewModel> SelectedAssignedLoginAcessGroups
    //    //{
    //    //    get { return AssignedAcessLoginsGroups.Where(p => p.IsSelected).ToList(); }
    //    //}


    //    public EntityWithEditStatusModel<Content> SelectedAccount
    //    {
    //        get { return _selectedAccount; }
    //        set
    //        {
    //            _selectedAccount = value;
    //            NotifyOfPropertyChange("SelectedAccount");
    //        }
    //    }

    //    public BindableCollection<EntityWithEditStatusModel<Content>> Accounts
    //    {
    //        get { return _accounts; }
    //        set
    //        {
    //            if (Equals(value, _accounts)) return;
    //            _accounts = value;
    //            NotifyOfPropertyChange(() => Accounts);
    //        }
    //    }


    //    //private void NotifyAcessGroups()
    //    //{
    //    //    NotifyOfPropertyChange("AssignedAcessLoginsGroups");
    //    //    NotifyOfPropertyChange("AcessLoginsGroups");
    //    //}
    //    public AdministrationUserViewModel(IAAAServiceRemoteFasade autentificator, IWindowManager windowManager)
    //    {

    //        _remoteFacade = autentificator;
    //        _windowManager = windowManager;
    //        DisplayName = "���������� ����������";
    //        RefreshAll();
    //    }

    //    private void RefreshAll()
    //    {
    //        Logins = new ObservableCollection<LoginInfoItemViewModel>();
    //        AcessGroups = new ObservableCollection<RoleViewModel>();
    //        foreach (var p in _remoteFacade.GetAcessGroups())
    //        {
    //            AcessGroups.Add(new RoleViewModel
    //            {
    //                Id = p.Id,
    //                Name = p.Name
    //            });
    //        }
    //        foreach (var p in _remoteFacade.GetAllLogins().Where(l => l.Login != "system"))
    //        {
    //            var loginItem = new LoginInfoItemViewModel
    //            {
    //                Id = p.Id,
    //                Login = p.Login,
    //                IsWindowsIdentity = p.IsWindowsIdentity,
    //                AutoLogoffTime = (int)p.AutoLogoffTime.TotalMinutes,
    //                Email = p.Email,
    //                IsPasswordChanged = p.IsChangePassword,
    //                AcessGroups = new ObservableCollection<RoleViewModel>()
    //            };
    //            foreach (var a in p.AcessGroups)
    //            {
    //                var modelViewGroup = AcessGroups.FirstOrDefault(pp => pp.Id == a.Id);
    //                if (modelViewGroup != null) loginItem.AcessGroups.Add(modelViewGroup);
    //            }
    //            loginItem.ResetIsEdit();
    //            Logins.Add(loginItem);
    //        }
    //        SelectedLogin = Logins.FirstOrDefault();
    //        NotifyOfPropertyChange(() => SelectedLogin);
    //        NotifyAcessGroups();
    //    }

    //    #region SaveItemsCommand
    //    private RelayCommand _saveItemsCommandCommand;

    //    public RelayCommand SaveItemsCommand
    //    {
    //        get
    //        {
    //            return _saveItemsCommandCommand ??
    //                   (_saveItemsCommandCommand = new RelayCommand(ExecuteSaveItemCommand, CanExecuteSaveItemCommand));
    //        }
    //    }

    //    protected virtual bool CanExecuteSaveItemCommand(object obj)
    //    {
    //        return Logins.Any(p => p.IsEdited || p.IsAdded || p.IsDeleted); ;
    //    }

    //    protected virtual void ExecuteSaveItemCommand(object obj)
    //    {
    //        foreach (var login in Logins.Where(p => p.IsAdded))
    //        {
    //            if (login.Password != login.ConfirmPassword)
    //            {
    //                SelectedLogin = login;
    //                _windowManager.Alert("������", "������ �� ���������");
    //                return;
    //            }
    //        }
    //        foreach (var login in Logins.Where(p => p.IsAdded || p.IsEdited))
    //        {
    //            if (!string.IsNullOrEmpty(login.Email) && !IsValidEmail(login.Email))
    //            {
    //                SelectedLogin = login;
    //                _windowManager.Alert("������", "Email ������ �� �����");
    //                return;
    //            }
    //        }
    //        _remoteFacade.RemoveLogins(Logins.Where(p => p.IsDeleted).Select(p => new FullLoginInfo
    //        {
    //            Id = p.Id,
    //            AcessGroups = p.AcessGroups.Select(pp => new AcessGroup { Id = pp.Id, Name = pp.Name }).ToList(),
    //            AutoLogoffTime = p.AutoLogoffTime == 0 ? TimeSpan.Zero : new TimeSpan(0, p.AutoLogoffTime, 0),
    //            IsWindowsIdentity = p.IsWindowsIdentity,
    //            Login = p.Login,
    //            Password = p.Password
    //        }));
    //        _remoteFacade.SaveLogins(Logins.Where(p => p.IsEdited || p.IsAdded).Select(p => new FullLoginInfo
    //        {
    //            Id = p.Id,
    //            AcessGroups = p.AcessGroups.Select(pp => new AcessGroup { Id = pp.Id, Name = pp.Name }).ToList(),
    //            AutoLogoffTime = p.AutoLogoffTime == 0 ? TimeSpan.Zero : new TimeSpan(0, p.AutoLogoffTime, 0),
    //            IsWindowsIdentity = p.IsWindowsIdentity,
    //            Login = p.Login,
    //            Password = p.Password,
    //            Email = p.Email,
    //            IsChangePassword = p.IsPasswordChanged
    //        }));
    //        RefreshAll();
    //        NotifyOfPropertyChange("Logins");
    //    }
    //    #endregion

    //    #region AddItemsCommand
    //    private RelayCommand _addItemsCommandCommand;

    //    public RelayCommand AddItemsCommand
    //    {
    //        get
    //        {
    //            return _addItemsCommandCommand ??
    //                   (_addItemsCommandCommand = new RelayCommand(ExecuteAddItemCommand, CanExecuteAddItemCommand));
    //        }
    //    }

    //    protected virtual bool CanExecuteAddItemCommand(object obj)
    //    {
    //        return true;
    //    }

    //    protected virtual void ExecuteAddItemCommand(object obj)
    //    {
    //        Logins.Add(new LoginInfoItemViewModel
    //        {
    //            Login = "����� ������������",
    //            IsAdded = true,
    //            AcessGroups = new ObservableCollection<RoleViewModel>()
    //        });
    //    }
    //    #endregion

    //    #region RemoveItemsCommand
    //    private RelayCommand _removeItemsCommandCommand;

    //    public RelayCommand RemoveItemsCommand
    //    {
    //        get
    //        {
    //            return _removeItemsCommandCommand ??
    //                   (_removeItemsCommandCommand = new RelayCommand(ExecuteRemoveItemCommand, CanExecuteRemoveItemCommand));
    //        }
    //    }

    //    protected virtual bool CanExecuteRemoveItemCommand(object obj)
    //    {
    //        return SelectedLogin != null && SelectedLogin.AcessGroups != null && SelectedLogin.IsNotSystemLogin;
    //    }

    //    protected virtual void ExecuteRemoveItemCommand(object obj)
    //    {
    //        SelectedLogin.IsDeleted = true;
    //    }
    //    #endregion

    //    #region AssignAcessGroupLoginCommand
    //    private RelayCommand _assignAcessGroupLoginCommandCommand;

    //    public RelayCommand AssignAcessGroupLoginCommand
    //    {
    //        get
    //        {
    //            return _assignAcessGroupLoginCommandCommand ??
    //                   (_assignAcessGroupLoginCommandCommand =
    //                       new RelayCommand(ExecuteAssignAcessGroupLoginCommand, CanExecuteAssignAcessGroupLoginCommand, CommandType.Up, "", "��������� ������������ ������"));
    //        }
    //    }
    //    protected virtual bool CanExecuteAssignAcessGroupLoginCommand(object obj)
    //    {
    //        return SelectedLogin != null && SelectedLoginAcessGroups.Any() && SelectedLogin.IsNotSystemLogin;
    //    }

    //    protected virtual void ExecuteAssignAcessGroupLoginCommand(object obj)
    //    {
    //        foreach (var acessGroup in SelectedLoginAcessGroups)
    //        {
    //            SelectedLogin.AcessGroups.Add(acessGroup);
    //        }
    //        SelectedLogin.IsEdited = true;
    //        NotifyAcessGroups();
    //    }
    //    #endregion

    //    #region RemoveAssignAcessGroupLoginCommand
    //    private RelayCommand _removeAssignAcessGroupLoginCommandCommand;

    //    public RelayCommand RemoveAssignAcessGroupLoginCommand
    //    {
    //        get
    //        {
    //            return _removeAssignAcessGroupLoginCommandCommand ??
    //                   (_removeAssignAcessGroupLoginCommandCommand =
    //                       new RelayCommand(ExecuteRemoveAssignAcessGroupLoginCommand, CanExecuteRemoveAssignAcessGroupLoginCommand, CommandType.Down, "", "����� ���������� ������"));
    //        }
    //    }
    //    protected virtual bool CanExecuteRemoveAssignAcessGroupLoginCommand(object obj)
    //    {
    //        return SelectedLogin != null && SelectedAssignedLoginAcessGroups.Any() && SelectedLogin.IsNotSystemLogin;
    //    }

    //    protected virtual void ExecuteRemoveAssignAcessGroupLoginCommand(object obj)
    //    {
    //        foreach (var acessGroup in SelectedAssignedLoginAcessGroups)
    //        {
    //            SelectedLogin.AcessGroups.Remove(acessGroup);
    //        }
    //        SelectedLogin.IsEdited = true;
    //        NotifyAcessGroups();
    //    }
    //    #endregion

    //    #region ChangePasswordCommand
    //    private RelayCommand _changePasswordCommandCommand;
    //    private BindableCollection<EntityWithEditStatusModel<Content>> _accounts;

    //    public RelayCommand ChangePasswordCommand
    //    {
    //        get
    //        {
    //            return _changePasswordCommandCommand ??
    //                   (_changePasswordCommandCommand = new RelayCommand(ExecuteChangePasswordCommand, CanExecuteChangePasswordCommand, CommandType.None, "�������� ������", "�������� ������"));
    //        }
    //    }

    //    protected virtual bool CanExecuteChangePasswordCommand(object obj)
    //    {
    //        return SelectedLogin != null && SelectedLogin.Id != 0 && SelectedLogin.Password != null && SelectedLogin.Password == SelectedLogin.ConfirmPassword;
    //    }

    //    bool IsValidEmail(string email)
    //    {
    //        return Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
    //    }

    //    protected virtual void ExecuteChangePasswordCommand(object obj)
    //    {
    //        _remoteFacade.ChangePassword(new FullLoginInfo
    //        {
    //            Id = SelectedLogin.Id,
    //            Password = SelectedLogin.Password
    //        });
    //        SelectedLogin.Password = null;
    //        SelectedLogin.ConfirmPassword = null;
    //    }
    //    #endregion
    //}

    //public class AdministrationAccountViewModel:SelectableScreen
    //{

    //}


    //[View(typeof(ReceivingToolBarView), Context = "ToolBar")]
    //[View(typeof(ReceivingContextMenuView), Context = "ContextMenu")]

    public interface IAccountViewModelFactory
    {
        AccountViewModel Create(AccountShortInfo info, BindableCollection<ShortEntity> roles);
        void Release(object obj);
    }

    public interface IRoleViewModelFactory
    {
        RoleViewModel Create(ShortEntity info, BindableCollection<ShortEntity> authorizeObject);
        void Release(object obj);
    }
}