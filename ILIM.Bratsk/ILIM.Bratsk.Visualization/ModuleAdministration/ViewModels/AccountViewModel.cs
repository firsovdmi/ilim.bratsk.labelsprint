using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.Views;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels
{
    [View(typeof (AccountInRoleView), "AccountInRole")]
    public class AccountViewModel : Screen, IEditStatus
    {
        private BindableCollection<ShortEntity> _accountRoles;
        private RelayCommand _addRoleCommand;
        private string _confirmedPassword;
        private Account _content;
        private string _error;
        private bool _hasErrors;
        private bool _isAdded;
        private bool _isChangePassword;
        private bool _isDeleted;
        private bool _isEdited;
        private BindableCollection<ShortEntity> _notAccountRoles;
        private string _password;
        private RelayCommand _removeRoleCommand;
        private BindableCollection<ShortEntity> _roles;
        private readonly IAAAServiceRemoteFasade _aaaService;

        public AccountViewModel(
            IAAAServiceRemoteFasade aaaService,
            AccountShortInfo info,
            BindableCollection<ShortEntity> roles)
        {
            _aaaService = aaaService;
            AccountShortInfo = info;
            Roles = roles;
        }

        #region Public Methods

        public bool Validate()
        {
            var results = new List<ValidationResult>();
            return Validator.TryValidateObject(Content, new ValidationContext(Content, null, null), results, true) &&
                   (ConfirmedPassword == Password && IsChangePassword || !IsChangePassword);
        }

        #endregion

        #region Properties

        public bool IsChangePassword
        {
            get { return _isChangePassword; }
            set
            {
                if (value.Equals(_isChangePassword)) return;
                _isChangePassword = value;
                IsEdited = true;
                NotifyOfPropertyChange("IsChangePassword");
                NotifyOfPropertyChange(() => ConfirmedPassword);
                NotifyOfPropertyChange(() => Password);
            }
        }

        //public bool IsPasswordEqualsConfirmation
        //{
        //    get { return Password == ConfirmedPassword && IsChangePassword; }

        //}

        public string Password
        {
            get { return _password; }
            set
            {
                if (value == _password) return;
                _password = value;
                IsEdited = true;
                NotifyOfPropertyChange("Password");
                //NotifyOfPropertyChange(() => IsPasswordEqualsConfirmation);
            }
        }

        public string ConfirmedPassword
        {
            get { return _confirmedPassword; }
            set
            {
                if (value != Password && IsChangePassword)
                    throw new ValidationException("������ �� ���������");
                if (value == _confirmedPassword) return;
                _confirmedPassword = value;
                IsEdited = true;
                NotifyOfPropertyChange("ConfirmedPassword");
                //NotifyOfPropertyChange(() => IsPasswordEqualsConfirmation);
            }
        }

        public Account Content
        {
            get { return _content; }
            set
            {
                if (Equals(value, _content)) return;
                _content = value;
                NotifyOfPropertyChange(() => Content);
            }
        }

        public AccountShortInfo AccountShortInfo { get; private set; }

        public BindableCollection<ShortEntity> Roles
        {
            get { return _roles; }
            set
            {
                if (Equals(value, _roles)) return;
                _roles = value;
                NotifyOfPropertyChange("Roles");
            }
        }

        public BindableCollection<ShortEntity> AccountRoles
        {
            get { return _accountRoles; }
            set
            {
                if (Equals(value, _accountRoles)) return;
                _accountRoles = value;
                NotifyOfPropertyChange("AccountRoles");
            }
        }

        public BindableCollection<ShortEntity> NotAccountRoles
        {
            get { return _notAccountRoles; }
            set
            {
                if (Equals(value, _notAccountRoles)) return;
                _notAccountRoles = value;
                NotifyOfPropertyChange("NotAccountRoles");
            }
        }

        public RelayCommand AddRoleCommand
        {
            get
            {
                if (_addRoleCommand == null)
                {
                    _addRoleCommand = new RelayCommand(ExecuteAddRoleCommand, CanExecuteAddRoleCommand,
                        CommandType.MoveRight, "���������", "��������� ����");
                }
                return _addRoleCommand;
            }
        }

        public RelayCommand RemoveRoleCommand
        {
            get
            {
                if (_removeRoleCommand == null)
                {
                    _removeRoleCommand = new RelayCommand(ExecuteRemoveRoleCommand, CanExecuteRemoveRoleCommand,
                        CommandType.MoveLeft, "����� ����������", "����� ����������� ����");
                }
                return _removeRoleCommand;
            }
        }

        #endregion

        #region RemoveRoleCommand

        private bool CanExecuteRemoveRoleCommand(object roles)
        {
            return roles != null;
        }

        private void ExecuteRemoveRoleCommand(object roles)
        {
            if (roles != null)
            {
                var shortEntities = roles as IEnumerable<object>;
                if (shortEntities != null)
                {
                    var selected = shortEntities.Where(p => p is ShortEntity).ToArray();
                    if (selected.Any())
                    {
                        IsEdited = true;
                        foreach (ShortEntity shortEntity in selected)
                        {
                            var accountInRole =
                                Content.AccountInRoles.FirstOrDefault(p => p.RoleID == shortEntity.ID);
                            if (accountInRole != null) Content.AccountInRoles.Remove(accountInRole);
                            if (AccountRoles.Contains(shortEntity)) AccountRoles.Remove(shortEntity);
                            NotAccountRoles.Add(shortEntity);
                        }
                    }
                }
            }
        }

        #endregion

        #region AddRoleCommand

        private bool CanExecuteAddRoleCommand(object roles)
        {
            return roles != null;
        }

        private void ExecuteAddRoleCommand(object roles)
        {
            if (roles != null)
            {
                var shortEntities = roles as IEnumerable<object>;
                if (shortEntities != null)
                {
                    var selected = shortEntities.Where(p => p is ShortEntity).ToArray();
                    if (selected.Any())
                    {
                        IsEdited = true;
                        foreach (ShortEntity shortEntity in selected)
                        {
                            Content.AccountInRoles.Add(new AccountInRole
                            {
                                ID = Guid.NewGuid(),
                                AccountID = Content.ID,
                                RoleID = shortEntity.ID
                            });
                            AccountRoles.Add(shortEntity);
                            if (NotAccountRoles.Contains(shortEntity)) NotAccountRoles.Remove(shortEntity);
                        }
                    }
                }
            }
        }

        #endregion

        #region Implementation of IEditStatus

        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                if (value.Equals(_isEdited)) return;
                _isEdited = value;
                NotifyOfPropertyChange("IsEdited");
            }
        }

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                if (value.Equals(_isDeleted)) return;
                _isDeleted = value;
                NotifyOfPropertyChange("IsDeleted");
            }
        }

        public bool IsAdded
        {
            get { return _isAdded; }
            set
            {
                if (value.Equals(_isAdded)) return;
                _isAdded = value;
                NotifyOfPropertyChange("IsAdded");
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        ///     �������������
        /// </summary>
        private void Initialize()
        {
            var data = _aaaService.GedAccountByID(AccountShortInfo.ID);
            if (data == null)
                Content =
                    new Account
                    {
                        ID = AccountShortInfo.ID,
                        Login = AccountShortInfo.Login,
                        AccountInRoles = new List<AccountInRole>()
                    };
            else
            {
                Content = data;
                if (data.AccountInRoles == null)
                    data.AccountInRoles = new List<AccountInRole>();
            }

            Content.PropertyChanged += _content_PropertyChanged;
            var accountRoles = Content.AccountInRoles.Select(p => p.RoleID).ToArray();

            AccountRoles = new BindableCollection<ShortEntity>(Roles.Where(p => accountRoles.Contains(p.ID)));
            NotAccountRoles = new BindableCollection<ShortEntity>(Roles.Where(p => !accountRoles.Contains(p.ID)));
            Roles.CollectionChanged += Roles_CollectionChanged;
        }

        private void Roles_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var accountRoles = Content.AccountInRoles.Select(p => p.RoleID).ToArray();
            AccountRoles = new BindableCollection<ShortEntity>(Roles.Where(p => accountRoles.Contains(p.ID)));
            NotAccountRoles = new BindableCollection<ShortEntity>(Roles.Where(p => !accountRoles.Contains(p.ID)));
        }

        /// <summary>
        ///     ������ �������� ����������
        /// </summary>
        /// <param name="action"></param>
        private void RunAsync(Action action)
        {
            Task.Factory.StartNew(action);
        }

        private void _content_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Login")
                AccountShortInfo.Login = Content.Login;
            IsEdited = true;
            //ValidateProperty(sender,e.PropertyName);
        }

        private void ValidateProperty(object o, string propertyName)
        {
            Validator.ValidateProperty(o.GetType().GetProperty(propertyName).GetValue(o, null),
                new ValidationContext(o, null, null) {MemberName = propertyName});
        }

        #endregion

        #region Overrides of Screen

        /// <summary>
        ///     Called when initializing.
        /// </summary>
        protected override void OnInitialize()
        {
            Initialize();
        }


        /// <summary>
        ///     Called when activating.
        /// </summary>
        protected override void OnActivate()
        {
        }

        #endregion
    }
}