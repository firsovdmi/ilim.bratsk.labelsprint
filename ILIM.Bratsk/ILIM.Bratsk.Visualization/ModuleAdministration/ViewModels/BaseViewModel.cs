﻿using System;
using Caliburn.Micro;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels
{
    public class BaseViewModel : PropertyChangedBase
    {
        private bool _isAdded;
        private bool _isDeleted;
        private bool _isEdited;
        public Guid Id { get; set; }
        public bool IsSelected { get; set; }

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                NotifyOfPropertyChange("IsDeleted");
            }
        }

        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                _isEdited = value;
                NotifyOfPropertyChange("IsEdited");
            }
        }

        public bool IsAdded
        {
            get { return _isAdded; }
            set
            {
                _isAdded = value;
                NotifyOfPropertyChange("IsAdded");
            }
        }
    }
}