﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.Helpers;
using ILIM.Bratsk.Visualization.Infrastructure;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels
{
    public class EventsViewModel : Screen
    {
        private IEventService _eventService;

        public EventsViewModel(IEventService eventService)
        {
            _eventService = eventService;
            Filter=new EventFilter {Count=100};
            Initialize();
            Refrsh();
        }

        public EnumForBinding<EventStatusEnum> StatusesSource { get; set; }
        public EnumForBinding<EventSourceEnum> EventSourcesSource { get; set; }
        public EnumForBinding<EventTypeEnum> EventTypesSource { get; set; }
        public EnumForBinding<PrinterModeEnum> PrinterModesSource { get; set; }
        public string SelectedStatuses
        {get { return string.Join(",", StatusesSource.Items.Where(p => p.IsSelected)); }}
        private void Initialize()
        {
            DisplayName = "События";
            StatusesSource=new EnumForBinding<EventStatusEnum>();
            EventSourcesSource = new EnumForBinding<EventSourceEnum>();
            EventTypesSource = new EnumForBinding<EventTypeEnum>();
            PrinterModesSource = new EnumForBinding<PrinterModeEnum>();
        }

        public ObservableCollection<Event> Items { get; set; }
        public EventFilter Filter { get; set; }

        void Refrsh()
        {
            Filter.Statuses = StatusesSource.Items.Where(p => p.IsSelected).Select(p => (int)p.Value).ToList();
            Filter.EventSources = EventSourcesSource.Items.Where(p => p.IsSelected).Select(p => (int)p.Value).ToList();
            Filter.EventTypes = EventTypesSource.Items.Where(p => p.IsSelected).Select(p => (int)p.Value).ToList();
            Filter.PrinterModeEnums = PrinterModesSource.Items.Where(p => p.IsSelected).Select(p => (int)p.Value).ToList();
            Items = new ObservableCollection<Event>(_eventService.GetByFilter(Filter));
            NotifyOfPropertyChange(()=>Items);
        }


        private RelayCommand _refreshCommand;
        public RelayCommand RefreshCommand
        {
            get
            {
                return _refreshCommand ??
                       (_refreshCommand =
                        new RelayCommand(ExecuteRefreshCommand, CanRefreshCommand, CommandType.Refresh,
                                         "Обновить", "Обновить"));
            }
        }

        private void ExecuteRefreshCommand(object obj)
        {
            Refrsh();
        }

        private bool CanRefreshCommand(object obj)
        {
            return true;
        }
    }


    public class EnumDescriptions : PropertyChangedBase
    {
        public object Value { get; set; }
        public string Name { get; set; }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; NotifyOfPropertyChange(() => IsSelected); }
        }

        private bool _isSelected;
    }

    public class EnumForBinding<T> : PropertyChangedBase
    {
        public ObservableCollection<EnumDescriptions> Items { get; set; }
        public bool IsAllItems { get; set; }

        public string SelectedToString
        {
            get { return string.Join(",", Items.Where(p => p.IsSelected).Select(p => p.Name)); }
        }

        public EnumForBinding()
        {
            IsAllItems = true;
            Items = new ObservableCollection<EnumDescriptions>();
            var type = typeof(T);
            var names = Enum.GetNames(type);
            foreach (var name in names)
            {
                var field = type.GetField(name);
                var fds = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
                foreach (DescriptionAttribute fd in fds)
                {
                    var item = new EnumDescriptions {Value = field.GetValue(name), Name = fd.Description};
                    item.PropertyChanged += item_PropertyChanged;
                    Items.Add(item);
                }
            }
        }

        void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            NotifyOfPropertyChange(() => SelectedToString);
        }
    }
}
