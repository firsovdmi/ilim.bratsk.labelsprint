using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.Views;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels
{
    [View(typeof (RoleAuthorizeObjectView), "RoleAuthorizeObject")]
    public class RoleViewModel : Screen, IEditStatus
    {
        private RelayCommand _addAuthorizeObjectCommand;
        private BindableCollection<ShortEntity> _authorizeObject;
        private Role _content;
        private bool _isAdded;
        private bool _isDeleted;
        private bool _isEdited;
        private BindableCollection<ShortEntity> _notRoleAuthorizeObject;
        private RelayCommand _removeAuthorizeObjectCommand;
        private BindableCollection<ShortEntity> _roleAuthorizeObject;
        private readonly IAAAServiceRemoteFasade _aaaService;

        public RoleViewModel(
            IAAAServiceRemoteFasade aaaService,
            ShortEntity info,
            BindableCollection<ShortEntity> authorizeObject)
        {
            _aaaService = aaaService;
            RoleShortInfo = info;
            AuthorizeObject = authorizeObject;
        }

        #region Public Methods

        public bool Validate()
        {
            var results = new List<ValidationResult>();
            return Validator.TryValidateObject(Content, new ValidationContext(Content, null, null), results, true);
        }

        #endregion

        #region Properties

        public Role Content
        {
            get { return _content; }
            set
            {
                if (Equals(value, _content)) return;
                _content = value;
                NotifyOfPropertyChange(() => Content);
            }
        }

        public ShortEntity RoleShortInfo { get; private set; }

        public BindableCollection<ShortEntity> AuthorizeObject
        {
            get { return _authorizeObject; }
            set
            {
                if (Equals(value, _authorizeObject)) return;
                _authorizeObject = value;
                NotifyOfPropertyChange("AuthorizeObject");
            }
        }

        public BindableCollection<ShortEntity> RoleAuthorizeObject
        {
            get { return _roleAuthorizeObject; }
            set
            {
                if (Equals(value, _roleAuthorizeObject)) return;
                _roleAuthorizeObject = value;
                NotifyOfPropertyChange("RoleAuthorizeObject");
            }
        }

        public BindableCollection<ShortEntity> NotRoleAuthorizeObject
        {
            get { return _notRoleAuthorizeObject; }
            set
            {
                if (Equals(value, _notRoleAuthorizeObject)) return;
                _notRoleAuthorizeObject = value;
                NotifyOfPropertyChange("NotRoleAuthorizeObject");
            }
        }

        public RelayCommand AddAuthorizeObjectCommand
        {
            get
            {
                if (_addAuthorizeObjectCommand == null)
                {
                    _addAuthorizeObjectCommand = new RelayCommand(ExecuteAuthorizeObjectRoleCommand,
                        CanExecuteAddAuthorizeObjectCommand, CommandType.MoveRight, "���������", "��������� ����");
                }
                return _addAuthorizeObjectCommand;
            }
        }

        public RelayCommand RemoveAuthorizeObjectCommand
        {
            get
            {
                if (_removeAuthorizeObjectCommand == null)
                {
                    _removeAuthorizeObjectCommand = new RelayCommand(ExecuteRemoveAuthorizeObjectCommand,
                        CanExecuteRemoveAuthorizeObjectCommand, CommandType.MoveLeft, "����� ����������",
                        "����� ����������� ����");
                }
                return _removeAuthorizeObjectCommand;
            }
        }

        #endregion

        #region RemoveRoleCommand

        private bool CanExecuteRemoveAuthorizeObjectCommand(object roles)
        {
            return roles != null;
        }

        private void ExecuteRemoveAuthorizeObjectCommand(object roles)
        {
            if (roles != null)
            {
                var shortEntities = roles as IEnumerable<object>;
                if (shortEntities != null)
                {
                    var selected = shortEntities.Where(p => p is ShortEntity).ToArray();
                    if (selected.Any())
                    {
                        IsEdited = true;
                        foreach (ShortEntity shortEntity in selected)
                        {
                            var authorizeObjectsForRole =
                                Content.AuthorizeObjectsForRoles.FirstOrDefault(
                                    p => p.AuthorizeObjectID == shortEntity.ID);
                            if (authorizeObjectsForRole != null)
                                Content.AuthorizeObjectsForRoles.Remove(authorizeObjectsForRole);
                            if (RoleAuthorizeObject.Contains(shortEntity)) RoleAuthorizeObject.Remove(shortEntity);
                            NotRoleAuthorizeObject.Add(shortEntity);
                        }
                    }
                }
            }
        }

        #endregion

        #region AddRoleCommand

        private bool CanExecuteAddAuthorizeObjectCommand(object roles)
        {
            return roles != null;
        }

        private void ExecuteAuthorizeObjectRoleCommand(object roles)
        {
            if (roles != null)
            {
                var shortEntities = roles as IEnumerable<object>;
                if (shortEntities != null)
                {
                    var selected = shortEntities.Where(p => p is ShortEntity).ToArray();
                    if (selected.Any())
                    {
                        IsEdited = true;
                        foreach (ShortEntity shortEntity in selected)
                        {
                            Content.AuthorizeObjectsForRoles.Add(new AuthorizeObjectsForRole
                            {
                                ID = Guid.NewGuid(),
                                RoleID = Content.ID,
                                AuthorizeObjectID = shortEntity.ID
                            });
                            RoleAuthorizeObject.Add(shortEntity);
                            if (NotRoleAuthorizeObject.Contains(shortEntity))
                                NotRoleAuthorizeObject.Remove(shortEntity);
                        }
                    }
                }
            }
        }

        #endregion

        #region Implementation of IEditStatus

        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                if (value.Equals(_isEdited)) return;
                _isEdited = value;
                NotifyOfPropertyChange("IsEdited");
            }
        }

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                if (value.Equals(_isDeleted)) return;
                _isDeleted = value;
                NotifyOfPropertyChange("IsDeleted");
            }
        }

        public bool IsAdded
        {
            get { return _isAdded; }
            set
            {
                if (value.Equals(_isAdded)) return;
                _isAdded = value;
                NotifyOfPropertyChange("IsAdded");
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        ///     �������������
        /// </summary>
        private void Initialize()
        {
            var data = _aaaService.GetRolesByID(RoleShortInfo.ID);
            if (data == null)
                Content =
                    new Role
                    {
                        ID = RoleShortInfo.ID,
                        Name = RoleShortInfo.Name,
                        AuthorizeObjectsForRoles = new List<AuthorizeObjectsForRole>()
                    };
            else
            {
                Content = data;
                if (data.AuthorizeObjectsForRoles == null)
                    data.AuthorizeObjectsForRoles = new List<AuthorizeObjectsForRole>();
            }

            Content.PropertyChanged += _content_PropertyChanged;
            var roleAuthorizeObjects = Content.AuthorizeObjectsForRoles.Select(p => p.AuthorizeObjectID).ToArray();

            RoleAuthorizeObject =
                new BindableCollection<ShortEntity>(AuthorizeObject.Where(p => roleAuthorizeObjects.Contains(p.ID)));
            NotRoleAuthorizeObject =
                new BindableCollection<ShortEntity>(AuthorizeObject.Where(p => !roleAuthorizeObjects.Contains(p.ID)));
        }

        /// <summary>
        ///     ������ �������� ����������
        /// </summary>
        /// <param name="action"></param>
        private void RunAsync(Action action)
        {
            Task.Factory.StartNew(action);
        }

        private void _content_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name")
                RoleShortInfo.Name = Content.Name;
            IsEdited = true;
        }

        private void ValidateProperty(object o, string propertyName)
        {
            Validator.ValidateProperty(o.GetType().GetProperty(propertyName).GetValue(o, null),
                new ValidationContext(o, null, null) {MemberName = propertyName});
        }

        #endregion

        #region Overrides of Screen

        /// <summary>
        ///     Called when initializing.
        /// </summary>
        protected override void OnInitialize()
        {
            Initialize();
        }


        /// <summary>
        ///     Called when activating.
        /// </summary>
        protected override void OnActivate()
        {
        }

        #endregion
    }
}