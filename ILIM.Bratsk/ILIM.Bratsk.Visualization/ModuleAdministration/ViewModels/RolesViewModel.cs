using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;
using Castle.Core.Internal;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.Views;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels
{
    [View(typeof (ToolBarView), "ToolBar")]
    public class RolesViewModel : Conductor<RoleViewModel>.Collection.OneActive
    {
        private RelayCommand<RoleViewModel> _addCommand;
        private BindableCollection<ShortEntity> _authorizeObjects;
        private RelayCommand<RoleViewModel> _deleteItemCommand;
        private RelayCommand<RoleViewModel> _saveItemCommand;
        private RelayCommand<RoleViewModel> _saveItemsCommand;
        private readonly IAAAServiceRemoteFasade _aaaService;
        private readonly IEventAggregator _eventAggregator;
        private readonly IRoleViewModelFactory _roleViewModelFactory;
        private readonly IWindowManager _windowManager;

        public RolesViewModel(
            IWindowManager windowManager,
            IAAAServiceRemoteFasade aaaService,
            IRoleViewModelFactory roleViewModelFactory,
            IEventAggregator eventAggregator
            )
        {
            _windowManager = windowManager;
            _aaaService = aaaService;
            _roleViewModelFactory = roleViewModelFactory;
            _eventAggregator = eventAggregator;
        }

        #region PublicProperties

        public BindableCollection<ShortEntity> AuthorizeObjects
        {
            get { return _authorizeObjects; }
            set
            {
                if (Equals(value, _authorizeObjects)) return;
                _authorizeObjects = value;
                NotifyOfPropertyChange("AuthorizeObjects");
            }
        }

        #region Dictionary cache

        #endregion

        #endregion

        #region Commands

        public RelayCommand<RoleViewModel> AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand<RoleViewModel>(ExecuteAddCommand, CanAddCommand, CommandType.RoleAdd,
                        "��������", "�������� ����");
                }
                return _addCommand;
            }
        }

        public RelayCommand<RoleViewModel> SaveCommand
        {
            get
            {
                if (_saveItemCommand == null)
                {
                    _saveItemCommand = new RelayCommand<RoleViewModel>(ExecuteSaveCommand, CanExecuteSaveCommand,
                        CommandType.Save, "���������", "��������� � ���� ������");
                }
                return _saveItemCommand;
            }
        }

        public RelayCommand<RoleViewModel> SaveAllCommand
        {
            get
            {
                if (_saveItemsCommand == null)
                {
                    _saveItemsCommand = new RelayCommand<RoleViewModel>(ExecuteSaveItemsCommand,
                        CanExecuteSaveItemsCommand, CommandType.SaveAll, "��������� ���", "��������� ��� � ���� ������");
                }
                return _saveItemsCommand;
            }
        }

        public RelayCommand<RoleViewModel> DeleteCommand
        {
            get
            {
                if (_deleteItemCommand == null)
                {
                    _deleteItemCommand = new RelayCommand<RoleViewModel>(ExecuteDeleteItemCommand,
                        CanExecuteDeleteItemCommand, CommandType.Delete, "�������", "������� ����");
                }
                return _deleteItemCommand;
            }
        }

        #region AddCommand

        private bool CanAddCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddCommand(object obj)
        {
            var newItem = _roleViewModelFactory.Create(new ShortEntity {ID = Guid.NewGuid(), Name = "�����"},
                AuthorizeObjects);
            newItem.IsAdded = true;
            Items.Add(newItem);
            ActivateItem(newItem);
        }

        #endregion

        #region SaveCommand

        private bool CanExecuteSaveCommand(RoleViewModel obj)
        {
            return obj != null && (obj.IsAdded || obj.IsEdited || obj.IsDeleted);
        }

        private void ExecuteSaveCommand(RoleViewModel accountViewModel)
        {
            try
            {
                Save(accountViewModel);
            }
            catch (ValidationException ex)
            {
                _windowManager.Alert("������", "������ �� ���������");
            }
        }

        private void Save(RoleViewModel roleViewModel)
        {
            if (!roleViewModel.Validate())
            {
                throw new ValidationException("������ ��������", null, roleViewModel);
            }
            if (roleViewModel.IsAdded && !roleViewModel.IsDeleted)
            {
                _aaaService.CreateRole(roleViewModel.Content);
                _eventAggregator.PublishOnCurrentThread(new EntityAdded<Role>(roleViewModel.Content, this));
            }
            else if (roleViewModel.IsDeleted && !roleViewModel.IsAdded)
            {
                _aaaService.DeleteRole(roleViewModel.Content);
                Items.Remove(roleViewModel);
                _roleViewModelFactory.Release(roleViewModel);
                _eventAggregator.PublishOnCurrentThread(new EntityDeleted<Role>(roleViewModel.Content, this));
            }
            else if (roleViewModel.IsDeleted && roleViewModel.IsAdded)
            {
                Items.Remove(roleViewModel);
                _roleViewModelFactory.Release(roleViewModel);
                _eventAggregator.PublishOnCurrentThread(new EntityDeleted<Role>(roleViewModel.Content, this));
            }
            else
            {
                _aaaService.UpdateRole(roleViewModel.Content);
                _eventAggregator.PublishOnCurrentThread(new EntityEdited<Role>(roleViewModel.Content, this));
            }
            roleViewModel.IsAdded = false;
            roleViewModel.IsEdited = false;
            roleViewModel.IsDeleted = false;
        }

        #endregion

        #region SaveAllCommand

        protected virtual bool CanExecuteSaveItemsCommand(RoleViewModel obj)
        {
            return Items != null && Items.Any(p => (p.IsEdited || p.IsDeleted || p.IsAdded));
        }

        protected virtual void ExecuteSaveItemsCommand(RoleViewModel obj)
        {
            var changedItems = Items.Where(p => (p.IsEdited || p.IsDeleted || p.IsAdded)).ToArray();

            try
            {
                foreach (var item in changedItems)
                {
                    Save(item);
                }
            }
            catch (ValidationException ex)
            {
                if (ex.Value is RoleViewModel)
                    ActivateItem((RoleViewModel) ex.Value);
                _windowManager.Alert("������", "������ �� ���������");
            }
        }

        #endregion

        #region DeleteCommand

        protected virtual bool CanExecuteDeleteItemCommand(RoleViewModel obj)
        {
            return obj != null;
        }

        protected virtual void ExecuteDeleteItemCommand(RoleViewModel obj)
        {
            obj.IsDeleted = true;
        }

        #endregion

        #endregion

        #region Private methods

        /// <summary>
        ///     �������������
        /// </summary>
        private void Initialize()
        {
            AuthorizeObjects = new BindableCollection<ShortEntity>(_aaaService.GetAuthorizeObjectsShortInfo());
            RunAsync(() =>
            {
                foreach (var item in _aaaService.GetRolesShorInfo())
                {
                    Items.Add(_roleViewModelFactory.Create(item, AuthorizeObjects));
                }
            });
        }

        /// <summary>
        ///     ������ �������� ����������
        /// </summary>
        /// <param name="action"></param>
        private void RunAsync(Action action)
        {
            Task.Factory.StartNew(action);
        }

        #endregion

        #region Overrides of Screen

        /// <summary>
        ///     Called when initializing.
        /// </summary>
        protected override void OnInitialize()
        {
        }

        /// <summary>
        ///     Called when activating.
        /// </summary>
        protected override void OnActivate()
        {
            base.OnActivate();
            DisplayName = "����";
            Initialize();
        }

        #region Overrides of OneActive

        /// <summary>
        ///     Called when deactivating.
        /// </summary>
        /// <param name="close">Inidicates whether this instance will be closed.</param>
        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
            Items.ForEach(p => _roleViewModelFactory.Release(p));
        }

        #endregion

        public override void CanClose(Action<bool> callback)
        {
            if (Items.Any(p => p.IsAdded || p.IsDeleted || p.IsEdited))
                _windowManager.Confirm("�� �������?",
                    "���� �������� ������������� ������.\n�� �������, ��� ������ ��� �������?",
                    () => { callback(true); },
                    () => { callback(false); });
            else
            {
                callback(true);
            }
        }

        #endregion
    }
}