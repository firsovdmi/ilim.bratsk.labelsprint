using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Caliburn.Micro;
using Castle.Core.Internal;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Log;
using ILIM.Bratsk.Infrastructure.Log.Model;
using ILIM.Bratsk.Visualization.Converters;
using ILIM.Bratsk.Visualization.Extentions;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ViewModels;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels
{
    public class LogViewModel : Screen, IHandle<EntityChanges<Account>>, IAccountDataGiver
    {
        private BindableCollection<SelectableViewModel<AccountShortInfo>> _accounts;
        private RelayCommand<AccountViewModel> _addCommand;
        private RelayCommand _clearLog;
        private RelayCommand<AccountViewModel> _deleteItemCommand;
        private bool _isWatchLog;
        private BindableCollection<LogItem> _items;
        private BindableCollection<SelectableViewModel<LogingLevel>> _levels;
        private LogRequestParameters _logRequestParameters;
        private RelayCommand _refreshCommand;
        private RelayCommand<AccountViewModel> _saveItemCommand;
        private RelayCommand<AccountViewModel> _saveItemsCommand;
        private LogItem _selectedItem;
        private RelayCommand _startWatchLog;
        private RelayCommand _stopWatchLog;
        private readonly IAAAServiceRemoteFasade _aaaService;
        private readonly IAccountDataService _accountDataService;
        private readonly ILogPublisherRemote _logPublisherRemote;
        private readonly LogSubscriber _logSubscriber;
        private readonly IReadLogRemoteService _readLogRemoteService;
        private readonly object _refreshLocker = new object();
        private readonly IWindowManager _windowManager;

        public LogViewModel(
            IWindowManager windowManager,
            IAAAServiceRemoteFasade aaaService,
            IReadLogRemoteService readLogRemoteService,
            IAccountDataService accountDataService,
            ILogPublisherRemote logPublisherRemote,
            LogSubscriber logSubscriber
            )
        {
            _windowManager = windowManager;
            _aaaService = aaaService;
            _readLogRemoteService = readLogRemoteService;
            _accountDataService = accountDataService;
            _logPublisherRemote = logPublisherRemote;
            _logSubscriber = logSubscriber;
        }

        #region Implementation of IHandle<EntityChanges<ActiveAccount>>

        /// <summary>
        ///     Handles the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(EntityChanges<Account> message)
        {
            var oldEntity = Accounts.FirstOrDefault(p => p.Content.ID == message.Content.ID);

            if (message is EntityDeleted<Account> && oldEntity != null)
                Accounts.Remove(oldEntity);
            if (message is EntityAdded<Account> || message is EntityEdited<Account>)
            {
                var newEntity = _aaaService.GetShortAccount(message.Content.ID);
                if (message is EntityAdded<Account>)
                    Accounts.Add(new SelectableViewModel<AccountShortInfo>(newEntity));
                if (message is EntityEdited<Account> && oldEntity != null)
                    oldEntity.Content.Login = newEntity.Login;
            }
        }

        #endregion

        #region PublicProperties

        public IAccountDataService AccountDataService
        {
            get { return _accountDataService; }
        }

        public LogItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (Equals(value, _selectedItem)) return;
                _selectedItem = value;
                NotifyOfPropertyChange("SelectedItem");
            }
        }

        public BindableCollection<SelectableViewModel<AccountShortInfo>> Accounts
        {
            get { return _accounts; }
            set
            {
                if (Equals(value, _accounts)) return;
                _accounts = value;
                NotifyOfPropertyChange("Accounts");
            }
        }

        public BindableCollection<SelectableViewModel<LogingLevel>> Levels
        {
            get { return _levels; }
            set
            {
                if (Equals(value, _levels)) return;
                _levels = value;
                NotifyOfPropertyChange("Levels");
            }
        }

        public string SelectedAccounts
        {
            get
            {
                if (Accounts != null && Accounts.Any(p => p.IsSelected))
                    return String.Join(", ", Accounts.Where(p => p.IsSelected).Select(p => p.Content.Login));
                return "";
            }
        }

        public string SelectedLevels
        {
            get
            {
                if (Levels != null && Levels.Any(p => p.IsSelected))
                {
                    var converter = new EnumDescriptionToStringConverter();
                    return String.Join(", ",
                        Levels.Where(p => p.IsSelected)
                            .Select(p => converter.Convert(p.Content, typeof (string), null, CultureInfo.CurrentCulture)));
                }
                return "";
            }
        }

        public LogRequestParameters LogRequestParameters
        {
            get { return _logRequestParameters; }
            set
            {
                if (Equals(value, _logRequestParameters)) return;
                _logRequestParameters = value;
                NotifyOfPropertyChange("LogRequestParameters");
            }
        }

        public BindableCollection<LogItem> Items
        {
            get { return _items; }
            set
            {
                if (Equals(value, _items)) return;
                _items = value;
                NotifyOfPropertyChange("Items");
            }
        }

        public bool IsWatchLog
        {
            get { return _isWatchLog; }
            set
            {
                if (value.Equals(_isWatchLog)) return;
                _isWatchLog = value;
                NotifyOfPropertyChange("IsWatchLog");
            }
        }

        #endregion

        #region Commands

        public RelayCommand ClearLog
        {
            get
            {
                if (_clearLog == null)
                {
                    _clearLog = new RelayCommand(ExecuteClearLog, CanExecuteClearLog, CommandType.Clear, "�������� ���",
                        "�������� ���");
                }
                return _clearLog;
            }
        }

        public RelayCommand StopWatchLog
        {
            get
            {
                if (_stopWatchLog == null)
                {
                    _stopWatchLog = new RelayCommand(ExecuteStopWatchLog, CanExecuteStopWatchLog, CommandType.Stop,
                        "����������", "�������� ��������� ���� � �������� �������");
                }
                return _stopWatchLog;
            }
        }

        public RelayCommand StartWatchLog
        {
            get
            {
                if (_startWatchLog == null)
                {
                    _startWatchLog = new RelayCommand(ExecuteStartWatchLog, CanExecuteStartWatchLog, CommandType.Start,
                        "�����", "������ ��������� ���� � �������� �������");
                }
                return _startWatchLog;
            }
        }

        public RelayCommand RefreshCommand
        {
            get
            {
                if (_refreshCommand == null)
                {
                    _refreshCommand = new RelayCommand(ExecuteRefreshCommand, CanExecuteRefreshCommand,
                        CommandType.Refresh, "��������", "��������");
                }
                return _refreshCommand;
            }
        }

        #region RefreshCommand

        private bool CanExecuteRefreshCommand(object obj)
        {
            return !IsWatchLog;
        }

        private void ExecuteRefreshCommand(object obj)
        {
            if (!Monitor.TryEnter(_refreshLocker))
                return;
            try
            {
                if (Items == null || !Items.Any())
                    Items =
                        new BindableCollection<LogItem>(
                            _readLogRemoteService.GetLog(LogRequestParameters).OrderByDescending(p => p.Time));
                else
                {
                    var newItemsBatch =
                        _readLogRemoteService.GetLog(LogRequestParameters).OrderByDescending(p => p.Time);

                    var intersect = Items.Select(p => p.ID).Intersect(newItemsBatch.Select(p => p.ID));
                    var itemsToRemove = Items.Where(p => !intersect.Contains(p.ID)).ToList();
                    Items.RemoveRange(itemsToRemove);
                    Items.AddRange(newItemsBatch.Where(p => !intersect.Contains(p.ID)));
                    if (LogRequestParameters.IsUseTop && Items.Count > LogRequestParameters.TopCount)
                        Items.RemoveRange(Items.OrderByDescending(p => p.Time).Skip(LogRequestParameters.TopCount));
                }
            }
            finally
            {
                Monitor.Exit(_refreshLocker);
            }
        }

        #endregion

        #region StartWatchLog

        private bool CanExecuteStartWatchLog(object obj)
        {
            return !IsWatchLog;
        }

        private void ExecuteStartWatchLog(object obj)
        {
            try
            {
                _logPublisherRemote.Subscribe();
                _logSubscriber.Published += _logSubscriber_Published;
                IsWatchLog = true;
            }
            catch (Exception)
            {
                _logPublisherRemote.UnSubscribe();
                IsWatchLog = false;
            }
        }

        #endregion

        #region ClearLog

        private bool CanExecuteClearLog(object obj)
        {
            return true;
        }

        private void ExecuteClearLog(object obj)
        {
            Items.Clear();
        }

        #endregion

        #region StopWatchLog

        private bool CanExecuteStopWatchLog(object obj)
        {
            return IsWatchLog;
        }

        private void ExecuteStopWatchLog(object obj)
        {
            try
            {
                _logPublisherRemote.UnSubscribe();
                _logSubscriber.Published -= _logSubscriber_Published;
                IsWatchLog = false;
            }
            finally
            {
            }
        }

        #endregion

        #endregion

        #region Private methods

        /// <summary>
        ///     �������������
        /// </summary>
        private void Initialize()
        {
            DisplayName = "������";


            var accountData = _accountDataService.GetCurrentUserData(GetType().FullName);
            if (accountData == null)
            {
                LogRequestParameters = new LogRequestParameters();
            }
            else
            {
                try
                {
                    using (TextReader reader = new StringReader(accountData.Data))
                    {
                        var serializer = new XmlSerializer(typeof (LogRequestParameters));
                        LogRequestParameters = (LogRequestParameters) serializer.Deserialize(reader);
                    }
                }
                catch (Exception)
                {
                    LogRequestParameters = new LogRequestParameters();
                }
            }
            Accounts =
                new BindableCollection<SelectableViewModel<AccountShortInfo>>(
                    _aaaService.GetAllAccountsShortInfo().Select(p => new SelectableViewModel<AccountShortInfo>(p)));
            Accounts.ForEach(p =>
            {
                p.IsSelected =
                    (LogRequestParameters != null && LogRequestParameters.AccountIds != null &&
                     LogRequestParameters.AccountIds.Contains(p.Content.ID));
                p.PropertyChanged += AccountsPropertyChanges;
            });

            Levels =
                new BindableCollection<SelectableViewModel<LogingLevel>>(
                    Enum.GetValues(typeof (LogingLevel))
                        .OfType<LogingLevel>()
                        .Select(p => new SelectableViewModel<LogingLevel>(p)));
            Levels.ForEach(p =>
            {
                p.IsSelected =
                    (LogRequestParameters != null && LogRequestParameters.LoginLevels != null &&
                     LogRequestParameters.LoginLevels.Contains(p.Content));
                p.PropertyChanged += LevelsPropertyChanges;
            });
            Items = new BindableCollection<LogItem>();
        }

        private void LevelsPropertyChanges(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                NotifyOfPropertyChange(() => SelectedLevels);
                LogRequestParameters.LoginLevels = Levels.Where(p => p.IsSelected).Select(p => p.Content).ToList();
            }
        }

        private void AccountsPropertyChanges(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                NotifyOfPropertyChange(() => SelectedAccounts);
                LogRequestParameters.AccountIds =
                    Accounts.Where(p => p.IsSelected).Select(p => (Guid?) p.Content.ID).ToList();
            }
        }

        /// <summary>
        ///     ������ �������� ����������
        /// </summary>
        /// <param name="action"></param>
        private void RunAsync(Action action)
        {
            Task.Factory.StartNew(action);
        }

        #endregion

        #region Overrides of Screen

        /// <summary>
        ///     Called when activating.
        /// </summary>
        protected override void OnActivate()
        {
            base.OnActivate();

            Initialize();
        }

        private void _logSubscriber_Published(object sender, PublishedEventArgs e)
        {
            var query = e.Published.OrderByDescending(p => p.Time).AsQueryable();
            if (!string.IsNullOrEmpty(LogRequestParameters.Type))
                query = query.Where(p => p.Type.Contains(LogRequestParameters.Type));
            if (!string.IsNullOrEmpty(LogRequestParameters.Par1))
                query = query.Where(p => p.Par1.Contains(LogRequestParameters.Par1));
            if (!string.IsNullOrEmpty(LogRequestParameters.Par2))
                query = query.Where(p => p.Par2.Contains(LogRequestParameters.Par2));
            if (!string.IsNullOrEmpty(LogRequestParameters.Par3))
                query = query.Where(p => p.Par3.Contains(LogRequestParameters.Par3));
            if (!string.IsNullOrEmpty(LogRequestParameters.Par1))
                query = query.Where(p => p.Par1.Contains(LogRequestParameters.Par1));
            if (!string.IsNullOrEmpty(LogRequestParameters.LogSource))
                query = query.Where(p => p.Source.Contains(LogRequestParameters.LogSource));

            if (!LogRequestParameters.IsGetAllLoginLevels)
                query = query.Where(p => LogRequestParameters.LoginLevels.Contains(p.LogLevel));
            if (!LogRequestParameters.IsGetAllAccounts)
                query = query.Where(p => LogRequestParameters.AccountIds.Contains(p.AccountID));

            if (LogRequestParameters.TopCount > 0)
                foreach (var logItem in query)
                {
                    if (LogRequestParameters.IsUseTop && Items.Count() >= LogRequestParameters.TopCount)
                        Items.RemoveRange(Items.Skip(Items.Count() - 1));
                    Items.Insert(0, logItem);
                }
        }

        #region Overrides of OneActive

        /// <summary>
        ///     Called when deactivating.
        /// </summary>
        /// <param name="close">Inidicates whether this instance will be closed.</param>
        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
            try
            {
                _logPublisherRemote.UnSubscribe();
            }
            finally
            {
                _logSubscriber.Published -= _logSubscriber_Published;
            }
            RunAsync(() =>
            {
                try
                {
                    var accountData = new AccountData {DataID = GetType().FullName};
                    using (var stringWriter = new StringWriter())
                    {
                        using (var textWriter = XmlWriter.Create(stringWriter))
                        {
                            var serializer = new XmlSerializer(typeof (LogRequestParameters));
                            serializer.Serialize(textWriter, LogRequestParameters);
                        }
                        accountData.Data = stringWriter.ToString();
                    }

                    _accountDataService.UpdateOrCreateCurrentUserDataByDataID(accountData);
                }
                catch (Exception)
                {
                }
            });
        }

        #endregion

        #endregion
    }
}