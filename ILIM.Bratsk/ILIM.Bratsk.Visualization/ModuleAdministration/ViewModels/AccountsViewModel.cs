using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;
using Castle.Core.Internal;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.Views;
using Action = System.Action;

namespace ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels
{
    [View(typeof (ToolBarView), "ToolBar")]
    public class AccountsViewModel : Conductor<AccountViewModel>.Collection.OneActive, IHandle<EntityChanges<Role>>
    {
        private RelayCommand<AccountViewModel> _addCommand;
        private RelayCommand<AccountViewModel> _deleteItemCommand;
        private RelayCommand<AccountViewModel> _refreshCommand;
        private BindableCollection<ShortEntity> _roles;
        private RelayCommand<AccountViewModel> _saveItemCommand;
        private RelayCommand<AccountViewModel> _saveItemsCommand;
        private readonly IAAAServiceRemoteFasade _aaaService;
        private readonly IAccountViewModelFactory _accountViewModelFactory;
        private readonly IWindowManager _windowManager;

        public AccountsViewModel(
            IWindowManager windowManager,
            IAAAServiceRemoteFasade aaaService,
            IAccountViewModelFactory accountViewModelFactory
            )
        {
            _windowManager = windowManager;
            _aaaService = aaaService;
            _accountViewModelFactory = accountViewModelFactory;
        }

        #region PublicProperties

        public BindableCollection<ShortEntity> Roles
        {
            get { return _roles; }
            set
            {
                if (Equals(value, _roles)) return;
                _roles = value;
                NotifyOfPropertyChange("Roles");
            }
        }

        #region Dictionary cache

        #endregion

        #endregion

        #region Implementation of IHandle<EntityChanges<Role>>

        /// <summary>
        ///     Handles the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(EntityChanges<Role> message)
        {
            var old = Roles.FirstOrDefault(p => p.ID == message.Content.ID);

            if (message is EntityDeleted<Role> && old != null)
                Roles.Remove(old);
            if (message is EntityAdded<Role> || message is EntityEdited<Role>)
            {
                var newDict = _aaaService.GetShortRole(message.Content.ID);
                if (message is EntityAdded<Role>)
                    Roles.Add(newDict);
                if (message is EntityEdited<Role> && old != null)
                    old.Name = newDict.Name;
            }
        }

        #endregion

        #region Commands

        public RelayCommand<AccountViewModel> AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand<AccountViewModel>(ExecuteAddCommand, CanAddCommand,
                        CommandType.AddAccount, "��������", "�������� ������� ������");
                }
                return _addCommand;
            }
        }

        public RelayCommand<AccountViewModel> SaveCommand
        {
            get
            {
                if (_saveItemCommand == null)
                {
                    _saveItemCommand = new RelayCommand<AccountViewModel>(ExecuteSaveCommand, CanExecuteSaveCommand,
                        CommandType.Save, "���������", "��������� � ���� ������");
                }
                return _saveItemCommand;
            }
        }

        public RelayCommand<AccountViewModel> SaveAllCommand
        {
            get
            {
                if (_saveItemsCommand == null)
                {
                    _saveItemsCommand = new RelayCommand<AccountViewModel>(ExecuteSaveItemsCommand,
                        CanExecuteSaveItemsCommand, CommandType.SaveAll, "��������� ���", "��������� ��� � ���� ������");
                }
                return _saveItemsCommand;
            }
        }

        public RelayCommand<AccountViewModel> DeleteCommand
        {
            get
            {
                if (_deleteItemCommand == null)
                {
                    _deleteItemCommand = new RelayCommand<AccountViewModel>(ExecuteDeleteItemCommand,
                        CanExecuteDeleteItemCommand, CommandType.Delete, "�������", "������� ������� ������");
                }
                return _deleteItemCommand;
            }
        }

        public RelayCommand<AccountViewModel> RefreshCommand
        {
            get
            {
                if (_refreshCommand == null)
                {
                    _refreshCommand = new RelayCommand<AccountViewModel>(ExecuteRefreshCommand, CanExecuteRefreshCommand,
                        CommandType.Refresh, "��������", "�������� ������");
                }
                return _refreshCommand;
            }
        }

        #region AddCommand

        private bool CanAddCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddCommand(object obj)
        {
            var newItem = _accountViewModelFactory.Create(new AccountShortInfo {ID = Guid.NewGuid(), Login = "�����"},
                Roles);
            newItem.IsAdded = true;
            Items.Add(newItem);
            ActivateItem(newItem);
        }

        #endregion

        #region SaveCommand

        private bool CanExecuteSaveCommand(AccountViewModel obj)
        {
            return obj != null && (obj.IsAdded || obj.IsEdited || obj.IsDeleted);
        }

        private void ExecuteSaveCommand(AccountViewModel accountViewModel)
        {
            try
            {
                Save(accountViewModel);
            }
            catch (ValidationException ex)
            {
                _windowManager.Alert("������", ex.Message);
            }
        }

        private void Save(AccountViewModel accountViewModel)
        {
            Account savedAccaunt = null;
            if (!accountViewModel.Validate())
            {
                throw new ValidationException("������ �������� ������������ ������", null, accountViewModel);
            }
            if (accountViewModel.IsAdded && !accountViewModel.IsDeleted)
            {
                if (accountViewModel.IsChangePassword)
                {
                    savedAccaunt = _aaaService.CreateAccount(accountViewModel.Content, accountViewModel.Password);
                }
                else
                {
                    savedAccaunt = _aaaService.CreateAccount(accountViewModel.Content, null);
                }
            }
            else if (accountViewModel.IsDeleted && !accountViewModel.IsAdded)
            {
                _aaaService.DeleteAccount(accountViewModel.Content);
                Items.Remove(accountViewModel);
                _accountViewModelFactory.Release(accountViewModel);
            }
            else if (accountViewModel.IsDeleted && accountViewModel.IsAdded)
            {
                Items.Remove(accountViewModel);
                _accountViewModelFactory.Release(accountViewModel);
            }
            else
            {
                if (accountViewModel.IsChangePassword)
                {
                    savedAccaunt = _aaaService.UpdateAccountAndPassword(accountViewModel.Content,
                        accountViewModel.Password);
                }
                else
                {
                    savedAccaunt = _aaaService.UpdateAccount(accountViewModel.Content);
                }
            }
            if (savedAccaunt != null)
            {
                accountViewModel.Content.RowVersion = savedAccaunt.RowVersion;
                accountViewModel.Content.ID = savedAccaunt.ID;
            }
            accountViewModel.IsChangePassword = false;
            accountViewModel.IsAdded = false;
            accountViewModel.IsEdited = false;
            accountViewModel.IsDeleted = false;
        }

        #endregion

        #region SaveAllCommand

        protected virtual bool CanExecuteSaveItemsCommand(AccountViewModel obj)
        {
            return Items != null && Items.Any(p => (p.IsEdited || p.IsDeleted || p.IsAdded));
        }

        protected virtual void ExecuteSaveItemsCommand(AccountViewModel obj)
        {
            var changedItems = Items.Where(p => (p.IsEdited || p.IsDeleted || p.IsAdded)).ToArray();

            try
            {
                foreach (var item in changedItems)
                {
                    Save(item);
                }
            }
            catch (ValidationException ex)
            {
                if (ex.Value is AccountViewModel)
                    ActivateItem((AccountViewModel) ex.Value);
                _windowManager.Alert("������", "������ �� ���������");
            }
        }

        #endregion

        #region RefreshCommand

        protected virtual bool CanExecuteRefreshCommand(AccountViewModel obj)
        {
            return ActiveItem != null;
        }

        protected virtual void ExecuteRefreshCommand(AccountViewModel obj)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region DeleteCommand

        protected virtual bool CanExecuteDeleteItemCommand(AccountViewModel obj)
        {
            return obj != null;
        }

        protected virtual void ExecuteDeleteItemCommand(AccountViewModel obj)
        {
            obj.IsDeleted = true;
        }

        #endregion

        #endregion

        #region Private methods

        /// <summary>
        ///     �������������
        /// </summary>
        private void Initialize()
        {
            Roles = new BindableCollection<ShortEntity>(_aaaService.GetRolesShorInfo());
            RunAsync(() =>
            {
                foreach (var item in _aaaService.GetAllAccountsShortInfo())
                {
                    Items.Add(_accountViewModelFactory.Create(item, Roles));
                }
            });
        }

        /// <summary>
        ///     ������ �������� ����������
        /// </summary>
        /// <param name="action"></param>
        private void RunAsync(Action action)
        {
            Task.Factory.StartNew(action);
        }

        #endregion

        #region Overrides of Screen

        /// <summary>
        ///     Called when initializing.
        /// </summary>
        protected override void OnInitialize()
        {
        }

        /// <summary>
        ///     Called when activating.
        /// </summary>
        protected override void OnActivate()
        {
            base.OnActivate();
            DisplayName = "������� ������";
            Initialize();
        }

        #region Overrides of OneActive

        /// <summary>
        ///     Called when deactivating.
        /// </summary>
        /// <param name="close">Inidicates whether this instance will be closed.</param>
        protected override void OnDeactivate(bool close)
        {
            base.OnDeactivate(close);
            Items.ForEach(p => _accountViewModelFactory.Release(p));
        }

        #endregion

        public override void CanClose(Action<bool> callback)
        {
            if (Items.Any(p => p.IsAdded || p.IsDeleted || p.IsEdited))
                _windowManager.Confirm("�� �������?",
                    "���� �������� ������������� ������.\n�� �������, ��� ������ ��� �������?",
                    () => { callback(true); },
                    () => { callback(false); });
            else
            {
                callback(true);
            }
        }

        #endregion
    }
}