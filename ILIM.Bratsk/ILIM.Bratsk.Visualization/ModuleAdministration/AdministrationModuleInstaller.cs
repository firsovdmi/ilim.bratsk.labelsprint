using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.MenuAction;
using ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleAdministration
{
    public class AdministrationModuleInstaller : IWindsorInstaller
    {
        #region Implementation of IWindsorInstaller

        /// <summary>
        ///     Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IModule>().ImplementedBy<AdministrationModule>());
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof (AdministrationMenuAction<>))
                    .If(p => p != typeof (AdministrationMenuAction<>))
                    .WithServices(typeof (AdministrationMenuAction)));

            container.Register(
                Component.For<RolesViewModel>()
                    .ImplementedBy<RolesViewModel>()
                    .LifestyleCustom<RealisableSingletoneLifestyleManager>());
            container.Register(Component.For<RoleViewModel>().ImplementedBy<RoleViewModel>().LifestyleTransient());

            container.Register(
                Component.For<AccountsViewModel>()
                    .ImplementedBy<AccountsViewModel>()
                    .LifestyleCustom<RealisableSingletoneLifestyleManager>());
            container.Register(Component.For<AccountViewModel>().ImplementedBy<AccountViewModel>().LifestyleTransient());

            container.Register(
                Component.For<LogViewModel>()
                    .ImplementedBy<LogViewModel>()
                    .LifestyleCustom<RealisableSingletoneLifestyleManager>());
            container.Register(
                Component.For<EventsViewModel>()
                         .ImplementedBy<EventsViewModel>());

            container.Register(Component.For<IAccountViewModelFactory>().AsFactory().LifestyleTransient());
            container.Register(Component.For<IRoleViewModelFactory>().AsFactory().LifestyleTransient());
        }

        #endregion
    }
}