using System.Collections.Generic;
using AutoMapper;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Infrastructure.AAA.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.ModuleAdministration.MenuAction;
using ILIM.Bratsk.Visualization.ModuleAdministration.ViewModels;

namespace ILIM.Bratsk.Visualization.ModuleAdministration
{
    public class AdministrationModule : IModule
    {
        public AdministrationModule(IMenuManager menuManager,
            IToolBarManager toolBarManager,
            IEnumerable<AdministrationMenuAction> actionItems,
            AdministrationMenuAction administrationActionItems
            )
        {
            menuManager.ShowItem(new ActionItem("�����������������", null),3);
            foreach (var actionItem in actionItems)
                menuManager.WithParent("�����������������")
                    .ShowItem(actionItem);

            Mapper.CreateMap<Account, AccountViewModel>();
            Mapper.CreateMap<AccountViewModel, Account>();
        }
    }
}