﻿using System;
using System.Globalization;
using System.Windows;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class BoolTrueToCollapseConverter : ConvertorBase<BoolTrueToCollapseConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool) value) return Visibility.Collapsed;
            return Visibility.Visible;
        }
    }
}