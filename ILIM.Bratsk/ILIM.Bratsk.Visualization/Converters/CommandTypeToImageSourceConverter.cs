using System;
using System.Globalization;
using System.Windows.Data;
using ILIM.Bratsk.Visualization.Behaviour;
using ILIM.Bratsk.Visualization.Infrastructure;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class CommandTypeToImageSourceConverter : IValueConverter
    {
        #region Implementation of IMultiValueConverter

        /// <summary>
        ///     Converts source values to a value for the binding target. The data binding engine calls this method when it
        ///     propagates the values from source bindings to the binding target.
        /// </summary>
        /// <returns>
        ///     A converted value.If the method returns null, the valid null value is used.A return value of
        ///     <see cref="T:System.Windows.DependencyProperty" />.<see cref="F:System.Windows.DependencyProperty.UnsetValue" />
        ///     indicates that the converter did not produce a value, and that the binding will use the
        ///     <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> if it is available, or else will use the default
        ///     value.A return value of <see cref="T:System.Windows.Data.Binding" />.
        ///     <see cref="F:System.Windows.Data.Binding.DoNothing" /> indicates that the binding does not transfer the value or
        ///     use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> or the default value.
        /// </returns>
        /// <param name="values">
        ///     The array of values that the source bindings in the
        ///     <see cref="T:System.Windows.Data.MultiBinding" /> produces. The value
        ///     <see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the source binding has no value to
        ///     provide for conversion.
        /// </param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        public object Convert(object values, Type targetType, object parameter, CultureInfo culture)
        {
            var type = values is CommandType ? (CommandType) values : CommandType.None;
            switch (type)
            {
                case CommandType.Add:
                    return ImageSources.AddDict;
                case CommandType.Act:
                    return ImageSources.Act;
                case CommandType.Save:
                    return ImageSources.SaveImage;
                case CommandType.SaveAll:
                    return ImageSources.SaveAllImage;
                case CommandType.AddTTN:
                    return ImageSources.AddTTN;
                case CommandType.Delete:
                    return ImageSources.DeleteImage;
                case CommandType.Upload:
                    return ImageSources.Download;
                case CommandType.AddSubTask:
                    return ImageSources.NewSection;
                case CommandType.Connect:
                    return ImageSources.Connect;
                case CommandType.Diconect:
                    return ImageSources.Disconnect;
                case CommandType.Up:
                    return ImageSources.Up;
                case CommandType.Down:
                    return ImageSources.Down;
                case CommandType.Refresh:
                    return ImageSources.Refresh;
                case CommandType.Edit:
                    return ImageSources.Edit;
                case CommandType.ExportPdf:
                    return ImageSources.ExportPdf;
                case CommandType.ExportXls:
                    return ImageSources.ExportXls;
                case CommandType.ExportWord:
                    return ImageSources.ExportWord;
                case CommandType.Print:
                    return ImageSources.Print;
                case CommandType.MoveLeft:
                    return ImageSources.Left;
                case CommandType.MoveRight:
                    return ImageSources.Right;
                case CommandType.AddAccount:
                    return ImageSources.AddAccount;
                case CommandType.Roles:
                    return ImageSources.Roles;
                case CommandType.RoleAdd:
                    return ImageSources.RoleAdd;
                case CommandType.BatchEdit:
                    return ImageSources.BatchEdit;
                case CommandType.BatchAdd:
                    return ImageSources.BatchAdd;
                case CommandType.Copy:
                    return ImageSources.Copy;
                case CommandType.Start:
                    return ImageSources.Start;
                case CommandType.Stop:
                    return ImageSources.Stop;
                case CommandType.Clear:
                    return ImageSources.Clear;
                case CommandType.Divided:
                    return ImageSources.Divided;
            }

            return null;
        }

        /// <summary>
        ///     Converts a binding target value to the source binding values.
        /// </summary>
        /// <returns>
        ///     An array of values that have been converted from the target value back to the source values.
        /// </returns>
        /// <param name="value">The value that the binding target produces.</param>
        /// <param name="targetTypes">
        ///     The array of types to convert to. The array length indicates the number and types of values
        ///     that are suggested for the method to return.
        /// </param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}