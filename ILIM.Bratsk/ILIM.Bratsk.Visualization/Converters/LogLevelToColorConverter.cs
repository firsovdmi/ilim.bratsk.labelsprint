using System;
using System.Globalization;
using System.Windows.Media;
using ILIM.Bratsk.Infrastructure.Log;

namespace ILIM.Bratsk.Visualization.Converters
{
    internal class LogLevelToColorConverter : ConvertorBase<LogLevelToColorConverter>
    {
        #region ����� IValueConverter

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var logLevel = value is LogingLevel ? (LogingLevel) value : LogingLevel.Trace;
            switch (logLevel)
            {
                case LogingLevel.Error:
                case LogingLevel.Fatal:
                    return new SolidColorBrush(Colors.LightCoral);
                case LogingLevel.Warn:
                    return new SolidColorBrush(Colors.Yellow);
            }
            return new SolidColorBrush(Colors.Transparent);
        }

        #endregion
    }
}