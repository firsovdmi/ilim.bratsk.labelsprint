using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class NullToVisibleConverter : IValueConverter
    {
        public Visibility EqualNullVisibility { get; set; }
        public Visibility NotEqualNullVisibility { get; set; }

        #region ����� IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return EqualNullVisibility;
            return NotEqualNullVisibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region MarkupExtension members

        #endregion
    }
}