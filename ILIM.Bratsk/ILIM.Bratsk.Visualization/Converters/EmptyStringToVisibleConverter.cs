﻿using System;
using System.Globalization;
using System.Windows;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class EmptyStringToVisibleConverter : ConvertorBase<EmptyStringToVisibleConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var stringValue = (string) value;
            return String.IsNullOrEmpty(stringValue) || stringValue=="0" ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}