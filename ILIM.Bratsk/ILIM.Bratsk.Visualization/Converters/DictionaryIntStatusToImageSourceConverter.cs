using System;
using System.Globalization;
using System.Windows.Data;
using ILIM.Bratsk.Visualization.Behaviour;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class DictionaryStatusIntToImageSourceConverter : IValueConverter
    {
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((int) value == 1) return ImageSources.DictionaryStatusDeleted;
            if ((int) value == 2) return ImageSources.DictionaryStatusEdited;
            return null;
        }
    }
}