﻿using System;
using System.Globalization;

namespace ILIM.Bratsk.Visualization.Converters
{
    internal class BoolInvertConverter : ConvertorBase<BoolInvertConverter>
    {
        #region Члены IValueConverter

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var b = value is bool && (bool) value;
            return !b;
        }

        #endregion
    }
}