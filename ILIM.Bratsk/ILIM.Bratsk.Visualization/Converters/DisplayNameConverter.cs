﻿using System;
using System.Globalization;
using System.Linq;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class DisplayNameConverter : MultiConvertorBase<DisplayNameConverter>
    {
        #region Overrides of MultiConvertorBase<SelectedItemsToStringConverter>

        /// <summary>
        ///     Must be implemented in inheritor.
        /// </summary>
        public override object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value.Count() > 3)
            {
                var isAdded = value[0] is bool && (bool) value[0];
                var isDeleted = value[1] is bool && (bool) value[0];
                var isEdited = value[2] is bool && (bool) value[0];
                var displayName = value[3] as string;
                if ((isAdded || isEdited || isDeleted) && displayName != null)
                {
                    return displayName + "*";
                }
                if (displayName != null)
                {
                    return displayName;
                }
            }
            return "";
        }

        #endregion
    }
}