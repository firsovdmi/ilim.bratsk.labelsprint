using System;
using System.Globalization;

namespace ILIM.Bratsk.Visualization.Converters
{
    internal class SingleLineConverter : ConvertorBase<SingleLineConverter>
    {
        #region ����� IValueConverter

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = (string) value;
            if (str == null)
                return str;
            var pos = str.IndexOf('\r');
            return (pos > 0) ? str.Substring(0, pos) : str;
        }

        #endregion
    }
}