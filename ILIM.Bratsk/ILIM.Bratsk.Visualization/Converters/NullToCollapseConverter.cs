﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class NullToCollapseConverter : ConvertorBase<NullToCollapseConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ( value==null) return Visibility.Collapsed;
            return Visibility.Visible;
        }

    }
}