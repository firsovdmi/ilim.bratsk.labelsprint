﻿using System;
using System.Windows;
using System.Windows.Data;

namespace PAG.WBD.Milk.Visualization.Converters
{
    class BoolToVisibleConverter:IValueConverter
    {
        #region Члены IValueConverter

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool) value ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
