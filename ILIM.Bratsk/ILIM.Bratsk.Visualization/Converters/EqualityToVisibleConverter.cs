using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class EqualityToVisibleConverter<T> : IValueConverter where T : struct
    {
        public T Value { get; set; }
        public Visibility EqualVisibility { get; set; }
        public Visibility NotEqualVisibility { get; set; }

        #region ����� IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is T)
                return Value.Equals(value) ? EqualVisibility : NotEqualVisibility;
            return NotEqualVisibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region MarkupExtension members

        #endregion
    }
}