﻿using System;
using System.Globalization;
using System.Windows;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class BoolTrueToShowConverter : ConvertorBase<BoolTrueToShowConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool) value) return Visibility.Visible;
            return Visibility.Collapsed;
        }
    }
}