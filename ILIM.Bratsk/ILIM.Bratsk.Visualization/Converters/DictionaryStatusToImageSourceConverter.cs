using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using System.Windows.Media;
using ILIM.Bratsk.Visualization.Behaviour;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class DictionaryStatusToImageSourceConverter : MultiConvertorBase<DictionaryStatusToImageSourceConverter>
    {
        #region Implementation of IMultiValueConverter

        /// <summary>
        ///     Converts source values to a value for the binding target. The data binding engine calls this method when it
        ///     propagates the values from source bindings to the binding target.
        /// </summary>
        /// <returns>
        ///     A converted value.If the method returns null, the valid null value is used.A return value of
        ///     <see cref="T:System.Windows.DependencyProperty" />.<see cref="F:System.Windows.DependencyProperty.UnsetValue" />
        ///     indicates that the converter did not produce a value, and that the binding will use the
        ///     <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> if it is available, or else will use the default
        ///     value.A return value of <see cref="T:System.Windows.Data.Binding" />.
        ///     <see cref="F:System.Windows.Data.Binding.DoNothing" /> indicates that the binding does not transfer the value or
        ///     use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> or the default value.
        /// </returns>
        /// <param name="values">
        ///     The array of values that the source bindings in the
        ///     <see cref="T:System.Windows.Data.MultiBinding" /> produces. The value
        ///     <see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the source binding has no value to
        ///     provide for conversion.
        /// </param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values != null && values.Count() > 2 && values[0] is bool && values[1] is bool && values[2] is bool)
            {
                var isDeleted = (bool) values[0];
                var isAdded = (bool) values[1];
                var isEdited = (bool) values[2];
                if (isDeleted)
                    return ImageSources.DictionaryStatusDeleted;
                if (isEdited || isAdded)
                    return ImageSources.DictionaryStatusEdited;
            }

            return null;
        }

        #endregion
    }

    public class IsLoadedToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var boolValue = (bool?) value;
            if (boolValue == true) return ImageSources.Download;
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class IsCanLoadToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var boolValue = (bool?) value;
            if (boolValue == true) return Brushes.Green;
            return Brushes.Red;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}