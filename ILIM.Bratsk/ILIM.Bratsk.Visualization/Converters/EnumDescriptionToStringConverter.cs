using System;
using System.ComponentModel;
using System.Globalization;

namespace ILIM.Bratsk.Visualization.Converters
{
    //[ValueConversion(typeof(object), typeof(String))]
    internal class EnumDescriptionToStringConverter : ConvertorBase<EnumDescriptionToStringConverter>
    {
        #region ����� IValueConverter

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // To get around the stupid WPF designer bug
            if (value != null)
            {
                var fi = value.GetType().GetField(value.ToString());

                // To get around the stupid WPF designer bug
                if (fi != null)
                {
                    var attributes =
                        (DescriptionAttribute[]) fi.GetCustomAttributes(typeof (DescriptionAttribute), false);
                    return ((attributes.Length > 0) &&
                            (!String.IsNullOrEmpty(attributes[0].Description)))
                        ? attributes[0].Description
                        : value.ToString();
                }
            }

            return string.Empty;
        }

        #endregion
    }
}