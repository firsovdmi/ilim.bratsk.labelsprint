using System;
using System.ComponentModel;
using System.Globalization;

namespace ILIM.Bratsk.Visualization.Converters
{
    internal class EnumToStringConverter : ConvertorBase<EnumToStringConverter>
    {
        #region ����� IValueConverter

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // To get around the stupid WPF designer bug
            if (value != null)
            {
                var fi = value.GetType().GetField(value.ToString());

                // To get around the stupid WPF designer bug
                if (fi != null)
                {
                    var attributes =
                        (DescriptionAttribute[]) fi.GetCustomAttributes(typeof (DescriptionAttribute), false);
                    return ((attributes.Length > 0) &&
                            (!String.IsNullOrEmpty(attributes[0].Description)))
                        ? attributes[0].Description
                        : value.ToString();
                }
            }

            return string.Empty;
        }


        /// <summary>
        ///     Override if needed.
        /// </summary>
        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var stringVal = value as string;
            if (stringVal != null)
            {
                foreach (var o in Enum.GetValues(targetType))
                {
                    var fi = o.GetType().GetField(o.ToString());
                    if (fi != null)
                    {
                        var attributes =
                            (DescriptionAttribute[]) fi.GetCustomAttributes(typeof (DescriptionAttribute), false);
                        if (attributes.Length > 0)
                            foreach (var descriptionAttribute in attributes)
                            {
                                if (descriptionAttribute.Description == stringVal)
                                    return o;
                            }
                        else
                        {
                            if (o.ToString() == stringVal)
                                return o;
                        }
                    }
                }
            }

            return null;
        }

        #endregion
    }
}