﻿using System;
using System.Globalization;
using System.Linq;

namespace ILIM.Bratsk.Visualization.Converters
{
    public class SelectedItemsToStringConverter : MultiConvertorBase<SelectedItemsToStringConverter>
    {
        #region Overrides of MultiConvertorBase<SelectedItemsToStringConverter>

        /// <summary>
        ///     Must be implemented in inheritor.
        /// </summary>
        public override object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value.Count() > 1)
            {
                var selectedAll = value[0] is bool && (bool) value[0];
                if (!selectedAll)
                {
                    var selectedValues = value[1] as string;
                    if (selectedValues != null)
                        return selectedValues;
                }
                else
                {
                    return "Все";
                }
            }
            return "";
        }

        #endregion
    }
}