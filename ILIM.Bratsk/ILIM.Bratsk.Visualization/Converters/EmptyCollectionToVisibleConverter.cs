﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows;

namespace ILIM.Bratsk.Visualization.Converters
{
    internal class EmptyCollectionToVisibleConverter : ConvertorBase<EmptyCollectionToVisibleConverter>
    {
        #region Члены IValueConverter

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var ienum = value as ICollection;

            if (value != null && ienum.Count > 0)
                return Visibility.Visible;
            return Visibility.Collapsed;
        }

        #endregion
    }
}