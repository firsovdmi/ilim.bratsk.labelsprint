using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ILIM.Bratsk.Visualization.Behaviour
{
    public static class ImageSources
    {
        private const String Path = "Icons";
        private static readonly Dictionary<string, ImageSource> _cashe = new Dictionary<string, ImageSource>();

        public static ImageSource DictionaryStatusDeleted
        {
            get { return GetIcon("cancel_18.png"); }
        }

        public static ImageSource DictionaryStatusEdited
        {
            get { return GetIcon("Edit_15.png"); }
        }

        public static ImageSource AddDict
        {
            get { return GetIcon("AddDictItem.png"); }
        }

        public static ImageSource Act
        {
            get { return GetIcon("Act.png"); }
        }

        public static ImageSource SaveImage
        {
            get { return GetIcon("disk-black.png"); }
        }

        public static ImageSource SaveAllImage
        {
            get { return GetIcon("disks-black.png"); }
        }

        public static ImageSource DeleteImage
        {
            get { return GetIcon("Remove.png"); }
        }

        public static ImageSource AddTTN
        {
            get { return GetIcon("AddTtn.png"); }
        }

        public static ImageSource Connect
        {
            get { return GetIcon("plug-connect.png"); }
        }

        public static ImageSource Disconnect
        {
            get { return GetIcon("plug-connect.png"); }
        }

        public static ImageSource StartProduction
        {
            get { return GetIcon("StartProduction.png"); }
        }

        public static ImageSource Download
        {
            get { return GetIcon("drive-upload.png"); }
        }

        public static ImageSource NewSection
        {
            get { return GetIcon("NewSection.png"); }
        }

        public static ImageSource Up
        {
            get { return GetIcon("arrow-up.png"); }
        }

        public static ImageSource Down
        {
            get { return GetIcon("arrow-down.png"); }
        }

        public static ImageSource Refresh
        {
            get { return GetIcon("reload_icon.png"); }
        }

        public static ImageSource Edit
        {
            get { return GetIcon("Edited.png"); }
        }

        public static ImageSource ExportPdf
        {
            get { return GetIcon("pdf.png"); }
        }

        public static ImageSource ExportXls
        {
            get { return GetIcon("xls.png"); }
        }

        public static ImageSource ExportWord
        {
            get { return GetIcon("word.png"); }
        }

        public static ImageSource Print
        {
            get { return GetIcon("icon_print.png"); }
        }

        public static ImageSource Left
        {
            get { return GetIcon("arrow-180.png"); }
        }

        public static ImageSource Right
        {
            get { return GetIcon("arrow.png"); }
        }

        public static ImageSource AddAccount
        {
            get { return GetIcon("user--plus.png"); }
        }

        public static ImageSource Roles
        {
            get { return GetIcon("users.png"); }
        }

        public static ImageSource RoleAdd
        {
            get { return GetIcon("users-plus.png"); }
        }

        public static ImageSource BatchEdit
        {
            get { return GetIcon("task--pencil.png"); }
        }

        public static ImageSource BatchAdd
        {
            get { return GetIcon("task--plus.png"); }
        }

        public static ImageSource Paste
        {
            get { return GetIcon("Paste.png"); }
        }

        public static ImageSource Copy
        {
            get { return GetIcon("Copy.png"); }
        }

        public static ImageSource Start
        {
            get { return GetIcon("control.png"); }
        }

        public static ImageSource Stop
        {
            get { return GetIcon("control-stop-square.png"); }
        }

        public static ImageSource Clear
        {
            get { return GetIcon("eraser.png"); }
        }

        public static ImageSource Divided
        {
            get { return GetIcon("Divide.png"); }
        }

        private static ImageSource GetIcon(string name)
        {
            if (_cashe.ContainsKey(name) == false)
            {
                var image = new BitmapImage();
                image.BeginInit();
                {
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
                    var uri =
                        String.Format("pack://application:,,,/ILIM.Bratsk.Visualization;component/{0}/{1}",
                            Path, name);
                    image.UriSource = new Uri(uri);
                }
                image.EndInit();
                _cashe.Add(name, image);
            }

            return _cashe[name];
        }
    }
}