using System.Windows;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.Behaviour
{
    public class EndEditBehavior
    {
        public static void SetEndEditCommand(DependencyObject target, ICommand value)
        {
            target.SetValue(EndEditCommandProperty, value);
        }

        public static ICommand GetEndEditCommand(DependencyObject target)
        {
            return (ICommand) target.GetValue(EndEditCommandProperty);
        }

        private static void EndEditChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var element = target as RadGridView;
            if (element != null)
            {
                if ((e.NewValue != null) && (e.OldValue == null))
                {
                    element.RowEditEnded += element_RowEditEnded;
                }
                else if ((e.NewValue == null) && (e.OldValue != null))
                {
                    element.RowEditEnded -= element_RowEditEnded;
                }
            }
        }

        private static void element_RowEditEnded(object sender, GridViewRowEditEndedEventArgs e)
        {
            var element = (UIElement) sender;
            var command = (ICommand) element.GetValue(EndEditCommandProperty);
            command.Execute(e.EditedItem);
        }

        public static DependencyProperty EndEditCommandProperty = DependencyProperty.RegisterAttached("EndEditCommand",
            typeof (ICommand),
            typeof (EndEditBehavior),
            new FrameworkPropertyMetadata(null, EndEditChanged));
    }
}