﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using Telerik.Windows.Controls;
using Telerik.Windows.Persistence;

namespace ILIM.Bratsk.Visualization.Extentions
{
    public class PersistenceManagerTool
    {
        private UserControl _control;
        private RadGridView _grid;
        private Popup _popup;

        public PersistenceManagerTool(UserControl control, RadGridView grid, Popup popup = null)
        {
            _control = control;
            _grid = grid;
            control.DataContextChanged += control_DataContextChanged;
            control.Unloaded += control_Unloaded;
            grid.MouseRightButtonUp += grid_MouseRightButtonUp;
            CloseManager.Shutdown += CloseManager_Shutdown;
            _popup = popup;
        }

        private string _dataId
        {
            get { return _control.GetType().FullName + "_viewData"; }
        }

        private void grid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_popup != null) _popup.IsOpen = true;
        }

        private void CloseManager_Shutdown()
        {
            SaveSettings();
        }

        private void control_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            RestoreSettings();
        }

        private void control_Unloaded(object sender, RoutedEventArgs e)
        {
            SaveSettings();
        }

        private void SaveSettings()
        {
            return;
            try
            {
                var manager = new PersistenceManager();
                var stream = manager.Save(_grid);
                stream.Position = 0;
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    var str = reader.ReadToEnd();
                    var ads = ((IAccountDataGiver)(_control).DataContext).AccountDataService;
                    SetViewPersistence(str, ads);
                }
                Dispatch();
            }
            catch
            {
            }
        }

        private void RestoreSettings()
        {
            return;
            var data = GetViewPersistence(((IAccountDataGiver)(_control).DataContext).AccountDataService);
            if (data == null) return;
            try
            {
                using (var stream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write(data);
                        writer.Flush();
                        stream.Position = 0;
                        var manager = new PersistenceManager();
                        manager.Load(_grid, stream);
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public void Dispatch()
        {
            _control.DataContextChanged -= control_DataContextChanged;
            _control.Unloaded -= control_Unloaded;
            CloseManager.Shutdown -= CloseManager_Shutdown;
            _grid.MouseRightButtonUp -= grid_MouseRightButtonUp;
            _control = null;
            _grid = null;
            _popup = null;
        }

        public string GetViewPersistence(IAccountDataService accountDataService)
        {
            var accountData = accountDataService.GetCurrentUserData(_dataId);
            if (accountData != null)
                return accountDataService.GetCurrentUserData(_dataId).Data;
            return null;
        }

        /// <summary>
        ///     Запуск действий асинхронно
        /// </summary>
        /// <param name="action"></param>
        private void RunAsync(Action action)
        {
            Task.Factory.StartNew(action);
        }

        public void SetViewPersistence(string persistance, IAccountDataService accountDataService)
        {
            RunAsync(() =>
            {
                try
                {
                    var accountData = new AccountData { DataID = _dataId };
                    accountData.Data = persistance;
                    accountDataService.UpdateOrCreateCurrentUserDataByDataID(accountData);
                }
                catch (Exception e)
                {
                }
            });
        }
    }


    public static class CloseManager
    {
        public delegate void ShutdownDelegate();

        public static event ShutdownDelegate Shutdown; // The event that can be subscribed to

        public static void RiseShutdown()
        {
            if (Shutdown != null)
                Shutdown();
            Thread.Sleep(500); // Временное решение. Ждем пока все сохранялки все сохранят перед глобальным shutdown
        }
    }
}