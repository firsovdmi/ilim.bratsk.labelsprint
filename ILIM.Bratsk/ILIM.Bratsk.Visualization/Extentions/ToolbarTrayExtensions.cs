using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using Telerik.Windows.Controls;

namespace ILIM.Bratsk.Visualization.Extentions
{
    public class ToolbarTrayExtensions
    {
        private readonly RadToolBarTray tray;

        private ToolbarTrayExtensions(RadToolBarTray tray)
        {
            this.tray = tray;
        }

        private static void ItemsSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var tray = (RadToolBarTray) o;

            var extentions = GetToolbarTrayExtensions(tray);
            if (extentions == null)
            {
                extentions = new ToolbarTrayExtensions(tray);
                SetToolbarTrayExtensions(tray, extentions);
            }

            if (e.OldValue != null)
            {
                tray.Items.Clear();
                if (e.OldValue is INotifyCollectionChanged)
                {
                    ((INotifyCollectionChanged) e.OldValue).CollectionChanged -=
                        extentions.ItemsSource_CollectionChanged;
                }
            }

            if (e.NewValue != null)
            {
                foreach (var item in (IEnumerable) e.NewValue)
                {
                    tray.Items.Add(GenerateItemContainer(item, tray.ItemTemplate));
                }

                if (e.NewValue is INotifyCollectionChanged)
                {
                    ((INotifyCollectionChanged) e.NewValue).CollectionChanged +=
                        extentions.ItemsSource_CollectionChanged;
                }
            }
        }

        private static FrameworkElement GenerateItemContainer(object dataItem, DataTemplate template)
        {
            var itemContainer = (FrameworkElement) template.LoadContent();
            itemContainer.DataContext = dataItem;
            return itemContainer;
        }

        private void ItemsSource_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (var item in e.OldItems)
                {
                    var itemContainer =
                        tray.Items.OfType<FrameworkElement>().FirstOrDefault(el => el.DataContext == item);
                    if (itemContainer != null)
                    {
                        tray.Items.Remove(itemContainer);
                    }
                }
            }

            if (e.NewItems != null)
            {
                foreach (var item in e.NewItems)
                {
                    tray.Items.Add(GenerateItemContainer(item, tray.ItemTemplate));
                }
            }

            if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                tray.Items.Clear();
            }
        }

        #region Attached

        public static IEnumerable GetItemsSource(DependencyObject obj)
        {
            return (IEnumerable) obj.GetValue(ItemsSourceProperty);
        }

        public static void SetItemsSource(DependencyObject obj, IEnumerable value)
        {
            obj.SetValue(ItemsSourceProperty, value);
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.RegisterAttached("ItemsSource", typeof (IEnumerable), typeof (ToolbarTrayExtensions),
                new PropertyMetadata(ItemsSourceChanged));


        private static ToolbarTrayExtensions GetToolbarTrayExtensions(DependencyObject obj)
        {
            return (ToolbarTrayExtensions) obj.GetValue(ToolbarTrayExtensionsProperty);
        }

        private static void SetToolbarTrayExtensions(DependencyObject obj, ToolbarTrayExtensions value)
        {
            obj.SetValue(ToolbarTrayExtensionsProperty, value);
        }

        private static readonly DependencyProperty ToolbarTrayExtensionsProperty =
            DependencyProperty.RegisterAttached("ToolbarTrayExtensions", typeof (ToolbarTrayExtensions),
                typeof (ToolbarTrayExtensions));

        #endregion
    }
}