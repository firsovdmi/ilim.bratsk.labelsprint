﻿using System.IO;
using System.Xml.Serialization;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.RemoteFacade;
using ILIM.Bratsk.Visualization.Infrastructure;

namespace ILIM.Bratsk.Visualization.Extentions
{
    internal static class AccountDataManager
    {


        public static DefaultValues GetDefaultValues(IAccountDataService accountDataService)
        {
            var savedData = accountDataService.GetCurrentUserData("DefaultValues");
            return savedData != null ? Desirialize<DefaultValues>(savedData.Data) : new DefaultValues();
        }

        public static void SaveDefaultValues(DefaultValues defaultValues, IAccountDataService accountDataService)
        {
            SaveData("DefaultValues", defaultValues, accountDataService);
        }

        private static void SaveData<T>(string key, T data, IAccountDataService accountDataService)
        {
            var xmlSerializer = new XmlSerializer(typeof (T));
            var textWriter = new StringWriter();
            xmlSerializer.Serialize(textWriter, data);
            var accauntData = new AccountData {DataID = key, Data = textWriter.ToString()};
            accountDataService.UpdateOrCreateCurrentUserDataByDataID(accauntData);
        }

        private static T Desirialize<T>(string data)
        {
            var serializer = new XmlSerializer(typeof (T));
            var rdr = new StringReader(data);
            return (T) serializer.Deserialize(rdr);
        }
    }
}