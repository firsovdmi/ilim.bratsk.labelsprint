﻿using ILIM.Bratsk.Domain.RemoteFacade;

namespace ILIM.Bratsk.Visualization.Extentions
{
    public interface IAccountDataGiver
    {
        IAccountDataService AccountDataService { get; }
    }
}