﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Caliburn.Micro;
using ILIM.Bratsk.Infrastructure.Model;
using ILIM.Bratsk.Visualization.Infrastructure;
using ILIM.Bratsk.Visualization.Model;

namespace ILIM.Bratsk.Visualization.Extentions
{
    public static class Extensions
    {
        public static T Clone<T>(this T source)
        {
            if (!typeof (T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T) formatter.Deserialize(stream);
            }
        }

        public static bool PropertiesEquals<T>(this T self, T obj)
        {
            var result = true;

            var type = typeof (T);

            foreach (var item in type.GetProperties())
            {
                if (item.GetValue(self, null) != item.GetValue(obj, null))
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        public static void UpdateProperties<T>(this T self, T from)
        {
            var type = typeof (T);

            foreach (var item in type.GetProperties())
            {
                try
                {
                    item.SetValue(self, item.GetValue(from, null), null);
                }
                catch (Exception e)
                {
                }
            }
        }

        public static void RefreshItems<T>(this ObservableCollection<EntityWithEditStatusModel<T>> items,
            ObservableCollection<EntityWithEditStatusModel<T>> newItems, IWindowManager windowManager)
            where T : Entity, new()
        {
            var expireItems = new List<string>();
            foreach (var newItem in newItems)
            {
                var oldItem = items.FirstOrDefault(p => p.Content.ID == newItem.Content.ID);
                if (oldItem == null)
                {
                    items.Add(newItem);
                }
                else if (oldItem.Content.RowVersion != newItem.Content.RowVersion)
                {
                    if (!oldItem.IsEdited)
                    {
                        oldItem.Content = newItem.Content;
                    }
                    else
                    {
                        expireItems.Add(oldItem.Content.Name + " -> " + newItem.Content.Name);
                    }
                }
            }

            var removeItems =
                items.Where(p => !p.IsAdded).Where(item => newItems.All(p => p.Content.ID != item.Content.ID)).ToList();
            foreach (var removeItem in removeItems)
            {
                items.Remove(removeItem);
            }

            if (expireItems.Count > 0)
            {
                windowManager.Alert(@"Предупреждение",
                    @"С момента последнего обновления значения некоторых элементов изменились." + "\n" +
                    string.Join("\n", expireItems));
            }
        }

        public static int GetHashCodeByProperties<T>(this T obj)
        {
            var type = typeof (T);

            var hashCode = 0;

            foreach (var item in type.GetProperties())
            {
                var val = item.GetValue(obj, null);

                if (val != null)
                    hashCode ^= val.GetHashCode();
            }

            return hashCode;
        }

        public static string ShortName(string fullName)
        {
            try
            {
                var split = fullName.Split(' ');
                var result = split[0];
                return split.Skip(1).Aggregate(result, (current, item) => current + " " + item[0] + ".");
            }
            catch (Exception e)
            {
            }
            return "";
        }

        public static void AddSorted<T>(this IList<T> list, T item, IComparer<T> comparer = null)
        {
            if (comparer == null)
                comparer = Comparer<T>.Default;

            int i = 0;
            while (i < list.Count && comparer.Compare(list[i], item) < 0)
                i++;

            list.Insert(i, item);
        }
    }

    public class EnumViewModel
    {
        public List<string> GetEnumGecriptions(Type type)
        {
            var strings = new List<string>();
            foreach (var o in Enum.GetValues(type))
            {
                var fi = o.GetType().GetField(o.ToString());
                if (fi != null)
                {
                    var attributes =
                        (DescriptionAttribute[]) fi.GetCustomAttributes(typeof (DescriptionAttribute), false);
                    strings.Add((attributes.Length > 0) &&
                                (!String.IsNullOrEmpty(attributes[0].Description))
                        ? attributes[0].Description
                        : o.ToString());
                }
            }
            return strings;
        }
    }
}