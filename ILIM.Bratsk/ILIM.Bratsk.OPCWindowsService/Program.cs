﻿using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;

namespace ILIM.Bratsk.HostService
{
    internal static class Program
    {
        /// <summary>
        ///     Главная точка входа для приложения.
        /// </summary>
        private static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if (args[0] == "/i")
                {
                    ManagedInstallerClass.InstallHelper(new[] {Assembly.GetExecutingAssembly().Location});
                }
                else if (args[0] == "/u")
                {
                    ManagedInstallerClass.InstallHelper(new[] {"/u", Assembly.GetExecutingAssembly().Location});
                }
                else if (args[0] == "/d")
                {
                    var dbgService = new OPCService();
                    dbgService.OnDebug();
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new OPCService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}