﻿using System.ComponentModel;
using System.Configuration;

namespace ILIM.Bratsk.HostService.Properties
{
    // Этот класс позволяет обрабатывать определенные события в классе параметров:
    //  Событие SettingChanging возникает перед изменением значения параметра.
    //  Событие PropertyChanged возникает после изменения значения параметра.
    //  Событие SettingsLoaded возникает после загрузки значений параметров.
    //  Событие SettingsSaving возникает перед сохранением значений параметров.
    internal sealed class Settings
    {
        private void SettingChangingEventHandler(object sender, SettingChangingEventArgs e)
        {
            // Добавьте здесь код для обработки события SettingChangingEvent.
        }

        private void SettingsSavingEventHandler(object sender, CancelEventArgs e)
        {
            // Добавьте здесь код для обработки события SettingsSaving.
        }
    }


    public sealed class CommonSettings
    {
        private CommonSettings()
        {
        }

        static CommonSettings()
        {
            Config = ((CommonSettingsSection) (ConfigurationManager.GetSection("CommonSettings")));
        }

        public static CommonSettingsSection Config { get; private set; }
    }

    public sealed class CommonSettingsSection : ConfigurationSection
    {
        [ConfigurationProperty("LocalMachine")]
        public LocalMachineElement LocalMachine
        {
            get { return ((LocalMachineElement) (this["LocalMachine"])); }
        }

        public sealed class LocalMachineElement : ConfigurationElement
        {
            [ConfigurationProperty("Name", IsRequired = true)]
            public string Name
            {
                get { return ((string) (this["Name"])); }
                set { this["Name"] = value; }
            }
        }
    }
}