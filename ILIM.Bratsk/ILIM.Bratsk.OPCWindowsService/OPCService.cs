﻿using System.ServiceProcess;
using System.Threading;
using Castle.Windsor;
using ILIM.Bratsk.Domain;
using ILIM.Bratsk.Service;

namespace ILIM.Bratsk.HostService
{
    public partial class OPCService : ServiceBase
    {
        public OPCService()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
            Thread.Sleep(Timeout.Infinite);
        }

        protected override void OnStart(string[] args)
        {
            var container = new WindsorContainer();
            container.Install(new RemoteServiceInstaller());
            container.ResolveAll<IModule>();
        }

        protected override void OnStop()
        {
        }
    }
}