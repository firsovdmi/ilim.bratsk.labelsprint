using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class FactDataRepository : RepositoryEntityWithMarkedAsDelete<FactData>, IFactDataRepository
    {
        public FactDataRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override FactData Create(FactData entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}