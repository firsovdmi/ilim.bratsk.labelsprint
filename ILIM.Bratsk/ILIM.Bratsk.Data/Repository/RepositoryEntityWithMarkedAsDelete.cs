﻿using System.Linq;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.Repository
{
    public abstract class RepositoryEntityWithMarkedAsDelete<T> : RepositoryEntity<T>, IRepositoryWithMarkedAsDelete<T>
        where T : EntityBase, IMarkableForDelete
    {
        public RepositoryEntityWithMarkedAsDelete(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public virtual T MarkAsDelete(T entity)
        {
            var entityToUpdate = _unitOfWork.Get<T>().FirstOrDefault(p => p.ID == entity.ID);
            if (entityToUpdate != null)
            {
                entityToUpdate.IsDeleted = true;
                return Update(entityToUpdate);
            }
            return null;
        }
    }
}