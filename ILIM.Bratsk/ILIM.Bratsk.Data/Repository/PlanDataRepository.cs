using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class PlanDataRepository : RepositoryEntityWithMarkedAsDelete<PlanData>, IPlanDataRepository
    {
        public PlanDataRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override PlanData Create(PlanData entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}