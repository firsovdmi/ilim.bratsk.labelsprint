using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class EventRepository : RepositoryEntityWithMarkedAsDelete<Event>, IEventRepository
    {
        public EventRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override Event Create(Event entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}