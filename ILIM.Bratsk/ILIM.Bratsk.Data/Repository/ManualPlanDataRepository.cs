using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class ManualPlanDataRepository : RepositoryEntityWithMarkedAsDelete<ManualPlanData>, IManualPlanDataRepository
    {
        public ManualPlanDataRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override ManualPlanData Create(ManualPlanData entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}