using System;
using System.Linq;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.Repository
{
    public abstract class RepositoryEntity<T> : IRepositoryEntity<T> where T : class, IEntity
    {
        protected readonly IUnitOfWorkDO _unitOfWork;

        public RepositoryEntity(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (IUnitOfWorkDO) unitOfWork;
        }

        public virtual IQueryable<T> Get()
        {
            return _unitOfWork.Get<T>();
        }

        public virtual IQueryable<T> GetTracked()
        {
            return _unitOfWork.GetTracked<T>();
        }

        public virtual T Create(T entity)
        {
            if (entity.ID == Guid.Empty)
                entity.ID = Guid.NewGuid();

            return _unitOfWork.Create(entity);
        }

        public virtual T Update(T entity)
        {
            return _unitOfWork.Update(entity);
        }

        public virtual void Delete(T entity)
        {
            _unitOfWork.Delete(entity);
        }

        public virtual T CreateOrUpdate(T entity)
        {
            return _unitOfWork.CreateOrUpdate(entity);
        }
    }
}