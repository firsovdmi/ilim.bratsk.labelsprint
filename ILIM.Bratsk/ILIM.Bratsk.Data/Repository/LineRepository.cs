using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class LineRepository : RepositoryEntityWithMarkedAsDelete<Line>, ILineRepository
    {
        public LineRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override Line Create(Line entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}