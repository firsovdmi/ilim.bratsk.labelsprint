using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class PrinterRepository : RepositoryEntityWithMarkedAsDelete<Printer>, IPrinterRepository
    {
        public PrinterRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override Printer Create(Printer entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}