using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class ApplicationSettingsRepository : RepositoryEntityWithMarkedAsDelete<ApplicationSettings>, IApplicationSettingsRepository
    {
        public ApplicationSettingsRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override ApplicationSettings Create(ApplicationSettings entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}