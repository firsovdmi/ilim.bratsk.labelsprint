using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class ServiceDataRepository : RepositoryEntityWithMarkedAsDelete<ServiceData>, IServiceDataRepository
    {
        public ServiceDataRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override ServiceData Create(ServiceData entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}