using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;
using ILIM.Bratsk.Infrastructure;

namespace ILIM.Bratsk.Data.Repository
{
    public class AccountDataRepository : RepositoryEntity<AccountData>, IAccountDataRepository
    {
        public AccountDataRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}