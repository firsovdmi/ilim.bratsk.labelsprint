using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class ManufactureRepository : RepositoryEntityWithMarkedAsDelete<Manufacture>, IManufactureRepository
    {
        public ManufactureRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

    }
}