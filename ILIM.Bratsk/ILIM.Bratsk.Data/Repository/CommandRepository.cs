using System;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Domain.Repository;

namespace ILIM.Bratsk.Data.Repository
{
    public class CommandRepository : RepositoryEntityWithMarkedAsDelete<Command>, ICommandRepository
    {
        public CommandRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override Command Create(Command entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}