﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class MilkFactoryContext : DbContext, IUnitOfWorkDO
    {
        private IDateTimeManager _dateTimeManager;

        public MilkFactoryContext(string connectionString)
            : base(connectionString)
        {
            _dateTimeManager = new DateTimeManager();
        }

        public MilkFactoryContext(IDateTimeManager dateTimeManager, string connectionString)
            : base(connectionString)
        {
            _dateTimeManager = dateTimeManager;
        }
        public DbSet<Entity> Entities { get; set; }
        public DbSet<Farm> Farms { get; set; }
        public DbSet<Sort> Sorts { get; set; }
        public DbSet<ThermalResistance> ThermalResistances { get; set; }

        public DbSet<MilkReceivingTask> MilkReceivingTasks { get; set; }
        public DbSet<MilkReceivingSubTask> MilkReceivingSubTasks { get; set; }
        public DbSet<ReceivingBatch> ReceivingBatches { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Analysis> Analysis { get; set; }


        #region Overrides of DbContext

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        ///             before the model has been locked down and used to initialize the context.  The default
        ///             implementation of this method does nothing, but it can be overridden in a derived class
        ///             such that the model can be further configured before it is locked down.
        /// </summary>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        ///             is created.  The model for that context is then cached and is for all further instances of
        ///             the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///             property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///             More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///             classes directly.
        /// </remarks>
        /// <param name="modelBuilder">The builder that defines the model for the context being created. </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Ignore<Entity>();
            modelBuilder.Configurations.Add(new TTNHeadMapConfiguration());
            modelBuilder.Configurations.Add(new EntityMapConfiguration());
            modelBuilder.Configurations.Add(new FarmMapConfiguration());
            modelBuilder.Configurations.Add(new SortMapConfiguration());
            modelBuilder.Configurations.Add(new ThermalResistanceMapConfiguration());
            modelBuilder.Configurations.Add(new MilkReceivingSubTaskMapConfiguration());
            modelBuilder.Configurations.Add(new MilkReceivingTaskMapConfiguration());
            modelBuilder.Configurations.Add(new ReceivingBatchMapConfiguration());
            modelBuilder.Configurations.Add(new StaffTaskMapConfiguration());
            modelBuilder.Configurations.Add(new AnalysisMapConfiguration());
            modelBuilder.Configurations.Add(new PositionMapConfiguration());

        }

        #endregion

        #region Implementation of IUnitOfWork

        public void Save()
        {
            this.SaveChanges();
        }

        #endregion

        #region Implementation of IUnitOfWorkDO

        public IQueryable<T> Get<T>() where T : class
        {
            return this.Set<T>().AsNoTracking();
        }

        public T Create<T>(T entity) where T : class
        {
            if (Entry(entity).State == EntityState.Detached)
                Set<T>().Add(entity);
            return entity;
        }

        public void Update<T>(T entity) where T : class
        {
            var entry = Entry(entity);
            entry.State = EntityState.Modified;
        }

        public void Delete<T>(T entity) where T : class
        {
            Set<T>().Attach(entity);
            if (Entry(entity).State != EntityState.Deleted)
                Set<T>().Remove(entity);
        }

        #endregion

        public override int SaveChanges()
        {
            DateTime saveTime = _dateTimeManager.GetDateTimeNow().ToUniversalTime();
            foreach (var entry in this.ChangeTracker.Entries().Where(p => p.Entity is Entity).Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            {
                Entity entityWithVersion = entry.Entity as Entity;
                if (entityWithVersion != null) entityWithVersion.DateTimeUpdate = saveTime;
            }
            return base.SaveChanges();
        }
    }
}
