using System;

namespace PAG.WBD.Milk.Data.EF
{
    public interface IDateTimeManager
    {
        DateTime GetDateTimeNow();
    }
}