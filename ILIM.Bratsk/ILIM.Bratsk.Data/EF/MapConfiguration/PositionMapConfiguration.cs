using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class PositionMapConfiguration : EntityTypeConfiguration<Position>
    {
        private const string TableName = "Position";
        public PositionMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}