using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class TTNHeadMapConfiguration : ComplexTypeConfiguration<TTNHead>
    {
        public TTNHeadMapConfiguration()
        {
            Property(p => p.Number).HasColumnName("TTNNumber");
        }
    }
}