using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class StaffTaskMapConfiguration : EntityTypeConfiguration<Staff>
    {
        private const string TableName = "Staff";
        public StaffTaskMapConfiguration()
        {
            HasOptional(e => e.Position).WithMany().HasForeignKey(m => m.PositionID).WillCascadeOnDelete(false);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}