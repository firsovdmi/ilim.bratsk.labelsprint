﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Domain.Model;
using PAG.WBD.Milk.Domain.Repository;

namespace PAG.WBD.Milk.Data.EF.Repository
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected readonly IUnitOfWorkDO _unitOfWork;

        public Repository(IUnitOfWorkDO unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public virtual IQueryable<T> Get()
        {
            return _unitOfWork.Get<T>();
        }
        public virtual T Create(T entity)
        {
            return _unitOfWork.Create<T>(entity);
        }
        public virtual void Update(T entity)
        {
            _unitOfWork.Update(entity);
        }
        public virtual void Delete(T entity)
        {
            _unitOfWork.Delete(entity);
        }

        public virtual void MarkAsDelete(T entity)
        {
            throw new NotImplementedException();
        }
    }
    public class FarmRepository : Repository<Farm>, IFarmRepository
    {
        public FarmRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override Farm Create(Farm entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create<Farm>(entity);
        }

        public override void MarkAsDelete(Farm entity)
        {
            var entityToUpdate=_unitOfWork.Get<Farm>().FirstOrDefault(p => p.ID == entity.ID);
            entityToUpdate.IsDeleted = true;
            Update(entityToUpdate);
        }
    }

    public class SortRepository : Repository<Sort>, ISortRepository
    {
        public SortRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override Sort Create(Sort entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create<Sort>(entity);
        }

        public override void MarkAsDelete(Sort entity)
        {
            var entityToUpdate = _unitOfWork.Get<Sort>().FirstOrDefault(p => p.ID == entity.ID);
            entityToUpdate.IsDeleted = true;
            Update(entityToUpdate);
        }
    }
}
