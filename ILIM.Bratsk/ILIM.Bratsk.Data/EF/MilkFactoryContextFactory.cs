using System.Data.Entity.Infrastructure;

namespace PAG.WBD.Milk.Data.EF
{
    public class MilkFactoryContextFactory : IDbContextFactory<MilkFactoryContext>
    {
        #region Implementation of IDbContextFactory<out MilkFactoryContext>

        /// <summary>
        /// Creates a new instance of a derived <see cref="T:System.Data.Entity.DbContext"/> type.
        /// </summary>
        /// <returns>
        /// An instance of TContext 
        /// </returns>
        public MilkFactoryContext Create()
        {
            return new MilkFactoryContext("WbdMilkDatabase");
        }

        #endregion
    }
}