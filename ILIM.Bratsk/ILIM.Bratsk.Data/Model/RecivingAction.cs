﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Data.Model
{
    public class RecivingAction : IRecivingAction
    {
        public int Id { get; set; }
        public ITank Tank { get; set; }
        public float Volume { get; set; }
        public int Section { get; set; }
        public IRecivingPost RecivePost { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Downtime { get; set; }
    }
    public interface IRecivingAction
    {
       int Id { get; set; }
       ITank Tank { get; set; }
       float Volume { get; set; }
       int Section { get; set; }
       IRecivingPost RecivePost { get; set; }
       DateTime StartTime { get; set; }
       DateTime EndTime { get; set; }
       int Downtime { get; set; }
    }
}
