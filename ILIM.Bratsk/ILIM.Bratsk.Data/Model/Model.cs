﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Data.Login;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Data.Model
{
    public class Model : IEnumerable<ITtnHead>
    {
        private List<ITtnHead> _items;

        public List<IManufacturer> Manufacturers { get; set; }
        public List<IContract> Contracts { get; set; }
        public List<IFioDriver> FioDrivers { get; set; }
        public List<IStaff> Staffs { get; set; }
        public List<IThermalResistance> ThermalResistance { get; set; }
        public List<IFarm> Farms { get; set; }
        public List<ISectionsName> SectionsNames { get; set; }
        public List<ISort> Sortes { get; set; }
        public List<IRecivingPost> RecivingPosts { get; set; }

        public LoginModel Login { get; set; }

        public void InitAllDictionaries()
        {
            using (var connection = new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();
                Manufacturers = new List<IManufacturer>(
                    InitDictionaries(connection, "Manufacturer")
                        .Select(p => new Manufacturer { Id = p.Id, Name = p.Name })
                        .ToList());
                Contracts = new List<IContract>(
                    InitDictionaries(connection, "Contract")
                        .Select(p => new Contract { Id = p.Id, Name = p.Name })
                        .ToList());
                FioDrivers = new List<IFioDriver>();
                FioDrivers.AddRange(
                    InitDictionaries(connection, "FioDriver")
                        .Select(p => new FioDriver { Id = p.Id, Name = p.Name })
                        .ToList());
                Staffs = new List<IStaff>(
                    InitDictionaries(connection, "Staff").Select(p => new Staff { Id = p.Id, Name = p.Name }).ToList());
                ThermalResistance = new List<IThermalResistance>(
                    InitDictionaries(connection, "ThermalResistance")
                        .Select(p => new ThermalResistance { Id = p.Id, Name = p.Name })
                        .ToList());
                Farms = new List<IFarm>(InitDictionaries(connection, "Farm").Select(p => new Farm { Id = p.Id, Name = p.Name }).ToList());
                SectionsNames = new List<ISectionsName>(
                    InitDictionaries(connection, "SectionsName")
                        .Select(p => new SectionsName { Id = p.Id, Name = p.Name })
                        .ToList());
                Sortes = new List<ISort>(InitDictionaries(connection, "Sort").Select(p => new Sort { Id = p.Id, Name = p.Name }).ToList());
                RecivingPosts = new List<IRecivingPost>(
                    InitDictionaries(connection, "RecivePost")
                        .Select(p => new RecivingPost { Id = p.Id, Name = p.Name })
                        .ToList());
                connection.Close();
            }
        }

        public void InitItems()
        {
            InitAllDictionaries();
            using (var connection = new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();
                SqlCommand command =
                    new SqlCommand(
                        "SELECT [Id] ,[Number] ,[RawCode] ,[Client] ,[Invoice] ,[Contract] ,[Manufacturer] ,[IsSelf] ,[FioDriver] ,[Master] ,[Operator] FROM [TTNHead]",
                        connection);

                SqlDataReader reader = command.ExecuteReader();
                _items = new List<ITtnHead>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var ttn = new TtnHead
                        {
                            Id = reader.GetValue<int>(0),
                            Number = reader.GetValue<int>(1),
                            RawCode = reader.GetValue<string>(2),
                            Client = reader.GetValue<string>(3),
                            Invoice = reader.GetValue<string>(4)
                            ,
                            Contract = Contracts.FirstOrDefault(p => p.Id == reader.GetValue<int>(5))
                            ,
                            Manufacturer = Manufacturers.FirstOrDefault(p => p.Id == reader.GetValue<int>(6))
                            ,
                            IsSelf = reader.GetValue<bool>(7)
                            ,
                            FioDrivers = FioDrivers.FirstOrDefault(p => p.Id == reader.GetValue<int>(8))
                            ,
                            Master = Staffs.FirstOrDefault(p => p.Id == reader.GetValue<int>(9))
                            ,
                            Operator = Staffs.FirstOrDefault(p => p.Id == reader.GetValue<int>(10))
                        };

                        _items.Add(ttn);
                    }
                }
                reader.Close();
                foreach (var ttn in _items)
                {
                    ttn.Section = GetSectionsByTtn(connection, ttn);
                }
            }
        }

        private IEnumerable<BaseDictionary> InitDictionaries(SqlConnection connection, string tabelMame)
        {
            var dictionary = new List<BaseDictionary>();


            var command = new SqlCommand("SELECT [Id] ,[Name] FROM " + tabelMame + " WHERE IsDeleted=0 OR IsDeleted IS NULL", connection);
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    dictionary.Add(new BaseDictionary
                    {
                        Id = reader.GetValue<int>(0),
                        Name = reader.GetValue<string>(1)
                    });
                }
            }
            reader.Close();

            return dictionary;
        }

        private IEnumerable<ISection> GetSectionsByTtn(SqlConnection connection, ITtnHead ttn)
        {
            var sections = new List<ISection>();



            var command =
                new SqlCommand(
                    "SELECT [Id] ,[AcidTTN] ,[DensityTTN] ,[FatTTN] ,[ProteinTTN] ,[ThermalResistance] ,[Farm] ,[Section] ,[CarNumber] ,[TTNVolume] ,[Sort] ,[AcidLab] ,[DensityLab] ,[FatLab] ,[ProteinLab] ,[RecommendationRecivePost] ,[TTNHead] ,[ParentSection] FROM TTNSection WHERE (ParentSection=0 OR ParentSection IS NULL) AND TTNHead = " +
                    ttn.Id, connection);
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    sections.Add(SectionFromReader(reader));
                }
            }

            reader.Close();
            command =
                new SqlCommand(
                    "SELECT [Id] ,[AcidTTN] ,[DensityTTN] ,[FatTTN] ,[ProteinTTN] ,[ThermalResistance] ,[Farm] ,[Section] ,[CarNumber] ,[TTNVolume] ,[Sort] ,[AcidLab] ,[DensityLab] ,[FatLab] ,[ProteinLab] ,[RecommendationRecivePost] ,[TTNHead] ,[ParentSection] FROM TTNSection WHERE ParentSection<>0 AND ParentSection IS NOT NULL AND TTNHead = " +
                    ttn.Id, connection);
            reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var parentSection = sections.FirstOrDefault(p => p.Id == reader.GetValue<int>(17));
                    if (parentSection != null)
                    {
                        if (parentSection.SubSections == null) parentSection.SubSections = new List<ISection>();
                        ((List<ISection>)parentSection.SubSections).Add(SectionFromReader(reader));
                    }
                }
            }
            reader.Close();

            return sections;
        }

        private Section SectionFromReader(SqlDataReader reader)
        {
            return new Section
            {
                Id = reader.GetValue<int>(0),
                AcidTtn = reader.GetValue<float>(1),
                DensityTtn = reader.GetValue<float>(2),
                FatTtn = reader.GetValue<float>(3),
                ProteinTtn = reader.GetValue<float>(4),
                ThermalResistance = ThermalResistance.FirstOrDefault(p => p.Id == reader.GetValue<int>(5)),
                Farm = Farms.FirstOrDefault(p => p.Id == reader.GetValue<int>(6)),
                SectionN = SectionsNames.FirstOrDefault(p => p.Id == reader.GetValue<int>(7)),
                CarNumber = reader.GetValue<string>(8),
                TtnVolume = reader.GetValue<float>(9),
                Sort = Sortes.FirstOrDefault(p => p.Id == reader.GetValue<int>(10)),
                AcidLab = reader.GetValue<float>(11),
                DensityLab = reader.GetValue<float>(12),
                FatLab = reader.GetValue<float>(13),
                ProteinLab = reader.GetValue<float>(14),
                RecommendationRecivePost = RecivingPosts.FirstOrDefault(p => p.Id == reader.GetValue<int>(15))
            };
        }

        public void AddFioDrivers(IFioDriver item)
        {
            AddDictionaryItem("FioDriver", item);
            FioDrivers.Add(item);
        }

        public void AddStaffs(IStaff item)
        {
            AddDictionaryItem("Staff", item);
            Staffs.Add(item);
        }

        public void AddManufacturers(IManufacturer item)
        {
            AddDictionaryItem("Manufacturer", item);
            Manufacturers.Add(item);
        }

        public void AddContracts(IContract item)
        {
            AddDictionaryItem("Contract", item);
            Contracts.Add(item);
        }

        public void AddSectionsName(ISectionsName item)
        {
            AddDictionaryItem("SectionsName", item);
            SectionsNames.Add(item);
        }

        public void AddRecivePost(IRecivingPost item)
        {
            AddDictionaryItem("RecivePost", item);
            RecivingPosts.Add(item);
        }

        public void AddThermalResistance(IThermalResistance item)
        {
            AddDictionaryItem("ThermalResistance", item);
            ThermalResistance.Add(item);
        }

        public void AddFarm(IFarm item)
        {
            AddDictionaryItem("Farm", item);
            Farms.Add(item);
        }

        public void AddSort(ISort item)
        {
            AddDictionaryItem("Sort", item);
            Sortes.Add(item);
        }

        private void AddDictionaryItem(string tabelMame, IBaseDictionary item)
        {
            using (var connection = new SqlConnection(Settings.ConnectionString))
            {
                var sqlQuery = "INSERT INTO " + tabelMame + " (Name, IsDeleted) OUTPUT INSERTED.ID VALUES (@Name, 0)";
                var command = new SqlCommand(sqlQuery, connection);
                try
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@Name", item.Name);
                    item.Id = (int)command.ExecuteScalar();
                    connection.Close();
                }
                catch
                {
                }
            }
        }

        private void UpdateDictionaryItem(string tabelMame, IBaseDictionary item)
        {
            using (var connection = new SqlConnection(Settings.ConnectionString))
            {
                var sqlQuery = "UPDATE " + tabelMame + " Name=@Name, IsDeleted=@IsDeleted WHERE Id=@Id";
                var command = new SqlCommand(sqlQuery, connection);
                try
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@Id", item.Id);
                    command.Parameters.AddWithValue("@Name", item.Name);
                    command.Parameters.AddWithValue("@IsDeleted", item.IsDeleted);
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch
                {
                }
            }
        }

        public void UpdateDictionaryManufacturers(IEnumerable<IBaseDictionary> newItems)
        {
            UpdateDictionary(Manufacturers, newItems, "Manufacturer");
        }

        public void UpdateDictionaryContract(IEnumerable<IBaseDictionary> newItems)
        {
            UpdateDictionary(Contracts, newItems, "Contract");
        }

        public void UpdateDictionaryFioDrivers(IEnumerable<IBaseDictionary> newItems)
        {
            UpdateDictionary(FioDrivers, newItems, "FioDriver");
        }

        public void UpdateDictionaryStaffs(IEnumerable<IBaseDictionary> newItems)
        {
            UpdateDictionary(Staffs, newItems, "Staff");
        }

        public void UpdateDictionaryThermalResistance(IEnumerable<IBaseDictionary> newItems)
        {
            UpdateDictionary(ThermalResistance, newItems, "ThermalResistance");
        }

        public void UpdateDictionaryFarms(IEnumerable<IBaseDictionary> newItems)
        {
            UpdateDictionary(Farms, newItems, "Farm");
        }

        public void UpdateDictionarySectionsNames(IEnumerable<IBaseDictionary> newItems)
        {
            UpdateDictionary(SectionsNames, newItems, "SectionsName");
        }

        public void UpdateDictionaryRecivingPosts(IEnumerable<IBaseDictionary> newItems)
        {
            UpdateDictionary(RecivingPosts, newItems, "RecivePost");
        }

        public void UpdateDictionary(IEnumerable<IBaseDictionary> oldItems, IEnumerable<IBaseDictionary> newItems, string tabelName)
        {
            foreach (var newItem in newItems)
            {
                var baseDictionaries = oldItems as List<IBaseDictionary> ?? oldItems.ToList();
                var oldItem = baseDictionaries.FirstOrDefault(p => p.Id == newItem.Id);
                if (oldItem == null)
                {
                    AddDictionaryItem(tabelName, newItem);
                    continue;
                }
                if (!IsDifferentDictionaryItems(oldItem, newItem)) continue;
                UpdateDictionaryItem(tabelName, newItem);
            }
        }

        public void UpdateAcessLevels(IEnumerable<IAcessLevel> oldItems, IEnumerable<IAcessLevel> newItems, string tabelName)
        {
            foreach (var newItem in newItems)
            {
                var baseDictionaries = oldItems as List<IAcessLevel> ?? oldItems.ToList();
                var oldItem = baseDictionaries.FirstOrDefault(p => p.Id == newItem.Id);
                if (oldItem == null)
                {
                    AddAcessLevel(newItem);
                    continue;
                }
                if (!IsDifferentAcessLevel(oldItem, newItem)) continue;
                //UpdateDictionaryItem(tabelName, newItem);
            }
        }

        private void AddAcessLevel(IAcessLevel item)
        {
            using (var connection = new SqlConnection(Settings.ConnectionString))
            {
                var sqlQuery = "INSERT INTO AcessLevel (Name, AcessLevelN, IsDeleted) OUTPUT INSERTED.ID VALUES (@Name, @AcessLevelN, 0)";
                var command = new SqlCommand(sqlQuery, connection);
                try
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@Name", item.Name);
                    command.Parameters.AddWithValue("@AcessLevelN", item.AcessLevelN);
                    item.Id = (int)command.ExecuteScalar();
                    connection.Close();
                }
                catch
                {
                }
            }
        }

        private bool IsDifferentAcessLevel(IAcessLevel item1, IAcessLevel item2)
        {
            return (item1.Name != item2.Name || item1.IsDeleted != item2.IsDeleted || item1.AcessLevelN!=item2.AcessLevelN);
        }

        private bool IsDifferentDictionaryItems(IBaseDictionary item1, IBaseDictionary item2)
        {
            return (item1.Name != item2.Name || item1.IsDeleted != item2.IsDeleted);
        }

        public void SaveTtn(ITtnHead ttn)
        {
            const string sqlQuery = @"  begin tran 
                                     if exists (select * from TTNHead with (updlock,serializable) where Id = @Id) 
                                     begin 
                                      update TTNHead set Number=@Number ,RawCode=@RawCode ,Client=@Client ,Invoice=@Invoice ,[Contract]=@Contract ,Manufacturer=@Manufacturer ,IsSelf=@IsSelf ,FioDriver=@FioDriver ,[Master]=@Master ,Operator=@Operator  
                                      where Id = @Id 
                                     end 
                                     else 
                                     begin 
                                      insert TTNHead (Number ,RawCode ,Client ,Invoice ,[Contract] ,Manufacturer ,IsSelf ,FioDriver ,[Master] ,Operator ) 
                                      OUTPUT INSERTED.ID 
                                      values (@Number ,@RawCode ,@Client ,@Invoice ,@Contract ,@Manufacturer ,@IsSelf ,@FioDriver ,@Master ,@Operator  ) 
                                     end 
                                     commit tran ";
            using (var connection = new SqlConnection(Settings.ConnectionString))
            {
                var command = new SqlCommand(sqlQuery, connection);
                try
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@Id", ttn.Id);
                    command.Parameters.AddWithValue("@Number", ttn.Number);
                    command.Parameters.AddWithValue("@RawCode", ttn.RawCode ?? Convert.DBNull);
                    command.Parameters.AddWithValue("@Client", "");
                    command.Parameters.AddWithValue("@Invoice", ttn.Invoice ?? Convert.DBNull);
                    command.Parameters.AddWithValue("@Contract", ttn.Contract == null ? Convert.DBNull : ttn.Contract.Id);
                    command.Parameters.AddWithValue("@Manufacturer", ttn.Manufacturer == null ? Convert.DBNull : ttn.Manufacturer.Id);
                    command.Parameters.AddWithValue("@IsSelf", ttn.IsSelf);
                    command.Parameters.AddWithValue("@FioDriver", ttn.FioDrivers == null ? Convert.DBNull : ttn.FioDrivers.Id);
                    command.Parameters.AddWithValue("@Master", ttn.Master == null ? Convert.DBNull : ttn.Master.Id);
                    command.Parameters.AddWithValue("@Operator", ttn.Operator == null ? Convert.DBNull : ttn.Operator.Id);
                    if (ttn.Id == 0)
                    {
                        ttn.Id = (int)command.ExecuteScalar();
                    }
                    else
                    {
                        command.ExecuteNonQuery();
                    }
                    foreach (var section in ttn.Section)
                    {
                        AddOrUpdateSection(connection, section, ttn, null);
                        if (section.SubSections != null)
                        {
                            foreach (var subSection in section.SubSections)
                            {
                                AddOrUpdateSection(connection, subSection, ttn, section);
                            }
                        }
                    }
                    DeleteSections(connection, ttn);

                    connection.Close();
                }
                catch
                {
                }

            }
        }

        private void DeleteSections(SqlConnection connection, ITtnHead ttn)
        {
            if (ttn.Section == null) return;

            var allIds=new List<int>();
            foreach (var section in ttn.Section)
            {
                allIds.Add(section.Id);
                if (section.SubSections != null)
                {
                    foreach (var subSection in section.SubSections)
                    {
                        allIds.Add(subSection.Id);
                    }
                }
            }
            var idsToString = string.Join(", ", allIds.Select(p => p.ToString()));
            string sqlQuery = "DELETE FROM TtnSection WHERE TtnHead=@TtnHead ";
            if (allIds.Count > 0)
            {
             sqlQuery=sqlQuery+ " AND Id NOT IN (" + idsToString + ")";
            }
            var command = new SqlCommand(sqlQuery, connection);
            command.Parameters.AddWithValue("@TtnHead", ttn.Id);
            command.ExecuteNonQuery();
        }

        private void AddOrUpdateSection(SqlConnection connection, ISection section, ITtnHead ttn, ISection parreSection)
        {
            const string sqlQuery = @"  begin tran
                                     if exists (select * from TtnSection with (updlock,serializable) where Id = @Id)
                                     begin
                                        update TtnSection set  AcidTtn = @AcidTtn , DensityTtn=@DensityTtn  , FatTtn=@FatTtn  , ProteinTtn=@ProteinTtn  , ThermalResistance=@ThermalResistance  , Farm=@Farm  , Section=@Section  , CarNumber=@CarNumber  , TtnVolume=@TtnVolume  , Sort=@Sort  , AcidLab=@AcidLab  , DensityLab=@DensityLab  , FatLab=@FatLab  , ProteinLab=@ProteinLab  , RecommendationRecivePost=@RecommendationRecivePost  , TtnHead=@TtnHead  , ParentSection=@ParentSection  
                                        where Id = @Id
                                     end
                                     else
                                     begin
                                        insert TTNSection (AcidTtn  , DensityTtn  ,  FatTtn  ,  ProteinTtn  ,  ThermalResistance  ,  Farm  ,  [Section]  ,  CarNumber  ,  TtnVolume  ,  [Sort]  ,  AcidLab  ,  DensityLab  ,  FatLab  ,  ProteinLab  ,  RecommendationRecivePost  ,  TtnHead  ,  [ParentSection]  )
                                        OUTPUT INSERTED.ID 
                                        values            (@AcidTtn , @DensityTtn  , @FatTtn  , @ProteinTtn  , @ThermalResistance  , @Farm  , @Section  , @CarNumber  , @TtnVolume  , @Sort  , @AcidLab  , @DensityLab  , @FatLab  , @ProteinLab  , @RecommendationRecivePost  , @TtnHead  , @ParentSection )
                                     end
                                     commit tran";

            var command = new SqlCommand(sqlQuery, connection);
            try
            {
                command.Parameters.AddWithValue("@Id", section.Id);
                command.Parameters.AddWithValue("@AcidTtn", section.AcidTtn);
                command.Parameters.AddWithValue("@DensityTtn", section.DensityTtn);
                command.Parameters.AddWithValue("@FatTtn", section.FatTtn);
                command.Parameters.AddWithValue("@ProteinTtn", section.ProteinTtn);
                command.Parameters.AddWithValue("@ThermalResistance", section.ThermalResistance == null ? Convert.DBNull : section.ThermalResistance.Id);
                command.Parameters.AddWithValue("@Farm", section.Farm == null ? Convert.DBNull : section.Farm.Id);
                command.Parameters.AddWithValue("@Section", section.SectionN == null ? Convert.DBNull : section.SectionN.Id);
                command.Parameters.AddWithValue("@CarNumber", section.CarNumber ?? Convert.DBNull);
                command.Parameters.AddWithValue("@TtnVolume", section.TtnVolume);
                command.Parameters.AddWithValue("@Sort", section.Sort == null ? Convert.DBNull : section.Sort.Id);
                command.Parameters.AddWithValue("@AcidLab", section.AcidLab);
                command.Parameters.AddWithValue("@DensityLab", section.DensityLab);
                command.Parameters.AddWithValue("@FatLab", section.FatLab);
                command.Parameters.AddWithValue("@ProteinLab", section.ProteinLab);
                command.Parameters.AddWithValue("@RecommendationRecivePost", section.RecommendationRecivePost == null ? Convert.DBNull : section.RecommendationRecivePost.Id);
                command.Parameters.AddWithValue("@TtnHead", ttn.Id);
                command.Parameters.AddWithValue("@ParentSection", parreSection == null ? Convert.DBNull : parreSection.Id);

                if (section.Id == 0)
                {
                    section.Id = (int)command.ExecuteScalar();
                }
                else
                {
                    command.ExecuteNonQuery();
                }

            }
            catch
            {
            }
        }





        #region Члены IEnumerable<TtnHead>

        public IEnumerator<ITtnHead> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion

        #region Члены IEnumerable

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion
    }
}
