﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using ILIM.Bratsk.Data.MapConfiguration;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;
using ILIM.Bratsk.Infrastructure.DateTimeManager;
using ILIM.Bratsk.Infrastructure.Model;
using RefactorThis.GraphDiff;

namespace ILIM.Bratsk.Data
{
    public class FactoryContext : DbContext, IUnitOfWorkDO
    {
        private readonly IActiveAccountManager _activeAccountManager;
        private readonly IDateTimeManager _dateTimeManager;

        public FactoryContext(IActiveAccountManager activeAccountManager, string connectionString)
            : this(new DateTimeManager(), activeAccountManager, connectionString)
        {
        }

        public FactoryContext(IDateTimeManager dateTimeManager, IActiveAccountManager activeAccountManager,
            string connectionString)
            : base(connectionString)
        {
            _dateTimeManager = dateTimeManager;
            _activeAccountManager = activeAccountManager;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        //public DbSet<Entity> Entities { get; set; }
        public DbSet<ProcontPlanItem> ProcontPlanItems { get; set; }
        public DbSet<Line> Lines { get; set; }
        public DbSet<Printer> Printers { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<UserReportRequestParameters> UsersReportRequestParameters { get; set; }
        public DbSet<AccountData> AccountDatas { get; set; }
        public DbSet<PlanData> PlanData { get; set; }
        public DbSet<FactData> FactData { get; set; }
        public DbSet<ManualPlanData> ManualPlanData { get; set; }
        public DbSet<Command> Command { get; set; }
        public DbSet<Event> Event { get; set; }
        public DbSet<ApplicationSettings> Settings { get; set; }
        public DbSet<ServiceData> ServiceData { get; set; }

        #region Implementation of IUnitOfWork

        public void Save()
        {
            SaveChanges();
        }

        #endregion

        #region Overrides of DbContext

        /// <summary>
        ///     This method is called when the model for a derived context has been initialized, but
        ///     before the model has been locked down and used to initialize the context.  The default
        ///     implementation of this method does nothing, but it can be overridden in a derived class
        ///     such that the model can be further configured before it is locked down.
        /// </summary>
        /// <remarks>
        ///     Typically, this method is called only once when the first instance of a derived context
        ///     is created.  The model for that context is then cached and is for all further instances of
        ///     the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///     property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///     More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///     classes directly.
        /// </remarks>
        /// <param name="modelBuilder">The builder that defines the model for the context being created. </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Ignore<Entity>();
            modelBuilder.Ignore<EntityBase>();

            modelBuilder.Configurations.Add(new LineMapConfiguration());
        }

        #endregion

        public override int SaveChanges()
        {
            var saveTime = _dateTimeManager.GetDateTimeNow().ToUniversalTime();
            var activeAccountID = _activeAccountManager == null
                ? null
                : _activeAccountManager.ActiveAccount == null
                    ? null
                    : _activeAccountManager.ActiveAccount.Account == null
                        ? (Guid?) null
                        : _activeAccountManager.ActiveAccount.Account.ID;
            foreach (
                var entry in
                    ChangeTracker.Entries()
                        .Where(p => p.Entity is Entity)
                        .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            {
                var entityWithVersion = entry.Entity as Entity;
                if (entityWithVersion != null)
                {
                    entityWithVersion.DateTimeUpdate = saveTime;
                    entityWithVersion.UserUpdated = activeAccountID;
                    if (Entry(entityWithVersion).State == EntityState.Added)
                    {
                        entityWithVersion.DateTimeCreate = saveTime;
                        entityWithVersion.UserCreated = activeAccountID;
                    }
                }
            }
            return base.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        #region Implementation of IUnitOfWorkDO

        public IQueryable<T> Get<T>() where T : class, IEntity
        {
            return Set<T>().AsNoTracking();
        }

        public IQueryable<T> GetTracked<T>() where T : class, IEntity
        {
            return Set<T>();
        }

        public T Create<T>(T entity) where T : class, IEntity
        {
            if (Entry(entity).State == EntityState.Detached)
                Set<T>().Add(entity);
            return entity;
        }

        public T Update<T>(T entity) where T : class, IEntity
        {
            Set<T>().Attach(entity);
            var entry = Entry(entity);

            entry.State = EntityState.Modified;
            return entity;
        }

        public T CreateOrUpdate<T>(T entity) where T : class, IEntity
        {
            if (Set<T>().Any(p => p.ID == entity.ID))
            {
                Update(entity);
            }
            else
            {
                Create(entity);
            }
            return entity;
        }

        public void Delete<T>(T entity) where T : class, IEntity
        {
            Set<T>().Attach(entity);
            if (Entry(entity).State != EntityState.Deleted)
                Set<T>().Remove(entity);
        }


        public T Update<T>(T entity, Expression<Func<IUpdateConfiguration<T>, object>> config)
            where T : class, IEntity, new()
        {
            this.UpdateGraph(entity, config);
            return entity;
        }

        #endregion
    }
}