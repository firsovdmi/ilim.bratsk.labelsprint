using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.MapConfiguration
{
    public class EventMapConfiguration : EntityMapConfiguration<Event>
    {
        private const string TableName = "Event";

        public EventMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}