using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace PAG.WBD.Milk.Data.MapConfiguration
{
    public class ReportMapConfiguration : EntityMapConfiguration<Report>
    {
        private const string TableName = "Report";

        public ReportMapConfiguration()
        {
            Ignore(p => p.UserReportRequestParameter);
            HasMany(p => p.UserReportRequestParameters).WithRequired(p => p.Report);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}