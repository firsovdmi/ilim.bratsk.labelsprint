using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.MapConfiguration
{
    public class FactDataMapConfiguration : EntityMapConfiguration<FactData>
    {
        private const string TableName = "FactData";

        public FactDataMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}