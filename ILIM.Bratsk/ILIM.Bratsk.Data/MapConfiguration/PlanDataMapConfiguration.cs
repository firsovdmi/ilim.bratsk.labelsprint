using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.MapConfiguration
{
    public class PlanDataMapConfiguration : EntityMapConfiguration<PlanData>
    {
        private const string TableName = "PlanData";

        public PlanDataMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}