using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.MapConfiguration
{
    public class ApplicationSettingsSettingsMapConfiguration : EntityMapConfiguration<ApplicationSettings>
    {
        private const string TableName = "CommonSettings";

        public ApplicationSettingsSettingsMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}