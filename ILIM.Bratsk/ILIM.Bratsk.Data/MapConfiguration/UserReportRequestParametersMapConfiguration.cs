using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.MapConfiguration
{
    public class UserReportRequestParametersMapConfiguration : EntityMapConfiguration<UserReportRequestParameters>
    {
        private const string TableName = "UserReportRequestParameter";

        public UserReportRequestParametersMapConfiguration()
        {
            Property(p => p.RequestParameters).HasColumnType("xml");
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}