using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.MapConfiguration
{
    public class ManualPlanDataMapConfiguration : EntityMapConfiguration<FactData>
    {
        private const string TableName = "ManualPlanData";

        public ManualPlanDataMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}