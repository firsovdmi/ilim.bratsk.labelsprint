using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.MapConfiguration
{
    public class ProcontPlanItemMapConfiguration : EntityMapConfiguration<ProcontPlanItem>
    {
        private const string TableName = "ProcontPlanItem";

        public ProcontPlanItemMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}