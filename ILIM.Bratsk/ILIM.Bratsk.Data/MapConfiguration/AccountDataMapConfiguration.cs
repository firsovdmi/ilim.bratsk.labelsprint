using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.MapConfiguration
{
    public class AccountDataMapConfiguration : EntityBaseMapConfiguration<AccountData>
    {
        private const string TableName = "AccountData";

        public AccountDataMapConfiguration()
        {
            Property(p => p.UserID)
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("I_UserID_DataID", 0) {IsUnique = true}));
            Property(p => p.DataID)
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("I_UserID_DataID", 1) {IsUnique = true}));
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}