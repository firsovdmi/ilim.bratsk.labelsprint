using ILIM.Bratsk.Domain.Model;
using ILIM.Bratsk.Infrastructure.Model;

namespace ILIM.Bratsk.Data.MapConfiguration
{
    public class LineMapConfiguration : EntityMapConfiguration<Line>
    {
        private const string TableName = "Line";

        public LineMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}