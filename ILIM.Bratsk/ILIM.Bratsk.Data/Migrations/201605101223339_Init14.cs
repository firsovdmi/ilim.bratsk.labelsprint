namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init14 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FactDatas", "CmdSource", c => c.Int());
            AlterColumn("dbo.PlanDatas", "CmdSource", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PlanDatas", "CmdSource", c => c.Int(nullable: false));
            AlterColumn("dbo.FactDatas", "CmdSource", c => c.Int(nullable: false));
        }
    }
}
