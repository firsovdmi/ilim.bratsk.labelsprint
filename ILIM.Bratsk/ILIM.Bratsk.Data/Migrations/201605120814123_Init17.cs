namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init17 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServiceDatas", "MaxBatchCodeYear", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ServiceDatas", "MaxBatchCodeYear");
        }
    }
}
