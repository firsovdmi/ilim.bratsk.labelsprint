namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init30 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Line", "SensorError", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Line", "SensorError");
        }
    }
}
