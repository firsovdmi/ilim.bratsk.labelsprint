namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init8 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "EventID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Events", "EventBegin", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "EventEnd", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "Description", c => c.String());
            DropColumn("dbo.Events", "CmdTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "CmdTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.Events", "Description");
            DropColumn("dbo.Events", "Status");
            DropColumn("dbo.Events", "EventEnd");
            DropColumn("dbo.Events", "EventBegin");
            DropColumn("dbo.Events", "EventID");
        }
    }
}
