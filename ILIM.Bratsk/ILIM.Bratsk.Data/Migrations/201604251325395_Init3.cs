namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FactDatas", "CelluloseName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FactDatas", "CelluloseName");
        }
    }
}
