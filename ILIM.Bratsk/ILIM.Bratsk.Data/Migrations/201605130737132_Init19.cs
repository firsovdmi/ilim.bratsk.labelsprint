namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init19 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FactDatas", "ReprintingCount", c => c.Int(nullable: false));
            AddColumn("dbo.Line", "PlcWeightFlag", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Line", "PlcWeightFlag");
            DropColumn("dbo.FactDatas", "ReprintingCount");
        }
    }
}
