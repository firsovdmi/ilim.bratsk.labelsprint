namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Line", "PrinterErrorIn", c => c.Int(nullable: false));
            AddColumn("dbo.Line", "PrinterErrorOut", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Line", "PrinterErrorOut");
            DropColumn("dbo.Line", "PrinterErrorIn");
        }
    }
}
