namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init26 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Line", "PlcCrossroadLeft", c => c.Int(nullable: false));
            AddColumn("dbo.Line", "PlcCrossroadRight", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Line", "PlcCrossroadRight");
            DropColumn("dbo.Line", "PlcCrossroadLeft");
        }
    }
}
