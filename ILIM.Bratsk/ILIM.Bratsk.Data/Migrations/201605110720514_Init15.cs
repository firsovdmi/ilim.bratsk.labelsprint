namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init15 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Printers", "TurnOff", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Printers", "TurnOff");
        }
    }
}
