namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ManualPlanDatas", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Printers", "SpecModeKipsCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Printers", "SpecModeKipsCount");
            DropColumn("dbo.ManualPlanDatas", "Status");
        }
    }
}
