namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ManualPlanDatas",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FactID = c.Int(nullable: false, identity: true),
                        State = c.Int(nullable: false),
                        BatchCode = c.String(),
                        PackNum = c.Int(nullable: false),
                        Empt = c.Int(nullable: false),
                        LineNum = c.Int(nullable: false),
                        WeightNet = c.Single(nullable: false),
                        WeightBr = c.Single(nullable: false),
                        PlanTime = c.DateTime(nullable: false),
                        WeightTime = c.DateTime(),
                        PackTime = c.DateTime(),
                        FactTime = c.DateTime(nullable: false),
                        CmdID = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ManualPlanDatas");
        }
    }
}
