namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init16 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ServiceDatas",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        MaxBatchCode = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ServiceDatas");
        }
    }
}
