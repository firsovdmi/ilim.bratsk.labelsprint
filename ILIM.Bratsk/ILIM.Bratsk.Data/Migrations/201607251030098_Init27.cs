namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init27 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Printers", "KipaNumberParameterName", c => c.String(maxLength: 255, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Printers", "KipaNumberParameterName");
        }
    }
}
