namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init20 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FactDatas", "WeightBrut", c => c.Single(nullable: false));
            AddColumn("dbo.ManualPlanDatas", "WeightBrut", c => c.Single(nullable: false));
            AddColumn("dbo.PlanDatas", "WeightBrut", c => c.Single());
            DropColumn("dbo.FactDatas", "WeightBr");
            DropColumn("dbo.ManualPlanDatas", "WeightBr");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ManualPlanDatas", "WeightBr", c => c.Single(nullable: false));
            AddColumn("dbo.FactDatas", "WeightBr", c => c.Single(nullable: false));
            DropColumn("dbo.PlanDatas", "WeightBrut");
            DropColumn("dbo.ManualPlanDatas", "WeightBrut");
            DropColumn("dbo.FactDatas", "WeightBrut");
        }
    }
}
