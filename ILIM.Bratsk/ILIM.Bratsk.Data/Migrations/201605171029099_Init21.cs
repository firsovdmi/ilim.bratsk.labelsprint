namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init21 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FactDatas", "PrinterNum", c => c.Int(nullable: false));
            AddColumn("dbo.FactDatas", "WeightNum", c => c.Int(nullable: false));
            AddColumn("dbo.PlanDatas", "PrinterNum", c => c.Int());
            AddColumn("dbo.PlanDatas", "WeightNum", c => c.Int(nullable: false));
            DropColumn("dbo.FactDatas", "LineNum");
            DropColumn("dbo.PlanDatas", "LineNum");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PlanDatas", "LineNum", c => c.Int());
            AddColumn("dbo.FactDatas", "LineNum", c => c.Int(nullable: false));
            DropColumn("dbo.PlanDatas", "WeightNum");
            DropColumn("dbo.PlanDatas", "PrinterNum");
            DropColumn("dbo.FactDatas", "WeightNum");
            DropColumn("dbo.FactDatas", "PrinterNum");
        }
    }
}
