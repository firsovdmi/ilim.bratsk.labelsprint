namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountDatas",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                        DataID = c.String(),
                        Data = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Commands",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CmdID = c.Int(nullable: false),
                        CmdTime = c.DateTime(nullable: false),
                        CmdSource = c.Int(nullable: false),
                        CmdType = c.Int(nullable: false),
                        BatchCode = c.String(),
                        PackFirst = c.Int(nullable: false),
                        PackLast = c.Int(nullable: false),
                        Empt = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CmdID = c.Int(nullable: false),
                        CmdTime = c.DateTime(nullable: false),
                        CmdSource = c.Int(nullable: false),
                        CmdType = c.Int(nullable: false),
                        BatchCode = c.String(),
                        PackFirst = c.Int(nullable: false),
                        PackLast = c.Int(nullable: false),
                        Empt = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FactDatas",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FactID = c.Int(nullable: false, identity: true),
                        State = c.Int(nullable: false),
                        BatchCode = c.String(),
                        PackNum = c.Int(nullable: false),
                        Empt = c.Int(nullable: false),
                        LineNum = c.Int(nullable: false),
                        WeightNet = c.Single(nullable: false),
                        WeightBr = c.Single(nullable: false),
                        PlanTime = c.DateTime(nullable: false),
                        WeightTime = c.DateTime(),
                        PackTime = c.DateTime(),
                        FactTime = c.DateTime(nullable: false),
                        CmdID = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Line",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        LineNumber = c.Int(nullable: false),
                        PlcIpAdress = c.String(),
                        PlcSlot = c.Int(nullable: false),
                        PlcRack = c.Int(nullable: false),
                        PlcDbn = c.Int(nullable: false),
                        PlcStartByte = c.Int(nullable: false),
                        BeforePrinterPosition1 = c.Int(nullable: false),
                        BeforePrinterPosition2 = c.Int(nullable: false),
                        HasPrinter = c.Boolean(nullable: false),
                        PlcOffsetCurrentWeight = c.Int(nullable: false),
                        PlcWeightSetPoint = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PlanDatas",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlanID = c.Int(nullable: false, identity: true),
                        PlanTime = c.DateTime(nullable: false),
                        CmdID = c.Int(nullable: false),
                        BatchCode = c.String(),
                        PackNum = c.Int(nullable: false),
                        Empt = c.Int(nullable: false),
                        LineNum = c.Int(),
                        Status = c.Int(nullable: false),
                        WeightNet = c.Single(),
                        WeightTime = c.DateTime(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Printers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Number = c.Int(nullable: false),
                        IpAdress = c.String(),
                        Port = c.Int(nullable: false),
                        StoredCount = c.Int(),
                        StoredBatchCode = c.String(),
                        Mode = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProcontPlanItems",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProcontId = c.Int(nullable: false),
                        BatchCode = c.String(),
                        PackNum = c.Int(nullable: false),
                        PlanTime = c.DateTime(nullable: false),
                        LineNum = c.Int(nullable: false),
                        Empt = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        RequestType = c.Int(nullable: false),
                        ReportAssembly = c.Binary(),
                        ReportTemplate = c.Binary(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        UserReportRequestParameter_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserReportRequestParameters", t => t.UserReportRequestParameter_ID)
                .Index(t => t.UserReportRequestParameter_ID);
            
            CreateTable(
                "dbo.UserReportRequestParameters",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserID = c.String(),
                        RequestParameters = c.String(),
                        ReportID = c.Guid(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Report_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Reports", t => t.ReportID, cascadeDelete: true)
                .ForeignKey("dbo.Reports", t => t.Report_ID)
                .Index(t => t.ReportID)
                .Index(t => t.Report_ID);
            
            CreateTable(
                "dbo.ApplicationSettings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SystemName = c.String(),
                        UserName = c.String(),
                        Value = c.String(),
                        Description = c.String(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserReportRequestParameters", "Report_ID", "dbo.Reports");
            DropForeignKey("dbo.Reports", "UserReportRequestParameter_ID", "dbo.UserReportRequestParameters");
            DropForeignKey("dbo.UserReportRequestParameters", "ReportID", "dbo.Reports");
            DropIndex("dbo.UserReportRequestParameters", new[] { "Report_ID" });
            DropIndex("dbo.UserReportRequestParameters", new[] { "ReportID" });
            DropIndex("dbo.Reports", new[] { "UserReportRequestParameter_ID" });
            DropTable("dbo.ApplicationSettings");
            DropTable("dbo.UserReportRequestParameters");
            DropTable("dbo.Reports");
            DropTable("dbo.ProcontPlanItems");
            DropTable("dbo.Printers");
            DropTable("dbo.PlanDatas");
            DropTable("dbo.Line");
            DropTable("dbo.FactDatas");
            DropTable("dbo.Events");
            DropTable("dbo.Commands");
            DropTable("dbo.AccountDatas");
        }
    }
}
