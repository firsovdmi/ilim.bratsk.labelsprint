namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init28 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Line", "NoTrackKips", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Line", "NoTrackKips");
        }
    }
}
