namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init29 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Line", "SensorError");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Line", "SensorError", c => c.Int(nullable: false));
        }
    }
}
