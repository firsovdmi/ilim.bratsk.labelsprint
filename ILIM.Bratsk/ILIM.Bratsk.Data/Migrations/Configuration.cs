using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.SqlServer;
using System.Globalization;

namespace ILIM.Bratsk.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<FactoryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            SetSqlGenerator("System.Data.SqlClient", new SqlServerMigrationSqlGeneratorFixed());
            SetSqlGenerator("System.Data.SqlServerCe.4.0", new SqlServerMigrationSqlGeneratorFixed());
        }

        internal class SqlServerMigrationSqlGeneratorFixed : SqlServerMigrationSqlGenerator
        {
            protected override string Generate(DateTime defaultValue)
            {
                var value = defaultValue.Ticks != 0
                    ? defaultValue.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture)
                    : "1753-01-01 00:00:00";

                return string.Format("'{0}'", value);
            }

            protected override void Generate(AddColumnOperation addColumnOperation)
            {
                SetCreatedUtcColumn(addColumnOperation.Column);

                base.Generate(addColumnOperation);
            }

            protected override void Generate(CreateTableOperation createTableOperation)
            {
                SetCreatedUtcColumn(createTableOperation.Columns);

                base.Generate(createTableOperation);
            }

            private static void SetCreatedUtcColumn(IEnumerable<ColumnModel> columns)
            {
                foreach (var columnModel in columns)
                {
                    SetCreatedUtcColumn(columnModel);
                }
            }

            private static void SetCreatedUtcColumn(PropertyModel column)
            {
                if (column.Name == "DateTimeCreate" || column.Name == "DateTimeUpdate")
                {
                    column.DefaultValueSql = "GETUTCDATE()";
                }

                if (column.Name == "IsSystem")
                {
                    column.DefaultValueSql = "0";
                }

                if (column.Name == "IsWaitsPrint")
                {
                    column.DefaultValueSql = "0";
                }
            }
        }
    }
}