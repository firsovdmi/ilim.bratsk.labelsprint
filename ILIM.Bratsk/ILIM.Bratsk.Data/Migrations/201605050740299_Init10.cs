namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init10 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FactDatas", "EventID", c => c.Int(nullable: false));
            AddColumn("dbo.PlanDatas", "EventID", c => c.Int(nullable: false));
            AlterColumn("dbo.Commands", "BatchCode", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.Events", "Description", c => c.String(maxLength: 1000, unicode: false));
            AlterColumn("dbo.Events", "BatchCode", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.FactDatas", "BatchCode", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.ManualPlanDatas", "BatchCode", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.PlanDatas", "BatchCode", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.Printers", "StoredBatchCode", c => c.String(maxLength: 20, unicode: false));
            DropColumn("dbo.Events", "CmdID");
            DropColumn("dbo.FactDatas", "CmdID");
            DropColumn("dbo.PlanDatas", "CmdID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PlanDatas", "CmdID", c => c.Int(nullable: false));
            AddColumn("dbo.FactDatas", "CmdID", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "CmdID", c => c.Int(nullable: false));
            AlterColumn("dbo.Printers", "StoredBatchCode", c => c.String());
            AlterColumn("dbo.PlanDatas", "BatchCode", c => c.String());
            AlterColumn("dbo.ManualPlanDatas", "BatchCode", c => c.String());
            AlterColumn("dbo.FactDatas", "BatchCode", c => c.String());
            AlterColumn("dbo.Events", "BatchCode", c => c.String());
            AlterColumn("dbo.Events", "Description", c => c.String(maxLength: 250, unicode: false));
            AlterColumn("dbo.Commands", "BatchCode", c => c.String());
            DropColumn("dbo.PlanDatas", "EventID");
            DropColumn("dbo.FactDatas", "EventID");
        }
    }
}
