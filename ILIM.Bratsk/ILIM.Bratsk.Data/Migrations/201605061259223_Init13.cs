namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init13 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Line", "WrapperWeight", c => c.Single(nullable: false));
            AddColumn("dbo.Line", "WireWeight", c => c.Single(nullable: false));
            AddColumn("dbo.Printers", "WeightBruttoParameterName", c => c.String(maxLength: 255, unicode: false));
            AddColumn("dbo.Printers", "WeightNettoParameterName", c => c.String(maxLength: 255, unicode: false));
            AddColumn("dbo.Printers", "DateParameterName", c => c.String(maxLength: 255, unicode: false));
            AddColumn("dbo.Printers", "BatchCodeParameterName", c => c.String(maxLength: 255, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Printers", "BatchCodeParameterName");
            DropColumn("dbo.Printers", "DateParameterName");
            DropColumn("dbo.Printers", "WeightNettoParameterName");
            DropColumn("dbo.Printers", "WeightBruttoParameterName");
            DropColumn("dbo.Line", "WireWeight");
            DropColumn("dbo.Line", "WrapperWeight");
        }
    }
}
