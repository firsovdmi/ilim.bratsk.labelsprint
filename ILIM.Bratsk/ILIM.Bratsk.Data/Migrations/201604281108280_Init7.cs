namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Line", "PrinterResetErrorIn", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Line", "PrinterResetErrorIn");
        }
    }
}
