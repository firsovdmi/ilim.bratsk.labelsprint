namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init23 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Line", "UseCustomWeight", c => c.Boolean(nullable: false));
            DropColumn("dbo.Line", "UseCutomWeight");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Line", "UseCutomWeight", c => c.Boolean(nullable: false));
            DropColumn("dbo.Line", "UseCustomWeight");
        }
    }
}
