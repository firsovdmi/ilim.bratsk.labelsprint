namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init22 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Line", "CustomWeight", c => c.Single(nullable: false));
            AddColumn("dbo.Line", "UseCutomWeight", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Line", "UseCutomWeight");
            DropColumn("dbo.Line", "CustomWeight");
        }
    }
}
