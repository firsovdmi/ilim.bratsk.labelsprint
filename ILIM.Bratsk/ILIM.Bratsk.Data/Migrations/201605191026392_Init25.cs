namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init25 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Line", "AdditiveForNetto", c => c.Single(nullable: false));
            AddColumn("dbo.Line", "AdditiveForBrutto", c => c.Single(nullable: false));
            DropColumn("dbo.Line", "WrapperWeight");
            DropColumn("dbo.Line", "WireWeight");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Line", "WireWeight", c => c.Single(nullable: false));
            AddColumn("dbo.Line", "WrapperWeight", c => c.Single(nullable: false));
            DropColumn("dbo.Line", "AdditiveForBrutto");
            DropColumn("dbo.Line", "AdditiveForNetto");
        }
    }
}
