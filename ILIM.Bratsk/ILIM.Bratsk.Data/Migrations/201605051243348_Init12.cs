namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Printers", "MainLabel", c => c.String(maxLength: 255, unicode: false));
            AddColumn("dbo.Printers", "EmptyLabel", c => c.String(maxLength: 255, unicode: false));
            AddColumn("dbo.Printers", "RequestLabel", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Printers", "RequestLabel");
            DropColumn("dbo.Printers", "EmptyLabel");
            DropColumn("dbo.Printers", "MainLabel");
        }
    }
}
