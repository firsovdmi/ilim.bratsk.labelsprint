namespace ILIM.Bratsk.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "EventType", c => c.Int(nullable: false));
            AddColumn("dbo.FactDatas", "CmdSource", c => c.Int(nullable: false));
            DropColumn("dbo.Events", "CmdType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "CmdType", c => c.Int(nullable: false));
            DropColumn("dbo.FactDatas", "CmdSource");
            DropColumn("dbo.Events", "EventType");
        }
    }
}
