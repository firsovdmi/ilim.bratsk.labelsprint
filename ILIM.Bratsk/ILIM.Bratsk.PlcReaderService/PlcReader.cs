﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Castle.Facilities.TypedFactory;
using Castle.Facilities.WcfIntegration;
using Castle.Facilities.WcfIntegration.Behaviors;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using PAG.WBD.Milk.Domain.RemoteFacade;
using PAG.WBD.Milk.Infrastructure.AAA;
using PAG.WBD.Milk.Infrastructure.AAA.AuthenticationClasses;
using PAG.WBD.Milk.Infrastructure.Log;
using PAG.WBD.Milk.PlcScaners;
using PAG.WBD.Milk.PlcScaners.CIP;
using PAG.WBD.Milk.PlcScaners.ReciveMilk;
using PAG.WBD.Milk.Service;
using Component = Castle.MicroKernel.Registration.Component;

namespace PAG.WBD.Milk.PlcReaderService
{
    public partial class PlcReader : ServiceBase
    {
        private BufferScaner _bufferScaner;
        private CipScaner _cipHandler;
        private ReciveMilkScaner _reciveHandler;
        private WindsorContainer _container;
        public PlcReader()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            Thread.Sleep(10000);
            OnStart(null);
            Thread.Sleep(Timeout.Infinite);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                _bufferScaner = new BufferScaner(PLCSettings.Config.PLC.IP, (int)PLCSettings.Config.PLC.Rack, (int)PLCSettings.Config.PLC.Slot, (int)PLCSettings.Config.Buffer.StartAdress, (int)PLCSettings.Config.Buffer.DbN, (int)PLCSettings.Config.Buffer.BufferSize, (int)PLCSettings.Config.Buffer.ScanIntervalInMilliseconds, PLCSettings.Config.ConnectionString.Value);
                _bufferScaner.StartScan();
            }
            catch { }

            try
            {
                _container = new WindsorContainer();
                _container.AddFacility<TypedFactoryFacility>();
                _container.AddFacility<WcfFacility>();

                _container.Register(Component.For<MessageLifecycleBehavior>());
                _container.Register(Component.For<LifestyleClientMessageAction>());
                _container.Register(Component.For<IActiveAccountManager>().ImplementedBy<ActiveAcountManagerClient>().OverridesExistingRegistration());

                _container.Register(Component.For(typeof(IChannelFactoryBuilder<>)).ImplementedBy(typeof(DefaultChannelFactoryBuilder<>)).IsDefault());

                _container.Register(Types.FromAssemblyContaining<IMilkReceivingTaskService>().InSameNamespaceAs<IMilkReceivingTaskService>().Configure(p =>
                {
                    p.AsWcfClient(new DefaultClientModel()
                    {
                        Endpoint = WcfEndpoint
                            .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                            .At(string.Format(@"net.tcp://" + CommonSettings.Config.LocalMachine.Name + ":8082/service/{0}", p.Implementation.Name.Substring(1)))
                    });
                }));

                _container.Register(Component.For<IAAAServiceRemoteFasade>().AsWcfClient(new DefaultClientModel
                {
                    Endpoint = WcfEndpoint
                        .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                        .At(string.Format(@"net.tcp://" + CommonSettings.Config.LocalMachine.Name + ":8082/service/AAAServiceRemoteFasade"))
                }));
                _container.Register(Component.For<IReadLogRemoteService>().AsWcfClient(new DefaultClientModel
                {
                    Endpoint = WcfEndpoint
                        .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                        .At(string.Format(@"net.tcp://" + CommonSettings.Config.LocalMachine.Name + ":8082/service/ReadLogRemoteService"))
                }));

                _container.Register(Component.For<Loginer>().LifestyleTransient());
                var loginer = _container.Resolve<Loginer>();
                loginer.Login();

                _container.Register(Component.For<CipScaner>().DependsOn((Dependency.OnValue("scanIntervalInMilliseconds", (int)PLCSettings.Config.Buffer.ScanIntervalInMilliseconds))));
                _container.Register(Component.For<ReciveMilkScaner>().DependsOn((Dependency.OnValue("scanIntervalInMilliseconds", (int)PLCSettings.Config.Buffer.ScanIntervalInMilliseconds))));
                _cipHandler = _container.Resolve<CipScaner>();
                _reciveHandler = _container.Resolve<ReciveMilkScaner>();
                _reciveHandler.StartScan();
                _cipHandler.StartScan();
            }
            catch { }
        }

        protected override void OnStop()
        {
            _bufferScaner.StopScan();
            _bufferScaner = null;
            _cipHandler.StopScan();
            _cipHandler = null;
            _reciveHandler.StopScan();
            _reciveHandler = null;
        }
    }
}
