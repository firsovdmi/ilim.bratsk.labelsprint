﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.PlcScaners
{
    public abstract class BaseCyclicHandler
    {
        protected Timer _timer;
        protected bool _fOff = false;
        protected readonly object _lock = new object();
        protected int _scanIntervalInMilliseconds;

        public void StartScan()
        {
            Initialize();
            _timer = new Timer(Callback, null, _scanIntervalInMilliseconds, Timeout.Infinite);
        }

        public void StopScan()
        {
            lock (_lock)
            {
                _fOff = true;
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
            }
        }

        private void Callback(Object state)
        {
            try
            {
                Handler();
            }
            catch { }

            lock (_lock)
            {
                if (!_fOff) _timer.Change(_scanIntervalInMilliseconds, Timeout.Infinite);
            }
        }
        /// <summary>
        /// Запуск действий асинхронно
        /// </summary>
        /// <param name="action"></param>
        protected  void RunAsync(Action action)
        {
             Task.Factory.StartNew(action);
        }
        protected abstract void Handler();

        protected virtual void Initialize()
        {
        }
    }
}
