﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PAG.WBD.Milk.PlcScaners
{
    static class ConnectionStringClass
    {
        public static string Value { get; set; }
    }

    public static class StaticClasses
    {
        public static T GetValue<T>(this SqlDataReader reader, string fieldName, T defaultVal = default(T))
        {
            return reader.IsDBNull(reader.GetOrdinal(fieldName))
                ? defaultVal
                : (T)Convert.ChangeType(reader[fieldName], typeof(T));
        }

        public static T GetValue<T>(this SqlDataReader reader, int fieldNumber, T defaultVal = default(T))
        {
            return reader.IsDBNull(fieldNumber) ? defaultVal : (T)Convert.ChangeType(reader[fieldNumber], typeof(T));
        }
    }

    public static class Tools
    {
        public static void Log(string msg, string group = "Без группы")
        {
            try
            {
                const string sqlText = @" INSERT INTO [SystemLogs] ([Date] ,[Description] ,[MessageGroup]) 
                                                VALUES (@Date, @Description, @MessageGroup)";
                using (var connection = new SqlConnection(ConnectionStringClass.Value))
                {

                    var command = new SqlCommand(sqlText, connection);
                    connection.Open();
                    command.Parameters.AddWithValue("@Date", DateTime.Now);
                    command.Parameters.AddWithValue("@Description", msg);
                    command.Parameters.AddWithValue("@MessageGroup", group);
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch
            {
            }
        }

    }
}
