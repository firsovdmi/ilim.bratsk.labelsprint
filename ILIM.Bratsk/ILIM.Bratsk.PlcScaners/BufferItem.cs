﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Domain.Model;
using PAG.WBD.Milk.Domain.Model.PlcClasses;
using PAG.WBD.Milk.Domain.Model.PlcClasses.CommonClasses;
using PAG.WBD.Milk.Domain.Model.PlcClasses.Jobs;
using PAG.WBD.Milk.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace PAG.WBD.Milk.PlcScaners
{
    class BufferItem
    {
        public BufferItem(PAG.WBD.Milk.Domain.Model.PlcClasses.CommonClasses.Adress adress, int dbn)
        {
            var tmpAdress=new Adress(adress);
            Busy = new PlcProperty<bool>(tmpAdress, dbn);
            tmpAdress += 8*2;
            ActionType = new PlcProperty<Int16>(tmpAdress, dbn);
            tmpAdress += 8 * 2;
            RecordId = new PlcProperty<Int32>(tmpAdress, dbn);
            tmpAdress += 8 * 4;
            ParentRecordId = new PlcProperty<Int32>(tmpAdress, dbn);
            tmpAdress += 8 * 4;
            Value1 = new PlcProperty<float>(tmpAdress, dbn);
            tmpAdress += 8 * 4;
            Value2 = new PlcProperty<float>(tmpAdress, dbn);
            tmpAdress += 8 * 4;
            Value3 = new PlcProperty<float>(tmpAdress, dbn);
            tmpAdress += 8 * 4;
            Value4 = new PlcProperty<float>(tmpAdress, dbn);
            tmpAdress += 8 * 4;
            Value5 = new PlcProperty<float>(tmpAdress, dbn);
            tmpAdress += 8 * 4;
            ModelId = new PlcProperty<Int16>(tmpAdress, dbn);
            tmpAdress += 8 * 2;
            NodeId = new PlcProperty<Int16>(tmpAdress, dbn);
            tmpAdress += 8 * 2;
            PortInN = new PlcProperty<Int16>(tmpAdress, dbn);
            tmpAdress += 8 * 2;
            PortOutN = new PlcProperty<Int16>(tmpAdress, dbn);
            tmpAdress += 8 * 2;
            RecordDateTime = new PlcProperty<DateTime>(tmpAdress, dbn);
        }
        public PlcProperty<bool> Busy { get; set; }
        public PlcProperty<Int16> ActionType { get; set; }
        public PlcProperty<Int32> RecordId { get; set; }
        public PlcProperty<Int32> ParentRecordId { get; set; }
        public PlcProperty<Int16> ModelId { get; set; }
        public PlcProperty<Int16> NodeId { get; set; }
        public PlcProperty<Int16> PortInN { get; set; }
        public PlcProperty<Int16> PortOutN { get; set; }
        public PlcProperty<float> Value1 { get; set; }
        public PlcProperty<float> Value2 { get; set; }
        public PlcProperty<float> Value3 { get; set; }
        public PlcProperty<float> Value4 { get; set; }
        public PlcProperty<float> Value5 { get; set; }
        public PlcProperty<DateTime> RecordDateTime { get; set; }

        private IEnumerable<ReadDirectJob> _allReadJobs;

        public PlcBuffer AsPlcBuffer
        {
            get
            {
                return new PlcBuffer
                    {
                        ActionType = ActionType.Value,
                        RecordId = RecordId.Value,
                        ParentRecordId=ParentRecordId.Value,
                        ModelId=ModelId.Value,
                        NodeId=NodeId.Value,
                        PortInN=PortInN.Value,
                        PortOutN = PortOutN.Value,
                        Value1 = Value1.Value,
                        Value2 = Value2.Value,
                        Value3 = Value3.Value,
                        Value4 = Value4.Value,
                        Value5 = Value5.Value,
                        RecordDateTime=RecordDateTime.Value
                    };
            }
        }

        public IEnumerable<ReadDirectJob> AllReadJobs
        {
            get
            {
                if (_allReadJobs == null)
                {
                    var rJobs= new List<ReadDirectJob>
                    {
                        Busy.ReadJobs,
                        ActionType.ReadJobs,
                        RecordId.ReadJobs,
                        ParentRecordId.ReadJobs,
                        ModelId.ReadJobs,
                        NodeId.ReadJobs,
                        PortInN.ReadJobs,
                        PortOutN.ReadJobs,
                        Value1.ReadJobs,
                        Value2.ReadJobs,
                        Value3.ReadJobs,
                        Value4.ReadJobs,
                        Value5.ReadJobs,
                        RecordDateTime.ReadJobs
                    };
                    _allReadJobs = rJobs;
                }
                return _allReadJobs;
            }
        }


        public void ReciveData(BatchDirectJobs rJobs)
        {
            if (rJobs != null)
            {
                Busy.FindValue(rJobs);
                ActionType.FindValue(rJobs);
                RecordId.FindValue(rJobs);
                ParentRecordId.FindValue(rJobs);
                ModelId.FindValue(rJobs);
                NodeId.FindValue(rJobs);
                PortInN.FindValue(rJobs);
                PortOutN.FindValue(rJobs);
                Value1.FindValue(rJobs);
                Value2.FindValue(rJobs);
                Value3.FindValue(rJobs);
                Value4.FindValue(rJobs);
                Value5.FindValue(rJobs);
                RecordDateTime.FindValue(rJobs);
            }
        }
    }
}
