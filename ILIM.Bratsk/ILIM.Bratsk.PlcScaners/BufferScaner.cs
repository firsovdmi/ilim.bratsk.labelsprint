﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using PAG.WBD.Milk.Domain.Model;
using PAG.WBD.Milk.Domain.Model.PlcClasses.CommonClasses;
using PAG.WBD.Milk.Domain.Model.PlcClasses.Jobs;
using PAG.WBD.Milk.PlcScaners.CIP;
using PAG.WBD.Milk.PlcScaners.ReciveMilk;
using PAG.WBD.Milk.Domain.RemoteFacade;
using PAG.WBD.Milk.Service.Services;

namespace PAG.WBD.Milk.PlcScaners
{
    public class BufferScaner : BaseCyclicHandler
    {
        readonly PlcAcessService _directConnector;
        private readonly Adress _startAdress;
        private readonly int _dbN;
        private readonly int _bufferSize;
        readonly List<BufferItem> _buffer = new List<BufferItem>();
        private string _connectionString;

        private string _sqlInsertQuery = @"INSERT INTO [dbo].[PlcBuffer]
           (
            [ID]                     
           ,[PlcId]                  
           ,[ActionType]             
           ,[RecordId]               
           ,[ParentRecordId]         
           ,[ModelId]                
           ,[NodeId]                 
           ,[PortInN]                
           ,[PortOutN]               
           ,[RecordDateTime]         
           ,[Value1]                 
           ,[Value2]                 
           ,[Value3]                 
           ,[Value4]                 
           ,[Value5]                 
           ,[Status]                 
           ,[Device]                 
           ,[IsDeleted]
           ,[Name]                   
           ,[UserCreated]
           ,[UserUpdated]
           ,[IsSystem]
           ,[DateTimeCreate]
           ,[DateTimeUpdate]
    )
    VALUES
    (
           @ID,
           @PlcId,
           @ActionType,
           @RecordId,
           @ParentRecordId,
           @ModelId,
           @NodeId,
           @PortInN,
           @PortOutN,
           @RecordDateTime,
           @Value1,
           @Value2,
           @Value3,
           @Value4,
           @Value5,
            0,
            '1',
            0,
            null,
            null,
            null,
            0,
           @DateTimeCreate,
           @DateTimeUpdate
    )";

        public BufferScaner(string ip, int rack, int slot, int startAdress, int dbN, int bufferSize, int scanIntervalInMilliseconds, string connectionString)
        {
            _directConnector = new PlcAcessService(ip, rack, slot);
            _startAdress = new Adress(startAdress * 8);
            _dbN = dbN;
            _bufferSize = bufferSize;
            _scanIntervalInMilliseconds = scanIntervalInMilliseconds;
            _connectionString = connectionString;
        }

        protected override void Handler()
        {
            var allReadJobs = new BatchDirectJobs(_buffer.SelectMany(p => p.AllReadJobs));
            allReadJobs = _directConnector.ProcessJobs(allReadJobs);
            foreach (var p in _buffer)
            {
                p.ReciveData(allReadJobs);
            }
            var busyItems = _buffer.Where(p => p.Busy.Value);
            var bufferItems = busyItems as IList<BufferItem> ?? busyItems.ToList();
            if (bufferItems.Any())
            {
                WriteDataToDb(bufferItems.Select(p => p.AsPlcBuffer));
                foreach (var p in bufferItems)
                {
                    p.Busy.Value = false;
                }
                var resetBusyJobs = new BatchDirectJobs(bufferItems.Select(p => p.Busy.WriteJobs));
                _directConnector.ProcessJobs(resetBusyJobs);
            }
        }

        protected override void Initialize()
        {
            var adress = new Adress(_startAdress);
            for (int i = 0; i < _bufferSize; i++)
            {
                _buffer.Add(new BufferItem(adress, _dbN));
                adress += 48 * 8;
            }
        }

        void WriteDataToDb(IEnumerable<PlcBuffer> items)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(_sqlInsertQuery, connection);
                connection.Open();
                foreach (var p in items)
                {
                    command.Parameters.AddWithValue("@ActionType", p.ActionType);
                    command.Parameters.AddWithValue("@RecordID", p.RecordId);
                    command.Parameters.AddWithValue("@ParentRecordID", p.ParentRecordId);
                    command.Parameters.AddWithValue("@ModelID", p.ModelId);
                    command.Parameters.AddWithValue("@NodeID", p.NodeId);
                    command.Parameters.AddWithValue("@PortInN", p.PortInN);
                    command.Parameters.AddWithValue("@PortOutN", p.PortOutN);
                    command.Parameters.AddWithValue("@RecordDateTime", p.RecordDateTime);
                    command.Parameters.AddWithValue("@Value1", GetFloatValue(p.Value1));
                    command.Parameters.AddWithValue("@Value2", GetFloatValue(p.Value2));
                    command.Parameters.AddWithValue("@Value3", GetFloatValue(p.Value3));
                    command.Parameters.AddWithValue("@Value4", GetFloatValue(p.Value4));
                    command.Parameters.AddWithValue("@Value5", GetFloatValue(p.Value5));
                    command.Parameters.AddWithValue("@PlcId", p.RecordId);
                    command.Parameters.AddWithValue("@ID", Guid.NewGuid());
                    command.Parameters.AddWithValue("@DateTimeCreate", DateTime.Now);
                    command.Parameters.AddWithValue("@DateTimeUpdate", DateTime.Now);
                    command.ExecuteNonQuery();
                    command.Parameters.Clear();
                }
                connection.Close();
            }
        }

        float GetFloatValue(float value)
        {
            return value > 0.001 && !float.IsInfinity(value) ? value : (float)0.0;
        }
    }
}
