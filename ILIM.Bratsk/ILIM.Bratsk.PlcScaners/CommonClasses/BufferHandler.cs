﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.PlcScaners.CIP;

namespace PAG.WBD.Milk.PlcScaners.CommonClasses
{
    abstract class BufferHandler
    {
        protected abstract int MinActionType { get; }
        protected abstract int MaxActionType { get; }

        public void Process()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStringClass.Value))
                {
                    connection.Open();
                    var command =
                        new SqlCommand(
                            "SELECT ID ,ActionType ,RecordID ,ParentRecordID ,ModelID ,NodeID ,PortInN ,PortOutN ,RecordDateTime ,Value1 ,Value2 ,Value3 ,Value4 ,Value5 ,Status ,Device   FROM PLCBuffer WHERE Status<>1 AND ActionType>=" + MinActionType + " AND ActionType<=" + MaxActionType,
                            connection);

                    SqlDataReader reader = command.ExecuteReader();
                    var newItems = new List<DictionaryItem>();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            int currentId = 0;
                            try
                            {
                                currentId = reader.GetValue<int>(0);
                                HandleRecord(new PlcRecord
                                {
                                    Id = reader.GetValue<int>(0),
                                    ActionType = reader.GetValue<int>(1),
                                    RecordId = reader.GetValue<int>(2),
                                    ParentRecordId = reader.GetValue<int>(3),
                                    ModelId = reader.GetValue<int>(4),
                                    NodeId = reader.GetValue<int>(5),
                                    PortInN = reader.GetValue<int>(6),
                                    PortOutN = reader.GetValue<int>(7),
                                    RecordDateTime = reader.GetValue<DateTime>(8),
                                    Value1 = reader.GetValue<float>(9),
                                    Value2 = reader.GetValue<float>(10),
                                    Value3 = reader.GetValue<float>(11),
                                    Value4 = reader.GetValue<float>(12),
                                    Value5 = reader.GetValue<float>(13)
                                });
                            }
                            catch (Exception ex)
                            {
                                string msg = string.Format("Запись PLCBuffer {0}", currentId);
                                msg = msg + "       " + ex.Message;
                                Tools.Log(msg, "Ошибка обработки PLCBuffer");
                            }

                        }
                    }
                }
            }
            catch
            {
            }
        }

        protected void SetStatusPlcBuffer(SqlCommand command, PlcRecord record)
        {
            command.CommandText = "UPDATE PLCBuffer SET Status=1 WHERE ID=@Id";
            command.Parameters.AddWithValue("@Id", record.Id);
            command.ExecuteNonQuery();
            command.Parameters.Clear();
        }

        protected abstract void HandleRecord(PlcRecord plcRecord);

        protected class PlcRecord
        {
            public int Id { get; set; }
            public int ActionType { get; set; }
            public int RecordId { get; set; }
            public int ParentRecordId { get; set; }
            public int ModelId { get; set; }
            public int NodeId { get; set; }
            public int PortInN { get; set; }
            public int PortOutN { get; set; }
            public float Value1 { get; set; }
            public float Value2 { get; set; }
            public float Value3 { get; set; }
            public float Value4 { get; set; }
            public float Value5 { get; set; }
            public DateTime RecordDateTime { get; set; }
        }
    }
}
