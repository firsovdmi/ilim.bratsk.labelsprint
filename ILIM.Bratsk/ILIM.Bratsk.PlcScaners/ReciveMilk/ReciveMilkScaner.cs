﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Domain.Model;
using PAG.WBD.Milk.Domain.RemoteFacade;
using PAG.WBD.Milk.PlcScaners.CIP;

namespace PAG.WBD.Milk.PlcScaners.ReciveMilk
{
    public class ReciveMilkScaner : BaseCyclicHandler
    {
        private IPlcBufferService _plcBuffer;

        public ReciveMilkScaner(IPlcBufferService plcBuffer, int scanIntervalInMilliseconds)
        {
            _plcBuffer = plcBuffer;
            _scanIntervalInMilliseconds = scanIntervalInMilliseconds;
        }

        protected override void Handler()
        {
            _plcBuffer.HandleReciveRecords();
        }
    }
}
