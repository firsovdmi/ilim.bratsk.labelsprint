﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PAG.WBD.Milk.PlcScaners.CIP
{
    class Dictionary : IEnumerable<DictionaryItem>
    {
        private List<DictionaryItem> _items;

        public void Add(DictionaryItem item)
        {
            _items.Add(item);
        }

        public void AddRange(IEnumerable<DictionaryItem> items)
        {
            _items.AddRange(items);
        }

        public DictionaryItem FindById(int id)
        {
            return _items.FirstOrDefault(p => p.Id == id);
        }

        public void InitFromDb(string tabelName)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionStringClass.Value))
                {
                    connection.Open();
                    var command =
                        new SqlCommand(
                            "SELECT Id ,Name ,PlcN   FROM " + tabelName + " WHERE IsDeleted<>1",
                            connection);

                    SqlDataReader reader = command.ExecuteReader();
                    _items = new List<DictionaryItem>();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            _items.Add( new DictionaryItem
                            {
                                Id = reader.GetValue<int>(0),
                                Name = reader.GetValue<string>(1),
                                PlcN = reader.GetValue<int>(2)
                            });
                        }
                    }
                }
            }
            catch
            {
            }
        }


        #region Члены IEnumerable<DictionaryItem>

        public IEnumerator<DictionaryItem> GetEnumerator()
        {
            return _items.GetEnumerator();
        }
        #endregion

        #region Члены IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion
    }
}
