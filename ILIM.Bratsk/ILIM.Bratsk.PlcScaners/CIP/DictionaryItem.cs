﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PAG.WBD.Milk.PlcScaners.CIP
{
    class DictionaryItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PlcN { get; set; }
        public bool IsDeleted { get; set; }
    }
}
