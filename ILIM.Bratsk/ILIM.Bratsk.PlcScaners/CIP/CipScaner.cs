﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PAG.WBD.Milk.Domain.Model;
using PAG.WBD.Milk.Domain.Model.PlcClasses.CommonClasses;
using PAG.WBD.Milk.Domain.Model.PlcClasses.Jobs;
using PAG.WBD.Milk.Domain.RemoteFacade;
using PAG.WBD.Milk.Infrastructure.Model;

namespace PAG.WBD.Milk.PlcScaners.CIP
{
    public class CipScaner : BaseCyclicHandler
    {
        private IPlcBufferService _plcBuffer;

        public CipScaner(IPlcBufferService plcBuffer,  int scanIntervalInMilliseconds)
        {
            _plcBuffer = plcBuffer;
            _scanIntervalInMilliseconds = scanIntervalInMilliseconds;
        }

        protected override void Handler()
        {
            _plcBuffer.HandleCipRecords();
        }
    }
}
