﻿using PAG.WBD.Milk.Domain.Infrastructure.AAA;
using PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    public interface IAuthenticatiorRemoteFacade
    {

        AuthenticationResult Login(LoginInfo loginInfo);
        AuthenticationResult Logoff(AuthenticationResult loginInfo);
        CheckLoginResult CheckLogin(Guid token, string memberName);
        CheckLoginResult CheckWindowsLogin(string userName, string memberName);
        void SaveLogin(FullLoginInfo login);
        void SaveLogins(IEnumerable<FullLoginInfo> logins);
        void ChangePassword(FullLoginInfo login);
        IEnumerable<FullLoginInfo> GetAllLogins();
        void RemoveLogins(IEnumerable<FullLoginInfo> logins);
        void AddLog(int loginId, string memberName);
        List<AcessGroup> GetAcessGroups();
        void SaveAcessGroups(IEnumerable<AcessGroup> items);
        void DeleteAcessGroups(IEnumerable<AcessGroup> items);
        void AssignAcessGroupsToUser(FullLoginInfo login, IEnumerable<AcessGroup> acessGroups);
        void AssignAcessGroupsToMembers(Members members);
        Members GetMembers();
        void AddMember(MemberItem item);
        List<MembersClass> GetMemberClasses();
        IEnumerable<MessageItem> GetMessageItems(MessageQuery query);
        List<string> GetMessagesTypes();
    }
}
