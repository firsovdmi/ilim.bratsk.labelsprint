﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;
using PAG.WBD.Milk.WCFAuthentication.ReflectionParser;
using WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    [AuthentificationClass("Аутентификация", "Управления правами доступа пользователей")]
    [ServiceContract]
    public interface IAuthenticatiorService
    {
        [AuthentificationMember("Вход пользователя")]
        [OperationContract]
        AuthenticationResult Login(LoginInfo loginInfo);
        [AuthentificationMember("Выход пользователя")]
        [OperationContract]
        AuthenticationResult Logoff(AuthenticationResult loginInfo);
        [OperationContract]
        CheckLoginResult CheckLogin(Guid token, string memberName);
        [OperationContract]
        CheckLoginResult CheckWindowsLogin(string userName, string memberName);
        [AuthentificationMember(@"Добавление/изменение пользователя")]
        [OperationContract]
        void SaveLogin(FullLoginInfo login);
        [AuthentificationMember(@"Добавление/изменение пользователей")]
        [OperationContract]
        void SaveLogins(IEnumerable<FullLoginInfo> logins);
        [AuthentificationMember("Изменить пароль пользователя")]
        [OperationContract]
        void ChangePassword(FullLoginInfo login);
        [OperationContract]
        IEnumerable<FullLoginInfo> GetAllLogins();
        [AuthentificationMember("Удалить пользователей")]
        [OperationContract]
        void RemoveLogins(IEnumerable<FullLoginInfo> logins);
        [OperationContract]
        void AddLog(int loginId, string memberName);
        [OperationContract]
        List<AcessGroup> GetAcessGroups();
        [AuthentificationMember("Добавить группу доступа")]
        [OperationContract]
        void AddAcessGroups(IEnumerable<AcessGroup> items);
        [AuthentificationMember("Удалить группу доступа")]
        [OperationContract]
        void DeleteAcessGroups(IEnumerable<AcessGroup> items);
        [AuthentificationMember("Назначить группы пользователю")]
        [OperationContract]
        void AssignAcessGroupsToUser(FullLoginInfo login, IEnumerable<AcessGroup> acessGroups);
        [AuthentificationMember("Назначить группы действию")]
        [OperationContract]
        void AssignAcessGroupsToMembers(Members members);
        [OperationContract]
        Members GetMembers();
        [OperationContract]
        void AddMember(MemberItem item);
        [OperationContract]
        List<MembersClass> GetMemberClasses();
        [OperationContract]
        IEnumerable<MessageItem> GetMessageItems(MessageQuery query);
        [OperationContract]
        List<string> GetMessagesTypes();
    }
}
