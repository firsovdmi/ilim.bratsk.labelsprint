﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;
using WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    public interface IStorageLogins
    {
        void SaveLogin(FullLoginInfo login);
        void SaveLogins(IEnumerable<FullLoginInfo> logins);
        void ChangePassword(FullLoginInfo login);
        void RemoveLogins(IEnumerable<FullLoginInfo> logins);
        FullLoginInfo GetLoginFullInfo(LoginInfo loginInfo);
        Members GetMembers();
        IEnumerable<FullLoginInfo> GetAllLogins();
        void AddLogItem(int loginId, int memberId);
        List<AcessGroup> GetAcessGroups();
        void SaveAcessGroups(IEnumerable<AcessGroup> items);
        void DeleteAcessGroups(IEnumerable<AcessGroup> items);
        void AssignAcessGroupsToUser(FullLoginInfo login, IEnumerable<AcessGroup> acessGroups);
        void AssignAcessGroupsToMembers(Members members);
        void AddMember(MemberItem item);
        List<MembersClass> GetMemberClasses();
        IEnumerable<MessageItem> GetMessageItems(MessageQuery query);
        List<string> GetMessagesTypes();
    }
}
