﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    public class MembersClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CustomName { get; set; }
        public string Description { get; set; }
    }
}
