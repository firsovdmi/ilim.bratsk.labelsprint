﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class AuthenticatiorFactory : IAuthenticatiorFactory
    {
        //#region Singletone Implementation


        //private static object _lockFlag = new object();

        ///// <summary>
        ///// Экземпляр синглтона
        ///// </summary>
        //private static Authenticatior instance = null;

        ///// <summary>
        ///// Get аксессор к экземпляру синглтона
        ///// </summary>
        //public static Authenticatior Instance
        //{
        //    get
        //    {
        //        if (instance != null)
        //            return instance;

        //        lock (_lockFlag)
        //        {
        //            instance = new Authenticatior();
        //            return instance;
        //        }
        //    }
        //}

        //private Authenticatior()
        //{
        //}

        ///// <summary>
        ///// Cброс коллекции
        ///// </summary>
        //public static void ResetInstance()
        //{
        //    instance = null;
        //}


        //#endregion

        private Members _acessMembers;
        internal Members AcessMembers
        {
            get
            {
                if (_acessMembers == null)
                {
                    _acessMembers = StorageInterface.GetMembers();
                }
                return _acessMembers;
            }
        }

        public string ConnectionString
        {
            get
            {
                return StorageDBManager.Instance.ConnectionString;
            }
            set
            {
                StorageDBManager.Instance.ConnectionString = value;
            }
        }

        List<ActiveLoginItem> _loginList = new List<ActiveLoginItem>();

        public AuthenticationResult Login(LoginInfo loginInfo)
        {
            AuthenticationResult result = new AuthenticationResult();
            FullLoginInfo loginItem = StorageDBManager.Instance.GetLoginFullInfo(loginInfo);
            if (loginItem == null)
            {
                result.IsSucces = false;
                result.Message = "Комбинация логина и пароля не найдена";
                return result;
            }

            var _existLogin = _loginList.FirstOrDefault(p => p.Login == loginItem.Login);
            if (_existLogin == null)
            {
                _loginList.Add(new ActiveLoginItem
                    {
                        Id=loginItem.Id,
                        Login = loginItem.Login,
                        Password = loginItem.Password,
                        AutoLogoffTime = loginItem.AutoLogoffTime,
                        LoginTime = DateTime.Now,
                        Token = result.Token = Guid.NewGuid(),
                        AcessGroups=loginItem.AcessGroups,
                    });
                AddLog(loginItem.Id, "Login");
            }
            else
            {
                _existLogin.AutoLogoffTime = loginItem.AutoLogoffTime;
                result.Token = _existLogin.Token;
                _existLogin.LoginTime = DateTime.Now;
            }
            result.AllowedMembers = AcessMembers.GetAllowedMembers(loginItem.AcessGroups).Select(p=>p.Name).ToList();
            result.IsSucces = true;
            result.Message = "Авторизация выполнена";
            return result;
        }

        public AuthenticationResult Logoff(AuthenticationResult loginInfo)
        {
            AuthenticationResult result = new AuthenticationResult();
            var login = _loginList.FirstOrDefault(p => p.Token == loginInfo.Token);
            if (login == null)
            {
                result.IsSucces = false;
                result.Message = "Пользователь не зарегистрирован";
                return result;
            }
            _loginList.Remove(login);
            result.IsSucces = true;
            result.Message = "Пользователь разлогинен";
            AddLog(login.Id, "Logoff");
            return result;
        }

        public ActiveLoginItem CheckLogin(Guid token, string memberName)
        {
            var result = _loginList.FirstOrDefault(p => p.Token == token);
            if (result == null) return result;
            if (result.IsExpire)
            {
                _loginList.Remove(result);
                return null;
            }
            if (!AcessMembers.CheckAcess(memberName, result.AcessGroups)) return null;
            return result;
        }

        public ActiveLoginItem CheckWindowsLogin(string UserName, string memberName)
        {
            FullLoginInfo loginItem = StorageDBManager.Instance.GetLoginFullInfo(new LoginInfo { Login = UserName, IsWindowsIdentity = true });
            if (loginItem == null) return null;
            if (AcessMembers.CheckAcess(memberName, loginItem.AcessGroups)) return null;
            return new ActiveLoginItem { IsWindowsIdentity = true, Login = UserName };
        }

        public IStorageLogins StorageInterface
        {
            get
            {
                return StorageDBManager.Instance;
            }
        }


        public void AddLog(int loginId, string memberName)
        {
            StorageInterface.AddLogItem(loginId, AcessMembers.GetIdByName(memberName, 0));
        }

        public void SaveLogin(FullLoginInfo login)
        {
            StorageInterface.SaveLogin(login);
        }

        public void RemoveLogin(FullLoginInfo login)
        {
            StorageInterface.RemoveLogin(login);
        }

    }
}
