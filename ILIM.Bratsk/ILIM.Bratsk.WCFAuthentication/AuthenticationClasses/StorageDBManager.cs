﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;
using PAG.WBD.Milk.WCFAuthentication;
using PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses;

namespace WCFAuthentication.AuthenticationClasses
{
    public class StorageDBManager : IStorageLogins
    {
        public string ConnectionString { get; set; }

        private const string SqlGetLogins = "SELECT Id, UserLogin, AutoLogoffTime, Email, IsChangePassword FROM Login WHERE IsDeleted=0";
        private const string SqlGetAcessGroups = "SELECT Id, Name FROM AcessGroup ";
        private const string SqlSeveAcessGroups = @" begin tran
                                                   update AcessGroup with (serializable) set Name=@Name where ID = @Id
                                                   if @@rowcount = 0
                                                   begin
                                                      INSERT INTO AcessGroup (Name) VALUES (@Name)
                                                   end
                                                 commit tran";
        private const string SqlRemoveAcessGroups = @" DELETE FROM LoginsAcessGroup WHERE AcessGroup=@Id
                                                 DELETE FROM MembersAcessGroup WHERE AcessGroup=@Id
                                                 DELETE FROM AcessGroup WHERE Id=@Id  ";
        private const string SqlClearAcessGroupsToUser = @" DELETE FROM LoginsAcessGroup WHERE Login=@Id";
        private const string SqlSetAcessGroupsToUser = @" INSERT INTO LoginsAcessGroup (Login, AcessGroup) VALUES (@Id, @AcessGroup)";
        private const string SqlClearAcessGroupsToMember = " DELETE FROM MembersAcessGroup WHERE Member=@Id";
        private const string SqlSetAcessGroupsToMember = @" DELETE FROM MembersAcessGroup WHERE Member=@Id AND AcessGroup=@AcessGroup
                                                  INSERT INTO MembersAcessGroup (Member, AcessGroup) VALUES (@Id, @AcessGroup)";
        private const string SqlAddLogItem = "INSERT INTO Log (Date, Login, Memder) VALUES (@Date, @Login, @Memder)";
        private const string SqlGetMemberClasses = "SELECT Id, Name, CustomName, Description FROM Classes";
        private const string SqlGetMembers = "SELECT am.Id, am.Member, am.Name, am.Description, c.Id, c.Name, c.CustomName, c.Description, am.IsShared FROM AcessMember am LEFT JOIN Classes c ON am.Class=c.Id";
        private const string SqlGetMemberGroups = "SELECT AcessGroup FROM MembersAcessGroup WHERE Member=@ItemId";
        private const string SqlFindLoginWindowsAuthentication = "SELECT ID ,Staff ,UserLogin ,Sha512Password ,IsWindowsAuthentication ,IsDeleted, AutoLogoffTime, Email, IsChangePassword FROM Login WHERE IsDeleted<>1 AND upper(UserLogin) = upper(@Login) AND IsWindowsAuthentication = 1";
        private const string SqlGetLoginGroups = "SELECT AcessGroup FROM LoginsAcessGroup WHERE Login=@ItemId";
        private const string SqlFindLogin = "SELECT ID ,Staff ,UserLogin ,Sha512Password ,IsWindowsAuthentication ,IsDeleted,AutoLogoffTime, Email, IsChangePassword FROM Login WHERE IsDeleted<>1 AND upper(UserLogin) = upper(@Login) AND SHA512Password = @SHA512Password AND IsWindowsAuthentication <> 1";
        private const string SqlRemoveLogin = "UPDATE Login SET IsDeleted = 1 WHERE ID = @Id";
        private const string SqlInsertOrUpdate = @" begin tran
                                                   update Login with (serializable) set AutoLogoffTime=@AutoLogoffTime, UserLogin = @Login, Email=@Email, IsChangePassword=@IsChangePassword  where ID = @Id
                                                   if @@rowcount = 0
                                                   begin
                                                      INSERT INTO Login (UserLogin, SHA512Password, IsWindowsAuthentication, IsDeleted, AutoLogoffTime) VALUES (@Login, @SHA512Password, @IsWindowsAuthentication, 0, @AutoLogoffTime)
                                                   end
                                                 commit tran";
        private const string SqlChangePassword = "UPDATE Login SET SHA512Password = @SHA512Password WHERE ID = @Id";
        private const string SqlGetMemberClassId = @"if exists (select * from Classes with (updlock,serializable) where Name = @Name) 
                                     begin 
                                      SELECT ID FROM Classes WHERE Name = @Name
                                     end
                                    else
                                    begin
                                     INSERT INTO Classes (Name, CustomName, Description) OUTPUT INSERTED.ID VALUES (@Name, @CustomName, @Description)
                                    end";
        private const string GetMessages = @"SELECT TOP(@TopCount) *
                              FROM
                              (
                              SELECT Date, UserLogin, c.CustomName, l.par1, l.par2, l.par3, am.Name, 1 as MessageKind
                                FROM Log l LEFT JOIN Login ln ON l.Login = ln.ID
                                LEFT JOIN AcessMember am ON l.Memder = am.Id
                                LEFT JOIN Classes c ON am.Class=c.Id
                                WHERE Date>=@FromTime AND Date<=@ToTime    
                                UNION    
                              SELECT Date, 'System', MessageGroup, par1, par2, par3, Description, 2 as MessageKind
  	                            FROM SystemLogs
  	                            WHERE Date>=@FromTime AND Date<=@ToTime
                              ) t
                              WHERE ('|' + @UserLogin + '|' like '%|' + UserLogin + '|%')  AND  ('|' + @Types + '|' like '%|' + CustomName + '|%') AND par1 like '%' + @par1 + '%' AND par2 like '%' + @par2 + '%' AND par3 like '%' + @par3 + '%'";
        private const string InitDefultUsersQuery = @"if not exists (select * from Login where UserLogin = 'sa') 
                                     begin 
                                      INSERT INTO Login (UserLogin, SHA512Password, IsWindowsAuthentication, IsDeleted, AutoLogoffTime) VALUES ('sa', '', 0, 0, 0)
                                     end
                                  if not exists (select * from Login where UserLogin = 'system') 
                                     begin 
                                      INSERT INTO Login (UserLogin, SHA512Password, IsWindowsAuthentication, IsDeleted, AutoLogoffTime) VALUES ('system', '', 0, 0, 0)
                                     end";
        private const string GetMessageTypes = @"SELECT CustomName FROM Classes UNION SELECT DISTINCT MessageGroup FROM SystemLogs";



        public IEnumerable<FullLoginInfo> GetAllLogins()
        {
            var ret = new List<FullLoginInfo>();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(SqlGetLogins, sqlConnection))
                {
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlReader.Read())
                        {
                            var item = new ActiveLoginItem
                            {
                                Id = sqlReader.GetValue<int>(0),
                                Login = sqlReader.GetValue<string>(1),
                                AutoLogoffTime = DBNull.Value == sqlReader[2] ? TimeSpan.Zero : new TimeSpan(0, sqlReader.GetValue<int>(2), 0),
                                Email = sqlReader.GetValue<string>(3),
                                IsChangePassword = sqlReader.GetValue<bool>(4)
                            };
                            item.AcessGroups = GetAcessGroupsByLogin(item);
                            ret.Add(item);
                        }
                    }
                }
            }
            return ret;
        }

        public void SaveLogin(FullLoginInfo login)
        {
            ExecuteNoQuery(SqlInsertOrUpdate, login);
        }

        public void SaveLogins(IEnumerable<FullLoginInfo> logins)
        {
            foreach (var login in logins)
            {
                ExecuteNoQuery(SqlInsertOrUpdate, login);
                AssignAcessGroupsToUser(login, login.AcessGroups);
            }
        }

        public void ChangePassword(FullLoginInfo login)
        {
            ExecuteNoQuery(SqlChangePassword, login);
        }

        public void RemoveLogins(IEnumerable<FullLoginInfo> logins)
        {
            foreach (var login in logins)
            {
                AssignAcessGroupsToUser(login, new List<AcessGroup>());
                ExecuteNoQuery(SqlRemoveLogin, login);
            }
        }

        public FullLoginInfo GetLoginFullInfo(LoginInfo loginInfo)
        {
            if (loginInfo == null || loginInfo.Login == null) return null;

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                string sqlQuery = loginInfo.IsWindowsIdentity ? SqlFindLoginWindowsAuthentication : SqlFindLogin;
                using (SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection))
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@Login", System.Data.SqlDbType.NVarChar) { Value = loginInfo.Login });
                    sqlCommand.Parameters.Add(new SqlParameter("@SHA512Password", System.Data.SqlDbType.NVarChar) { Value = loginInfo.SHA512Password == null ? "" : loginInfo.SHA512Password });
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        if (sqlReader.Read())
                        {
                            var ret = new ActiveLoginItem
                            {
                                Id = sqlReader.GetValue<int>(0),
                                Password = loginInfo.Password,
                                Login = loginInfo.Login,
                                AutoLogoffTime = DBNull.Value == sqlReader[6] ? TimeSpan.Zero : new TimeSpan(0, sqlReader.GetValue<int>(6), 0),
                                Email = sqlReader.GetValue<string>(7),
                                IsChangePassword = sqlReader.GetValue<bool>(8)
                            };
                            ret.AcessGroups = GetAcessGroupsByLogin(ret);
                            return ret;
                        }
                        return null;
                    }
                }
            }
        }

        public Members GetMembers()
        {
            var ret = new Members();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlCommand = new SqlCommand(SqlGetMembers, sqlConnection);
                var sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    var newMember = new MemberItem
                    {
                        Id = sqlReader.GetValue<int>(0),
                        Name = sqlReader.GetValue<string>(1),
                        CustomName = sqlReader.GetValue<string>(2),
                        Description = sqlReader.GetValue<string>(3),
                        MemberClass = new MembersClass
                        {
                            Id = sqlReader.GetValue<int>(4),
                            Name = sqlReader.GetValue<string>(5),
                            CustomName = sqlReader.GetValue<string>(6),
                            Description = sqlReader.GetValue<string>(7)
                        },
                        IsShared = sqlReader.GetValue<bool>(8)
                    };
                    newMember.AcessGroups = GetAcessGroupsByMember(newMember);
                    ret.Add(newMember);
                }
                return ret;
            }
        }

        public List<MembersClass> GetMemberClasses()
        {
            var ret = new List<MembersClass>();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlCommand = new SqlCommand(SqlGetMemberClasses, sqlConnection);
                var sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    ret.Add(new MembersClass
                    {
                        Id = sqlReader.GetValue<int>(0),
                        Name = sqlReader.GetValue<string>(1),
                        CustomName = sqlReader.GetValue<string>(2),
                        Description = sqlReader.GetValue<string>(3),
                    });
                }
            }
            return ret;
        }

        public void AddMember(MemberItem item)
        {
            const string sqlQuery = @"if not exists (select * from AcessMember with (updlock,serializable) where Member = @Member) 
                                     begin 
                                      insert AcessMember (Member, Name, Description, Class) values (@Member, @Name, @Description, @Class) 
                                     end
                                    else
                                    begin
                                     update AcessMember set Name=@Name, Description=@Description, Class=@Class WHERE Member = @Member
                                    end";
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(sqlQuery, connection);

                connection.Open();
                command.Parameters.AddWithValue("@Member", item.Name);
                command.Parameters.AddWithValue("@Name", item.CustomName);
                command.Parameters.AddWithValue("@Description", item.Description);
                command.Parameters.AddWithValue("@Class", GetMemberClass(item.MemberClass));
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        private int GetMemberClass(MembersClass memberClass)
        {

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(SqlGetMemberClassId, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@Name", memberClass.Name);
                    sqlCommand.Parameters.AddWithValue("@CustomName", memberClass.CustomName);
                    sqlCommand.Parameters.AddWithValue("@Description", memberClass.Description);
                    memberClass.Id = (int)sqlCommand.ExecuteScalar();
                }
            }
            return memberClass.Id;
        }

        public void AddLogItem(int loginId, int memberId)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(SqlAddLogItem, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@Login", loginId);
                    sqlCommand.Parameters.AddWithValue("@Memder", memberId);
                    sqlCommand.Parameters.AddWithValue("@Date", DateTime.Now);
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        public List<AcessGroup> GetAcessGroups()
        {
            var acessGroups = new List<AcessGroup>();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(SqlGetAcessGroups, sqlConnection))
                {
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlReader.Read())
                        {
                            acessGroups.Add(new AcessGroup { Id = sqlReader.GetValue<int>(0), Name = sqlReader.GetValue<string>(1) });
                        }
                    }
                }
            }
            return acessGroups;
        }

        public void SaveAcessGroups(IEnumerable<AcessGroup> items)
        {
            AddOrRemoveAcessGroups(SqlSeveAcessGroups, items);
        }

        public void DeleteAcessGroups(IEnumerable<AcessGroup> items)
        {
            AddOrRemoveAcessGroups(SqlRemoveAcessGroups, items);
        }

        public void AssignAcessGroupsToUser(FullLoginInfo login, IEnumerable<AcessGroup> acessGroups)
        {
            AssignAcessGroups(SqlClearAcessGroupsToUser, SqlSetAcessGroupsToUser, login.Id, acessGroups);
        }

        public void AssignAcessGroupsToMembers(Members members)
        {
            foreach (var member in members)
            {
                AssignAcessGroups(SqlClearAcessGroupsToMember, SqlSetAcessGroupsToMember, member.Id, member.AcessGroups);
            }
        }

        private void AssignAcessGroups(string clearSql, string sqlText, int id, IEnumerable<AcessGroup> acessGroups)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(clearSql, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@Id", id);
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand.Parameters.Clear();
                    sqlCommand.CommandText = sqlText;
                    foreach (var group in acessGroups)
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", id);
                        sqlCommand.Parameters.AddWithValue("@AcessGroup", group.Id);
                        sqlCommand.ExecuteNonQuery();
                        sqlCommand.Parameters.Clear();
                    }
                }
            }
        }

        private void AddOrRemoveAcessGroups(string sqlText, IEnumerable<AcessGroup> items)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(sqlText, sqlConnection))
                {
                    foreach (var item in items)
                    {
                        sqlCommand.Parameters.AddWithValue("@Id", item.Id);
                        sqlCommand.Parameters.AddWithValue("@Name", item.Name);
                        sqlCommand.ExecuteNonQuery();
                        sqlCommand.Parameters.Clear();
                    }
                }
            }
        }

        private List<AcessGroup> GetAcessGroupsByLogin(ActiveLoginItem item)
        {
            return GetAcessGroups(SqlGetLoginGroups, item.Id);
        }

        private List<AcessGroup> GetAcessGroupsByMember(MemberItem item)
        {
            return GetAcessGroups(SqlGetMemberGroups, item.Id);
        }

        private List<AcessGroup> GetAcessGroups(string sqlText, int itemId)
        {
            var allGroups = GetAcessGroups();
            var ret = new List<AcessGroup>();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(sqlText, sqlConnection))
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@ItemId", System.Data.SqlDbType.Int) { Value = itemId });
                    using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlReader.Read())
                        {
                            var gr = allGroups.FirstOrDefault(p => p.Id == sqlReader.GetValue<int>(0));
                            if (gr != null) ret.Add(gr);
                        }
                    }
                }
            }
            return ret;
        }

        private void ExecuteNoQuery(string sqlText, FullLoginInfo login)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(sqlText, sqlConnection))
                {
                    AddParametersToCommand(sqlCommand, login);
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        private void AddParametersToCommand(SqlCommand command, FullLoginInfo login)
        {
            command.Parameters.Add(new SqlParameter("@Id", System.Data.SqlDbType.Int) { Value = login.Id });
            command.Parameters.Add(new SqlParameter("@Login", System.Data.SqlDbType.NVarChar) { Value = login.Login ?? "" });
            command.Parameters.Add(new SqlParameter("@SHA512Password", System.Data.SqlDbType.NVarChar) { Value = login.SHA512Password == null || login.IsWindowsIdentity ? "" : login.SHA512Password });
            command.Parameters.Add(new SqlParameter("@AutoLogoffTime", System.Data.SqlDbType.Int) { Value = login.AutoLogoffTime.TotalMinutes });
            command.Parameters.Add(new SqlParameter("@IsWindowsAuthentication", System.Data.SqlDbType.Bit) { Value = login.IsWindowsIdentity });
            command.Parameters.Add(new SqlParameter("@Email", System.Data.SqlDbType.NVarChar) { Value = login.Email ?? "" });
            command.Parameters.Add(new SqlParameter("@IsChangePassword", System.Data.SqlDbType.Bit) { Value = login.IsChangePassword });
        }

        public IEnumerable<MessageItem> GetMessageItems(MessageQuery query)
        {
            var ret = new List<MessageItem>();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(GetMessages, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@FromTime", query.FromTime);
                    sqlCommand.Parameters.AddWithValue("@ToTime", query.ToTime);
                    sqlCommand.Parameters.AddWithValue("@TopCount", query.TopCount);
                    sqlCommand.Parameters.AddWithValue("@UserLogin", string.Join("|", query.LoginsId));
                    sqlCommand.Parameters.AddWithValue("@Types", string.Join("|", query.Types));
                    sqlCommand.Parameters.AddWithValue("@Par1", query.Par1 ?? "");
                    sqlCommand.Parameters.AddWithValue("@Par2", query.Par2 ?? "");
                    sqlCommand.Parameters.AddWithValue("@Par3", query.Par3 ?? "");
                    var sqlReader = sqlCommand.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        ret.Add(new MessageItem
                        {
                            Time = sqlReader.GetValue<DateTime>(0),
                            Login = sqlReader.GetValue<string>(1),
                            Type = sqlReader.GetValue<string>(2),
                            Par1 = sqlReader.GetValue<string>(3),
                            Par2 = sqlReader.GetValue<string>(4),
                            Par3 = sqlReader.GetValue<string>(5),
                            Message = sqlReader.GetValue<string>(6),
                            MessageKind = sqlReader.GetValue<int>(7),
                        });
                    }
                }
            }
            return ret;
        }

        public StorageDBManager(string connectionString)
        {
            ConnectionString = connectionString;
            InitDefultUsers();
        }

        private void InitDefultUsers()
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(InitDefultUsersQuery, sqlConnection))
                {
                    sqlCommand.ExecuteNonQuery();
                }
            }
        }

        public List<string> GetMessagesTypes()
        {
            var ret = new List<string>();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(GetMessageTypes, sqlConnection))
                {
                    var sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                    {
                        ret.Add(sqlReader.GetValue<string>(0));
                    }
                }
            }
            return ret;
        }
    }
}
