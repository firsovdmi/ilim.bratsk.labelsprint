﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    public class MessageItem
    {
        public DateTime Time { get; set; }
        public string Login { get; set; }
        public string Type { get; set; }
        public string Par1 { get; set; }
        public string Par2 { get; set; }
        public string Par3 { get; set; }
        public string Message { get; set; }
        public int MessageKind { get; set; }
    }
}
