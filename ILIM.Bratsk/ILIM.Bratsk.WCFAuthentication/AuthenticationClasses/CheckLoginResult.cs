﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    public  class CheckLoginResult
    {
        public ActiveLoginItem LoginItem { get; set; }
        public string Message { get; set; }
        public bool IsOk { get; set; }
        public FaultDetailTypes FaultType { get; set; }
    }
}
