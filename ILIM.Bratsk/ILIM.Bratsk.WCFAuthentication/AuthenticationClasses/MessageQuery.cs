﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    public class MessageQuery
    {
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public List<string> LoginsId { get; set; }
        public List<string> Types { get; set; }
        public string Par1 { get; set; }
        public string Par2 { get; set; }
        public string Par3 { get; set; }
        public int TopCount { get; set; }
    }
}
