﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCFAuthentication.AuthenticationClasses
{
    public class AuthenticationExceptionPAG : Exception
    {
        public AuthenticationExceptionPAG(string s):base(s)
        {

        }
    }
}
