﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;
using WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class AuthenticatiorService : IAuthenticatiorService
    {
        private Members _acessMembers;
        internal Members AcessMembers
        {
            get { return _acessMembers ?? (_acessMembers = StorageInterface.GetMembers()); }
        }

        public string ConnectionString
        {
            set
            {
                StorageInterface = new StorageDBManager(value);
            }
        }

        List<ActiveLoginItem> _loginList = new List<ActiveLoginItem>();

        public AuthenticationResult Login(LoginInfo loginInfo)
        {
            var result = new AuthenticationResult();
            FullLoginInfo loginItem = StorageInterface.GetLoginFullInfo(loginInfo);
            if (loginItem == null)
            {
                result.IsSucces = false;
                result.Message = "Комбинация логина и пароля не найдена";
                return result;
            }

            var existLogin = _loginList.FirstOrDefault(p => p.Login == loginItem.Login);
            if (existLogin == null)
            {
                _loginList.Add(new ActiveLoginItem
                    {
                        Id = loginItem.Id,
                        Login = loginItem.Login,
                        Password = loginItem.Password,
                        AutoLogoffTime = loginItem.AutoLogoffTime,
                        LoginTime = DateTime.Now,
                        Token = result.Token = Guid.NewGuid(),
                        AcessGroups = loginItem.AcessGroups,
                    });
            }
            else
            {
                existLogin.AutoLogoffTime = loginItem.AutoLogoffTime;
                result.Token = existLogin.Token;
                existLogin.LoginTime = DateTime.Now;
            }
            result.AllowedMembers = AcessMembers.GetAllowedMembers(loginItem.AcessGroups).Select(p => p.Name).ToList();
            result.IsSucces = true;
            result.Message = "Авторизация выполнена";
            return result;
        }

        public AuthenticationResult Logoff(AuthenticationResult loginInfo)
        {
            AuthenticationResult result = new AuthenticationResult();
            var login = _loginList.FirstOrDefault(p => p.Token == loginInfo.Token);
            if (login == null)
            {
                result.IsSucces = false;
                result.Message = "Пользователь не зарегистрирован";
                return result;
            }
            _loginList.Remove(login);
            result.IsSucces = true;
            result.Message = "Пользователь разлогинен";
            return result;
        }

        public CheckLoginResult CheckLogin(Guid token, string memberName)
        {
            var ret = new CheckLoginResult();
            ret.IsOk = false;
            ret.FaultType = FaultDetailTypes.Unknown;
            ret.LoginItem = _loginList.FirstOrDefault(p => p.Token == token);
            if (ret.LoginItem == null)
            {
                ret.FaultType= FaultDetailTypes.NotLogined;
                return ret;
            }
            if (ret.LoginItem.IsExpire)
            {
                _loginList.Remove(ret.LoginItem);
                ret.FaultType = FaultDetailTypes.NotLogined;
                return ret;
            }
            if (!AcessMembers.CheckAcess(memberName, ret.LoginItem.AcessGroups))
            {
                ret.FaultType = FaultDetailTypes.AcessDeny;
                return ret;
            }
            ret.IsOk = true;
            return ret;
        }

        public CheckLoginResult CheckWindowsLogin(string userName, string memberName)
        {
            var ret = new CheckLoginResult();
            ret.IsOk = false;
            FullLoginInfo loginItem = StorageInterface.GetLoginFullInfo(new LoginInfo { Login = userName, IsWindowsIdentity = true });
            if (loginItem == null)
            {
                ret.FaultType = FaultDetailTypes.NotLogined;
                return ret;
            }
            if (!AcessMembers.CheckAcess(memberName, ret.LoginItem.AcessGroups))
            {
                ret.FaultType = FaultDetailTypes.AcessDeny;
                return ret;
            }
            ret.IsOk = true;
            ret.LoginItem=new ActiveLoginItem { IsWindowsIdentity = true, Login = userName };
            return ret;
        }

        public IStorageLogins StorageInterface { get; set; }


        public void AddLog(int loginId, string memberName)
        {
            StorageInterface.AddLogItem(loginId, AcessMembers.GetIdByName(memberName, 0));
        }

        public IEnumerable<FullLoginInfo> GetAllLogins()
        {
            return StorageInterface.GetAllLogins();
        }

        public void SaveLogin(FullLoginInfo login)
        {
            StorageInterface.SaveLogin(login);
        }

        public void ChangePassword(FullLoginInfo login)
        {
            StorageInterface.ChangePassword(login);
        }

        public void RemoveLogins(IEnumerable<FullLoginInfo> login)
        {
            StorageInterface.RemoveLogins(login);
        }

        public List<AcessGroup> GetAcessGroups()
        {
            return StorageInterface.GetAcessGroups();
        }

        public void AddAcessGroups(IEnumerable<AcessGroup> items)
        {
            StorageInterface.SaveAcessGroups(items);
        }

        public void DeleteAcessGroups(IEnumerable<AcessGroup> items)
        {
            StorageInterface.DeleteAcessGroups(items);
        }

        public void AssignAcessGroupsToUser(FullLoginInfo login, IEnumerable<AcessGroup> acessGroups)
        {
            StorageInterface.AssignAcessGroupsToUser(login, acessGroups);
        }

        public void AssignAcessGroupsToMembers(Members members)
        {
            StorageInterface.AssignAcessGroupsToMembers(members);
        }

        public void SaveLogins(IEnumerable<FullLoginInfo> logins)
        {
            StorageInterface.SaveLogins(logins);
        }

        public Members GetMembers()
        {
            return StorageInterface.GetMembers();
        }

        public void AddMember(MemberItem item)
        {
            StorageInterface.AddMember(item);
        }

        public List<MembersClass> GetMemberClasses()
        {
            return StorageInterface.GetMemberClasses();
        }

        public IEnumerable<MessageItem> GetMessageItems(MessageQuery query)
        {
            return StorageInterface.GetMessageItems(query);
        }

        public List<string> GetMessagesTypes()
        {
            return StorageInterface.GetMessagesTypes();
        }
    }
}
