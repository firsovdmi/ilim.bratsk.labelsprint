﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    [ServiceContract]
    public interface IAuthenticatior
    {
        [OperationContract]
        AuthenticationResult Login(LoginInfo loginInfo);
        [OperationContract]
        AuthenticationResult Logoff(AuthenticationResult loginInfo);
        [OperationContract]
        ActiveLoginItem CheckLogin(Guid token, string memberName);
        [OperationContract]
        ActiveLoginItem CheckWindowsLogin(string UserName, string memberName);
        [OperationContract]
        void SaveLogin(FullLoginInfo login);
        [OperationContract]
        void RemoveLogin(FullLoginInfo login);
        void AddLog(int loginId, string memberName);
    }
}
