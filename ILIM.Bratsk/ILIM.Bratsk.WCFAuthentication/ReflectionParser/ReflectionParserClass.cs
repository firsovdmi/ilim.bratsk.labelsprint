﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.WCFAuthentication.ReflectionParser
{
    public class ReflectionParserClass
    {
        public void GetAllMembersFromAssembly(string assemblyName)
        {
            foreach (var type in GetAssembly(assemblyName).GetTypes().Where(type => type.GetCustomAttributes(typeof(AuthentificationClassAttribute), true).Length > 0))
            {
                var authentificationClassAttribute = (AuthentificationClassAttribute)type.GetCustomAttribute(typeof(AuthentificationClassAttribute));
                var members = type.GetMembers().Where(p => p.IsDefined(typeof(AuthentificationMemberAttribute), false));
                foreach (var member in members)
                {
                    var authentificationMemberAttribute = (AuthentificationMemberAttribute)member.GetCustomAttribute(typeof(AuthentificationMemberAttribute));
                    IaaaManager.Instance.AuthenticatiorServerContract.AddMember(new MemberItem
                    {
                        Name = member.Name,
                        CustomName = authentificationMemberAttribute.Name,
                        Description = authentificationMemberAttribute.Description,
                        MemberClass = new MembersClass
                        {
                            Name = type.Name,
                            CustomName = authentificationClassAttribute.Name,
                            Description = authentificationClassAttribute.Description
                        }
                    });
                }
            }
        }

        private static Assembly GetAssembly(string assemblyPath)
        {
            var assemblyName = Path.GetFileName(assemblyPath);
            if (assemblyName != null && assemblyName.IndexOf(",", System.StringComparison.Ordinal) != -1)
            {
                assemblyName = assemblyName.Split(',')[0].Trim();
            }
            if (assemblyName != null && assemblyName.IndexOf(".dll", System.StringComparison.Ordinal) == -1)
            {
                assemblyName += ".dll";
            }
            var a = AppDomain.CurrentDomain.GetAssemblies().LastOrDefault(p => p.ManifestModule.ScopeName == assemblyName);
            if (a != null) return a;

            var raw = GetTempBin(assemblyPath);
            return Assembly.Load(raw);
        }

        private static byte[] GetTempBin(string assemblyName)
        {
            if (!File.Exists(assemblyName))
            {
                return new byte[] { 0 };
            }
            using (var fs = new FileStream(assemblyName, FileMode.Open))
            {
                var raw = new byte[fs.Length];
                fs.Read(raw, 0, (int)fs.Length);
                return raw;
            }
        }

    }
}
