﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.WCFAuthentication.ReflectionParser
{
    public class AuthentificationMemberAttribute : Attribute
    {
        public string Name { get; set; }
        public string Description { get; set; }


        public AuthentificationMemberAttribute(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public AuthentificationMemberAttribute(string name) : this(name, "") { }
        public AuthentificationMemberAttribute() : this("", "") { }
    }
}
